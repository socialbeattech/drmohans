<?php
require('wp-config.php');
//API for gravity form
$api_key = '88d03b0335';
$private_key = 'a90c9d8534dc6e2';
 
//set route
$route = 'forms/3/entries';

function calculate_signature( $string, $private_key ) {
    $hash = hash_hmac( 'sha1', $string, $private_key, true );
    $sig = rawurlencode( base64_encode( $hash ) );
    return $sig;
}


//creating request URL
$expires = strtotime( '+60 mins' );
$string_to_sign = sprintf( '%s:%s:%s:%s', $api_key, 'POST', $route, $expires );
$sig = calculate_signature( $string_to_sign, $private_key );
$url = get_site_url() . '/gravityformsapi/' . $route . '?api_key=' . $api_key . '&signature=' . $sig . '&expires=' . $expires;

$entries = array(
    array(
        'date_created' => date('Y-m-d H:i:s'),
        'is_starred'   => 0,
        'is_read'      => 0,
        'ip'           => '::1',
        'source_url'   => $_GET['url'],
        'currency'     => 'USD',
        'created_by'   => 1,
        'user_agent'   => 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
        'status'       => 'active',
        '1'            => $_GET['Name'],
        '2'            => $_GET['Phone'],
        '3'            => $_GET['Email'],
        '4'            => $_GET['Patient_type'],
        '5'            => $_GET['Location'],
        '9'            => $_GET['services'],
        '6'            => $_GET['date1'],
        '7'            => $_GET['Comments'],
        '10'           => $_GET['URL']
    )
);
 
//json encode array
$entry_json = json_encode( $entries );

//retrieve data
$response = wp_remote_request( $url , array( 'method' => 'POST', 'body' => $entry_json, 'timeout' => 25 ) );
/*if ( wp_remote_retrieve_response_code( $response ) != 200 || ( empty( wp_remote_retrieve_body( $response ) ) ) ){
    //http request failed
    die( 'There was an error attempting to access the API.' );
}*/
if ( (wp_remote_retrieve_response_code( $response ) != 200) || (  wp_remote_retrieve_body( $response ) =="" ) ){
    //http request failed
    /*echo "<br>wp_remote_retrieve_response_code :".wp_remote_retrieve_response_code( $response );
	echo "<br>wp_remote_retrieve_body :";
	print_r(wp_remote_retrieve_body( $response )); */
	die( 'There was an error attempting to access the API.' );
	
}

//result is in the response "body" and is json encoded.
$body = json_decode( wp_remote_retrieve_body( $response ), true );

$Name = $_GET['Name'];
$Phone = $_GET['Phone'];
$Email = $_GET['Email'];
$Patient_type = $_GET['Patient_type'];
$Location = $_GET['Location'];
$services = $_GET['services'];
$date1 = $_GET['date1'];
$Comments = $_GET['Comments'];
$URL = $_GET['URL'];




	
    GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );



//Gravity form mail Notifications

// Multiple recipients
$to = 'drmohansmails@gmail.com,drmohans@diabetes.ind.in,dranjana@drmohans.com,appointments-gop@drmohans.com,parvathi@drmohans.com,manjusreemathy@drmohans.com,appointments-vip@drmohans.com,recheck@drmohans.com,prathap@drmohans.com,drmohansappointments@gmail.com,appointments-web@drmohans.com';

// Subject
$subject = 'டாக்டர். மோகன்ஸ் இணையதளத்தை அணுகியதற்கு நன்றி';


// Message
$message = '<html>
<head>
  <title>Drmohans AMP Enquiry</title>
</head>
<body>
  <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
    <tr><td bgcolor="#EAF2FA"><strong>Name:</strong></td></tr>
    <tr bgcolor="#FFFFFF">'.$Name.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>Phone :</strong></td></tr>
    <tr bgcolor="#FFFFFF">'.$Phone.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>Email:</strong></td></tr>
    <tr bgcolor="#FFFFFF"><td>'.$Email.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>Patient Type:</strong></td></tr>
    <tr bgcolor="#FFFFFF"><td>'.$Patient_type.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>Location:</strong></td></tr>
    <tr bgcolor="#FFFFFF"><td>'.$Location.'</td></tr>
	<tr bgcolor="#EAF2FA"><td><strong>services:</strong></td></tr>
    <tr bgcolor="#FFFFFF"><td>'.$services.'</td></tr>
	<tr bgcolor="#EAF2FA"><td><strong>date:</strong></td></tr>
    <tr bgcolor="#FFFFFF"><td>'.$date1.'</td></tr>
	<tr bgcolor="#EAF2FA"><td><strong>Comments:</strong></td></tr>
    <tr bgcolor="#FFFFFF"><td>'.$Comments.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>source_url:</strong></td></tr> 
    <tr bgcolor="#FFFFFF"><td>'.$URL.'</td></tr> 
  </table>
</body>
</html>';

// To send HTML mail, the Content-type header must be set
$headers = "From: Dr.Mohan’s Diabetes Specialities Centre <admin@drmohans.com>\r\n";
$headers .= "Reply-To: recheck@drmohans.com\r\n";
$headers .= 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "X-Mailer: PHP/".phpversion();

// Mail it
mail($to, $subject, $message, $headers);

//End of gravity mail notifications


//Thank You mail notifications
// Multiple recipients
$to = $Email;

// Subject
$subject = 'டாக்டர். மோகன்ஸ் இணையதளத்தை அணுகியதற்கு நன்றி';


// Message
$message = '<html>
<head>
  <title>Drmohans AMP Enquiry</title>
</head>
<body>
  </br></br><span style="font-weight: 400;">உங்களின் மருத்துவ தேவைக்காக டாக்டர் மோகனை  தேர்வு செய்தமைக்கு நன்றி. உங்களின் சந்திப்பு நேரத்தை உறுதி செய்ய எங்கள் டீம் உறுப்பினர் உங்களை தொடர்பு கொள்வார்.</span>

<span style="font-weight: 400;">காலையில் டாக்டர். மோகன்ஸ் டயாபடிஸ் ஸ்பெஷாலிட்டி சென்டரில் சந்திக்க அனுமதி நேரம் கிடைக்கப் பெற்றவர்கள் செய்ய வேண்டிய விஷயங்கள்</span>
<h3>சந்திப்பு நேரத்தின் விவரம்:</h3>
<ul>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">எங்களின் வெளிநோயாளி பிரிவு காலை 6.30 மணி முதல் செயல்படும் (திங்கள்-சனி) .</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">வெறும் வயிற்றுடன் காலை 7 முதல் 7.30 மணிக்குள் மையத்திற்கு வந்துவிட வேண்டும். </span></li>
</ul>
<h3><b style="font-size: 1.125rem;">சாப்பிடாமல் இருக்கும் முறை:</b></h3>
<ul>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">"சாப்பிடாமல் இருப்பது" என்பது, முந்தைய நாள் இரவு 9.30 மணிக்கு சாப்பிட்ட பின்னர் 8 முதல் 10 மணி நேரத்திற்கு, வேறு எதையும் சாப்பிடக் கூடாது (தண்ணீர் மட்டும் அருந்தலாம்) ரத்த மாதிரி எடுக்கப்படும் வரை. </span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">ஒருவேளை நீரிழிவு நோய்க்கான மருந்துகளை எடுத்துக் கொள்பவராக இருந்தால், அவற்றை வழக்கம் போல் எடுத்துக் கொள்ளவும்.</span></li>
</ul>
<h3><b>மருத்துவ பரிசோதனை முறைகள்:</b></h3>
<ul>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">அனைத்து மருத்துவப் பரிசோதனை, உணவுக் கட்டுப்பாட்டு நிபுணரை சந்தித்த பின்னர், உரிய பரிசோதனை முடிவுகளுடன் மருத்துவரை சந்திக்க குறைந்தது 5 முதல் 6 மணி நேரம் பிடிக்கும் என்பதால் அதற்கு உரிய கால அவகாசத்துடன் மருத்துவமனைக்கு வரவும்.</span></li>
</ul>
<h3><b>தேவையான மருத்துவ அறிக்கை மற்றும் ஆவணங்கள்:</b></h3>
<ul>
 	<li style="font-weight: 400;"><span style="font-weight: 400;"> முந்தைய மருத்துவப் பரிசோதனை முடிவுகள் இருந்தால் அவற்றை மறக்காமல் எடுத்துக் கொண்டு வரவும்.</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">அதேபோல் முந்தைய மருத்துவ கோப்புகள்,அறுவை சிகிக்சை மற்றும் மருத்துவ தகவல்கள் இருந்தாலும், அவற்றையும் கொண்டு வரவும்.  குறிப்பாக நீங்கள் முதல் முறையாக எங்கள் மருத்துவமனைக்கு வரும் போது அல்லது நீண்ட கால இடைவெளிக்கு பின்னர் எங்கள் மருத்துவமனைக்கு வரும் போது</span></li>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">மருத்துவ சோதனைக்கான நாளில், உங்களுக்கான மருந்துகள் அனைத்தையும் எடுத்து வந்து விடவும். வழக்கம் போல் அதனை உரிய நேரத்தில் உட்கொள்ளவும்.</span></li>
</ul>
<h3><strong>துணைக்கு ஒருவரை அழைத்து வரவும்:</strong></h3>
<ul>
 	<li style="font-weight: 400;"><span style="font-weight: 400;">ஒரு நோயாளிக்கு கட்டாயம் ஒரு துணைவர் மட்டுமே உடன் வர அனுமதிக்கப்படுவார். 10 வயதிற்கு உட்பட்ட குழந்தைகளை நோயாளிகள் தங்களுக்கு துணையாக அழைத்து வர வேண்டாம்.</span></li>
</ul>
<span style="font-weight: 400;">உங்களுடைய விருப்பம் மற்றும் நேரத்தை செலவிட்டதற்காக மீண்டும் ஒருமுறை நன்றி சொல்லிக் கொள்கிறோம்.</span>



<span style="font-weight: 400;">உங்கள் உண்மையுள்ள,</span>

<span style="font-weight: 400;">DMDSC Team</span> 

	
  </div>
</body>
</html>';

// To send HTML mail, the Content-type header must be set
$headers = "From: Dr.Mohan’s Diabetes Specialities Centre <admin@drmohans.com>\r\n";
$headers .= "Reply-To: recheck@drmohans.com\r\n";
$headers .= 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "X-Mailer: PHP/".phpversion();

// Mail it
mail($to, $subject, $message, $headers);


 
//print_r($body); exit;
if( $body['status'] > 202 ){
    $error = $body['response'];
 
        //entry update failed, get error information, error could just be a string
    if ( is_array( $error )){
        $error_code     = $error['code'];
        $error_message  = $error['message'];
        $error_data     = isset( $error['data'] ) ? $error['data'] : '';
        $status     = "Code: {$error_code}. Message: {$error_message}. Data: {$error_data}.";
    }
    else{
        $status = $error;
    }
    die( "Could not post entries. {$status}" );
}
else
{
	header('Location:https://drmohans.com/tamil-lp/amp-thankyou');
}