<?php

/**

 * The template for displaying the footer.

 *

 * Contains the closing of the #content div and all content after

 *

 * @package melinda

 */



?>
<?php wp_footer(); ?>
<section class="footer">
    <div class="fullcolumn">
		<div class="container">
		    <div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <div class="row">
				    <div class="col-12 col-md-6">
						<p>© Copyright 2019 Dr. Mohan's Diabetes Specialities Centre</p>
					</div>
					<div class="col-12 col-md-6 beat">
						<p>Designed & developed by Social Beat</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<section class="mobile-button">
	<div class="container">
		<div class="fixed-bottom ">
			<a class="enqire" id="enquire-now" href="#form1" class="scroll" style="font-family: 'montserratlight_allfont_net';">தகவல் கேட்க</a>
			<a class="call" id="callers"  href="tel:(+91)98878 89878" style="font-family: 'montserratlight_allfont_net';">அழையுங்கள்</a>
		</div>
	</div>
</section>
	</div>
	</div>
</section>
</body>
</html>