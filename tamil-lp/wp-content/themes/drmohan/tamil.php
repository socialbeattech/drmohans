<?php
/**
 * Template Name:Tamil-LP
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Assetz
 */

get_header();

while ( have_posts() ) : the_post();
?>
	
 
 </section>
	
 <section class="first-part">
    <div class="fullcolumn">
	    <div class="container">
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 sugar-center">
			    <p>சர்க்கரை நோய் சிகிச்சையில் 25+ ஆண்டுகள் அனுபவம், 4,50,000க்கும் மேலான நோயாளிகளுக்கு திருப்திகரமான சிகிச்சைகளை டாக்டர் மோகன்ஸ் டயாபிடிஸ் ஸ்பெஷாலிடிஸ் சென்டர் சிறப்பாக வழங்கி வருகிறது. சர்க்கரை நோய் சிகிச்சையில் நம்பிக்கை அளிக்கும் வகையில் பணியாற்றி, சர்வதேச அளவிலான அங்கீகாரம் பெற்றதுடன், உலகத் தர சிகிச்சை வசதிகளை கொண்டுள்ளது. </p>
			</div>
		</div>
	</div>
 </section>
 
 <section class="ours-help">
    <div class="fullcolumn">
	    <div class="container">
		    <div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <h1>எங்கள் சேவைகள்</h1>
			</div>
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 diet-foods ">
			    <div class="row">
				    <div class="col-12 col-md-6 col-sm-6 ">
					    <div class="col-12 col-sm-12 ">
						    <div class="row">
								<div class="col-12 col-sm-12 col-md-6  rice-img ">
									<img src="<?php echo get_template_directory_uri() ?>/images/rice.jpg" alt="rice" width="206" height="132" />
								</div>
								<div class="col-12 col-sm-12 col-md-6 food-items ">
									<h3>ஹை ஃபைபர் ரைஸ் </h3>
									<p>டாக்டர் மோகன்ஸ் ஹை பைபர் ரைஸ்</p>
									<span>இப்போது கிடைக்கும்</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-sm-6 ">
					    <div class="col-12 col-sm-12 ">
						    <div class="row">
								<div class="col-12 col-sm-12 col-md-6  rice-img">
									<img src="<?php echo get_template_directory_uri() ?>/images/rice-brown.jpg" alt="rice-brown" width="206" height="132" />
								</div>
								<div class="col-12 col-sm-12 col-md-6 food-items ">
									<h3>ஃபைபர் ப்ரவுன் அரிசி </h3>
									<p>டாக்டர்.மோகன்ஸ் ஹை ஃபைபர் ப்ரவுன் அரிசி</p>
									<span>இப்போது கிடைக்கும்</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
				<div class="col-12 col-md-12 col-sm-12 col-lg-12  diet-foods">
			    <div class="row">
				    <div class="col-12 col-md-6 col-sm-6">
					    <div class="col-12 col-sm-12 ">
						    <div class="row">
								<div class="col-12 col-sm-12 col-md-6  rice-img">
									<img src="<?php echo get_template_directory_uri() ?>/images/rice-flwaks.jpg" alt="rice-flwaks" width="206" height="132" />
								</div>
								<div class="col-12 col-sm-12 col-md-6 food-items ">
									<h3>ரைஸ் ஃபிளேக்ஸ் </h3>
									<p>டாக்டர்.மோகன்ஸ் ஹை ஃபைபர் ப்ரவுன் ரைஸ் ஃபிளேக்ஸ்</p>
									<span>இப்போது கிடைக்கும்</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-sm-6 ">
					    <div class="col-12 col-sm-12 ">
						    <div class="row">
								<div class="col-12 col-sm-12 col-md-6  rice-img ">
									<img src="<?php echo get_template_directory_uri() ?>/images/corn.jpg" alt="corn" width="206" height="132" />
								</div>
								<div class="col-12 col-sm-12 col-md-6 food-items ">
									<h3>டயட் சிவ்டா </h3>
									<p>டாக்டர்.மோகனின் டயட் சிவ்டாி</p>
									<span>இப்போது கிடைக்கும்</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 </section>
 
 <section class="Health-check">
    <div class="fullcolumn">
	    <div class="container">
		    <div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <h1>மாஸ்டர் ஹெல்த் செக்கப்</h1>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <div class="row">
				    <div class="col-12 col-md-3 tips-img">
						<img src="<?php echo get_template_directory_uri() ?>/images/sugar-level.jpg" alt="sugar-level" width="206" height="132" />
					</div>
					<div class="col-12 col-md-9 text-para">
					    <p>உடலின் சர்க்கரை அளவு மற்றும் சர்க்கரை நோயால் ஏற்படும் அனைத்து சிக்கல்களையும் அடையாளம் காணும் வகையில் எங்களது ஸ்பெஷல் சிகிச்சை பேக்கேஜ் உள்ளது.</p>
					    <p>இந்த சர்க்கரை நோய் பரிசோதனையில் எல்.எஃப்.டி, இ.சி.ஜி சோதனை, தைராய்டு சோதனை, ஃபூட் பிரஷ்ஷர் சோதனை, மருத்துவர் அளிக்கும் ஆலோசனைகள் மற்றும் பிற வசதிகளும் அடங்கும்</p>
						<span>டோமல்குடா மற்றும் கோபாலபுரம் கிளைகளில் மட்டுமே ஏற்றுக் கொள்ளப்படும்</span>
					</div>
				</div>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 pt-2">
			    <h1>120 நிமிட எக்ஸ்பிரஸ் செக்கப்</h1>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <div class="row">
				    <div class="col-12 col-md-3 tips-img">
						<img src="<?php echo get_template_directory_uri() ?>/images/120-mins.jpg" alt="120-mins" width="206" height="132" />
					</div>
					<div class="col-12 col-md-9 text-para">
					    <p>வெறும் 120 நிமிடங்களில் விரிவான சர்க்கரை நோய் பரிசோதனை மற்றும் மருத்துவரின் ஆலோசனையைப் பெற்றுக் கொள்ளலாம். அதன் பின்னர் உங்கள் வழக்கமான பணியில் ஈடுபடலாம்.</p>
					    <p>டோமல்குடா மற்றும் கோபாலபுரம் மையங்களில் மட்டுமே ஏற்றுக் கொள்ளப்படும்</p>
						<span>நீங்கள் சிகிச்சை பெற விரும்பும் தேதிக்கு ஒருநாள் முன்னதாக, மாலை 4 மணிக்குள், இதற்கான முன்பதிவு செய்ய வேண்டும்</span>
					</div>
				</div>
			</div>
			
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 pt-2">
			    <h1>ஹோம் சர்வீசஸ்</h1>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <div class="row">
				    <div class="col-12 col-md-3 tips-img">
						<img src="<?php echo get_template_directory_uri() ?>/images/home-services.jpg" alt="home-services" width="206" height="132" />
					</div>
					<div class="col-12 col-md-9 text-para">
					    <p>சர்க்கரை நோய் சிகிச்சைக்கு, இனி மருத்துவமனைகளுக்கு செல்ல வேண்டிய சிரமம் இல்லை. எங்களைத் தொடர்பு கொண்டால், உங்கள் வீட்டிலேயே சவுகரியமாக அனைத்து சிகிச்சை வசதிகளையும் பெற்றுக் கொள்ளலாம்.</p>
					    
						    <div class="col-12 col-sm-12 people-choice">
							    <p>வழங்கப்படும் சேவைகள்</p>
							    <div class="row">
								    <div class="col-12 col-sm-12 col-md-4 helping-img">
										<img src="<?php echo get_template_directory_uri() ?>/images/blood-collection.jpg" alt="blood-collection" width="179" height="87" />
									</div>
									<div class="col-12 col-sm-12 col-md-4 helping-img">
										<img src="<?php echo get_template_directory_uri() ?>/images/home-delvry.jpg" alt="home-delvry" width="179" height="87" />
									</div>
									<div class="col-12 col-sm-12 col-md-4 helping-img">
										<img src="<?php echo get_template_directory_uri() ?>/images/foot-care.jpg" alt="foot-care" width="179" height="87" />
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-12 people-choice">
							    <div class="row">
								    <div class="col-12 col-sm-12 col-md-4 helping-img">
										<img src="<?php echo get_template_directory_uri() ?>/images/Physiotherapy.jpg" alt="Physiotherapy" width="179" height="87" />
									</div>
									<div class="col-12 col-sm-12 col-md-4 helping-img">
										<img src="<?php echo get_template_directory_uri() ?>/images/Fitness-Training.jpg" alt="Fitness-Training" width="179" height="87" />
									</div>
									<div class="col-12 col-sm-12 col-md-4 helping-img">
										<img src="<?php echo get_template_directory_uri() ?>/images/Health-Products.jpg" alt="Health-Products" width="179" height="87" />
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
 </section>
 
 <section class="last-part">
    <div class="fullcolumn">
		<div class="container">
		    <div class="col-12 col-md-12 col-sm-12 col-lg-12 pt-5 testimonial">
				<h1> வாடிக்கையாளர் நற்சான்றுகள்</h1>
				<img src="<?php echo get_template_directory_uri() ?>/images/underline.png" alt="underline" width="246" height="1" style="text-align:center;"/>
			</div>
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
				
			    <div class="col-12 col-md-8 swiper-text">
					<div id="myCasrousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<div class="carousel-item active">
								<p>”உங்கள் மருத்துவமனையில் அளிக்கப்படும் சேவைகள் மிகச் சிறப்பாக இருக்கிறது. பணியாளர்களும், அவர்கள் செய்யும் பணிவிடைகளும் மிகவும் திருப்திகரமாக உள்ளது. அதுமட்டுமல்லாமல் டாக்டர்.முத்துக்குமார் மற்றும் அவரது ஆலோசனைகள் எங்களுக்கு மிகவும் பிடித்திருக்கிறது. நன்றி”</p>
								<span><strong>- சந்திரன் என்</strong></span>
							</div>
							
							<div class="carousel-item">
								<p>“டாக்டர் மோகன்ஸ் கிளினிக்கில் அனைவரும் புன்னகையோடு வரவேற்கின்றனர். அனைத்து பரிசோதனைகளும் சிறப்பாகவும், துல்லியமாகவும் எடுக்கப்பட்டன. அங்குள்ள அனைவரும் என்னிடம் காட்டிய அக்கறையால், நான் நோயிலிருந்து முற்றிலும் குணமடைந்து விட்டதாகவே உணர்கிறேன்”</p>
								<span><strong>- திருமதி.முத்துலட்சுமி யு</strong></span>
							</div>
							
							<div class="carousel-item">
								<p>“என்னுடைய ஊர் கோயம்புத்தூர். சர்க்கரை நோய் சிகிச்சைக்காக டாக்டர்.சுகுணாவைத் தொடர்பு கொண்டேன். அவரும், மற்றவர்களும் சிறப்பாக ஒத்துழைப்பு அளித்தனர். உங்கள் சேவைக்கு என்னுடைய மனமார்ந்த நன்றி”</p>
								<span><strong>- திருமதி. தங்கம்</strong></span>
							</div>
						  </div>
  
					  <!-- Left and right controls -->
						<a class="carousel-control-prev" href="#myCasrousel" data-slide="prev">
							<span class="carousel-control-prev-icon"></span>
						</a>
						<a class="carousel-control-next" href="#myCasrousel" data-slide="next">
							<span class="carousel-control-next-icon"></span>
						</a>
					</div>
					
				</div>
			</div>
				<div class="col-12 col-md-4">
				</div>
		</div>
		
	</div>
</section>

<?php
endwhile; 
get_footer();<?php