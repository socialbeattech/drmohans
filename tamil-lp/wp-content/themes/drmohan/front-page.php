<?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<?php
/**
 * Template Name:  AMP-page
 *
 * @package  SocialBeat Landiing Page Template
 */
?>
<!doctype html>
<html amp lang="en">
	<head>
	<meta charset="utf-8">
	<script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-fx-collection" src="https://cdn.ampproject.org/v0/amp-fx-collection-0.1.js"></script>
	<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
	<script async custom-element="amp-lightbox-gallery" src="https://cdn.ampproject.org/v0/amp-lightbox-gallery-0.1.js"></script>
	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<title>டாக்டர்.மோகன்ஸ்</title>
	<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/sb-logo.png">
	<link rel="canonical" href="http://example.ampproject.org/article-metadata.html">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<title>Dr.mohans</title>
	 <link rel="icon" type="image/png" id="favicon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon_dr_mohans.png"/>
	<script async custom-element="amp-date-picker" src="https://cdn.ampproject.org/v0/amp-date-picker-0.1.js"></script>
	<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<style amp-boilerplate>
	body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}
	@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom>
*,::before,::after{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:rgba(0,0,0,0)}article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}
body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;
background-color:#fff}[tabindex="-1"]:focus{outline:0}hr{box-sizing:content-box;height:0;overflow:visible}h1,h2,h3,h4,h5,h6{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}abbr[title],abbr[data-original-title]{text-decoration:underline;-webkit-text-decoration:underline dotted;
text-decoration:underline dotted;cursor:help;border-bottom:0;-webkit-text-decoration-skip-ink:none;text-decoration-skip-ink:none}address{margin-bottom:1rem;font-style:normal;line-height:inherit}ol,ul,dl{margin-top:0;margin-bottom:1rem}ol ol,ul ul,ol ul,ul ol{margin-bottom:0}dt{font-weight:700}
dd{margin-bottom:.5rem;margin-left:0}blockquote{margin:0 0 1rem}b,strong{font-weight:bolder}small{font-size:80%}sub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}a{color:#007bff;text-decoration:none;background-color:transparent}
a:hover{color:#0056b3;text-decoration:underline}a:not([href]):not([tabindex]){color:inherit;text-decoration:none}a:not([href]):not([tabindex]):hover,a:not([href]):not([tabindex]):focus{color:inherit;text-decoration:none}a:not([href]):not([tabindex]):focus{outline:0}pre,code,kbd,
samp{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;font-size:1em}pre{margin-top:0;margin-bottom:1rem;overflow:auto}figure{margin:0 0 1rem}img{vertical-align:middle;border-style:none}svg{overflow:hidden;vertical-align:middle}table{border-collapse:collapse}
caption{padding-top:.75rem;padding-bottom:.75rem;color:#6c757d;text-align:left;caption-side:bottom}th{text-align:inherit}label{display:inline-block;margin-bottom:.5rem}button{border-radius:0}button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color}input,button,select,optgroup,
textarea{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button,input{overflow:visible}button,select{text-transform:none}select{word-wrap:normal}button,[type="button"],[type="reset"],[type="submit"]{-webkit-appearance:button}button:not(:disabled),[type="button"]:not(:disabled),
[type="reset"]:not(:disabled),[type="submit"]:not(:disabled){cursor:pointer}button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner{padding:0;border-style:none}input[type="radio"],input[type="checkbox"]{box-sizing:border-box;padding:0}input[type="date"],
input[type="time"],input[type="datetime-local"],input[type="month"]{-webkit-appearance:listbox}textarea{overflow:auto;resize:vertical}fieldset{min-width:0;padding:0;margin:0;border:0}legend{display:block;width:100%;max-width:100%;padding:0;margin-bottom:.5rem;font-size:1.5rem;
line-height:inherit;color:inherit;white-space:normal}progress{vertical-align:baseline}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto}[type="search"]{outline-offset:-2px;
-webkit-appearance:none}[type="search"]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}output{display:inline-block}summary{display:list-item;cursor:pointer}template{display:none}[hidden]{display:none}
h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{margin-bottom:.5rem;font-weight:500;line-height:1.2}h1,.h1{font-size:2.5rem}h2,.h2{font-size:2rem}h3,.h3{font-size:1.75rem}h4,.h4{font-size:1.5rem}h5,.h5{font-size:1.25rem}h6,.h6{font-size:1rem}hr{margin-top:1rem;margin-bottom:1rem;
border:0;border-top:1px solid rgba(0,0,0,0.1)}.list-unstyled{padding-left:0;list-style:none}.list-inline{padding-left:0;list-style:none}.list-inline-item{display:inline-block}.list-inline-item:not(:last-child){margin-right:.5rem}.img-fluid{max-width:100%;height:auto}
.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}

@media (min-width: 576px){.container{max-width:540px}}
@media (min-width: 768px){.container{max-width:720px}}
@media (min-width: 992px){.container{max-width:960px}}
@media (min-width: 1200px){.container{max-width:1140px}}
.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}
.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}
.no-gutters{margin-right:0;margin-left:0}
.no-gutters > .col,.no-gutters > [class*="col-"]{padding-right:0;padding-left:0}
.col-1,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-10,.col-11,.col-12,.col,.col-auto,.col-sm-1,.col-sm-2,
.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm,.col-sm-auto,
.col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12,
.col-md,.col-md-auto,.col-lg-1,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-10,
.col-lg-11,.col-lg-12,.col-lg,.col-lg-auto,.col-xl-1,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,
.col-xl-9,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl,.col-xl-auto{position:relative;width:100%;padding-right:15px;padding-left:15px}
.col{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}
.order-first{-ms-flex-order:-1;order:-1}.order-last{-ms-flex-order:13;order:13}
.order-0{-ms-flex-order:0;order:0}.order-1{-ms-flex-order:1;order:1}
.order-2{-ms-flex-order:2;order:2}.order-3{-ms-flex-order:3;order:3}
.order-4{-ms-flex-order:4;order:4}.order-5{-ms-flex-order:5;order:5}
.order-6{-ms-flex-order:6;order:6}.order-7{-ms-flex-order:7;order:7}
.order-8{-ms-flex-order:8;order:8}.order-9{-ms-flex-order:9;order:9}
.order-10{-ms-flex-order:10;order:10}.order-11{-ms-flex-order:11;order:11}
.order-12{-ms-flex-order:12;order:12}.offset-1{margin-left:8.333333%}
.offset-2{margin-left:16.666667%}.offset-3{margin-left:25%}.offset-4{margin-left:33.333333%}
.offset-5{margin-left:41.666667%}.offset-6{margin-left:50%}.offset-7{margin-left:58.333333%}
.offset-8{margin-left:66.666667%}.offset-9{margin-left:75%}.offset-10{margin-left:83.333333%}
.offset-11{margin-left:91.666667%}

@media (min-width: 576px){.col-sm{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;
flex-grow:1;max-width:100%}.col-sm-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-sm-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-sm-2{-ms-flex:0 0 16.666667%;
flex:0 0 16.666667%;max-width:16.666667%}.col-sm-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-sm-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.
col-sm-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-sm-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-sm-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-sm-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-sm-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-sm-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-sm-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-sm-first{-ms-flex-order:-1;order:-1}
.order-sm-last{-ms-flex-order:13;order:13}.order-sm-0{-ms-flex-order:0;order:0}
.order-sm-1{-ms-flex-order:1;order:1}.order-sm-2{-ms-flex-order:2;order:2}
.order-sm-3{-ms-flex-order:3;order:3}.order-sm-4{-ms-flex-order:4;order:4}
.order-sm-5{-ms-flex-order:5;order:5}.order-sm-6{-ms-flex-order:6;order:6}
.order-sm-7{-ms-flex-order:7;order:7}.order-sm-8{-ms-flex-order:8;order:8}
.order-sm-9{-ms-flex-order:9;order:9}.order-sm-10{-ms-flex-order:10;order:10}
.order-sm-11{-ms-flex-order:11;order:11}.order-sm-12{-ms-flex-order:12;order:12}
.offset-sm-0{margin-left:0}.offset-sm-1{margin-left:8.333333%}.offset-sm-2{margin-left:16.666667%}
.offset-sm-3{margin-left:25%}.offset-sm-4{margin-left:33.333333%}.offset-sm-5{margin-left:41.666667%}
.offset-sm-6{margin-left:50%}.offset-sm-7{margin-left:58.333333%}.offset-sm-8{margin-left:66.666667%}
.offset-sm-9{margin-left:75%}.offset-sm-10{margin-left:83.333333%}.offset-sm-11{margin-left:91.666667%}}

@media (min-width: 768px){.col-md{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;
max-width:100%}.col-md-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-md-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-md-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-md-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-md-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-md-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-md-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-md-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-md-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-md-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-md-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-md-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-md-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-md-first{-ms-flex-order:-1;order:-1}
.order-md-last{-ms-flex-order:13;order:13}.order-md-0{-ms-flex-order:0;order:0}
.order-md-1{-ms-flex-order:1;order:1}.order-md-2{-ms-flex-order:2;order:2}
.order-md-3{-ms-flex-order:3;order:3}.order-md-4{-ms-flex-order:4;order:4}
.order-md-5{-ms-flex-order:5;order:5}.order-md-6{-ms-flex-order:6;order:6}
.order-md-7{-ms-flex-order:7;order:7}.order-md-8{-ms-flex-order:8;order:8}
.order-md-9{-ms-flex-order:9;order:9}.order-md-10{-ms-flex-order:10;order:10}
.order-md-11{-ms-flex-order:11;order:11}.order-md-12{-ms-flex-order:12;order:12}
.offset-md-0{margin-left:0}.offset-md-1{margin-left:8.333333%}.offset-md-2{margin-left:16.666667%}
.offset-md-3{margin-left:25%}.offset-md-4{margin-left:33.333333%}.offset-md-5{margin-left:41.666667%}
.offset-md-6{margin-left:50%}.offset-md-7{margin-left:58.333333%}.offset-md-8{margin-left:66.666667%}
.offset-md-9{margin-left:75%}.offset-md-10{margin-left:83.333333%}.offset-md-11{margin-left:91.666667%}}

@media (min-width: 992px){.col-lg{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-lg-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-lg-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-lg-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-lg-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-lg-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-lg-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-lg-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-lg-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-lg-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-lg-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-lg-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-lg-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-lg-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-lg-first{-ms-flex-order:-1;order:-1}
.order-lg-last{-ms-flex-order:13;order:13}.order-lg-0{-ms-flex-order:0;order:0}
.order-lg-1{-ms-flex-order:1;order:1}.order-lg-2{-ms-flex-order:2;order:2}
.order-lg-3{-ms-flex-order:3;order:3}.order-lg-4{-ms-flex-order:4;order:4}
.order-lg-5{-ms-flex-order:5;order:5}.order-lg-6{-ms-flex-order:6;order:6}
.order-lg-7{-ms-flex-order:7;order:7}.order-lg-8{-ms-flex-order:8;order:8}
.order-lg-9{-ms-flex-order:9;order:9}.order-lg-10{-ms-flex-order:10;order:10}
.order-lg-11{-ms-flex-order:11;order:11}.order-lg-12{-ms-flex-order:12;order:12}
.offset-lg-0{margin-left:0}.offset-lg-1{margin-left:8.333333%}.offset-lg-2{margin-left:16.666667%}
.offset-lg-3{margin-left:25%}.offset-lg-4{margin-left:33.333333%}.offset-lg-5{margin-left:41.666667%}
.offset-lg-6{margin-left:50%}.offset-lg-7{margin-left:58.333333%}.offset-lg-8{margin-left:66.666667%}
.offset-lg-9{margin-left:75%}.offset-lg-10{margin-left:83.333333%}.offset-lg-11{margin-left:91.666667%}}

@media (min-width: 1200px){.col-xl{-ms-flex-preferred-size:0;flex-basis:0;-ms-flex-positive:1;flex-grow:1;max-width:100%}
.col-xl-auto{-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:100%}
.col-xl-1{-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}
.col-xl-2{-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}
.col-xl-3{-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
.col-xl-4{-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}
.col-xl-5{-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}
.col-xl-6{-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}
.col-xl-7{-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}
.col-xl-8{-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}
.col-xl-9{-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
.col-xl-10{-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}
.col-xl-11{-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}
.col-xl-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.order-xl-first{-ms-flex-order:-1;order:-1}
.order-xl-last{-ms-flex-order:13;order:13}.order-xl-0{-ms-flex-order:0;order:0}
.order-xl-1{-ms-flex-order:1;order:1}.order-xl-2{-ms-flex-order:2;order:2}
.order-xl-3{-ms-flex-order:3;order:3}.order-xl-4{-ms-flex-order:4;order:4}
.order-xl-5{-ms-flex-order:5;order:5}.order-xl-6{-ms-flex-order:6;order:6}
.order-xl-7{-ms-flex-order:7;order:7}.order-xl-8{-ms-flex-order:8;order:8}
.order-xl-9{-ms-flex-order:9;order:9}.order-xl-10{-ms-flex-order:10;order:10}
.order-xl-11{-ms-flex-order:11;order:11}.order-xl-12{-ms-flex-order:12;order:12}
.offset-xl-0{margin-left:0}.offset-xl-1{margin-left:8.333333%}.offset-xl-2{margin-left:16.666667%}
.offset-xl-3{margin-left:25%}.offset-xl-4{margin-left:33.333333%}.offset-xl-5{margin-left:41.666667%}
.offset-xl-6{margin-left:50%}.offset-xl-7{margin-left:58.333333%}.offset-xl-8{margin-left:66.666667%}
.offset-xl-9{margin-left:75%}.offset-xl-10{margin-left:83.333333%}.offset-xl-11{margin-left:91.666667%}}

.clearfix::after{display:block;clear:both;content:""}.d-none{display:none}.d-inline{display:inline}
.d-inline-block{display:inline-block}.d-block{display:block}.d-table{display:table}.d-table-row{display:table-row}
.d-table-cell{display:table-cell}.d-flex{display:-ms-flexbox;display:flex}
.d-inline-flex{display:-ms-inline-flexbox;display:inline-flex}

@media (min-width: 576px){.d-sm-none{display:none}.d-sm-inline{display:inline}.d-sm-inline-block{display:inline-block}
.d-sm-block{display:block}.d-sm-table{display:table}.d-sm-table-row{display:table-row}.d-sm-table-cell{display:table-cell}
.d-sm-flex{display:-ms-flexbox;display:flex}.d-sm-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 768px){.d-md-none{display:none}.d-md-inline{display:inline}.d-md-inline-block{display:inline-block}
.d-md-block{display:block}.d-md-table{display:table}.d-md-table-row{display:table-row}.d-md-table-cell{display:table-cell}
.d-md-flex{display:-ms-flexbox;display:flex}.d-md-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 992px){.d-lg-none{display:none}.d-lg-inline{display:inline}.d-lg-inline-block{display:inline-block}
.d-lg-block{display:block}.d-lg-table{display:table}.d-lg-table-row{display:table-row}.d-lg-table-cell{display:table-cell}
.d-lg-flex{display:-ms-flexbox;display:flex}.d-lg-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

@media (min-width: 1200px){.d-xl-none{display:none}.d-xl-inline{display:inline}.d-xl-inline-block{display:inline-block}
.d-xl-block{display:block}.d-xl-table{display:table}.d-xl-table-row{display:table-row}.d-xl-table-cell{display:table-cell}
.d-xl-flex{display:-ms-flexbox;display:flex}.d-xl-inline-flex{display:-ms-inline-flexbox;display:inline-flex}}

.justify-content-start{-ms-flex-pack:start;justify-content:flex-start}.justify-content-end{-ms-flex-pack:end;
justify-content:flex-end}.justify-content-center{-ms-flex-pack:center;justify-content:center}
.justify-content-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-start{-ms-flex-align:start;align-items:flex-start}.align-items-end{-ms-flex-align:end;align-items:flex-end}
.align-items-center{-ms-flex-align:center;align-items:center}
.align-items-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-center{-ms-flex-line-pack:center;align-content:center}
.align-content-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-auto{-ms-flex-item-align:auto;align-self:auto}.align-self-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-end{-ms-flex-item-align:end;align-self:flex-end}.align-self-center{-ms-flex-item-align:center;align-self:center}
.align-self-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-stretch{-ms-flex-item-align:stretch;align-self:stretch}

@media (min-width: 576px){.justify-content-sm-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-sm-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-sm-center{-ms-flex-pack:center;justify-content:center}
.justify-content-sm-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-sm-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-sm-start{-ms-flex-align:start;align-items:flex-start}.align-items-sm-end{-ms-flex-align:end;align-items:flex-end}
.align-items-sm-center{-ms-flex-align:center;align-items:center}
.align-items-sm-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-sm-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-sm-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-sm-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-sm-center{-ms-flex-line-pack:center;align-content:center}
.align-content-sm-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-sm-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-sm-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-sm-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-sm-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-sm-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-sm-center{-ms-flex-item-align:center;align-self:center}
.align-self-sm-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-sm-stretch{-ms-flex-item-align:stretch;align-self:stretch}}

@media (min-width: 768px){.justify-content-md-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-md-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-md-center{-ms-flex-pack:center;justify-content:center}
.justify-content-md-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-md-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-md-start{-ms-flex-align:start;align-items:flex-start}
.align-items-md-end{-ms-flex-align:end;align-items:flex-end}
.align-items-md-center{-ms-flex-align:center;align-items:center}
.align-items-md-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-md-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-md-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-md-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-md-center{-ms-flex-line-pack:center;align-content:center}
.align-content-md-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-md-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-md-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-md-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-md-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-md-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-md-center{-ms-flex-item-align:center;align-self:center}
.align-self-md-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-md-stretch{-ms-flex-item-align:stretch;align-self:stretch}}

@media (min-width: 992px){.justify-content-lg-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-lg-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-lg-center{-ms-flex-pack:center;justify-content:center}
.justify-content-lg-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-lg-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-lg-start{-ms-flex-align:start;align-items:flex-start}.align-items-lg-end{-ms-flex-align:end;align-items:flex-end}
.align-items-lg-center{-ms-flex-align:center;align-items:center}
.align-items-lg-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-lg-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-lg-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-lg-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-lg-center{-ms-flex-line-pack:center;align-content:center}
.align-content-lg-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-lg-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-lg-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-lg-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-lg-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-lg-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-lg-center{-ms-flex-item-align:center;align-self:center}
.align-self-lg-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-lg-stretch{-ms-flex-item-align:stretch;align-self:stretch}}

@media (min-width: 1200px){.justify-content-xl-start{-ms-flex-pack:start;justify-content:flex-start}
.justify-content-xl-end{-ms-flex-pack:end;justify-content:flex-end}
.justify-content-xl-center{-ms-flex-pack:center;justify-content:center}
.justify-content-xl-between{-ms-flex-pack:justify;justify-content:space-between}
.justify-content-xl-around{-ms-flex-pack:distribute;justify-content:space-around}
.align-items-xl-start{-ms-flex-align:start;align-items:flex-start}
.align-items-xl-end{-ms-flex-align:end;align-items:flex-end}
.align-items-xl-center{-ms-flex-align:center;align-items:center}
.align-items-xl-baseline{-ms-flex-align:baseline;align-items:baseline}
.align-items-xl-stretch{-ms-flex-align:stretch;align-items:stretch}
.align-content-xl-start{-ms-flex-line-pack:start;align-content:flex-start}
.align-content-xl-end{-ms-flex-line-pack:end;align-content:flex-end}
.align-content-xl-center{-ms-flex-line-pack:center;align-content:center}
.align-content-xl-between{-ms-flex-line-pack:justify;align-content:space-between}
.align-content-xl-around{-ms-flex-line-pack:distribute;align-content:space-around}
.align-content-xl-stretch{-ms-flex-line-pack:stretch;align-content:stretch}
.align-self-xl-auto{-ms-flex-item-align:auto;align-self:auto}
.align-self-xl-start{-ms-flex-item-align:start;align-self:flex-start}
.align-self-xl-end{-ms-flex-item-align:end;align-self:flex-end}
.align-self-xl-center{-ms-flex-item-align:center;align-self:center}
.align-self-xl-baseline{-ms-flex-item-align:baseline;align-self:baseline}
.align-self-xl-stretch{-ms-flex-item-align:stretch;align-self:stretch}}
.float-left{float:left}.float-right{float:right}.float-none{float:none}

@media (min-width: 576px){.float-sm-left{float:left}.float-sm-right{float:right}.float-sm-none{float:none}}

@media (min-width: 768px){.float-md-left{float:left}.float-md-right{float:right}.float-md-none{float:none}}

@media (min-width: 992px){.float-lg-left{float:left}.float-lg-right{float:right}.float-lg-none{float:none}}

@media (min-width: 1200px){.float-xl-left{float:left}.float-xl-right{float:right}.float-xl-none{float:none}}

.overflow-auto{overflow:auto}.overflow-hidden{overflow:hidden}.position-static{position:static}
.position-relative{position:relative}.position-absolute{position:absolute}.position-fixed{position:fixed}
.position-sticky{position:-webkit-sticky;position:sticky}.fixed-top{position:fixed;top:0;right:0;left:0;z-index:1030}
.fixed-bottom{position:fixed;right:0;bottom:0;left:0;z-index:1030}.w-25{width:25%}.w-50{width:50%}.w-75{width:75%}
.w-100{width:100%}.w-auto{width:auto}.h-25{height:25%}.h-50{height:50%}.h-75{height:75%}.h-100{height:100%}
.h-auto{height:auto}.mw-100{max-width:100%}.mh-100{max-height:100%}.min-vw-100{min-width:100vw}.min-vh-100{min-height:100vh}
.vw-100{width:100vw}.vh-100{height:100vh}.text-monospace{font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}
.text-justify{text-align:justify}.text-wrap{white-space:normal}
.text-nowrap{white-space:nowrap}.text-truncate{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}
.text-left{text-align:left}.text-right{text-align:right}
.text-center{text-align:center}

@media (min-width: 576px){.text-sm-left{text-align:left}.text-sm-right{text-align:right}.text-sm-center{text-align:center}}

@media (min-width: 768px){.text-md-left{text-align:left}.text-md-right{text-align:right}.text-md-center{text-align:center}}

@media (min-width: 992px){.text-lg-left{text-align:left}.text-lg-right{text-align:right}.text-lg-center{text-align:center}}

@media (min-width: 1200px){.text-xl-left{text-align:left}.text-xl-right{text-align:right}.text-xl-center{text-align:center}}

.text-lowercase{text-transform:lowercase}.text-uppercase{text-transform:uppercase}.text-capitalize{text-transform:capitalize}
.font-weight-light{font-weight:300}.font-weight-lighter{font-weight:lighter}.font-weight-normal{font-weight:400}
.font-weight-bold{font-weight:700}.font-weight-bolder{font-weight:bolder}.font-italic{font-style:italic}
.text-white{color:#fff}


.btn{display:inline-block;font-weight:400;color:#212529;text-align:center;vertical-align:middle;-webkit-user-select:none;
-moz-user-select:none;-ms-user-select:none;user-select:none;background-color:transparent;border:1px solid transparent;padding:.375rem .75rem;
font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,
border-color .15s ease-in-out,box-shadow .15s ease-in-out}@media (prefers-reduced-motion: reduce){.btn{transition:none}}
.btn:hover{color:#212529;text-decoration:none}.btn:focus,.btn.focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,0.25)}
.btn.disabled,.btn:disabled{opacity:.65}a.btn.disabled,fieldset:disabled a.btn{pointer-events:none}
.btn-primary{color:#fff;background-color:#007bff;border-color:#007bff}.btn-primary:hover{color:#fff;background-color:#0069d9;
border-color:#0062cc}.btn-primary:focus,.btn-primary.focus{box-shadow:0 0 0 .2rem rgba(38,143,255,0.5)}.btn-primary.disabled,
.btn-primary:disabled{color:#fff;background-color:#007bff;border-color:#007bff}
.btn-primary:not(:disabled):not(.disabled):active,
.btn-primary:not(:disabled):not(.disabled).active,
.show > .btn-primary.dropdown-toggle{color:#fff;background-color:#0062cc;border-color:#005cbf}
.btn-primary:not(:disabled):not(.disabled):active:focus,.btn-primary:not(:disabled):not(.disabled)
.active:focus,.show > .btn-primary.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(38,143,255,0.5)}
.btn-secondary{color:#fff;background-color:#6c757d;border-color:#6c757d}
.btn-secondary:hover{color:#fff;background-color:#5a6268;border-color:#545b62}
.btn-secondary:focus,.btn-secondary.focus{box-shadow:0 0 0 .2rem rgba(130,138,145,0.5)}
.btn-secondary.disabled,.btn-secondary:disabled{color:#fff;background-color:#6c757d;border-color:#6c757d}
.btn-secondary:not(:disabled):not(.disabled):active,.btn-secondary:not(:disabled):not(.disabled).active,
.show > .btn-secondary.dropdown-toggle{color:#fff;background-color:#545b62;border-color:#4e555b}
.btn-secondary:not(:disabled):not(.disabled):active:focus,.btn-secondary:not(:disabled):not(.disabled).active:focus,
.show > .btn-secondary.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(130,138,145,0.5)}
.btn-success{color:#fff;background-color:#28a745;border-color:#28a745}
.btn-success:hover{color:#fff;background-color:#218838;border-color:#1e7e34}.btn-success:focus,
.btn-success.focus{box-shadow:0 0 0 .2rem rgba(72,180,97,0.5)}.btn-success.disabled,
.btn-success:disabled{color:#fff;background-color:#28a745;border-color:#28a745}
.btn-success:not(:disabled):not(.disabled):active,.btn-success:not(:disabled):not(.disabled).active,
.show > .btn-success.dropdown-toggle{color:#fff;background-color:#1e7e34;border-color:#1c7430}
.btn-success:not(:disabled):not(.disabled):active:focus,.btn-success:not(:disabled):not(.disabled).active:focus,
.show > .btn-success.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(72,180,97,0.5)}
.btn-info{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-info:hover{color:#fff;
background-color:#138496;border-color:#117a8b}.btn-info:focus,.btn-info.focus{box-shadow:0 0 0 .2rem rgba(58,176,195,0.5)}
.btn-info.disabled,.btn-info:disabled{color:#fff;background-color:#17a2b8;border-color:#17a2b8}
.btn-info:not(:disabled):not(.disabled):active,.btn-info:not(:disabled):not(.disabled).active,.show > 
.btn-info.dropdown-toggle{color:#fff;background-color:#117a8b;border-color:#10707f}
.btn-info:not(:disabled):not(.disabled):active:focus,.btn-info:not(:disabled):not(.disabled).active:focus,
.show > .btn-info.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(58,176,195,0.5)}
.btn-warning{color:#212529;background-color:#ffc107;border-color:#ffc107}
.btn-warning:hover{color:#212529;background-color:#e0a800;border-color:#d39e00}
.btn-warning:focus,.btn-warning.focus{box-shadow:0 0 0 .2rem rgba(222,170,12,0.5)}
.btn-warning.disabled,.btn-warning:disabled{color:#212529;background-color:#ffc107;border-color:#ffc107}
.btn-warning:not(:disabled):not(.disabled):active,.btn-warning:not(:disabled):not(.disabled).active,.show > .btn-warning.dropdown-toggle{color:#212529;background-color:#d39e00;border-color:#c69500}.btn-warning:not(:disabled):not(.disabled):active:focus,
.btn-warning:not(:disabled):not(.disabled).active:focus,.show > .btn-warning.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(222,170,12,0.5)}.btn-danger{color:#fff;background-color:#dc3545;border-color:#dc3545}
.btn-danger:hover{color:#fff;background-color:#c82333;border-color:#bd2130}.btn-danger:focus,.btn-danger.focus{box-shadow:0 0 0 .2rem rgba(225,83,97,0.5)}
.btn-danger.disabled,.btn-danger:disabled{color:#fff;background-color:#dc3545;border-color:#dc3545}
.btn-danger:not(:disabled):not(.disabled):active,.btn-danger:not(:disabled):not(.disabled).active,.show > .btn-danger.dropdown-toggle{color:#fff;background-color:#bd2130;border-color:#b21f2d}
.btn-danger:not(:disabled):not(.disabled):active:focus,.btn-danger:not(:disabled):not(.disabled).active:focus,.show > .btn-danger.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(225,83,97,0.5)}
.btn-light{color:#212529;background-color:#f8f9fa;border-color:#f8f9fa}
.btn-light:hover{color:#212529;background-color:#e2e6ea;border-color:#dae0e5}.btn-light:focus,
.btn-light.focus{box-shadow:0 0 0 .2rem rgba(216,217,219,0.5)}.btn-light.disabled,
.btn-light:disabled{color:#212529;background-color:#f8f9fa;border-color:#f8f9fa}
.btn-light:not(:disabled):not(.disabled):active,.btn-light:not(:disabled):not(.disabled).active,.show > .btn-light.dropdown-toggle{color:#212529;background-color:#dae0e5;border-color:#d3d9df}
.btn-light:not(:disabled):not(.disabled):active:focus,.btn-light:not(:disabled):not(.disabled).active:focus,.show > .btn-light.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(216,217,219,0.5)}
.btn-dark{color:#fff;background-color:#343a40;border-color:#343a40}.btn-dark:hover{color:#fff;background-color:#23272b;
border-color:#1d2124}.btn-dark:focus,.btn-dark.focus{box-shadow:0 0 0 .2rem rgba(82,88,93,0.5)}.btn-dark.disabled,
.btn-dark:disabled{color:#fff;background-color:#343a40;border-color:#343a40}.btn-dark:not(:disabled):not(.disabled):active,
.btn-dark:not(:disabled):not(.disabled).active,.show > .btn-dark.dropdown-toggle{color:#fff;background-color:#1d2124;
border-color:#171a1d}.btn-dark:not(:disabled):not(.disabled):active:focus,.btn-dark:not(:disabled):not(.disabled).active:focus,.show > .btn-dark.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(82,88,93,0.5)}
.btn-outline-primary{color:#007bff;border-color:#007bff}.btn-outline-primary:hover{color:#fff;background-color:#007bff;
border-color:#007bff}.btn-outline-primary:focus,.btn-outline-primary.focus{box-shadow:0 0 0 .2rem rgba(0,123,255,0.5)}
.btn-outline-primary.disabled,.btn-outline-primary:disabled{color:#007bff;background-color:transparent}
.btn-outline-primary:not(:disabled):not(.disabled):active,.btn-outline-primary:not(:disabled):not(.disabled).active,.show > .btn-outline-primary.dropdown-toggle{color:#fff;
background-color:#007bff;border-color:#007bff}.btn-outline-primary:not(:disabled):not(.disabled):active:focus,
.btn-outline-primary:not(:disabled):not(.disabled).active:focus,.show > .btn-outline-primary.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(0,123,255,0.5)}
.btn-outline-secondary{color:#6c757d;border-color:#6c757d}.btn-outline-secondary:hover{color:#fff;background-color:#6c757d;
border-color:#6c757d}.btn-outline-secondary:focus,.btn-outline-secondary.focus{box-shadow:0 0 0 .2rem rgba(108,117,125,0.5)}
.btn-outline-secondary.disabled,.btn-outline-secondary:disabled{color:#6c757d;background-color:transparent}
.btn-outline-secondary:not(:disabled):not(.disabled):active,.btn-outline-secondary:not(:disabled):not(.disabled).active,
.show > .btn-outline-secondary.dropdown-toggle{color:#fff;background-color:#6c757d;border-color:#6c757d}
.btn-outline-secondary:not(:disabled):not(.disabled):active:focus,.btn-outline-secondary:not(:disabled):not(.disabled).active:focus,.show > .btn-outline-secondary.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(108,117,125,0.5)}
.btn-outline-success{color:#28a745;border-color:#28a745}.btn-outline-success:hover{color:#fff;background-color:#28a745;border-color:#28a745}.btn-outline-success:focus,
.btn-outline-success.focus{box-shadow:0 0 0 .2rem rgba(40,167,69,0.5)}.btn-outline-success.disabled,
btn-outline-success:disabled{color:#28a745;background-color:transparent}.btn-outline-success:not(:disabled):not(.disabled):active,.btn-outline-success:not(:disabled):not(.disabled).active,
.show > .btn-outline-success.dropdown-toggle{color:#fff;background-color:#28a745;border-color:#28a745}
.btn-outline-success:not(:disabled):not(.disabled):active:focus,.btn-outline-success:not(:disabled):not(.disabled).active:focus,
.show > .btn-outline-success.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(40,167,69,0.5)}.btn-outline-info{color:#17a2b8;border-color:#17a2b8}.btn-outline-info:hover{color:#fff;background-color:#17a2b8;
border-color:#17a2b8}.btn-outline-info:focus,.btn-outline-info.focus{box-shadow:0 0 0 .2rem rgba(23,162,184,0.5)}
.btn-outline-info.disabled,.btn-outline-info:disabled{color:#17a2b8;background-color:transparent}
.btn-outline-info:not(:disabled):not(.disabled):active,.btn-outline-info:not(:disabled):not(.disabled).active,
.show > .btn-outline-info.dropdown-toggle{color:#fff;background-color:#17a2b8;border-color:#17a2b8}
.btn-outline-info:not(:disabled):not(.disabled):active:focus,.btn-outline-info:not(:disabled):not(.disabled).active:focus,
.show > .btn-outline-info.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(23,162,184,0.5)}
.btn-outline-warning{color:#ffc107;border-color:#ffc107}.btn-outline-warning:hover{color:#212529;background-color:#ffc107;
border-color:#ffc107}.btn-outline-warning:focus,.btn-outline-warning.focus{box-shadow:0 0 0 .2rem rgba(255,193,7,0.5)}
.btn-outline-warning.disabled,.btn-outline-warning:disabled{color:#ffc107;background-color:transparent}
.btn-outline-warning:not(:disabled):not(.disabled):active,.btn-outline-warning:not(:disabled):not(.disabled).active,
.show > .btn-outline-warning.dropdown-toggle{color:#212529;background-color:#ffc107;border-color:#ffc107}
.btn-outline-warning:not(:disabled):not(.disabled):active:focus,.btn-outline-warning:not(:disabled):not(.disabled).active:focus,
.show > .btn-outline-warning.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(255,193,7,0.5)}
.btn-outline-danger{color:#dc3545;border-color:#dc3545}.btn-outline-danger:hover{color:#fff;background-color:#dc3545;
border-color:#dc3545}.btn-outline-danger:focus,.btn-outline-danger.focus{box-shadow:0 0 0 .2rem rgba(220,53,69,0.5)}
.btn-outline-danger.disabled,.btn-outline-danger:disabled{color:#dc3545;background-color:transparent}
.btn-outline-danger:not(:disabled):not(.disabled):active,.btn-outline-danger:not(:disabled):not(.disabled).active,
.show > .btn-outline-danger.dropdown-toggle{color:#fff;background-color:#dc3545;border-color:#dc3545}
.btn-outline-danger:not(:disabled):not(.disabled):active:focus,.btn-outline-danger:not(:disabled):not(.disabled).active:focus,
.show > .btn-outline-danger.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(220,53,69,0.5)}.btn-outline-light{color:#f8f9fa;
border-color:#f8f9fa}.btn-outline-light:hover{color:#212529;background-color:#f8f9fa;border-color:#f8f9fa}
.btn-outline-light:focus,.btn-outline-light.focus{box-shadow:0 0 0 .2rem rgba(248,249,250,0.5)}
.btn-outline-light.disabled,.btn-outline-light:disabled{color:#f8f9fa;background-color:transparent}
.btn-outline-light:not(:disabled):not(.disabled):active,.btn-outline-light:not(:disabled):not(.disabled).active,
.show > .btn-outline-light.dropdown-toggle{color:#212529;background-color:#f8f9fa;border-color:#f8f9fa}
.btn-outline-light:not(:disabled):not(.disabled):active:focus,.btn-outline-light:not(:disabled):not(.disabled).active:focus,
.show > .btn-outline-light.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(248,249,250,0.5)}.btn-outline-dark{color:#343a40;
border-color:#343a40}.btn-outline-dark:hover{color:#fff;background-color:#343a40;border-color:#343a40}
.btn-outline-dark:focus,.btn-outline-dark.focus{box-shadow:0 0 0 .2rem rgba(52,58,64,0.5)}.btn-outline-dark.disabled,.btn-outline-dark:disabled{color:#343a40;background-color:transparent}
.btn-outline-dark:not(:disabled):not(.disabled):active,.btn-outline-dark:not(:disabled):not(.disabled).active,.show > .btn-outline-dark.dropdown-toggle{color:#fff;
background-color:#343a40;border-color:#343a40}.btn-outline-dark:not(:disabled):not(.disabled):active:focus,
.btn-outline-dark:not(:disabled):not(.disabled).active:focus,.show > .btn-outline-dark.dropdown-toggle:focus{box-shadow:0 0 0 .2rem rgba(52,58,64,0.5)}
.btn-link{font-weight:400;color:#007bff;text-decoration:none}.btn-link:hover{color:#0056b3;text-decoration:underline}
.btn-link:focus,.btn-link.focus{text-decoration:underline;box-shadow:none}.btn-link:disabled,.btn-link.disabled{color:#6c757d;pointer-events:none}.btn-lg,.btn-group-lg > .btn{padding:.5rem 1rem;font-size:1.25rem;
line-height:1.5;border-radius:.3rem}.btn-sm,.btn-group-sm > .btn{padding:.25rem .5rem;font-size:.875rem;line-height:1.5;
border-radius:.2rem}.btn-block{display:block;width:100%}.btn-block + .btn-block{margin-top:.5rem}input[type="submit"]
.btn-block,input[type="reset"].btn-block,input[type="button"].btn-block{width:100%}


.header{background:url('<?php bloginfo('stylesheet_directory'); ?>/images/new-dr-banner.jpg');background-repeat: no-repeat;
width: 100%;background-size: cover;height: 773px;}
span.icon {float: left;}
span.number {padding: 5px 6px;font-size: 14px;text-decoration: none;}
.top-head {padding: 3% 0 0 6%;}
.sub-heading h1{font-size: 24px;color: #ea2324;line-height: 40px;font-weight: bold;}
.sub-heading {padding: 7% 0 0 4%;}
section.first-part {margin: 5% 0;}
section.ours-help {background: #f3f8f9;padding: 0 0 5% 0;}
.ours-help h1 {text-align: center;margin: 5% 0;padding: 5% 0;}
.diet-foods {padding: 1% 1% 2% 2%;}
.food-items h3 {font-size: 22px;}
.food-items p {font-size: 12px;}
.food-items span {color: red;font-size: 12px;}
section.Health-check {margin: 5% 0;}
section.Health-check h1 {padding: 3% 0;font-size: 24px;}
.Health-check span {color: red;font-size: 15px;}
.text-para {font-size: 15px;}
.people-choice p {padding: 11% 0 1% 11%;color: red;font-size: 25px;}
.helping-img {margin: 2% -7% 0% -1%;padding: 0 0% 1% 0%;}
.last-part {background: url('<?php bloginfo('stylesheet_directory'); ?>/images/footer-banner.jpg'); height: 480px;}
.testimonial {text-align: center;padding: 4% 0;}
.operation {margin: 0 0% 2% -18%;font-size: 14px;}
.appliance {padding: 0;margin: 3% 0 0 0;}
.gravity_holder {background: #fff;float: right;max-width: 325px;padding: 8px 20px;margin: 0 auto;border: 2px solid #dedede;
position: absolute;z-index: 9999;RIGHT: 10%;}
.gravity_holder h2 {text-align: center;padding: 3% 0;font-size: 18px;color: #ea2324;font-weight: bold;}
input#idName,input#idPhone,input#idEmail,input#idComments,input#iddate1 {width: 100%;margin: 5% 0;font-size: 14px;}
select#Location-list {width: 100%;margin: 4% 0;font-size: 14px;}
label#idpatient {margin: 2% 0;font-size: 14px;}
label#review {font-size: 14px;}
input#idSubmit {width: 76%;background: #ed1616;border: 1px solid #ed1616;color: #fff;padding: 3% 0%;margin: 2% 13%;}
.mobile-appoinment {display: none;}
.tell {float: right;left: 13%;}
section.footer {margin: 1% 0 0% 0;}
.beat p {float: right;}
section.mobile-button {display: none;}
.swiper p{font-size:16px;line-height: 24px;margin: 9% 2% 0 9%;}
.swiper span {font-size: 20px;float: right;}
.mobile-swiper{display:none;}


@media only screen and (max-width:767px){
.header{background:url('<?php bloginfo('stylesheet_directory'); ?>/images/mobilebanner2.jpg');background-repeat: no-repeat;
width: 100%;background-size: cover;height: auto;}	
.tips-img {text-align: center;}
.diet-foods{text-align: center;}
.people-choice {text-align: center;}
.people-choice p{font-size: 20px;padding: 0;}
section.Health-check h1{font-size: 20px;text-align: center;padding: 9% 0 0 0;}
.text-para {font-size: 14px;padding: 0 0 0 6%;}
.sub-heading h1 {font-size: 20px;line-height: 26px;}
.appliance{text-align:center;}
.operation{margin:0;}
.gravity_holder {position:relative;RIGHT: 0%;}
section.mobile-button {display: block;position: fixed;z-index: 9999;float: left;width: 100%;}
.fixed-bottom {position: fixed;right: 0;bottom: 0;left: 0;z-index: 1030;}
a.enqire {width: 50%;float: left;text-align: center;background: #2c296c;padding: 8px 0px;color: #fff;
border-right: 1px solid #fff;}
a.call {width: 50%;float: left;text-align: center;background: #2c296c;padding: 8px 0px;color: #fff;
border-left: 1px solid #fff;}
.tell {float: none;left: 0%;padding: 4% 7%;}
.top-head{margin: 0% 13%;}
.sub-heading{text-align: center;}
.sugar-center {margin: 9% 0;}
.food-items {padding-bottom: 4%;}
.testimonial h1{font-size: 28px;}
.beat p{float:none;}
.swiper{display:none;}
.mobile-swiper{display:block;}
.mobile-swiper p {font-size: 13px;line-height: 18px;}
.mobile-swiper span {font-size: 15px;float:right;}
.gravity_holder p{max-width: 391px;text-align: center;padding: 8px 20px;}
section.footer {margin: 1% 0 11% 0;text-align:center;}

}

</style>
</head>

<body>
<!-- Google Tag Manager -->
<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-WNWTW9R&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>
	<section class="header">
		<div class="container-fluid">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 top-head">
				<div class="row">
					<div class="col-12 col-md-3 logo-img">
						<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/drmohan-logo.png" width="194"  
						height="43"></amp-img>	
					</div>
					<div class="col-12 col-md-5"></div>
					<div class="col-12 col-md-4 tell">
						<span class="icon">
							<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/tell.png" alt="Logo" 
							height="32px" width="32px" ></amp-img>	
						</span>
						<span class="number"><a href="tel:+9198878 89878" style="color:black">+91-98878 89878</a></span>
					</div>
				</div>	
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 surgical">
				<div class="row">
					<div class="col-12 col-md-6 sub-heading">
					    <h1>மிகச்சிறந்த சர்க்கரை நோய் சிகிச்சை நிபுணர்களுக்கு அணுகுங்கள் டாக்டர்.மோகன்ஸ்!</h1>
							<div class="col-12 col-sm-12 appliance">
								<div class="row">
									<div class="col-12 col-md-4 col-sm-4 header-image">
										<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/heart.png" 
										alt="heart" width="70" height="70" /></amp-img>
									</div>
									<div class="col-12 col-md-8 col-sm-8 operation">
										<p>சர்க்கரை நோய்க்கான பிரத்யேக<br> அப்ளிகேஷன் கொண்டுள்ள ஒரே<br> சிகிச்சை மையம் டாக்டர்.மோகன்ஸ்<br>மட்டுமே</p>
									</div>
								</div>
							</div>	
							
							<div class="col-12 col-sm-12 appliance">
								<div class="row">
									<div class="col-12 col-md-4 col-sm-4 header-image">
										<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/time.png" 
											alt="time" width="70" height="70" /></amp-img>
									</div>
									<div class="col-12 col-md-8 col-sm-8 operation">
										<p>எங்களிடம் சர்க்கரை நோய்<br> சிகிச்சைக்கென பலதரப்பட்ட<br> மருந்துப் பொருட்கள் உள்ளன.</p>
									</div>
								</div>
							</div>
								
							<div class="col-12 col-sm-12 appliance">
								<div class="row">
									<div class="col-12 col-md-4 col-sm-4 header-image">
										<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/hospital.png" 
										alt="hospital" width="70" height="70" /></amp-img>
									</div>
									<div class="col-12 col-md-8 col-sm-8 operation">
										<p>சர்க்கரை நோய்க்கு நீங்கள்<br> விரும்பும் வகையிலான சிகிச்சை</p>
									</div>
								</div>
							</div>
					</div>	
					<div class="col-12 col-md-6 col-sm-6 book-appoinment">
					    <div class="gravity_holder " id="form1">
							<h2>முன்பதிவு செய்யுங்கள் </h2>
								<form action="//drmohans.com/tamil-lp/tamillptogravity.php" method="GET"  target="_top" class="appoinment">
									<input type="text" name="Name" id="idName" class="namefield" required pattern="[a-zA-Z][a-zA-Z\s]*" placeholder="பெயர்">
									<span visible-when-invalid="valueMissing" validation-for="idName">Your Name please!</span>
									<span visible-when-invalid="typeMismatch" validation-for="idName">Invalid Name!</span>
									<input type="text" name="Phone" id="idPhone" class="phonefield" required pattern="\d{10}" placeholder="தொலைபேசி எண்">
									<span visible-when-invalid="valueMissing" validation-for="idPhone">Your Number please!</span>
									<span visible-when-invalid="typeMismatch" validation-for="idPhonel">Invalid Number!</span>
									<input type="email" name="Email" id="idEmail" class="emailfield" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="இ-மெயில்">
									<span visible-when-invalid="valueMissing" validation-for="idEmail">Your email please!</span>
									<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Email!</span>
									<label id="idpatient">நோயாளி வகை</label><br>
									<input type="radio" id="radio" name="Patient_type" value="New Patient" required >
										<label for="New Patient" id="review" >புதிய நோயாளி</label><br>
										<input type="radio" id="radio" name="Patient_type" value="Review Patient" required>
										<label for="Review Patient" id="review">ஏற்கனவே வந்தவர்</label>
									<label for="Location"></label>
									<select name="Location" id="Location-list" required >
										<option value="">இருப்பிடத்தைத் தேர்வு செய்க</option>
										<option value="கோபாலபுரம் - சென்னை">கோபாலபுரம் - சென்னை</option>
										<option value="அண்ணா நகர்- சென்னை">அண்ணா நகர்- சென்னை</option>
										<option value="ஆவடி- சென்னை">ஆவடி- சென்னை</option>
										<option value="தாம்பரம் -சென்னை">தாம்பரம் -சென்னை</option>
										<option value="காரப்பாக்கம்  -சென்னை">காரப்பாக்கம்  -சென்னை</option>
										<option value="வடபழனி -சென்னை">வடபழனி -சென்னை</option>
										<option value="வேளச்சேரி- சென்னை">வேளச்சேரி- சென்னை</option>
										<option value="போரூர் - சென்னை">போரூர் - சென்னை</option>
										<option value="சேலையூர் - சென்னை">சேலையூர் - சென்னை</option>
										<option value="சூனாம்பேட்டை">சூனாம்பேட்டை</option>
										<option value="காஞ்சிபுரம்">காஞ்சிபுரம்</option>
										<option value="கோயம்புத்தூர்">கோயம்புத்தூர்</option>
										<option value="ஈரோடு">ஈரோடு</option>
										<option value="குடியாத்தம்">குடியாத்தம்</option>
										<option value="மதுரை">மதுரை</option>
										<option value="சேலம்">சேலம்</option>
										<option value="தஞ்சாவூர்">தஞ்சாவூர்</option>
										<option value="திருச்சிராப்பள்ளி">திருச்சிராப்பள்ளி</option>
										<option value="தூத்துக்குடி">தூத்துக்குடி</option>
										<option value="வேலூர்">வேலூர்</option>
										<option value="புதுச்சேரி">புதுச்சேரி</option>
										<option value="தோமல்குடா ,">தோமல்குடா ,</option>
										<option value="இந்திரா  பார்க் ரோடு, ஐதராபாத்">இந்திரா  பார்க் ரோடு, ஐதராபாத்</option>
										<option value="ஜூப்லி ஹில்ஸ்- ஐதராபாத்">ஜூப்லி ஹில்ஸ்- ஐதராபாத்</option>
										<option value="குக்கட்பள்ளி - ஐதராபாத்">குக்கட்பள்ளி - ஐதராபாத்</option>
										<option value="தில்சுக் நகர் - ஐதராபாத்">தில்சுக் நகர் - ஐதராபாத்</option>
										<option value="செகந்திராபாத்  - ஐதராபாத்">செகந்திராபாத்  - ஐதராபாத்</option>
										<option value="தொலிசவுக்கி  - ஐதராபாத்">தொலிசவுக்கி  - ஐதராபாத்</option>
										<option value="ஏ.எஸ் ராவ் நகர் - ஐதராபாத்">ஏ.எஸ் ராவ் நகர் - ஐதராபாத்</option>
										<option value="விஜயவாடா - ஆந்திரப் பிரதேசம்">விஜயவாடா - ஆந்திரப் பிரதேசம்</option>
										<option value="Jவிசாகப்பட்டினம் - ஆந்திரப்">விசாகப்பட்டினம் - ஆந்திரப்</option>
										<option value="பிரதேசம்">பிரதேசம்</option>
										<option value="ராஜமுந்திரி - ஆந்திரப் பிரதேசம்">ராஜமுந்திரி - ஆந்திரப் பிரதேசம்</option>
										<option value="புவனேஸ்வர் - ஒடிசா">புவனேஸ்வர் - ஒடிசா</option>
										
										<option value="டெல்லி - கிர்த்தி நகர்">டெல்லி - கிர்த்தி நகர்</option>
										<option value="இந்திரா நகர் - பெங்களூரு">இந்திரா நகர் - பெங்களூரு</option>
										<option value="ஜே.பி.நகர்  - பெங்களூரு">ஜே.பி.நகர்  - பெங்களூரு</option>
										<option value="மல்லேஸ்வரம் - பெங்களூரு">மல்லேஸ்வரம் - பெங்களூரு</option>
										<option value="ஒயிட்ஃபீல்டு - பெங்களூரு">ஒயிட்ஃபீல்டு - பெங்களூரு</option>
										
										<option value="மைசூருு">மைசூரு</option>
										<option value="திருவனந்தபுரம்">திருவனந்தபுரம்</option>
										<option value="கொச்சி">கொச்சி</option>
										<option value="கைகாலி - கொல்கத்தா">கைகாலி - கொல்கத்தா</option>
									</select>
									<select name="services" id="Location-list" required >
										<option value="">விரும்பும் சேவை</option>
										<option value="வழக்கமான முன்பதிவு">வழக்கமான முன்பதிவு</option>
										<option value="ஹோம் கேர்">ஹோம் கேர்</option>
										<option value="மாஸ்டர் ஹெல்த் செக்கப்">மாஸ்டர் ஹெல்த் செக்கப்</option>
									</select>
									<amp-date-picker id="simple-date-picker" type="single" mode="overlay"
									layout="container" on="select:AMP.setState({date1: event.date, dateType1: event.id})" format="YYYY-MM-DD"  input-selector="[name=date1]"
									class="example-picker space-between">
										<div class="icon-input"></div>
											<input name="date1" id="iddate1"  placeholder="விரும்பும் தேதி" required>
											<template type="amp-mustache" info-template>
											<span [text]="date1 != null ? 'You picked ' + date1 + '.' : 'You will see your chosen date here.'">You will see your chosen date here.</span>
											</template>
									</amp-date-picker>
									
										<input type="text" name="Comments" id="idComments" class="Comments" placeholder="கருத்துகள்">
										<input type="hidden" name="URL" value="<?php echo $actual_link; ?>" />
										<div class="tc pos"> 
											<input type="submit" value="சமர்ப்பிக்க" id="idSubmit" class="submit">
										</div>
								</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!--<section class="mobile-form">
		<div class="col-12 col-md-6 col-sm-6 mobile-appoinment">
			<div class="gravity_holder " id="form-1">
				<h2>முன்பதிவு செய்யுங்கள் </h2>
					<form action="//drmohans.com/tamil-lp/tamillptogravity.php" method="GET" target="_top" class="appoinment">
						<input type="text" name="Name" id="idName" class="namefield" required pattern="[a-zA-Z][a-zA-Z\s]*" placeholder="பெயர்">
						<span visible-when-invalid="valueMissing" validation-for="idName">Your Name please!</span>
						<span visible-when-invalid="typeMismatch" validation-for="idName">Invalid Name!</span>
						<input type="text" name="Phone" id="idPhone" class="phonefield" required pattern="\d{10}" placeholder="தொலைபேசி எண்">
						<span visible-when-invalid="valueMissing" validation-for="idPhone">Your Number please!</span>
						<span visible-when-invalid="typeMismatch" validation-for="idPhonel">Invalid Number!</span>
						<input type="email" name="Email" id="idEmail" class="emailfield" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="இ-மெயில்">
						<span visible-when-invalid="valueMissing" validation-for="idEmail">Your email please!</span>
						<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Email!</span>
						<label id="idpatient">நோயாளி வகை</label><br>
						<input type="radio" id="radio" name="Patient Type" value="New Patient" >
						<label for="New Patient" id="review" >புதிய நோயாளி</label><br>
						<input type="radio" id="radio" name="Patient Type" value="Review Patient">
						<label for="Review Patient" id="review">ஏற்கனவே வந்தவர்</label>
						<label for="Location"></label>
						<select name="Location" id="Location-list">
							<option value="இருப்பிடத்தைத் தேர்வு செய்க">இருப்பிடத்தைத் தேர்வு செய்க</option>
							<option value="கோபாலபுரம் - சென்னை">கோபாலபுரம் - சென்னை</option>
							<option value="அண்ணா நகர்- சென்னை">அண்ணா நகர்- சென்னை</option>
							<option value="ஆவடி- சென்னை">ஆவடி- சென்னை</option>
							<option value="தாம்பரம் -சென்னை">தாம்பரம் -சென்னை</option>
							<option value="காரப்பாக்கம்  -சென்னை">காரப்பாக்கம்  -சென்னை</option>
							<option value="வடபழனி -சென்னை">வடபழனி -சென்னை</option>
							<option value="வேளச்சேரி- சென்னை">வேளச்சேரி- சென்னை</option>
							<option value="போரூர் - சென்னை">போரூர் - சென்னை</option>
							<option value="சேலையூர் - சென்னை">சேலையூர் - சென்னை</option>
							<option value="சூனாம்பேட்டை">சூனாம்பேட்டை</option>
							<option value="காஞ்சிபுரம்">காஞ்சிபுரம்</option>
							<option value="கோயம்புத்தூர்">கோயம்புத்தூர்</option>
							<option value="ஈரோடு">ஈரோடு</option>
							<option value="குடியாத்தம்">குடியாத்தம்</option>
							<option value="மதுரை">மதுரை</option>
							<option value="சேலம்">சேலம்</option>
							<option value="தஞ்சாவூர்">தஞ்சாவூர்</option>
							<option value="திருச்சிராப்பள்ளி">திருச்சிராப்பள்ளி</option>
							<option value="தூத்துக்குடி">தூத்துக்குடி</option>
							<option value="வேலூர்">வேலூர்</option>
							<option value="புதுச்சேரி">புதுச்சேரி</option>
							<option value="தோமல்குடா ,">தோமல்குடா ,</option>
							<option value="இந்திரா  பார்க் ரோடு, ஐதராபாத்">இந்திரா  பார்க் ரோடு, ஐதராபாத்</option>
							<option value="ஜூப்லி ஹில்ஸ்- ஐதராபாத்">ஜூப்லி ஹில்ஸ்- ஐதராபாத்</option>
							<option value="குக்கட்பள்ளி - ஐதராபாத்">குக்கட்பள்ளி - ஐதராபாத்</option>
							<option value="தில்சுக் நகர் - ஐதராபாத்">தில்சுக் நகர் - ஐதராபாத்</option>
							<option value="செகந்திராபாத்  - ஐதராபாத்">செகந்திராபாத்  - ஐதராபாத்</option>
							<option value="தொலிசவுக்கி  - ஐதராபாத்">தொலிசவுக்கி  - ஐதராபாத்</option>
							<option value="ஏ.எஸ் ராவ் நகர் - ஐதராபாத்">ஏ.எஸ் ராவ் நகர் - ஐதராபாத்</option>
							<option value="விஜயவாடா - ஆந்திரப் பிரதேசம்">விஜயவாடா - ஆந்திரப் பிரதேசம்</option>
							<option value="Jவிசாகப்பட்டினம் - ஆந்திரப்">விசாகப்பட்டினம் - ஆந்திரப்</option>
							<option value="பிரதேசம்">பிரதேசம்</option>
							<option value="ராஜமுந்திரி - ஆந்திரப் பிரதேசம்">ராஜமுந்திரி - ஆந்திரப் பிரதேசம்</option>
							<option value="புவனேஸ்வர் - ஒடிசா">புவனேஸ்வர் - ஒடிசா</option>
							<option value="லக்னோ - உத்தரப் பிரதேசம்">லக்னோ - உத்தரப் பிரதேசம்</option>
							<option value="டெல்லி - கிர்த்தி நகர்">டெல்லி - கிர்த்தி நகர்</option>
							<option value="இந்திரா நகர் - பெங்களூரு">இந்திரா நகர் - பெங்களூரு</option>
							<option value="ஜே.பி.நகர்  - பெங்களூரு">ஜே.பி.நகர்  - பெங்களூரு</option>
							<option value="மல்லேஸ்வரம் - பெங்களூரு">மல்லேஸ்வரம் - பெங்களூரு</option>
							<option value="ஒயிட்ஃபீல்டு - பெங்களூரு">ஒயிட்ஃபீல்டு - பெங்களூரு</option>
							<option value="மங்களூரு">மங்களூரு</option>
							<option value="மைசூருு">மைசூரு</option>
							<option value="திருவனந்தபுரம்">திருவனந்தபுரம்</option>
							<option value="கொச்சி">கொச்சி</option>
							<option value="கைகாலி - கொல்கத்தா">கைகாலி - கொல்கத்தா</option>
						</select>
						<select name="Location" id="Location-list">
							<option value="இருப்பிடத்தைத் தேர்வு செய்க">இருப்பிடத்தைத் தேர்வு செய்க</option>
							<option value="கோபாலபுரம் - சென்னை">கோபாலபுரம் - சென்னை</option>
							<option value="அண்ணா நகர்- சென்னை">அண்ணா நகர்- சென்னை</option>
							<option value="ஆவடி- சென்னை">ஆவடி- சென்னை</option>
						</select>
						<amp-date-picker id="simple-date-picker" type="single" mode="overlay"
						layout="container" on="select:AMP.setState({date1: event.date, dateType1: event.id})" format="YYYY-MM-DD" open-after-select input-selector="[name=date1]"
						class="example-picker space-between">
							<div class="icon-input"></div>
								<input name="date1" id="iddate1"  placeholder="விரும்பும் தேதி">
									<template type="amp-mustache" info-template>
									<span [text]="date1 != null ? 'You picked ' + date1 + '.' : 'You will see your chosen date here.'">You will see your chosen date here.</span>
									</template>
						</amp-date-picker>
						<input type="text" name="Name" id="idComments" class="Comments" required pattern="[a-zA-Z][a-zA-Z\s]*" placeholder="கருத்துகள்">
						<input type="hidden" name="URL" value="" />
						<div class="tc pos"> 
							<input type="submit" value="சமர்ப்பிக்க" id="idSubmit" class="submit">
						</div>
					</form>
			</div>
		</div>
	</section>!-->
	
	<section class="first-part">
		<div class="container">
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 sugar-center">
			    <p>சர்க்கரை நோய் சிகிச்சையில் 25+ ஆண்டுகள் அனுபவம், 4,50,000க்கும் மேலான நோயாளிகளுக்கு திருப்திகரமான சிகிச்சைகளை டாக்டர் மோகன்ஸ் டயாபிடிஸ் ஸ்பெஷாலிடிஸ் சென்டர் சிறப்பாக வழங்கி வருகிறது. சர்க்கரை நோய் சிகிச்சையில் நம்பிக்கை அளிக்கும் வகையில் பணியாற்றி, சர்வதேச அளவிலான அங்கீகாரம் பெற்றதுடன், உலகத் தர சிகிச்சை வசதிகளை கொண்டுள்ளது. </p>
			</div>
		</div>
	</section>
	
	<section class="ours-help">	
		<div class="container">
		    <div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <h1>எங்கள் சேவைகள்<br>
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/underline.png" 
					alt="underline" width="246" height="1"/></am-img></h1>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 diet-foods ">
			    <div class="row">
				    <div class="col-12 col-md-6 col-sm-6 ">
					    <div class="col-12 col-sm-12 ">
						    <div class="row">
								<div class="col-12 col-sm-12 col-md-6  rice-img ">
									<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/rice.jpg" 
									alt="rice" width="206" height="132" /></amp-img>
								</div>
								<div class="col-12 col-sm-12 col-md-6 food-items ">
									<h3>ஹை ஃபைபர் ரைஸ் </h3>
									<p>டாக்டர் மோகன்ஸ் ஹை பைபர் ரைஸ்</p>
									<span>இப்போது கிடைக்கும்</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-sm-6 ">
					    <div class="col-12 col-sm-12 ">
						    <div class="row">
								<div class="col-12 col-sm-12 col-md-6  rice-img">
									<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/rice-brown.jpg" 
									alt="rice-brown" width="206" height="132" /></amp-img>
								</div>
								<div class="col-12 col-sm-12 col-md-6 food-items ">
									<h3>ஃபைபர் ப்ரவுன் அரிசி </h3>
									<p>டாக்டர்.மோகன்ஸ் ஹை ஃபைபர் ப்ரவுன் அரிசி</p>
									<span>இப்போது கிடைக்கும்</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12  diet-foods">
			    <div class="row">
				    <div class="col-12 col-md-6 col-sm-6">
					    <div class="col-12 col-sm-12 ">
						    <div class="row">
								<div class="col-12 col-sm-12 col-md-6  rice-img">
									<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/rice-flwaks.jpg" 
									alt="rice-flwaks" width="206" height="132" /></amp-img>
								</div>
								<div class="col-12 col-sm-12 col-md-6 food-items ">
									<h3>ரைஸ் ஃபிளேக்ஸ் </h3>
									<p>டாக்டர்.மோகன்ஸ் ஹை ஃபைபர் ப்ரவுன் ரைஸ் ஃபிளேக்ஸ்</p>
									<span>இப்போது கிடைக்கும்</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-sm-6 ">
					    <div class="col-12 col-sm-12 ">
						    <div class="row">
								<div class="col-12 col-sm-12 col-md-6  rice-img ">
									<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/corn.jpg" 
									alt="corn" width="206" height="132" /></amp-img>
								</div>
								<div class="col-12 col-sm-12 col-md-6 food-items ">
									<h3>டயட் சிவ்டா </h3>
									<p>டாக்டர்.மோகனின் டயட்  சிவ்டா</p>
									<span>இப்போது கிடைக்கும்</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</section>
	
	<section class="Health-check">
	    <div class="container">
		    <div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <h1>மாஸ்டர் ஹெல்த் செக்கப்</h1>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <div class="row">
				    <div class="col-12 col-md-3 tips-img">
						<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/sugar-level.jpg" 
						alt="sugar-level" width="206" height="132" /></amp-img>
					</div>
					<div class="col-12 col-md-9 text-para">
					    <p>உடலின் சர்க்கரை அளவு மற்றும் சர்க்கரை நோயால் ஏற்படும் அனைத்து சிக்கல்களையும் அடையாளம் காணும் வகையில் எங்களது ஸ்பெஷல் சிகிச்சை பேக்கேஜ் உள்ளது.</p>
					    <p>இந்த சர்க்கரை நோய் பரிசோதனையில் எல்.எஃப்.டி, இ.சி.ஜி சோதனை, தைராய்டு சோதனை, ஃபூட் பிரஷ்ஷர் சோதனை, மருத்துவர் அளிக்கும் ஆலோசனைகள் மற்றும் பிற வசதிகளும் அடங்கும்</p>
						<span>டோமல்குடா மற்றும் கோபாலபுரம் கிளைகளில் மட்டுமே ஏற்றுக் கொள்ளப்படும்</span>
					</div>
				</div>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <h1>120 நிமிட எக்ஸ்பிரஸ் செக்கப்</h1>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <div class="row">
				    <div class="col-12 col-md-3 tips-img">
						<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/120-mins.jpg" 
						alt="120-mins" width="206" height="132" /></amp-img>
					</div>
					<div class="col-12 col-md-9 text-para">
					    <p>வெறும் 120 நிமிடங்களில் விரிவான சர்க்கரை நோய் பரிசோதனை மற்றும் மருத்துவரின் ஆலோசனையைப் பெற்றுக் கொள்ளலாம். அதன் பின்னர் உங்கள் வழக்கமான பணியில் ஈடுபடலாம்.</p>
					    <p>டோமல்குடா மற்றும் கோபாலபுரம் மையங்களில் மட்டுமே ஏற்றுக் கொள்ளப்படும்</p>
						<span>நீங்கள் சிகிச்சை பெற விரும்பும் தேதிக்கு ஒருநாள் முன்னதாக, மாலை 4 மணிக்குள், இதற்கான முன்பதிவு செய்ய வேண்டும்</span>
					</div>
				</div>
			</div>
			
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <h1>ஹோம் சர்வீசஸ்</h1>
			</div>
			
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
			    <div class="row">
				    <div class="col-12 col-md-3 tips-img">
						<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/home-services.jpg" 
						alt="home-services" width="206" height="132" /></amp-img>
					</div>
					<div class="col-12 col-md-9 text-para">
					    <p>சர்க்கரை நோய் சிகிச்சைக்கு, இனி மருத்துவமனைகளுக்கு செல்ல வேண்டிய சிரமம் இல்லை. எங்களைத் தொடர்பு கொண்டால், உங்கள் வீட்டிலேயே சவுகரியமாக அனைத்து சிகிச்சை வசதிகளையும் பெற்றுக் கொள்ளலாம்.</p>
					    
						    <div class="col-12 col-sm-12 people-choice">
							    <p>வழங்கப்படும் சேவைகள்</p>
							    <div class="row">
								    <div class="col-12 col-sm-12 col-md-4 helping-img">
										<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/blood-collection.jpg"
										alt="blood-collection" width="179" height="87" /></amp-img>
									</div>
									<div class="col-12 col-sm-12 col-md-4 helping-img">
										<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/home-delvry.jpg" 
										alt="home-delvry" width="179" height="87" /></amp-img>
									</div>
									<div class="col-12 col-sm-12 col-md-4 helping-img">
										<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/foot-care.jpg" 
										alt="foot-care" width="179" height="87" /></amp-img>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-12 people-choice">
							    <div class="row">
								    <div class="col-12 col-sm-12 col-md-4 helping-img">
										<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/Physiotherapy.jpg" 
										alt="Physiotherapy" width="179" height="87" /></amp-img>
									</div>
									<div class="col-12 col-sm-12 col-md-4 helping-img">
										<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/Fitness-Training.jpg"
										alt="Fitness-Training" width="179" height="87" /></amp-img>
									</div>
									<div class="col-12 col-sm-12 col-md-4 helping-img">
										<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/Health-Products.jpg" 
										alt="Health-Products" width="179" height="87" /></amp-img>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="last-part">
		<div class="container">
			<div class="col-12 col-md-12 col-sm-12 col-lg-12 testimonial">
				<h1> வாடிக்கையாளர் நற்சான்றுகள்</h1>
					<amp-img src="<?php bloginfo('stylesheet_directory'); ?>/images/underline.png" 
					alt="underline" width="246" height="1"/></am-img>
			</div>
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-md-8 col-sm-8 swiper">	
						<amp-carousel class="carousel3" layout="responsive" height="100" width="300" autoplay delay="0" controls loop type="slides">	
							<div class="slide">
								<div class="captions">
									<p>”உங்கள் மருத்துவமனையில் அளிக்கப்படும் சேவைகள் மிகச் சிறப்பாக இருக்கிறது. பணியாளர்களும், அவர்கள் செய்யும் பணிவிடைகளும் மிகவும் திருப்திகரமாக உள்ளது. அதுமட்டுமல்லாமல் டாக்டர்.முத்துக்குமார் மற்றும் அவரது ஆலோசனைகள் எங்களுக்கு மிகவும் பிடித்திருக்கிறது. நன்றி”</p>
								<span><strong>- சந்திரன் என்</strong></span>
								</div>
							</div>

							<div class="slide">
								<div class="captions"> 
									<p>“டாக்டர் மோகன்ஸ் கிளினிக்கில் அனைவரும் புன்னகையோடு வரவேற்கின்றனர். அனைத்து பரிசோதனைகளும் சிறப்பாகவும், துல்லியமாகவும் எடுக்கப்பட்டன. அங்குள்ள அனைவரும் என்னிடம் காட்டிய அக்கறையால், நான் நோயிலிருந்து முற்றிலும் குணமடைந்து விட்டதாகவே உணர்கிறேன்”</p>
								<span><strong>- திருமதி.முத்துலட்சுமி யு</strong></span>
								</div>
							</div>          

							<div class="slide">
								<div class="captions"> 
									<p>“என்னுடைய ஊர் கோயம்புத்தூர். சர்க்கரை நோய் சிகிச்சைக்காக டாக்டர்.சுகுணாவைத் தொடர்பு கொண்டேன். அவரும், மற்றவர்களும் சிறப்பாக ஒத்துழைப்பு அளித்தனர். உங்கள் சேவைக்கு என்னுடைய மனமார்ந்த நன்றி”</p>
								<span><strong>- திருமதி. தங்கம்</strong></span>
								</div>
							</div> 
						</amp-carousel>

						
					</div>
					
					<div class="col-12 col-md-8 col-sm-8 mobile-swiper">	
						<amp-carousel class="carousel3" layout="responsive" height="300" width="300" autoplay delay="0" controls loop type="slides">	
							<div class="slide">
								<div class="captions">
									<p>”உங்கள் மருத்துவமனையில் அளிக்கப்படும் சேவைகள் மிகச் சிறப்பாக இருக்கிறது. பணியாளர்களும், அவர்கள் செய்யும் பணிவிடைகளும் மிகவும் திருப்திகரமாக உள்ளது. அதுமட்டுமல்லாமல் டாக்டர்.முத்துக்குமார் மற்றும் அவரது ஆலோசனைகள் எங்களுக்கு மிகவும் பிடித்திருக்கிறது. நன்றி”</p>
								<span><strong>- சந்திரன் என்</strong></span>
								</div>
							</div>

							<div class="slide">
								<div class="captions"> 
									<p>“டாக்டர் மோகன்ஸ் கிளினிக்கில் அனைவரும் புன்னகையோடு வரவேற்கின்றனர். அனைத்து பரிசோதனைகளும் சிறப்பாகவும், துல்லியமாகவும் எடுக்கப்பட்டன. அங்குள்ள அனைவரும் என்னிடம் காட்டிய அக்கறையால், நான் நோயிலிருந்து முற்றிலும் குணமடைந்து விட்டதாகவே உணர்கிறேன்”</p>
								<span><strong>- திருமதி.முத்துலட்சுமி யு</strong></span>
								</div>
							</div>          

							<div class="slide">
								<div class="captions"> 
									<p>“என்னுடைய ஊர் கோயம்புத்தூர். சர்க்கரை நோய் சிகிச்சைக்காக டாக்டர்.சுகுணாவைத் தொடர்பு கொண்டேன். அவரும், மற்றவர்களும் சிறப்பாக ஒத்துழைப்பு அளித்தனர். உங்கள் சேவைக்கு என்னுடைய மனமார்ந்த நன்றி”</p>
								<span><strong>- திருமதி. தங்கம்</strong></span>
								</div>
							</div> 
						</amp-carousel>

						
					</div>
					<div class="col-12 col-md-5 col-sm-5"></div>

				</div>
			</div>
		</div>
	</section>
	
	<section class="footer">
		<div class="container">
			<div class="col-12 col-md-12 col-sm-12 col-lg-12">
				<div class="row">
					<div class="col-12 col-md-6">
						<p style="font-size:13px">© காப்புரிமை 2019 டாக்டர். மோகன்ஸ் நீரிழிவு சிறப்பு சிகிச்சை மையம்</p>
					</div>
					<div class="col-12 col-md-6 beat">
						<p style="font-size:13px">இணையதள வடிவமைப்பு & உருவாக்கம் - சோஷியல் பீட்</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="mobile-button">
		<div class="container">
			<div class="fixed-bottom ">
				<a class="enqire" id="enquire-now" href="#form1" >தகவல் கேட்க</a>
				<a class="call" id="callers"  href="tel:(+91)98878 89878">அழையுங்கள்</a>
			</div>
		</div>
	</section>
</body>