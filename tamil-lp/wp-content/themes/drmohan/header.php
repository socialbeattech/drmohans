<?php
/*
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
	<title>டாக்டர்.மோகன்ஸ்</title>
	<meta name="description" content="சிறந்த சர்க்கரை நோய் சிகிச்சை நிபுணர்களுக்கு அணுகுங்கள் டாக்டர்.மோகன்ஸ்! உலகத் தர வசதிகள். 25+ ஆண்டுகள் அனுபவம். 4,50,000க்கும் மேற்பட்டோருக்கு சிகிச்சை">
  	<meta name="keywords" content="டாக்டர்.மோகன்ஸ்">
	<link rel="icon" href="<?php echo get_template_directory_uri() ?>/images/favicon.jpg" sizes="32x32" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
	 <link rel="icon" type="image/png" id="favicon" href="<?php echo get_template_directory_uri(); ?>/images/favicon_dr_mohans.png"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<section class="head-banner">	
			<div class="fullcolumn">
                <div class="container-fluid1">
				    <div class="col-12 col-md-12 col-sm-12 col-lg-12 header-side pt-3 pb-3">
					    <div class="row">
						    <div class="col-12 col-md-6">
							 <?php if(( is_home() ) || ( is_front_page() )) :?>
							    <img src="<?php echo get_template_directory_uri() ?>/images/drmohan-logo.png" alt="drmohan-logo" width="194" height="43" />                  
								<?php else: ?>
								<a href="https://lifeonline.co/drmohans-tamil/" target="_blank"> <img src="<?php echo get_template_directory_uri() ?>/images/drmohan-logo.png" alt="drmohan-logo" width="194" height="43" />
								<?php endif; ?>
							</div>
							<div class="col-12 col-md-6 tell">
						
							    <ul>
								    <li class="call">
									<a href="tel:+9198878 89878" style="color:black">+91-98878 89878</a>
									</li>
								    
									
								</ul>
								
							</div>
						</div>
					</div>
					
					<div class="col-12 col-md-12 col-sm-12 col-lg-12 best-diabetes">
					    <div class="row">
						    <div class="col-12 col-md-6">
							    <h1 style="font-size:24px;color:#ea2324;line-height: 40px;font-weight: bold;">மிகச்சிறந்த சர்க்கரை நோய் சிகிச்சை நிபுணர்களுக்கு அணுகுங்கள் டாக்டர்.மோகன்ஸ்!</h1><br>
							
								<div class="col-12 col-sm-12 appliance">
								    <div class="row">
										<div class="col-12 col-md-4 col-sm-4 header-image">
											<img src="<?php echo get_template_directory_uri() ?>/images/heart.png" alt="heart" width="70" height="70" />
											
										</div>
										<div class="col-12 col-md-8 col-sm-8 operation">
											<p>சர்க்கரை நோய்க்கான பிரத்யேக<br> அப்ளிகேஷன் கொண்டுள்ள ஒரே<br> சிகிச்சை மையம் டாக்டர்.மோகன்ஸ்<br>மட்டுமே</p>
										</div>
									</div>
								</div>
								
								<div class="col-12 col-sm-12 appliance">
								    <div class="row">
										<div class="col-12 col-md-4 col-sm-4 header-image">
											<img src="<?php echo get_template_directory_uri() ?>/images/time.png" alt="time" width="70" height="70" />
										</div>
										<div class="col-12 col-md-8 col-sm-8 operation">
											<p>எங்களிடம் சர்க்கரை நோய்<br> சிகிச்சைக்கென பலதரப்பட்ட<br> மருந்துப் பொருட்கள் உள்ளன.</p>
										</div>
									</div>
								</div>
								
								<div class="col-12 col-sm-12 appliance">
								    <div class="row">
										<div class="col-12 col-md-4 col-sm-4 header-image">
											<img src="<?php echo get_template_directory_uri() ?>/images/hospital.png" alt="hospital" width="70" height="70" />
										</div>
										<div class="col-12 col-md-8 col-sm-8 operation">
											<p>சர்க்கரை நோய்க்கு நீங்கள்<br> விரும்பும் வகையிலான சிகிச்சை</p>
										</div>
									</div>
								</div>
								
							</div>
							
							<div class="col-12 col-md-6 form ">
							    <?php if(( is_home() ) || ( is_front_page() )) :?>
									<div class="gravity_holder " id="form1">
										<h2>முன்பதிவு செய்யுங்கள் </h2><hr></hr>
										<?php gravity_form(1, $display_title = false, $display_description = false, $ajax = false, $tabindex, $echo = true ); ?>									
									</div>
								<?php else: ?>							
									<div class="form-holder text-center">
										<p class="gotham-bold fs-20 fs-xs-14 fs-sm-14">எங்களைத் தொடர்பு கொண்டதற்கு நன்றி! விரைவில் உங்களை நாங்கள் தொடர்புகொள்வோம்.</p>
									</div>
							<?php endif; ?>
							</div>							
						</div>
					</div>
                </div>				
			</div>
	</section>