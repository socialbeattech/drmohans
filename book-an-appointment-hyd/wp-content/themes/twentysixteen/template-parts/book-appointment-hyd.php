<?php
/**
 * Template Name: Book Appointment hyd
 *
 * 
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(hyd); ?>

<!-- Start of Branches -->
<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function($) {
$("#input_1_10").datepicker({ minDate:+1, dateFormat: 'dd/mm/yy', beforeShowDay: function(date) { var day = date.getDay(); return [(day != 0), '']; } });
});
</script>

<!-- End of Branches -->
<?php get_footer(); ?>