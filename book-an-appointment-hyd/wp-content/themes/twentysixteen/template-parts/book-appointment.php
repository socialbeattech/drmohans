<?php
/**
 * Template Name: Book Appointment
 *
 * 
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<!-- Start of Branches -->
<section class="our_branch_block clearfix">
<div class="branch_title">
<div class="branch_top_icon"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Branches_icons.png" alt="Branch" class="img-responsive" width="83" height="53" /></div>
<h1>Our Branches</h1>
</div>
	<div class="container">
		<div class="branch_services">
			<div class="col-xs-12 col-sm-5 col-md-5 branch_left">	
				<ul class="main_branch">
                            <li>Hyderabad</li>
				</ul> 				
				<ul class="subr_branch">
					<li><a href="http://drmohans.com/diabetes-centre-locations/hyderabad/indira-park-road-hyderabad?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Indira Park Road</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/hyderabad/jubilee-hills-hyderabad?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Jubilee Hills</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/hyderabad/kukatpally?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Kukatpally</a></li>
                    <li><a href="http://drmohans.com/diabetes-centre-locations/telangana/dilsukh-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Dilsukh Nagar</a></li>
                    <li><a href="http://drmohans.com/diabetes-centre-locations/telangana/secunderabad?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Secunderabad</a></li>
                    <li><a href="http://drmohans.com/diabetes-centre-locations/telangana/tolichowki?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Tolichowki</a></li>
                    <li><a href="https://drmohans.com/diabetes-centre-locations/telangana/a-s-rao-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">A.S.Rao Nagar</a></li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-7 branch_right">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Dr-mohans-branch-head.png" class="img-responsive" alt="Branch" width="554" height="323" />				
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function($) {
$("#input_1_10").datepicker({ minDate:+1, dateFormat: 'dd/mm/yy', beforeShowDay: function(date) { var day = date.getDay(); return [(day != 0), '']; } });
});
</script>

<!-- End of Branches -->
<?php get_footer(); ?>