<?php
	/**
	 * The template for displaying the header
	 *
	 * Displays all of the head element and everything up until the "site-content" div.
	 *
	 * @package WordPress
	 * @subpackage Twenty_Sixteen
	 * @since Twenty Sixteen 1.0
	 */

?><!DOCTYPE html>
	<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		
	<link rel="icon" href="http://drmohans.com/wp-content/uploads/cropped-m-32x32.png" />
	
	<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->

	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
		<?php wp_head(); ?>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KRGZJDF');</script>
<!-- End Google Tag Manager -->
	</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KRGZJDF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --> 
	<div id="page" class="site">
		<div class="site-inner">	
		
		<header id="masthead" class="site-header" role="banner">		
			<div class="container">
			
<!-- Start of Navigation . work for mobile alone, not for desktop and tablet -->
	 <nav class="hidden-sm hidden-md hidden-lg navbar navbar-default navbar-fixed-top" role="navigation">
        
<!-- Start of Toggle for mobile meun -->
		   <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>                
            </div>
<!-- End of Toggle for mobile menu -->
            
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
					<li class="expcheck">                     
						<a class="page-scroll" href="#" data-toggle="modal" data-target="#largeModal_express">120 Minutes Express Check Up</a>
                    </li>
                    
                    <li class="fibre">
                        <a class="page-scroll" href="#" data-toggle="modal" data-target="#largeModal_brownrice">High Fibre Rice</a>
                    </li>
					<li class="eveclinic">                   
						<a class="page-scroll" href="#" data-toggle="modal" data-target="#largeModal_clinic">Evening Clinic</a>
                    </li>
					<li class="homeserv">
                        <a class="page-scroll" href="#" data-toggle="modal" data-target="#largeModal_homeservices">Home Services</a>
                    </li>
					<li class="shopping">
                        <a class="page-scroll" href="http://diabetes.ind.in/" target="_blank">Diabetic Shop</a>
                    </li>					
                </ul>
            </div>
                   
    </nav>
	
<!-- End of Navigation . work for mobile alone, not for desktop and tablet -->
	
<!-- Start of logo and header -->
				<div class="logo_contact">
					<div class="col-xs-12 col-sm-5 col-md-5">
						<a href="<?php echo home_url(); ?>"><img src="http://drmohans.com/book-an-appointment/wp-content/uploads/2016/05/Drmohans-diabetes-lp.png" alt="Drmohans" width="216" height="70" /></a>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-7">	
					
						<div class="pull-right  text-right quality">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Diabates-Care.png" alt="Drmohans" width="100" height="83" />
						</div>
						<div class="pull-right text-right telephone" style="display : none; ">
							<span class="my-phone"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Call-us.png" alt="Drmohans" width="32" height="32" /></span>
							<div class="header_call_us">
								
								<div class="hyderabad_call"><p class="label_city">Hyderabad</p><p class="label_number"><a href="tel:+04043968888">040 - 43968888</a></p></div>
								<div class="hyderabad_call"><p class="label_city">Bangalore</p><p class="label_number"><a href="tel:+08043968888">080 - 43968888</a></p></div>
								<div class="chennai_call"><p class="label_city">Chennai</p><p class="label_number"><a href="tel:+04428353926">044 - 28353926</a></p></div>
								
							</div>
						</div>						

					</div>
				</div>
<!-- End of logo and header -->				
		
				
	<div class="our_services_block clearfix">
	<div class="container">
	<div class="our_services">
	<div class="col-xs-12 col-sm-7 col-md-9 service_left">
			<div class="service_paras">
				<div class="headings text-center">Consult the best diabetes care professionals only at Dr Mohan's </div>
				<div class="captions text-center">With more than 28 of experience in the prevention and management of diabetes with over 4,80,000+ satisfied patients, we have transformed into an international center of excellence in diabetes. DMDSC stands committed to provide world-class health care facilities for diabetes and all its complications. Based on 30 years of research experience with over 2000 research papers published in renowned journals.(Lancet,Nature) </div>
			</div>
<!-- Thankyou page content -->
			<div class="service_paras thankyou">
				<div class="captions text-center">Thanks for contacting us! We will get in touch with you shortly.</div>
			</div>
<!-- End : Thankyou page content hide for home page-->			

			<div class="service_icons">
				<div class="service_header">
					<h1>OUR SERVICES</h1>
				</div>
				<div class="service_menus">
								<!-- Start of row -1 -->
<div class="row-1">	
			
<!-- START : Brown Rice -->
	<div class="hidden-xs  diamond rice">
	<div class="diamond-inner rice_sub"><span class="icons_img rice_img"><a href="#" data-toggle="modal" data-target="#largeModal_brownrice"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Brown-Rice.png" alt="Drmohans" width="9" height="32" /></a></span>
	<span class="icons_caps rice_caps"><a href="#" data-toggle="modal" data-target="#largeModal_brownrice">High Fibre Rice<br>and Diabetic Friendly<br> food products</a></span>
	</div>	
	</div>	

	<!-- Content of popup Brown Rice -->
	<div class="modal fade" id="largeModal_brownrice" tabindex="-1" role="dialog" aria-labelledby="largeModal_brownrice" aria-hidden="true">
	<div class="modal-dialog modal-xs">
	<div class="modal-content">
	
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<header class="entry-header title_holder">	
	<span class="textcenter"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Brown-Rice.png" alt="Drmohans" width="9" height="32" /></span>
	<h1>High Fibre Rice<br>and Diabetic Friendly<br> food products</h1>								   
	</header>
	</div>
	
	<div class="modal-body">
	<p><strong>Now Available:</strong></p>
	<ul>
		<li><a href="http://diabetes.ind.in/shopping?p_p_id=konakart_portlet_WAR_konakart&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_konakart_portlet_WAR_konakart__spage=%2FSelectCat.do%3FcatId%3D23&_konakart_portlet_WAR_konakart_catId=23" target="_blank">Dr. Mohan's High Fibre Rice</a></li>
		<li><a href="http://diabetes.ind.in/shopping?p_p_id=konakart_portlet_WAR_konakart&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_konakart_portlet_WAR_konakart__spage=%2FSelectCat.do%3FcatId%3D23&_konakart_portlet_WAR_konakart_catId=23" target="_blank">Dr. Mohan's High Fibre Brown Rice</a></li>
		
	</ul>
	<p><strong>Launching Soon:</strong></p>
	<ul>
		<li>Dr. Mohan's High Fibre Brown Rice Flakes</li>
		<li>Dr. Mohan’s Diet Chivda</li>
	</ul>
	</div>
	
	</div>
	</div>
	</div>
	<!-- End Content of popup -->
<!-- End : Brown Rice -->	
<!-- START : Valentine's DaY -->	
<!-- Valentines Day removed and Changed to Master health checkup on 24-02-2016 -->		
	<div class="hidden-xs  diamond valentines">
		<div class="diamond-inner valentines_sub">
		<span class="icons_img valentines_img"><a href="#" data-toggle="modal" data-target="#largeModal_valentines"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Master-Checkup.png" alt="Drmohans" width="34" height="28" /></a></span>
		<span class="icons_caps valentines_caps"><a href="#" data-toggle="modal" data-target="#largeModal_valentines">World standard<br> Lab service</a></span>
		</div>
	</div>
			
			<!-- Content of popup Valentines Day -->
			<!-- Valentines Day removed and Changed to Master health checkup on 24-02-2016 -->
	<div class="modal fade" id="largeModal_valentines" tabindex="-1" role="dialog" aria-labelledby="largeModal_valentines" aria-hidden="true">
		<div class="modal-dialog modal-xs">
		<div class="modal-content">
		
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<header class="entry-header title_holder">	
		<span class="textcenter"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Master-Checkup.png" alt="Drmohans" width="34" height="28" /></span>
		<h1>World standard <br>Lab service</h1>								   
		</header>
		</div>
		
		<div class="modal-body">										 
		<ul>
		<li>Our exclusive package aims  to identify the sugar levels of the body and also all the complications related to diabetes.</li>		
		<li>This diabetic health check-up includes a number of screenings such as LFT, ECG Test, Thyroid Test, Foot pressure test, consultation and all other examinations as well.</li>		
		<li>Valid at Domalguda and Gopalapuram branch only. </li>		
		</ul>	
		</div>
		
		</div>
		</div>
	</div>
		<!-- End Content of popup -->
<!-- End : Valentine's DaY -->		

<!-- START: Express Check Up -->				
	<div class="hidden-xs  diamond express">
		<div class="diamond-inner express_sub">
		<span class="icons_img express_img"><a href="#" data-toggle="modal" data-target="#largeModal_express"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Express-Checku.png" alt="Drmohans" width="40" height="40" /></a></span>
		<span class="icons_caps express_caps"><a href="#" data-toggle="modal" data-target="#largeModal_express">Foot,Eye,Fitness<br> and Lifestyle check</a></span>
		</div>
	</div>
			
			<!-- Content of popup Express Check Up -->
	<div class="modal fade" id="largeModal_express" tabindex="-1" role="dialog" aria-labelledby="largeModal_express" aria-hidden="true">
		<div class="modal-dialog modal-xs">
		
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<header class="entry-header title_holder">	
		<span class="textcenter"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Express-Checku.png" alt="Drmohans" width="40" height="40" /></span>
		<h1>Foot,Eye,Fitness<br> and Lifestyle check</h1>								   
		</header>
		
		</div>
		<div class="modal-body">										 
		<ul>
		<li>Get a comprehensive diabetes check and doctor’s consultation all in just 120 minutes flat and go back to your daily routine.</li>
		<li>Valid at Gopalapuram and Domalguda centre only</li>
		<li>The appointment must be booked before 4 PM one day prior to the preferred date.</li>
		</ul>					 
		</div>
		
		</div>
		</div>			
	</div>
			<!-- End Content of popup -->
<!-- End: Express Check Up -->	

</div>
<!-- End of row -1 -->
<!-- Start of row -2 -->
<div class="row-2">

<!-- START: Express Women's Day -->	

	<div class="hidden-xs diamond-second womens">
	<div class="diamond-second-inner womens_sub">
	<span class="icons_img womens_img custom_icons"><span class="glyphicon glyphicon-shopping-cart glyphicon-large"></span></span>
	<span class="icons_caps womens_caps"><a href="http://diabetes.ind.in/" target="_blank">Diabetic foot wear and pharmacy.</br></a></span>
	</div>
	</div>
	
	<!-- Content of popup Women's Day Offer -->
	<div class="modal fade" id="largeModal_womensday" tabindex="-1" role="dialog" aria-labelledby="largeModal_womensday" aria-hidden="true">
		<div class="modal-dialog modal-xs">
		<div class="modal-content">
		<div class="modal-header">
		
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<header class="entry-header title_holder">	
		<span class="textcenter custom_icons"><span class="glyphicon glyphicon-shopping-cart glyphicon-large"></span></span>		
		<h1>Diabetic foot wear and pharmacy</h1>								   
		</header>
		</div>
		<div class="modal-body">
		
		<ul>
		<li>Book an appointment for any of your treatments and avail a 15% discount on your bills.</li>
		<li>Valid only in the Evening Clinic</li>
		<li>Valid for women patients only.</li>		
		<li>Valid from 7th March to 11th March 2016 at Gopalapuram (Chennai) and Domalguda (Hyderabad) branches only.</li>	
		<li>Not valid in Pharmacy and Healthcare products.</li>
		</ul>								 

		</div>
		</div>
		</div>
	</div>
	<!-- End Content of popup -->
<!-- End: Express Women's Day -->	

<!-- START: Home Services -->	

	
	<div class="hidden-xs  diamond-second homes">
		<div class="diamond-second-inner homes_sub">
		<span class="icons_img homes_img"><a href="#" data-toggle="modal" data-target="#largeModal_homeservices"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Home-Services.png" alt="Drmohans" width="31" height="25" /></a></span>
		<span class="icons_caps homes_caps"><a href="#" data-toggle="modal" data-target="#largeModal_homeservices">Blood collection and pharmacy at your doorstep on call</a></span>
		</div>
	</div>
	
		<!-- Content of popup Home Services -->
		<div class="modal fade" id="largeModal_homeservices" tabindex="-1" role="dialog" aria-labelledby="largeModal_homeservices" aria-hidden="true">
			<div class="modal-dialog modal-xs">
			<div class="modal-content">
			
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<header class="entry-header title_holder">	
			<span class="textcenter"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Home-Services.png" alt="Drmohans" width="31" height="25" /></span>
			<h1>Blood collection and pharmacy at your doorstep on call</h1>								   
			</header>
			</div>
			
			<div class="modal-body">			
			<P>Now you need not take the pains of travelling for your diabetes care. Just call us and get all your investigations done from the comfort of your home.</p>
			<p><u>Services Offered:</u></p>
			<ul>
				<li>Blood Collection</li>
				<li>Home Delivery Of Medicines</li>
				<li>Diabetes Foot Care</li>
				<li>Physiotherapy</li>
				<li>Fitness Training</li>
				<li>Diabetes Footwear & Health Products</li>
			</ul>
			</div>
			
			</div>
			</div>
		</div>
		<!-- End Content of popup -->
<!-- End: Home Services -->
		
<!-- START: Evening Clinic -->
	<div class="hidden-xs diamond-second event">
		<div class="diamond-second-inner event_sub">
		<span class="icons_img event_img"><a href="#" data-toggle="modal" data-target="#largeModal_clinic"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Event-Clinic.png" alt="Drmohans" width="25" height="25" /></a></span>
		<span class="icons_caps event_caps"><a href="#" data-toggle="modal" data-target="#largeModal_clinic">Diet consultation and education</a></span>
		</div>
	</div>
	
	<!-- Content of popup Evening Clinic -->
	<div class="modal fade" id="largeModal_clinic" tabindex="-1" role="dialog" aria-labelledby="largeModal_clinic" aria-hidden="true">
		<div class="modal-dialog modal-xs">
		<div class="modal-content">
		
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<header class="entry-header title_holder">		
		<span class="textcenter"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Event-Clinic.png" alt="Drmohans" width="25" height="25" /></span>		
		<h1>Diet consultation and education</h1>								   
		</header>
		</div>
		
		<div class="modal-body">										 
		<ul>
		<li>Book an appointment at your convenient time without the hassle of missing office work or other important engagements.</li>			
		<li>Valid at Gopalapuram (Chennai) branch from Monday to Friday, 6 PM to 8 PM</li>			
		<li>Valid at Domalguda (Hyderabad) branch from Monday to Friday, 5.30 PM to 7.30 PM</li>		
		</ul>
		</div>
		
		</div>
		</div>
	</div>
	<!-- End Content of popup -->
<!-- End: Evening Clinic -->
	
</div>
<!-- End of row -2 -->
		
				</div>	
			</div>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-3 service_right">
	<?php gravity_form( 1, $display_title = true, $ajax = true, $tabindex, $echo = true ); ?>
	</div>
	</div> <!-- our_services -->
	</div> <!-- container -->
	</div><!-- our_services_block -->
			
</div>	<!-- header container -->
</header><!-- header -->
			
	
			<div id="content" class="site-content">