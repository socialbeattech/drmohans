<?php
/**
 * Template Name: Book Appointment
 *
 * 
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<!-- Start of Branches -->
<section class="our_branch_block clearfix">
<div class="branch_title">
<div class="branch_top_icon"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Branches_icons.png" alt="Branch"  width="83" height="53" /></div>
<h1>Diabetes Management  Packages</h1>
</div>
	<div class="container">
		<div class="branch_services">
			<div class="col-xs-12 col-sm-5 col-md-5 branch_left">	
				 
				<ul class="main_branch">
					<li>Chennai</li>
				</ul>				
				<ul class="subr_branch">
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/anna-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Anna Nagar</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/avadi?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Avadi</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/gopalapuram-main-centre?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Gopalapuram</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/omr-karapakkam?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">OMR- Karapakkam</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/porur?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Porur</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/selaiyur?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Selaiyur</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/tambaram?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Tambaram</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/vadapalani-clinic?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Vadapalani</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/velachery?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Velachery</a></li>
						</ul>
			</div>
			<div class="col-xs-12 col-sm-7  col-md-7 text-center">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Dr-mohans-branch-head.png"  alt="Branch" width="500" height="532" />				
			</div>
		</div>
	</div>
</section>

<!-- End of Branches -->
<?php get_footer(); ?>