<?php
require('wp-config.php');
//API for gravity form
$api_key = '7267462139';
$private_key = '073fa89748ba99d';
 
//set route
$route = 'forms/10/entries';

function calculate_signature( $string, $private_key ) {
    $hash = hash_hmac( 'sha1', $string, $private_key, true );
    $sig = rawurlencode( base64_encode( $hash ) );
    return $sig;
}


//creating request URL
$expires = strtotime( '+60 mins' );
$string_to_sign = sprintf( '%s:%s:%s:%s', $api_key, 'POST', $route, $expires );
$sig = calculate_signature( $string_to_sign, $private_key );
$url = get_site_url() . '/gravityformsapi/' . $route . '?api_key=' . $api_key . '&signature=' . $sig . '&expires=' . $expires;

$entries = array(
    array(
        'date_created' => date('Y-m-d H:i:s'),
        'is_starred'   => 0,
        'is_read'      => 0,
        'ip'           => '::1',
        'source_url'   => $_GET['url'],
        'currency'     => 'USD',
        'created_by'   => 1,
        'user_agent'   => 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
        'status'       => 'active',
        '1'            => $_GET['Name'],
        '2'            => $_GET['Email'],
        '3'            => $_GET['Phone'],
        '4'            => $_GET['Location'],
        '10'            => $_GET['Comments'],
		'9'            => $_GET['date1'],
		'8'            => $_GET['url']
    )
);
 
//json encode array
$entry_json = json_encode( $entries );

//retrieve data
$response = wp_remote_request( $url , array( 'method' => 'POST', 'body' => $entry_json, 'timeout' => 25 ) );
/*if ( wp_remote_retrieve_response_code( $response ) != 200 || ( empty( wp_remote_retrieve_body( $response ) ) ) ){
    //http request failed
    die( 'There was an error attempting to access the API.' );
}*/
if ( (wp_remote_retrieve_response_code( $response ) != 200) || (  wp_remote_retrieve_body( $response ) =="" ) ){
    //http request failed
    /*echo "<br>wp_remote_retrieve_response_code :".wp_remote_retrieve_response_code( $response );
	echo "<br>wp_remote_retrieve_body :";
	print_r(wp_remote_retrieve_body( $response )); */
	die( 'There was an error attempting to access the API.' );
	
}

//result is in the response "body" and is json encoded.
$body = json_decode( wp_remote_retrieve_body( $response ), true );

$Name = $_GET['Name'];
$Email = $_GET['Email'];
$Phone = $_GET['Phone'];
$Location = $_GET['Location'];
$comments = $_GET['Comments'];
$source_url = $_GET['url'];

	
    GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );



//Gravity form mail Notifications

// Multiple recipients
$to = 'velmurugan@socialbeat.in';

// Subject
$subject = 'Drmohans AMP Enquiry';


// Message
$message = '<html>
<head>
  <title>Drmohans AMP Enquiry</title>
</head>
<body>
  <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
    <tr><td bgcolor="#EAF2FA"><strong>Name:</strong></td></tr>
    <tr bgcolor="#FFFFFF">'.$Name.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>Email :</strong></td></tr>
    <tr bgcolor="#FFFFFF">'.$Email.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>Location:</strong></td></tr>
    <tr bgcolor="#FFFFFF"><td>'.$Location.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>comments:</strong></td></tr>
    <tr bgcolor="#FFFFFF"><td>'.$comments.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>Locations:</strong></td></tr>
    <tr bgcolor="#FFFFFF"><td>'.$Locations.'</td></tr>
    <tr bgcolor="#EAF2FA"><td><strong>source_url:</strong></td></tr> 
    <tr bgcolor="#FFFFFF"><td>'.$source_url.'</td></tr> 
  </table>
</body>
</html>';

// To send HTML mail, the Content-type header must be set
$headers = "From: KLAY Schools in India <admin@drmohans.com>\r\n";
$headers .= "Reply-To: enquiry@klayschools.com\r\n";
$headers .= 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "X-Mailer: PHP/".phpversion();

// Mail it
mail($to, $subject, $message, $headers);

//End of gravity mail notifications



 
//print_r($body); exit;
if( $body['status'] > 202 ){
    $error = $body['response'];
 
        //entry update failed, get error information, error could just be a string
    if ( is_array( $error )){
        $error_code     = $error['code'];
        $error_message  = $error['message'];
        $error_data     = isset( $error['data'] ) ? $error['data'] : '';
        $status     = "Code: {$error_code}. Message: {$error_message}. Data: {$error_data}.";
    }
    else{
        $status = $error;
    }
    die( "Could not post entries. {$status}" );
}
else
{
	header('Location:https://drmohans.com/thankyou/');
}