<?php
	/**
	 * The template for displaying the header
	 *
	 * Displays all of the head element and everything up until the "site-content" div.
	 *
	 * @package WordPress
	 * @subpackage Twenty_Sixteen
	 * @since Twenty Sixteen 1.0
	 */

?><!DOCTYPE html>
	<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		
	
	<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->

	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
		<?php wp_head(); ?>

	</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<div class="site-inner">	
		
		<header id="masthead" class="site-header" role="banner">		
			<div class="container">	
<!-- Start of logo and header -->
				<div class="logo_contact">
					<div class="col-xs-12 col-sm-5 col-md-5">
						<a href="<?php echo home_url(); ?>"><img src="http://drmohans.com/home-care-chennai/wp-content/uploads/2017/06/Homecares.png" alt="Drmohans" width="255" height="60" /></a>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-7">	
					
						<div class="pull-right  text-right quality">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Diabates-Care.png" alt="Drmohans" width="100" height="83" />
						</div>
						<div class="pull-right text-right telephone" style="display : none; ">
							<span class="my-phone"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Call-us.png" alt="Drmohans" width="32" height="32" /></span>
							<div class="header_call_us">
								
								<div class="hyderabad_call"><p class="label_city">Hyderabad</p><p class="label_number"><a href="tel:+04043968888">040 - 43968888</a></p></div>
								<div class="hyderabad_call"><p class="label_city">Bangalore</p><p class="label_number"><a href="tel:+08043968888">080 - 43968888</a></p></div>
								<div class="chennai_call"><p class="label_city">Chennai</p><p class="label_number"><a href="tel:+04428353926">044 - 28353926</a></p></div>
								
							</div>
						</div>						

					</div>
				</div>
<!-- End of logo and header -->				
		
				
	<div class="our_services_block clearfix">
	<div class="container">
	<div class="our_services">
	<div class="col-xs-12 col-sm-7 col-md-9 service_left">
			<div class="service_paras">
				<div class="headings text-center">THE BEST DIABETES CARE NOW AT THE COMFORT OF YOUR OWN HOME</div>
				<div class="captions text-center">Now you need not take the pains of travelling for your diabetes care. Just call us and get all your investigations done from the comfort of your home.</div>
			</div>
<!-- Thankyou page content -->
			<div class="service_paras thankyou">
				<div class="captions text-center">Thanks for contacting us! We will get in touch with you shortly.</div>
			</div>
<!-- End : Thankyou page content hide for home page-->			

			<div class="service_paras">
				<div class="service_paras">
					<div class="headings text-left">Services Offered</div>
					<div class="modal-body">
						<ul>
							<li>Blood Collection</li>
							<li>Home Delivery Of Medicines</li>
							<li>Diabetes Foot Care</li>
							<li>Insulin Management</li>
							<li>Physiotherapy</li>
							<li>Fitness Training</li>
							<li>Diabetes Footwear & Health Products</li>
						</ul>
					</div>
				</div>	
			</div>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-3 service_right">
	<?php gravity_form( 1, $display_title = true, $ajax = true, $tabindex, $echo = true ); ?>
	</div>
	</div> <!-- our_services -->
	</div> <!-- container -->
	</div><!-- our_services_block -->
			
</div>	<!-- header container -->
</header><!-- header -->
			
<div id="content" class="site-content">
				