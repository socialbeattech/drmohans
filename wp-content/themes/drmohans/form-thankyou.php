<?php
/**
 *  Template Name: Form Thankyou
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?>
<?php include('spec-header.php'); ?>
	<div class="container-fluid padd0"><!-- Banner -->
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
			<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner <?php if(get_field('mobile_banner',get_the_ID())) { ?> d-none d-md-block <?php } ?>"/>
	<?php endif; ?>
	<?php if(get_field('mobile_banner',get_the_ID())) {?>
		<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
	<?php } ?>
	</div><!-- Banner Text-->
	<div class="wow zoomIn thankyou-banner-caption carousel-caption"> 
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48 colorfff">Thank You</h1>
		<h3 class="wow zoomIn text-left fs-30 colorfff Helvetica_Thin">for choosing Dr. Mohan's for your healthcare.<br>
We will have a team member connect with <br>
you shortly to confirm your appointment.</h3>
	</div>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list Helvetica_Light bg-light-grey">
		<div class="container">
			<p class="color1d2e34 text-center thanyou-title-p"> Things to do before coming for an early morning appointment at <br class="sm-disp-none"	>Dr. Mohan’s Diabetes Specialities Centre</p>
		</div>
		<div class="container">
			<div class="row p-tb-30">
				<div class="col-12 col-sm-12 col-md-4 d-sm-none">
					<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/thankyou-img1.jpg">
				</div>
				<div class="col-12 col-sm-12 col-md-8">
					<label class="thanyou-subtitletitle">Appointment Schedule</label>	
					<ul class="primary-ul">
						<li>Our out-patient unit functions from 6:30am onwards from Monday to Saturday. </li>
						<li>Reporting time at the Centre:  7:00am - 7:30am in the fasted state.</li>
					</ul>
				</div>
				<div class="col-12 col-sm-12 col-md-4 d-none d-md-block">
					<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/thankyou-img1.jpg">
				</div>
			</div>
			<div class="row p-tb-30">
				<div class="col-12 col-sm-12 col-md-4">
					<img class="img-fluid " src="<?php echo get_template_directory_uri();?>/images/fasting-diet.jpg">
				</div>
				<div class="col-12 col-sm-12 col-md-8 md-p-l-40px">
					<label  class="thanyou-subtitletitle">Fasting Requirements</label>	
					<ul class="primary-ul">
						<li>“Fasted state” means that the last meal should have been taken not later than 9:30pm on the previous night (8-10 hours fast). Thereafter, please refrain from consuming anything other than water until the fasting blood sample is taken. </li>
						<li>If you are on medications for diabetes, please take them as usual till the previous night.</li>
					</ul>
				</div>
			</div>
			<div class="row p-tb-30">
				<div class="col-12 col-sm-12 col-md-4 d-sm-none">
					<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/reg-checkup.jpg">
				</div>
				<div class="col-12 col-sm-12 col-md-8">
					<label class="thanyou-subtitletitle">Checkup Routine</label>	
					<ul class="primary-ul">
						<li>Kindly be prepared to spend about 5-6 hours to undergo all the investigations, meet the dietician, fitness trainer and then to consult the doctor with the reports. </li>
					</ul>
				</div>
				
				<div class="col-12 col-sm-12 col-md-4 d-none d-md-block">
					<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/reg-checkup.jpg">
				</div>
			</div>
			<div class="row p-tb-30">
				<div class="col-12 col-sm-12 col-md-4 md-p-l40px">
					<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/test-res.jpg">
				</div>
				<div class="col-12 col-sm-12 col-md-8 md-p-l-40px">
					<label class="thanyou-subtitletitle">Reports and Document Requirement</label>	
					<ul class="primary-ul">
						<li>If you have any old medical reports please bring them without fail.</li>
						<li>Similarly, previous hospital files, surgery and medical information should also be carried especially if it is your first visit or annual visit or follow up visit after a long duration of time.</li>
						<li>On the day of the test, please bring all your usual medicines with you and take them as you usually do.</li>
					</ul>
				</div>
			</div>
			<div class="row p-tb-30">
				<div class="col-12 col-sm-12 col-md-4 d-sm-none">
					<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/consult-dr.jpg">
				</div>
				<div class="col-12 col-sm-12 col-md-8">
					<label class="thanyou-subtitletitle">Bringing an Attendant </label>	
					<ul class="primary-ul">
						<li>Only one attendant is permitted to accompany the patient. It is not advisable to allow children below 10 years of age to accompany patients.</li>
					</ul>
				</div>
				<div class="col-12 col-sm-12 col-md-4 d-none d-md-block">
					<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/consult-dr.jpg">
				</div>
			</div>
		</div>
	</section>
	<?php $args_default = array( 'category_name' => 'Generic', 'post_type' => 'post', 'posts_per_page' => '3');
			   $spec_blogs_default = new WP_Query( $args_default ); 
			   if ( $spec_blogs_default->have_posts() ) {  ?>
	<div class="fullwidth knows_diabetes">
		<div class="container" style=" margin-bottom: 5.5em;">
			<h1 class="text-center understand-ur-diabetes">Understand Your Diabetes</h1>
			<div class="row my-5">
				<?php while($spec_blogs_default->have_posts()): $spec_blogs_default->the_post(); ?>
					<div class="col-12 col-sm-4">
						<div class="col-12 col-sm-12 pl-4 pt-4 Helvetica_Light focolor understands wow slideInDown">
							<h2 class="text-left fs-22 Helvetica_Bold focolor"><?php the_title(); ?></h2>
							<?php the_excerpt(); ?>
							<p><span><a href="<?php the_permalink(); ?>" class="fs-18 Helvetica_Light focolor hover-border">Read More</a></span></p>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
			<p class="text-center">
			<a href="<?php echo get_home_url();?>/category/blogs" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold" style="padding: 12px 23px;background: rgb(237, 22, 22);">READ ALL BLOGS</a></p>
		</div>
	</div>
	<?php } 
	wp_reset_postdata(); ?>
<?php include('spec-footer.php'); ?>
