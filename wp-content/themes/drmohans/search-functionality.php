<?php
/**
 *  Template Name: test search Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
 <div class="container" style="margin-top:60px">
<form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
  <div>
    <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="Search" />
  </div>
</form>
</div>
<?php 
$args = array(
    's'              => 'h'
);
$wp_query = new WP_Query( $args );
if ( $wp_query->have_posts() ) :
	$string = '';
	while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
		//$exclude .= get_the_ID($wp_query->ID).',';
		$string .=  '<a href="'.get_permalink($wp_query->ID) .'" title="'.get_the_ID($wp_query->ID).'">'.get_the_title().'</a><br>';

	endwhile; 

endif; 

echo $string;
?>
<?php include('spec-footer.php'); ?>