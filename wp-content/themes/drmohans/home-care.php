<?php
/**
 *  Template Name: Home Care Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<div class="container-fluid padd0"><!-- Banner --><?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
			<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner <?php if(get_field('mobile_banner',get_the_ID())) { ?> d-none d-md-block <?php } ?>"/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) {?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	</div><!-- Banner Text-->
	<div class="wow zoomIn homecare-banner-caption carousel-caption">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Home Diabetes Care</h1>
		<h3 class="wow zoomIn text-left fs-30">All your tests and consultations from <br/> the comfort of your home!</h3>
	</div>
	<section class="breadcrumb">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="padd-top-bottom-70 Helvetica_Light" style="padding-bottom:0 !important;">
		<div class="container text-center">
			<p class="color-red fs-42 margin-btm30 speciality-intro">Avail diabetes care from the comfort of your home</p>
			<p class="fs-18 color-41 lheight35">At Dr Mohan’s, we keep pushing ourselves to extend the best care possible in the field of diabetes. By introducing home care services, we now offer home blood collection, medicine delivery, nursing care, physiotherapy and more that will help us monitor your health holistically from the comfort of your home. Leave the travelling to us!<p>
			
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list Helvetica_Light">
		<div class="container">
			<label class="lbl-title services-section-title HelveticaNeueLTStd-Md m-b0 text-center ">
				Services offered 
			</label>
			<p class="sevices-subtitle text-center">at your doorstep</p>
			<ul class="homecare-services-offered-ul text-center">
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/icon-1.png"/>
					<label class="service-title">Physiotherapy & <br>Fitness Training </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/icon-2.png"/>
					<label class="service-title">Diet<br>Consultation  </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/icon-3.png"/>
					<label class="service-title">Blood Sample <br>Collection </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/icon-4.png"/>
					<!--label class="service-title">Medicine<br>Delivery  </label-->
					<label class="service-title">Home Delivery <br>of Medicines </label>
				</li>
			</ul>
			<ul class="homecare-services-offered-ul text-center">
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/icon-5.png"/>
					<label class="service-title">Doctor<br>Checkup</label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/icon-6.png"/>
					<label class="service-title">Nursing <br>Care</label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/icon-7.png"/>
					<label class="service-title">Injection Routine <br>and Surgical Dressing </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/icon-8.png"/>
					<label class="service-title">Patient<br> Monitoring </label>
				</li>
			</ul>
			<p class="fs-14 text-center">To avail Home Care Services, call us at 044 4396 8885,7358555344 or Email us at homecare@drmohans.com and our team will take care of the rest.</p>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-light-blue padd-top-bottom-70">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 lbl-title">Advantages of Home Care Service</label>
			<div class="row">
				<div class="col-12">
					<ul class="primary-ul">
						<li>Blood samples can be collected from your residence.
						</li><li>You can avoid the hassle of early morning travel. We will come to your home for both – Fasting and postprandial blood sample collection.
						</li><li>You can come to see our doctor as per the appointment schedule when the reports are ready- no need to wait at the clinic!
						</li><li>You will be able to save your valuable time.
						</li><li>No additional charges for Home blood sample collection for lab value more than Rs.1000/-
						</li><li>  Rs. 100/- will be charged per visit, n case of lab bill value below Rs. 1000/-
						</li><li>We also provide free delivery of medicines and Dr Mohan’s Rice Products at home ( Minimum Bill value Rs.1000/- with in 5 Km distance)
						</li><li>Convenience of having nursing care and physiotherapy at home</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-dark-grey padd-top-bottom-70 section-speciality-form">
		<div class="container">
			<label class="text-center lbl-title Helvetica_Thin fs-46 colorfff">4,50,000+ Patients Trust Us</label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 speciality-video">
				<iframe width="793" height="512" src="https://www.youtube.com/embed/AVLTUptkHK0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="book_appintment darkbg">
						<div class="gravity_holder">
							<label class="col-12 col-sm-12 lbl-title text-left pb-2 my-auto fs-22 whcolor no-gutters">Book an Appointment</label>
							<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
							<?php gravity_form( 1, $display_title = false, $display_description = false, $tabindex, $ajax = true, $echo = true ); ?>
						</div>							
					</div>							
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>