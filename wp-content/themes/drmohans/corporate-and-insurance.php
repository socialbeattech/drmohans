<?php
/**
 *  Template Name: Corporate and Insurance Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<div class="container-fluid padd0"><!-- Banner -->
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
			<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner <?php if(get_field('mobile_banner',get_the_ID())) { ?> d-none d-md-block <?php } ?>"/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) {?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	</div><!-- Banner Text-->
	<div class="wow zoomIn corporate-banner-caption carousel-caption">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Corporate and <br>Insurance Services</h1>
		<h3 class="wow zoomIn text-left fs-30">Exclusive benefits designed<br/>for Corporate Employees</h3>
		<p class="m-b0 ins-banner-a"><a href="#corporate_services" class="color-red Helvetica_Roman" style="margin-right:12px">CORPORATE SERVICES</a><span class="color-red Helvetica_Roman" style="font-weight:bold"> | </span><a href="#insurance_services" class="Helvetica_Roman color-red" style="margin-left: 12px;">INSURANCE SERVICES</a></p>
	</div>
	<section class="">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section id="corporate_services" class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 marginauto">
					<label class="text-left Helvetica_Thin fs-46 lbl-title">Our Services</label>
					<p>With the aim of creating awareness about diabetes and to control complications, we provide the best quality medical services to our corporate clients.<br>
					A formal recognition of DMDSC as the preferred healthcare destination, provides corporate employees and their dependents access to quality healthcare with extra personal care, minimal formalities during Out Patient Service as well as admission (IP), and lesser financial burden.<br>
					We conduct special life style modification programs for corporates and their employees to prevent chronic diseases and to improve their productivity at the workplace and at home. Our program is designed for all age groups and is suitable for all segments of society.</p>

				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="book_appintment cp-ins-form">
						<div class="gravity_holder">
							<label class="col-12 col-sm-12 lbl-title text-left pb-2 m-b0 text-center fs-22 whcolor no-gutters Helvetica_Bold">Connect with us for<br> Information</label>
							<?php gravity_form( 6, $display_title = false, $display_description = false, $tabindex, $ajax = false, $echo = true ); ?>
						</div>							
					</div>				
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-light-blue padd-top-bottom-70">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 lbl-title">Key Benefits</label>
			<div class="row Helvetica_Bold">
			<?php if( have_rows('key_benefits') ){ 
				$count = 1; ?>
				<?php while ( have_rows('key_benefits') ) : the_row();  ?>
				<div class="col-12 col-sm-4 text-center">
					<div class="col-12 cp-services-keys">
						<img src="<?php the_sub_field('key_benefit_icon'); ?>" alt="Keys"/>
					</div>
					<div class="col-12">
					<label><?php the_sub_field('key_text'); ?></label>
					</div>
				</div>
				<?php if($count == 3){ ?>
					</div>
					<div class="row cp_row" >
				<?php } ?>
				<?php $count++; ?>
				<?php endwhile; ?>
			<?php } ?>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 bg-dark-grey ">
		<div class="container">
			<label class="lbl-title text-center Helvetica_Thin fs-46 colorfff">Esteemed Corporate Clients</label>
			<div class="modal" id="esteemed_corp_clients"> 
				<div class="modal-dialog">
					<div class="modal-content esteemed_corp_clients">
						<span class="text-right close colorfff" data-dismiss="modal">X</span>
						<label class="lbl-title text-center Helvetica_Thin fs-46 colorfff">Esteemed Corporate Clients</label>
						<div class="row">
						<div class="col-12 col-sm-4">
							<ul class="corporates primary-ul"><li>Air Force Association</li><li>National Insurance</li><li>Airport Authority of India</li><li>Office of the Principal Accountant General</li><li>BHEL</li><li>ONGC</li><li>BSNL</li><li>Oriental Insurance</li><li>Central Bank of India</li><li>Power Grid Corporation</li><li>CPCL</li><li>Reserve Bank of India</li><li>Chennai Port Trust</li><li>SHAR</li><li>Coal India</li><li>State Bank of India</li><li>Department of Atomic Energy</li><li>State Bank of Travancore</li><li>HUDCO</li><li>Steel Authority of India</li><li>Indian Airlines</li><li>TNPL</li><li>Indian Oil Corporation</li><li>United India Insurance</li><li>Madras High Court</li></ul>
						</div>
						<div class="col-12 col-sm-4">
							<ul class="corporates primary-ul"><li>Areva T&amp;D</li><li>RANE</li><li>Ashok Leyland</li><li>Reliance Industries</li><li>Amrutanjan Health Care</li><li>RR Donnelley</li><li>Barry Wehmiller International Resources</li><li>Salem Textiles</li><li>Brakes India</li><li>Sundaram Brake Linings</li><li>Carborundum Universal</li><li>Sundaram Clayton</li><li>Caterpillar</li><li>Sundaram Fasteners</li><li>Client Network Services</li><li>SPIC</li><li>Dalmia Cements</li><li>Tata Communications (VSNL)</li><li>Hexaware Technologies</li><li>Tata Motors</li><li>HCL</li><li>Tata Steel</li><li>Hi – Tech Carbon</li><li>Technip</li><li>Infosys</li><li>Wipro</li><li>Mahindra Satyam</li><li>SCOP International</li><li>TI Cycles</li><li>India Cements</li><li>Titan Industries</li><li>Indus Ind Bank</li><li>Times of India</li><li>ISOFT</li><li>TVS Electronics</li><li>ITC</li><li>TVS Hospitals</li><li>KCP</li><li>TVS Sundaram</li><li>L&amp;T</li><li>TVS Logistics</li><li>Logica</li><li>Tvs Infotech</li><li>Lucas TVS</li><li>VIT University</li><li>Madras Fertilizers</li><li>Wabco India</li><li>Madras Rubber Factory</li><li>William Hare</li><li>Malladi Drugs &amp; Pharmaceuticals</li><li>Novo Nordisk</li><li>Maersk</li><li>Orchid Chemicals &amp; Pharmaceuticals</li><li>Needle Industries</li><li>Polaris Group</li><li>Precision Group of Companies</li><li>Indian Bank</li><li>Central Bank Of India</li><li>Axis Bank</li><li>Andhra Bank</li><li>Punjab National Bank</li></ul>
						</div>
						<div class="col-12 col-sm-4">
							<ul class="corporates primary-ul"><li>Anna Nagar Towers Club</li><li>Tamilnadu Elementary School Teachers Association</li><li>Asiad Colony Flat Owners Welfare Association</li><li>SKGM Residents Welfare Association</li><li>Kailash Colony Welfare Association</li><li>Blue Circle Club Association</li><li>DIGNITY Foundation</li><li>Retired Food Officers Welfare Society</li><li>Tamil Nadu Senior Citizens Association</li><li>Vintage Badminton Club</li><li>World Jain Mission</li><li>Andhra Bank Retd Employees Association</li><li>Annai Flats Association</li><li>Hope Walkers Association</li><li>Ganapathy Industries and Traders Welfare Association</li><li>Center for Rural Health and Social Education</li><li>T.N Retired Offical Association &amp; its affiliated association</li><li>Rotary Club of Secunderabad</li><li>Tamil Nadu Agricultural University Pensioners Association</li><li>Dr. Reddy’s Foundation</li><li>Thiruvengada Nagar Residents Welfare Association</li><li>All India Vaish Federation</li><li>The Andhra Pradesh Hotals Association</li><li>St. John Ambulance Association</li><li>Tamilnadu Agricultural Engineers Association</li><li>State Government Pensioners Family Welfare Association</li><li>A.P State Govt. Retired Employees Association</li><li>A.P High Court Advocates Association</li></ul>
						</div>
						</div>
					</div>							
				</div>							
			</div>	
			<div class="modal" id="esteemed_ins_clients"> 
				<div class="modal-dialog">
					<div class="modal-content esteemed_corp_clients">
						<span class="text-right close colorfff" data-dismiss="modal">X</span>
						<label class="lbl-title text-center Helvetica_Thin fs-46 colorfff">Insurance Services</label>
						<div class="row">
						<div class="col-12 col-sm-6">
							<ul class="corporates primary-ul"><li>Genins India TPA Ltd</li><li>United HealthCare India (P) Ltd.</li><li>Medicare TPA Services (I) (P) Ltd</li><li>Alankit Health Care Ltd.,</li><li>Heritage Health TPA P Ltd</li><li>E – Meditek TPA Services Ltd</li><li>Spurthi Meditech TPA PVT. Ltd</li><li>Good Health Plan</li></ul>
						</div>
						<div class="col-12 col-sm-4">
							<ul class="corporates primary-ul"><li>Bajaj Allianz General Insurance Co Ltd</li><li>Chola Mandalam – Ms General Insurance</li><li>Future Generali Insurance</li><li>ICICI Lombard General Insurance Co Ltd</li><li>IFFCO-TOKIO General Insurance Co Ltd</li><li>Max Bupa General Insurance Co Ltd</li><li>Star Health Insurance</li><li>HDFC ERGO</li><li>Reliance General Insurance</li></ul>
						</div>
						</div>
					</div>							
				</div>							
			</div>	
			<div class="row">
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/prev-slide.png" alt="Nav" class="client-prev img-responsive"/>
				</div>
				<?php 
				$clients_images = get_field('corporate_clients'); ?>
				<div class="col-8 col-sm-10 corporate-slider">
					<div class="swiper-container" id="corporate-client-slider">
						<div class="swiper-wrapper">
						<?php foreach( $clients_images as $image ){ ?>
							<div class="swiper-slide m-tb-auto">
								<img src="<?php echo $image['url']; ?>" alt="clients"/>
							</div>
						<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/next-slide-white.png" alt="Nav" class="client-next img-responsive"/>
				</div>
			</div>
				<p class="text-center mt-30 m-b0"><button class="btn-a" data-toggle="modal" data-target="#esteemed_corp_clients">List of Empanelled Companies</button></p>
		</div>
	</section>
	<section id="insurance_services" class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container">
			<label class="lbl-title text-center Helvetica_Thin fs-46 ">Insurance Services</label>
			<div class="row">
				<div class="col-12 text-center">
					<p>With the commitment to provide world-class healthcare and to enable our patients to get use of special privileges like Cashless Admission, DMDSC has empanelled the following TPA & Health Insurance Companies For cashless admission, the patients need to produce their health insurance policy card / policy details at the time of hospitalization.  <br>
					Note: Cashless approval is fully dependent on the patient’s condition and based on the policy terms and conditions.
					</p>
				</div>
			</div>
				<p class="text-center m-b0"> <button class="btn-a" data-toggle="modal" data-target="#esteemed_ins_clients">List of Empanelled Companies</button></p>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 lbl-title">Our Partners</label>
			<div class="row">
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/black-slide-prev.png" alt="clients" class="partner-prev cur-pointer img-responsive"/>
				</div>
				<?php 
				$partners_images = get_field('partners_slide'); ?>
				<div class="col-8 col-sm-10 speciality-slider">
					<div class="swiper-container" id="corporate-partner-slider">
						<div class="swiper-wrapper">
						<?php foreach( $partners_images as $image ){ ?>
							<div class="swiper-slide m-tb-auto">
								<img src="<?php echo $image['url']; ?>" alt="speciality"/>
							</div>
						<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/black-next-slide.png" alt="Next" class="partner-next cur-pointer img-responsive"/>
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-grey-cloud padd-top-bottom-70">
		<div class="container">
			<div class="row mt-30">
				<div class="col-12 col-sm-6 br-red">
				<span class="text-uppercase Helvetica_Bold fs-22">FOR CORPORATE SERVICES</span>
					<p class="fs-18 m-b0">
					
For Government, Public Sector and Private Corporate <br> Contact : Rajkumar, Manager, Corporate Relations<br> Mob: 733 873 8191

For Insurance, TPAs & Insurance linked Corporate <br> Contact : Yuvanesan, Senior Manager, Corporate Relations.<br> Mob: 994 000 2967
					<!--Manager – Corporate Relations<br>
					Dr.Mohan’s Diabetes Specialities Centre<br>
					No.6b, Conran Smith Road,<br>
					Gopalapuram, Chennai - 600086<br>
					Ph: 9962428888<br>
					044-43968765/8489/8888 <br-->
					Mail us at cbd@drmohans.com , insurance@drmohans.com
					</p>
				</div>
				<div class="col-12 col-sm-4 margin-lr-auto mt-xs-40">
				<span class="text-uppercase Helvetica_Bold fs-22">FOR INSURANCE SERVICES</span>
				<p class="fs-18">
				044- 43968765 / 43968888<br>
				Email: insurance@drmohans.com
				 </p>
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>