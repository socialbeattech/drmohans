<?php
/**
 *  Template Name: Specialities
 *
 * 
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
 ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if (is_page('specialities')) { ?>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/style-speciality.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/custom-styles.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/swiper.min.css">
<?php } ?>
<?php wp_head(); ?>
<?php if (is_page('specialities')) { ?>
<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/swiper.min.js'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/custom.js'></script>
<?php } ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead">
		<div class="header">
			<div class="container">
				<div class="col-12 col-sm-6 col-md-12 col-lg-3 no-gutters header_logo">
					<a href="https://drmohans.com"><img src="<?php echo get_template_directory_uri();?>/images/Drmohans-logo.png" alt="Drmohans" width="194" height="43" /></a>
				</div>
				<div class="col-12 col-sm-6 col-md-12 col-lg-9 float-right no-gutters header_menus">
					<?php if ( has_nav_menu( 'top' ) ) : ?>
						<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	
	<div id="form-slideout" class="form-slideout">
	<a href=""  class="fixed-btn text-uppercase col-xs-10 text-center padd5"> <img class="sticky-a active" src="<?php echo get_template_directory_uri();?>/images/book-app.png"></a><br style="margin-top:5px"> 
	<a href=""class="fixed-btn text-uppercase col-xs-10 text-center padd5"><img class="sticky-a" src="<?php echo get_template_directory_uri();?>/images/location-sticky.png"></a> <br>
	<a href=""class="fixed-btn text-uppercase col-xs-10 text-center padd5"><img class="sticky-a" src="<?php echo get_template_directory_uri();?>/images/enquiry-sticky.png"></a><br>
	<a href=""class="fixed-btn text-uppercase col-xs-10 text-center padd5"><img class="sticky-a" src="<?php echo get_template_directory_uri();?>/images/sticky-search.png"></a></div>

	<div id="form-slideout-mobile" class="form-slideout-mobile">
	<a href=""  class="fixed-btn text-uppercase col-3 text-center padd5" style="padding:0;"> <img class="sticky-a active img-fluid" src="<?php echo get_template_directory_uri();?>/images/book-app.png"></a>
	<a href=""class="fixed-btn text-uppercase col-3 text-center padd5 " style="padding:0;"><img class="sticky-a img-fluid" src="<?php echo get_template_directory_uri();?>/images/location-sticky.png"></a> 
	<a href=""class="fixed-btn text-uppercase col-3 text-center padd5" style="padding:0;"><img class="sticky-a img-fluid" src="<?php echo get_template_directory_uri();?>/images/enquiry-sticky.png"></a>
	<a href=""class="fixed-btn text-uppercase col-3 text-center padd5" style="padding:0;"><img class="sticky-a img-fluid" src="<?php echo get_template_directory_uri();?>/images/sticky-search.png"></a></div>
	<div class="container-fluid padd0">
	<img src="<?php echo get_template_directory_uri();?>/images/banner-specaility.jpg" alt="Banner" class="img-responsive banner"/>
	</div>
	<div class="spec-banner-caption carousel-caption">
		<h1 class="animated bounceInLeft text-left Helvetica_Roman fs-48" >Diabetes Footcare</h1>
		<h3 class="animated bounceInLeft text-left fs-30">Dr. Mohan’s Diabetes <br> Specialities Centre</h3>
	</div>
	<?php if(is_home() || is_front_page()): ?>
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<div class="swiper-slide"><img src="<?php echo get_template_directory_uri();?>/images/Banner-1.png" alt="Banner" class="img-fluid" width="1366" height="650" />
				<div class="carousel-caption">
					<h1 class="animated bounceInLeft text-left Helvetica_Roman fs-24">Everything you need to</h1>
					<h2 class="animated bounceInRight text-left Helvetica_Medium fs-60">Fight Diabetes</h2>
					<h3 class="animated bounceInLeft text-right Helvetica_Roman fs-24">is right here, at</h3>
					<h4 class="animated bounceInRight text-right Helvetica_Medium fs-60">Dr Mohans</h4>
					<h5 class="animated bounceIn"><a href="" class="fs-26 whcolor text-lowercase">Explore</a></h5>
				</div>
			</div>
			<div class="swiper-slide"><img src="<?php echo get_template_directory_uri();?>/images/Banner-2.png" alt="Banner" class="img-fluid" width="1366" height="650" /></div>
			<div class="swiper-slide"><img src="<?php echo get_template_directory_uri();?>/images/Banner-3.png" alt="Banner" class="img-fluid" width="1366" height="650" /></div>
			<div class="swiper-slide"><img src="<?php echo get_template_directory_uri();?>/images/Banner-4.png" alt="Banner" class="img-fluid" width="1366" height="650" /></div>
		</div>
			<!-- Add Arrows -->
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
	</div>
	<?php endif; ?>
	<div class="site-content-contain">
		<div id="content" class="fullwidth">

	<section class="padd-top-bottom-70 Helvetica_Light">
		<div class="container text-center">
			<p class="color-red fs-42 margin-btm30">Understanding Diabetes Foot Care</p>
			<p class="fs-18 color-41 lheight35">Foot is one of the major organs that can get affected by Diabetes. The Foot clinic at Dr. Mohan’s Diabetes Specialities Centre is fully equipped with facilities for early detection and diagnosis of diabetic foot complications.</p>
			<p class="paddtop20">
			<a href=""> <button  class="btn-primary-speciality">Know More </button></a>
			<a href="" > <button  class="btn-primary-speciality xs-margin-top10">Watch Video </button></a>
			</p>
		</div>
	</section>
	<section class="bg-light-blue padd-top-bottom-70 Helvetica_Light">
		<div class="container text-center">
		<label class="fs-46 color000">Dr Mohan's  - Specializing in your Diabetes</label><br/>
		  <ul class="nav nav-tabs bordernone xs-txt-left justify-content-center">
			<li class="active list-specialities color-red"><a data-toggle="tab" href="#diabetes_specialities"  class="color-red">Diagnosis and Tests</a></li>
			<li class="list-specialities color-red border-rl"><a data-toggle="tab" href="#treatment_infrastructure" class="color-red">Treatment and Infrastructure</a></li>
			<li class="list-specialities color-red"><a data-toggle="tab" href="#FAQ" class="color-red">FAQ's</a></li>
		  </ul>
		  <div class="tab-content">
			<div id="diabetes_specialities" class="tab-pane in active col-sm-12 text-left treatments">
				<span class="title fs-24">Foot Examination</span>
				<p class="content fs-18">A routine foot examination to check for early symptoms of Neuropathy (Condition where the nerves get affected), Ischemia (reduced blood supply), deformities in foot, callus formations resulting from neuropathy and infection and necrosis is done at the foot clinic of Dr. Mohan’s Diabetes Specialities Centre.</p>
				<span class="title fs-24">Biothesiometry</span>
				<p class="content">Biothesiometer helps to detect and quantify early sensory loss in diabetic patients.</p>
				<span class="title fs-24">Doppler</span>
				<p class="content">Doppler helps in detection of decreased blood circulation (Ischaemia).</p>
				<span class="title fs-24">Foot Pressure Distribution Measurement</span>
				<p class="content">Foot Pressure Distribution measurement system is used for measuring the pressures in regions that are prone to get calluses and corns in the foot. This instrument also used for designing special diabetic footwear that effectively redistributes the pressures thereby preventing the formation of calluses and corns in diabetic foot.</p>
			</div>
			<div id="treatment_infrastructure" class="tab-pane fade col-sm-12 text-left treatments">
				<span class="title fs-24">Treatment and Infrastructure</span>
				<p class="content fs-18">A routine foot examination to check for early symptoms of Neuropathy (Condition where the nerves get affected), Ischemia (reduced blood supply), deformities in foot, callus formations resulting from neuropathy and infection and necrosis is done at the foot clinic of Dr. Mohan’s Diabetes Specialities Centre.</p>
				<span class="title fs-24">Biothesiometry</span>
				<p class="content">Biothesiometer helps to detect and quantify early sensory loss in diabetic patients.</p>
				<span class="title fs-24">Doppler</span>
				<p class="content">Doppler helps in detection of decreased blood circulation (Ischaemia).</p>
				<span class="title fs-24">Foot Pressure Distribution Measurement</span>
				<p class="content">Foot Pressure Distribution measurement system is used for measuring the pressures in regions that are prone to get calluses and corns in the foot. This instrument also used for designing special diabetic footwear that effectively redistributes the pressures thereby preventing the formation of calluses and corns in diabetic foot.</p>
			</div>
			<div id="FAQ" class="tab-pane fade col-sm-12 text-left treatments">
				<span class="title fs-24">FAQ</span>
				<p class="content fs-18">A routine foot examination to check for early symptoms of Neuropathy (Condition where the nerves get affected), Ischemia (reduced blood supply), deformities in foot, callus formations resulting from neuropathy and infection and necrosis is done at the foot clinic of Dr. Mohan’s Diabetes Specialities Centre.</p>
				<span class="title fs-24">Biothesiometry</span>
				<p class="content">Biothesiometer helps to detect and quantify early sensory loss in diabetic patients.</p>
				<span class="title fs-24">Doppler</span>
				<p class="content">Doppler helps in detection of decreased blood circulation (Ischaemia).</p>
				<span class="title fs-24">Foot Pressure Distribution Measurement</span>
				<p class="content">Foot Pressure Distribution measurement system is used for measuring the pressures in regions that are prone to get calluses and corns in the foot. This instrument also used for designing special diabetic footwear that effectively redistributes the pressures thereby preventing the formation of calluses and corns in diabetic foot.</p>
			</div>
		</div>
		</div>
		<div class="container">
		<div class="row">
			<div class="col-2 col-sm-1 marginauto">
				<img src="<?php echo get_template_directory_uri();?>/images/prev.png" class="spec-prev img-responsive"/>
			</div>
			<div class="col-8 col-sm-10 speciality-slider">
				<div class="swiper-container" id="speciality-slider">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<img src="<?php echo get_template_directory_uri();?>/images/speciality-slide1.jpg"/>
						</div>
						<div class="swiper-slide">
							<img src="<?php echo get_template_directory_uri();?>/images/speciality-slide2.jpg"/>
						</div>
						<div class="swiper-slide">
							<img src="<?php echo get_template_directory_uri();?>/images/speciality-slider3.jpg"/>
						</div>
						<div class="swiper-slide">
							<img src="<?php echo get_template_directory_uri();?>/images/speciality-slide1.jpg"/>
						</div>
						<div class="swiper-slide">
							<img src="<?php echo get_template_directory_uri();?>/images/speciality-slide2.jpg"/>
						</div>
						<div class="swiper-slide">
							<img src="<?php echo get_template_directory_uri();?>/images/speciality-slider3.jpg"/>
						</div>
					</div>
				</div>
			</div>
			<div class="col-2 col-sm-1 marginauto">
				<img src="<?php echo get_template_directory_uri();?>/images/next.png" class="spec-next img-responsive"/>
			</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-dark-grey padd-top-bottom-70">
		<div class="container">
			<h1 class="text-center Helvetica_Thin fs-46 colorfff">Understand Your Diabetes</h1>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8">
				<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/video.png"/>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="book_appintment">
						<div class="gravity_holder">
							<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters">Book an Appointment</h1>
							<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
							<?php gravity_form( 1, $display_title = false, $display_description = false, $ajax = true, $tabindex, $echo = true ); ?>
						</div>							
					</div>							
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth shops padd-top-bottom-70">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-4 float-left">								
					<div class="col-12 col-sm-12 locate-center">	
						<div class="col-12 col-sm-12 borders_stuff wow flipInX">	
							<h2 class="col-12 col-sm-12 text-center fs-26 whcolor pt-4 Helvetica_Bold">Locate Center</h2>
							<select class="select_box">
								<option>Andhra Pradesh</option>
								<option>Tamil Nadu</option>
								<option>karnataka</option>
							</select>
							<p class="col-12 col-sm-12 text-center mt-3"><a href="" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold">View centers</a></p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-4 float-left">								
					<div class="col-12 col-sm-12 defeat-diabetes">	
						<div class="col-12 col-sm-12 borders_stuff wow flipInX">	
							<h2 class="col-12 col-sm-12 text-center fs-26 whcolor pt-4 Helvetica_Bold">Defeat Diabetes</h2>
							<select class="select_box">
								<option>Diabetes 01</option>
								<option>Diabetes 02</option>
								<option>Diabetes 03</option>
							</select>
							<p class="col-12 col-sm-12 text-center mt-3"><a href="" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold">Read Blogs</a></p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-4 float-left">								
					<div class="col-12 col-sm-12 diabetes-products">	
						<div class="col-12 col-sm-12 borders_stuff wow flipInX">	
							<h2 class="col-12 col-sm-12 text-center fs-26 whcolor pt-4 Helvetica_Bold">Diabetes Products</h2>
							<p class="colorfff bg-none margin-bm-1em">All the essentials from food products to books on health care, to help manage your Diabetes</p>
							<p class="col-12 col-sm-12 text-center mt-3"><a href="" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold">Know More</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth knows_diabetes">
			<div class="container" style=" margin-bottom: 5.5em;">
				<h1 class="text-center understand-ur-diabetes">Understand Your Diabetes</h1>
				<div class="row my-5">
					<div class="col-12 col-sm-4">
						<div class="col-12 col-sm-12 pl-4 pt-4 understands wow bounceInDown">
							<h2 class="text-left fs-22 Helvetica_Bold focolor">MANAGING DIABETES</h2>
							<p class="text-left fs-18 Helvetica_Light focolor margin-0">Blood Sugar Monitoring –When to check and why?</p>
							<p><span><a href="http://beat.social/demo/drMohansWebsite/blogInner" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
					<div class="col-12 col-sm-4">		
						<div class="col-12 col-sm-12 pl-4 pt-4 understands wow bounceInDown">		
							<h2 class="text-left fs-22 Helvetica_Bold focolor">FOOD & DIET</h2>
							<p class="text-left fs-18 Helvetica_Light focolor margin-0">Fruits that Diabetics can eat</p>
							<p><span><a href="http://beat.social/demo/drMohansWebsite/blogInner" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="col-12 col-sm-12 pl-4 pt-4 understands wow bounceInDown">
							<h2 class="text-left fs-22 Helvetica_Bold focolor">STAYING ACTIVE</h2>
							<p class="text-left fs-18 Helvetica_Light focolor margin-0">Exercise tips for people with Type-2 diabetes</p>
							<p><span><a href="http://beat.social/demo/drMohansWebsite/blogInner" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
				</div>
				<p class="text-center">
				<a href="http://beat.social/demo/drMohansWebsite/blogMain.html" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold" style="    padding: 12px 23px;background: rgb(237, 22, 22);">READ ALL BLOGS</a></p>
				<!--div class="row">
					<div class="col-12 col-sm-4">
						<div class="col-12 col-sm-12 pl-4 pt-4 understands wow bounceInUp">
							<h2 class="text-left fs-22 Helvetica_Bold focolor">FOOD & DIET</h2>
							<p class="text-left fs-18 Helvetica_Light focolor margin-0">Top Power Foods for Diabetes</p>
							<p><span><a href="" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
					<div class="col-12 col-sm-4">		
						<div class="col-12 col-sm-12 pl-4 pt-4 understands wow bounceInUp">		
							<h2 class="text-left fs-22 Helvetica_Bold focolor">MANAGING DIABETES</h2>
							<p class="text-left fs-18 Helvetica_Light focolor margin-0">10 things Diabetics can do to better self-manage</p>
							<p><span><a href="" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="col-12 col-sm-12 pl-4 pt-4 understands wow bounceInUp">
							<h2 class="text-left fs-22 Helvetica_Bold focolor">DIABETIC PRODUCTS</h2>
							<p class="text-left fs-18 Helvetica_Light focolor margin-0">Did you know how healthy Brown rice is for you</p>
							<p><span><a href="" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
				</div-->
				
			</div>
		</section>
	</div><!-- #content -->

	<footer class="fullwidth">
		<div class="fullwidth footer">
			<div class="container">
				<div class="row pt-4 pb-3">
					<div class="col-12 col-sm-4 no-guttersright">
						<h1 class="col-12 col-sm-12 text-uppercase no-gutters">Main centre address</h1>
						<p>No.6-B, Conron Smith Road,</p>
						<p>Near Satyam Cinemas, Gopalapuram,</p>
						<p>Chennai, Tamil Nadu 600086</p>
						<p>Ph: 044 43968888</p>
						<p>Email : <a href="mailto:appointments-gop@drmohans.com">appointments-gop@drmohans.com</a></p>
					</div>
					<div class="col-12 col-sm-3">
						<h1 class="col-12 col-sm-12 text-uppercase no-gutters">quick links</h1>	
						<ul class="col-12 col-sm-12 no-gutters list-none">				
							<li><a href="">Dr Mohans Integrated Care</a></li>
							<li><a href="">Consultation Procedures</a></li>
							<li><a href="">120 minute Express Checkout</a></li>
							<li><a href="">Comprehensive health Checkup</a></li>
							<li><a href="">Evening Clinic</a></li>
							<li><a href="">Emergency Contact</a></li>
							<li><a href="">Advise and Counselling</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-3">
						<h1 class="col-12 col-sm-12 text-uppercase no-gutters">Locate Clinics</h1>
							<ul class="col-12 col-sm-12 no-gutters list-none">
								<li><a href="">Main Centre Gopalpuram</a>