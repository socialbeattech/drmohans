<?php
/**
 *  Template Name: Partners Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?>
<?php include('spec-header.php'); ?>
<style>
.img-1{
	width:344px;
}
.img-2{
	width:200px;
}
@media(max-width:479px){
	.img-1{
		width:100%;
	}
}
</style>
	<div class="container-fluid padd0"><!-- Banner -->
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class=" banner banner d-none d-md-block"/>
	<?php endif; ?>
	<?php if(get_field('mobile_banner')) {?>
	<img src="<?php the_field('mobile_banner'); ?>" alt="Banner" class="banner d-sm-block d-md-none"/>
	<?php } ?>
	</div><!-- Banner Text-->
	<div class="aboutus-banner-caption carousel-caption partners">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Our Partners</h1>
		<!--h3 class="animated bounceInLeft text-left fs-30">Respected and Trusted Diabetes Center</h3-->
	</div>
	<section class="breadcrumb">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="abt-bg fullwidth Helvetica_Light padd-top-bottom-70 abt-us-links">
		<div class="container">
			<!--p class="text-center Helvetica_Thin fs-18 colorfff">To achieve these objectives, the following institutions were established by Dr. V. Mohan and Late Dr. Rema Mohan</p-->
			<div class="row">
				<div class="col-12">
					<ul class="abt-link-ul">
						<li class="abt-link img-1"><a href="<?php echo get_home_url();?>/patient-care/diabetes-obesity-centre/"><img src="<?php echo get_template_directory_uri();?>/images/drmohans-weight-management-entre.png" style="background:#fff"/></a></li>
						<li class="abt-link img-1"><a href="http://dmhcp.in/"><img src="<?php echo get_template_directory_uri();?>/images/health-care-logo-final.jpg"/></a></li>
						<li class="abt-link img-1"><a href="http://diabetescourses.in/"><img src="<?php echo get_template_directory_uri();?>/images/dmdea-logo.jpg"/></a></li>
						<li class="abt-link img-2"><a href="http://mdrf.in/"><img src="<?php echo get_template_directory_uri();?>/images/mdrf-logo.jpg"/></a></li>
						<li class="abt-link img-2"><a href="<?php echo get_home_url();?>/csr/"><img src="<?php echo get_template_directory_uri();?>/images/direct-logo.jpg"/></a></li>
						<?php /*
						<li class="abt-link"><a href="<?php echo get_home_url();?>/patient-care/diabetes-obesity-centre/"><img src="<?php echo get_template_directory_uri();?>/images/about-us-1.jpg"/></a></li>
						<li class="abt-link"><a href="http://dmhcp.in/"><img src="<?php echo get_template_directory_uri();?>/images/about-us-2.jpg"/></a></li>
						<li class="abt-link"><a href="http://diabetescourses.in/"><img src="<?php echo get_template_directory_uri();?>/images/edu-logo.jpg"/></a></li>
						<li class="abt-link"><a href="http://mdrf.in/"><img src="<?php echo get_template_directory_uri();?>/images/mdrs.jpg"/></a></li>
						<li class="abt-link"><a href="<?php echo get_home_url();?>/csr/"><img src="<?php echo get_template_directory_uri();?>/images/about-us-5.jpg"/></a></li> */ ?>
					</ul>
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>