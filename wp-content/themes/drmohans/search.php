<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="container-fluid bg-lgrey">
		<div class="row">
			<div class="col-12 col-md-8 videos-leftbar bg-lgrey cat-leftbar">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
				?>
				<?php if ( have_posts() ) : ?>
				<header class="page-header">
					<?php if ( have_posts() ) : ?>
						<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyseventeen' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
					<?php else : ?>
						<h1 class="page-title"><?php _e( 'Nothing Found', 'twentyseventeen' ); ?></h1>
					<?php endif; ?>
				</header><!-- .page-header -->
			<?php endif; ?>
				
				<?php
					if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part( 'template-parts/post/content', 'excerpt' );

						endwhile; // End of the loop.

						the_posts_pagination( array(
							'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
							'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
							'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
						) );

					else : ?>

						<p style="padding-top:50px"><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
						<?php
							get_search_form();

					endif;
					?>
			</div>
			<div class="col-12 col-md-4 no-guttersright bg-dl-grey Helvetica_Light leftbar-videopage">
				<label class=" lbl-title featured-post-title" style="padding-top: 45px;">FEATURED POSTS</label>
				<?php
					$args = array(
							'posts_per_page' => 5,
							'meta_key' => 'meta-checkbox',
							'meta_value' => 'yes'
						);
						$featured = new WP_Query($args);
					 
					if ($featured->have_posts()): 
						while($featured->have_posts()): $featured->the_post(); ?>
						<div class="featured-post">
							<label class="featured-title"><?php the_title(); ?></label>
							<p class="featured-content"><?php the_excerpt(); ?></p>
							<a href="<?php the_permalink(); ?>" class="featured-readmore">Read More</a>
						</div>
							<?php
						endwhile;
					endif;
					wp_reset_postdata();
				?>
				<p class="para-feature">
				At Dr.Mohans we are dedicated to support you in all your requirements during your patient journey
				</p>
				<div class="book_appintment col-12 col-sm-9 sidebar-form">
					<div class="gravity_holder gravity_holder-sidebar">
						<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
						<?php gravity_form( 1, $display_title = false, $display_description = false, $ajax = true, $echo = true ); ?>
					</div>							
				</div>
			</div>
		</div>
	</div>
<?php get_footer();
