<?php
/**
 *  Template Name: Investor Investment Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>

	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner <?php if(get_field('mobile_banner',get_the_ID())) { ?>  banner d-none d-md-block <?php } ?> "/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) { ?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	<!-- Banner Text-->
	<div class="aboutus-banner-caption carousel-caption partners">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Investor Info</h1>
	</div>
	<section class="">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list">
		<div class="container">
		<label class="text-left Helvetica_Roman fs-48">Transfer of Shares To IEPF</label>
			<div class="row">
				<div class="">
					<?php if(have_rows('info')){ ?>
						<ul class="primary-ul" style="padding-left:80px !important">
						<?php while ( have_rows('info') ) : the_row(); ?>
							<li>
								<a href="<?php the_sub_field('download_file'); ?>" target="_blank" download class="fs- 18 d-block"><?php the_sub_field('title'); ?><span class="fs-14">&nbsp;&nbsp;(Download Here)</span></a>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="col-12 text-center">
					<div class="d-flex">
						<label class="m-0 Helvetica_Roman">Name of the Nodal Officer : </label><span> Mr.R.Sabesan</span>
					</div>
					<div class="d-flex">
						<label class="m-0 Helvetica_Roman">Designation : </label><span>Chief Financial Officer</span>
					</div>
					<div class="d-flex">
						<label class="m-0 Helvetica_Roman">Email : </label><span><a href="mailto:sabesan@drmohans.com">sabesan@drmohans.com</a></span>
					</div>

					<div class="d-flex">
						<label class="m-0 Helvetica_Roman">Phone : </label><span><a href="tel:044 4396 8888">044-4396 8888 </a></span>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>