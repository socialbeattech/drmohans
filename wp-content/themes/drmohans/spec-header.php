
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/style-speciality.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/swiper.min.css">
<?php wp_head(); ?>
<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/swiper.min.js'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/custom.js'></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

     <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NGXBRHZ');</script>
    <!-- End Google Tag Manager -->  
<body <?php body_class(); ?>>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGXBRHZ"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->	
    <header id="masthead">
		<div class="header">
			<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-3 no-gutters header_logo">
					<a href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri();?>/images/logo-new.png" alt="Drmohans"/></a>
					<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
						<?php
						echo twentyseventeen_get_svg( array( 'icon' => 'bars' ) );
						echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );
						_e( '', 'twentyseventeen' );
						?>
					</button>
				</div>
				<div class="col-12 col-sm-12 col-md-11 col-lg-7 no-gutters header_menus" style="float:none!important">
					<?php if ( has_nav_menu( 'top' ) ) : ?>
						<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
					<?php endif; ?>
				</div>
				<div class="  d-none d-sm-none d-md-block d-lg-block col-lg-2 no-gutters header_menus m-auto" style="padding: 0 34px;">
					<div id="b_app" class="cur-pointer text-center color-fff fs-16 " style="color: #fff;  background: rgb(237, 22, 22);padding: 10px;font-family: 'BEBASNEUEREGULAR';">Book Appointment</div>
				</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	<div id="form-slideout" class="form-slideout floating-btns">
		<a href="javascript:void(0)" class="fixed-btn text-uppercase text-center padd5 book-appointment">
		</a>
		<div class="div-to-extend" id="extend_bookapp">		
			<div class="extend-content">
				<div class="book_appintment">
					<div class="gravity_holder"> 
						<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">Book an Appointment</h1>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
						<?php gravity_form( 3, $display_title = false, $display_description = false,$tabindex, $ajax = false, $echo = true ); ?>
					</div>							
				</div>	
			</div>
			<a class="close-extend">X</a>
		</div>
		<a href="javascript:void(0)" class="fixed-btn text-uppercase text-center padd5 location"></a>
		<div class="div-to-extend" id="extend_bookapp">		
			<div class="extend-content" style="top:-16%">
				<div class="book_appintment">
					<div class="gravity_holder">
						<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">We are located across<br>35+ centres in India.</h1>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Locate us for consultation</h2>
						<select id="id_states1" name="state" class="id_states1 location-select sticky-select">
							<option value="0">States</option>						
							<?php echo wpb_list_child_pages(); //picks up page title and link from functions.php ?>
							</select>
						<select id="id_cities1" name="city" class="id_cities1 location-select sticky-select">
							<option value="0">Cities</option>
						</select><br>
						<label class="alert1"></label>
						<button id="submit_inv1" class="submit_inv1 speciality-b">Search</button>
					</div>							
				</div>	
			</div>
			<a class="close-extend">X</a>
		</div>
		<a href="javascript:void(0)" class="fixed-btn text-uppercase text-center padd5 enquire"></a>
		<div class="div-to-extend">		
			<div class="extend-content">	
				<div class="book_appintment">
					<div class="gravity_holder">
						<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">Enquire</h1>
						<?php gravity_form( 2, $display_title = false, $display_description = false,$tabindex, $ajax = false, $echo = true ); ?>
					</div>							
				</div>	
			</div>
			<a class="close-extend">X</a>
		</div>
		<a href="javascript:void(0)" class="fixed-btn text-uppercase text-center padd5 search"></a>
		<div class="div-to-extend" style="top:0">		
			<div class="book_appintment">
				<div class="gravity_holder" style="padding-bottom:5%;">
					<input type="text" class="search-input padd0" name="" value="" placeholder="Enter your search keyword" style="padding:3px !important"/>
					<img class="search-submit" src="<?php echo get_template_directory_uri();?>/images/search-icon.png"/>
					<label class="err_search colorfff"></label>
					<div class="search-result">
					</div>
				</div>							
			</div>	
			<a class="close-extend">X</a>
		</div> 
	</div>
	<div class="modal" id="modalbookapp"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
				<span class="text-right close colorfff" data-dismiss="modal">X</span>
				<div class="gravity_holder">
					<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters">Book an Appointment</h1>
					<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
					<?php gravity_form( 4, $display_title = false, $display_description = false,$tabindex, $ajax = false, $echo = true ); ?>
				</div>							
			</div>							
		</div>	
	</div>	
	<div class="modal" id="modalenq"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
				<span class="text-right close colorfff" data-dismiss="modal">X</span>
				<div class="gravity_holder">
					<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">Enquire</h1>
					<?php gravity_form( 5, $display_title = false, $display_description = false, $ajax = false, $echo = true ); ?>	
				</div>							
			</div>							
		</div>	
	</div>	
	<div class="modal" id="modallocation"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
			<span class="text-right close colorfff" data-dismiss="modal">X</span>
				<div class="gravity_holder" style="padding-bottom:5%">
					<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">We are located across<br>35+ centres in India.</h1>
					<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Locate us for consultation</h2>
					<select id="id_states" name="state" class="id_states location-select sticky-select">
						<option value="0">States</option>
							<?php echo wpb_list_child_pages(); //picks up page title and link from functions.php ?>
					</select>
						<?php //echo wpb_list_child_pages(); ?>
					<select id="id_cities" name="city" class="id_cities location-select sticky-select">
						<option value="0">Cities</option>
					</select><br>
					<label class="alert"></label>
					<button id="submit_inv" class="submit_inv speciality-b">Search</button>
				</div>							
			</div>							
		</div>	
	</div>	
	<div class="modal" id="modalsearch"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
			<span class="text-right close colorfff" data-dismiss="modal">X</span>
			<div class="book_appintment">
				<div class="gravity_holder" style="padding-bottom:5%;position: relative;margin-top: 20px;">
					<input type="text" class="search-input1 padd0" name="" value="" placeholder="Enter your search keyword" style="padding: 4px;"/>
					<img class="search-submit1" src="<?php echo get_template_directory_uri();?>/images/search-icon.png"/ style="bottom: 45%;right: 32px;">
					<label class="err_search1 colorfff"></label>
					<div class="search-result1">
						
					</div>
				</div>							
			</div>						
			</div>							
		</div>	
	</div>	
	<div id="form-slideout-mobile " class="form-slideout-mobile ">
	<a href=""  class="fixed-btn text-uppercase  text-center padd5 book-appointment" data-toggle="modal" data-target="#modalbookapp" style="padding:0;"></a>
	
	<a href=""class="fixed-btn text-uppercase text-center padd5 location" style="padding:0;" data-toggle="modal" data-target="#modallocation"></a> 
	
	<a href=""class="fixed-btn text-uppercase text-center padd5 enquire" data-toggle="modal" data-target="#modalenq" style="padding:0;"></a>
	
	<a href=""class="fixed-btn text-uppercase text-center search padd5" style="padding:0;" data-toggle="modal" data-target="#modalsearch"></a></div>