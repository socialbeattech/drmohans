<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
	<div class="container-fluid bg-lgrey">
		<div class="row">
			<div class="col-12 col-md-8 videos-leftbar bg-lgrey cat-leftbar">
				<?php if ( have_posts() ) : ?>
				<header class="page-header">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
				?>
					<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</header><!-- .page-header -->
			<?php endif; ?>

			<div class="row" style="margin:0">
				<?php
				if ( have_posts() ) : ?>
					<?php
					/* Start the Loop */
					$count =1;
					while ( have_posts() ) : the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */?>
						<div class="col-12 col-sm-6 blogs_article no-guttersleft">
							<a href="<?php the_permalink(); ?>" class="no-gutters"><img class="cat-thumb" src="<?php the_post_thumbnail_url(); ?>" alt="Drmohans" width="350" height="190" /></a>
							<h3 class="text-uppercase category-title fs-22"><?php the_title(); ?></h3>
							<h4 class="category-excerpt"><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h4>
						</div><?php
						if($count%2==0){ ?>
							</div>
							<div class="row" style="margin:0">
							<?php
						}
					$count++;
					endwhile;
				else :

					get_template_part( 'template-parts/post/content', 'none' );

				endif; ?>
			</div>
			</div>
			<div class="col-12 col-md-4 no-guttersright bg-dl-grey Helvetica_Light leftbar-videopage">
				<label class=" lbl-title featured-post-title" style="padding-top: 45px;">FEATURED POSTS</label>
				<?php
					$args = array(
							'posts_per_page' => 5,
							'meta_key' => 'meta-checkbox',
							'meta_value' => 'yes'
						);
						$featured = new WP_Query($args);

					if ($featured->have_posts()):
						while($featured->have_posts()): $featured->the_post(); ?>
						<div class="featured-post">
							<label class="featured-title"><?php the_title(); ?></label>
							<p class="featured-content"><?php the_excerpt(); ?></p>
							<a href="<?php the_permalink(); ?>" class="featured-readmore">Read More</a>
						</div>
							<?php
						endwhile;
					endif;
					wp_reset_postdata();
				?>
				<p class="para-feature">
				At Dr.Mohans we are dedicated to support you in all your requirements during your patient journey
				</p>
				<div class="book_appintment row sidebar-form">
					<div class="gravity_holder gravity_holder-sidebar">
						<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
						<?php gravity_form( 1, $display_title = false, $display_description = false, $ajax = true, $echo = true ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer();
