<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
 include('spec-header.php'); ?>
 <?php global $post;  ?>
	<div class="container-fluid bg-lgrey Helvetica_Light">
		<div class="row">
			<div class="col-12 col-md-8 blog-content color29">
			<?php
			/* Start the Loop */
				while ( have_posts() ) : the_post(); ?>
				
					<?php if (has_post_thumbnail( $post->ID ) ): ?>
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
						<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner"/>
					<?php endif; ?>
					<span class="fs-18 color-red"><?php $cat = get_the_category( $post->ID ); ?><a href="<?php echo get_category_link($cat[1]->cat_ID); ?>" class="color-red"> <?php echo $cat[1]->name; ?></a></span> <br/>
					<span class="fs-22 Helvetica_Bold color29"><?php the_title(); ?></span> <br/>
					<span class="date_published fs-18 color29"><?php echo get_the_date(); ?></span> 
					<div class="blog-content-holder">
					<?php  the_content(); ?>
					</div>
					<?php
				endwhile; // End of the loop. ?>
			</div>
			<div class="col-12 col-md-4 no-guttersright bg-dl-grey Helvetica_Light leftbar-videopage">
				<div class="book_appintment row sidebar-form newsletter-blog">
					<div class="gravity_holder gravity_holder-sidebar">
						<h4 class="col-12 col-sm-12 text-left my-auto fs-22 whcolor no-gutters mt-12" style="margin-top:12px !important;">Subscribe to Newsletter</h4>
						<?php gravity_form( 7, $display_title = false, $display_description = false, $ajax = true, $echo = true ); ?>
					</div>							
				</div>
				<!--h1 class="featured-post-title">Categories</h1-->
				<ul class="categories-widget pt-1 m-b0">
					<?php wp_list_categories(); ?>
				</ul>
				<h1 class="featured-post-title">FEATURED POSTS</h1>
				<?php
					$args = array(
							'posts_per_page' => 5,
							'meta_key' => 'meta-checkbox',
							'meta_value' => 'yes'
						);
						$featured = new WP_Query($args);
					 
					if ($featured->have_posts()): 
						while($featured->have_posts()): $featured->the_post(); ?>
						<div class="featured-post">
							<label class="featured-title"><?php the_title(); ?></label>
							<p class="featured-content"><?php the_excerpt(); ?></p>
							<a href="<?php the_permalink(); ?>" class="featured-readmore">Read More</a>
						</div>
							<?php
						endwhile;
					endif;
					wp_reset_postdata();
				?>
				<p class="para-feature">
				At Dr.Mohans we are dedicated to support you in all your requirements during your patient journey
				</p>
				<div class="book_appintment row sidebar-form">
					<div class="gravity_holder gravity_holder-sidebar">
						<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters">Book an Appointment</h1>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
						<?php gravity_form( 1, $display_title = false, $display_description = false, $ajax = true, $echo = true ); ?>
					</div>							
				</div>
			</div>
		</div>
	</div>
	<div class="container">
	<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

	the_post_navigation( array(
		'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
		'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
	) );
	?>
	</div>
<?php include('spec-footer.php'); ?>
