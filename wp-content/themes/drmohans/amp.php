<?php
/**
 * Template Name:  Desktop-amp
 *
 * @package  SocialBeat Landiing Page Template
 */
?>
<!doctype html>
<html amp lang="en">
	<head>
	<meta charset="utf-8">
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-fx-collection" src="https://cdn.ampproject.org/v0/amp-fx-collection-0.1.js"></script>
	<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
	<script async custom-element="amp-lightbox-gallery" src="https://cdn.ampproject.org/v0/amp-lightbox-gallery-0.1.js"></script>
	<script async custom-element="amp-date-picker" src="https://cdn.ampproject.org/v0/amp-date-picker-0.1.js"></script>
	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
	 <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	 <script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
	 <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
	<title>Dr.Mohan's</title>
	<link rel="canonical" href="http://example.ampproject.org/article-metadata.html">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom>
 /*bootstrap CSS*/
   			*,*:after,*:before{box-sizing:border-box}
   			.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;box-sizing: border-box;}
        	.container-fluid {width: 100%;padding-right: 15px;padding-left: 15px;margin-right: auto; margin-left: auto; box-sizing: border-box;}
        	.row {display: -webkit-box;display: -ms-flexbox;display: flex;-ms-flex-wrap: wrap;flex-wrap: wrap;margin-right: -15px;margin-left: -15px}
			
            .col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px;box-sizing: border-box}
            .col{-ms-flex-preferred-size:0;flex-basis:0%;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-auto{-webkit-box-flex:0;-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-1{-webkit-box-flex:0;-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-2{-webkit-box-flex:0;-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-3{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-4{-webkit-box-flex:0;-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-5{-webkit-box-flex:0;-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-6{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-7{-webkit-box-flex:0;-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-8{-webkit-box-flex:0;-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-9{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-10{-webkit-box-flex:0;-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-11{-webkit-box-flex:0;-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%;}
       
            @media(min-width:768px){.container{max-width:720px}.col-md{-ms-flex-preferred-size:0;flex-basis:0%;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-md-auto{-webkit-box-flex:0;-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-md-1{-webkit-box-flex:0;-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-md-2{-webkit-box-flex:0;-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-md-3{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-md-4{-webkit-box-flex:0;-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-md-5{-webkit-box-flex:0;-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-md-7{-webkit-box-flex:0;-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-md-8{-webkit-box-flex:0;-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-md-9{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-md-10{-webkit-box-flex:0;-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-md-11{-webkit-box-flex:0;-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-md-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}
            
            @media only screen and (min-width:768px) and (max-width:949px){.fs-sm-1{font-size:1px;}.fs-sm-2{font-size:2px;}.fs-sm-3{font-size:3px;}.fs-sm-4{font-size:4px;}.fs-sm-5{font-size:5px;}.fs-sm-6{font-size:6px;}.fs-sm-7{font-size:7px;}.fs-sm-8{font-size:8px;}.fs-sm-9{font-size:9px;}.fs-sm-10{font-size:10px;}.fs-sm-11{font-size:11px;}.fs-sm-12{font-size:12px;}.fs-sm-13{font-size:13px;}.fs-sm-14{font-size:14px;}.fs-sm-15{font-size:15px;}.fs-sm-16{font-size:16px;}.fs-sm-17{font-size:17px;}.fs-sm-18{font-size:18px;}.fs-sm-19{font-size:19px;}.fs-sm-20{font-size:20px;}.fs-sm-21{font-size:21px;}.fs-sm-22{font-size:22px;}.fs-sm-23{font-size:23px;}.fs-sm-24{font-size:24px;}.fs-sm-25{font-size:25px;}.fs-sm-26{font-size:26px;}.fs-sm-27{font-size:27px;}.fs-sm-28{font-size:28px;}.fs-sm-29{font-size:29px;}.fs-sm-30{font-size:30px;}.fs-sm-31{font-size:31px;}.fs-sm-32{font-size:32px;}.fs-sm-33{font-size:33px;}.fs-sm-34{font-size:34px;}.fs-sm-35{font-size:35px;}.fs-sm-36{font-size:36px;}.fs-sm-37{font-size:37px;}.fs-sm-38{font-size:38px;}.fs-sm-39{font-size:39px;}.fs-sm-40{font-size:40px;}.fs-sm-41{font-size:41px;}.fs-sm-42{font-size:42px;}.fs-sm-43{font-size:43px;}.fs-sm-44{font-size:44px;}.fs-sm-45{font-size:45px;}.fs-sm-46{font-size:46px;}.fs-sm-47{font-size:47px;}.fs-sm-48{font-size:48px;}.fs-sm-49{font-size:49px;}.fs-sm-50{font-size:50px;}.fs-sm-51{font-size:51px;}.fs-sm-52{font-size:52px;}.fs-sm-53{font-size:53px;}.fs-sm-54{font-size:54px;}.fs-sm-55{font-size:55px;}.fs-sm-56{font-size:56px;}.fs-sm-57{font-size:57px;}.fs-sm-58{font-size:58px;}.fs-sm-59{font-size:59px;}.fs-sm-60{font-size:60px;}.fs-sm-61{font-size:61px;}.fs-sm-62{font-size:62px;}.fs-sm-63{font-size:63px;}.fs-sm-64{font-size:64px;}.fs-sm-65{font-size:65px;}.fs-sm-66{font-size:66px;}.fs-sm-67{font-size:67px;}.fs-sm-68{font-size:68px;}.fs-sm-69{font-size:69px;}.fs-sm-70{font-size:70px;}.fs-sm-71{font-size:71px;}.fs-sm-72{font-size:72px;}.fs-sm-73{font-size:73px;}.fs-sm-74{font-size:74px;}.fs-sm-75{font-size:75px;}.fs-sm-76{font-size:76px;}.fs-sm-77{font-size:77px;}.fs-sm-78{font-size:78px;}.fs-sm-79{font-size:79px;}.fs-sm-80{font-size:80px;}.fs-sm-81{font-size:81px;}.fs-sm-82{font-size:82px;}.fs-sm-83{font-size:83px;}.fs-sm-84{font-size:84px;}.fs-sm-85{font-size:85px;}.fs-sm-86{font-size:86px;}.fs-sm-87{font-size:87px;}.fs-sm-88{font-size:88px;}.fs-sm-89{font-size:89px;}.fs-sm-90{font-size:90px;}.fs-sm-91{font-size:91px;}.fs-sm-92{font-size:92px;}.fs-sm-93{font-size:93px;}.fs-sm-94{font-size:94px;}.fs-sm-95{font-size:95px;}.fs-sm-96{font-size:96px;}.fs-sm-97{font-size:97px;}.fs-sm-98{font-size:98px;}.fs-sm-99{font-size:99px;}.fs-sm-100{font-size:100px;}.mt-sm-0{margin-top:0px;}.mt-sm-05{margin-top:05px;}.mt-sm-10{margin-top:10px;}.mt-sm-15{margin-top:15px;}.mt-sm-20{margin-top:20px;}.mt-sm-25{margin-top:25px;}.mt-sm-30{margin-top:30px;}.mt-sm-35{margin-top:35px;}.mt-sm-40{margin-top:40px;}.mt-sm-45{margin-top:45px;}.mt-sm-50{margin-top:50px;}.mt-sm-55{margin-top:55px;}.mt-sm-60{margin-top:60px;}.mt-sm-65{margin-top:65px;}.mt-sm-70{margin-top:70px;}.mt-sm-75{margin-top:75px;}.mt-sm-80{margin-top:80px;}.mt-sm-85{margin-top:85px;}.mt-sm-90{margin-top:90px;}.mt-sm-95{margin-top:95px;}.mt-sm-100{margin-top:100px;}.mb-sm-0{margin-bottom:0px;}.mb-sm-05{margin-bottom:05px;}.mb-sm-10{margin-bottom:10px;}.mb-sm-15{margin-bottom:15px;}.mb-sm-20{margin-bottom:20px;}.mb-sm-25{margin-bottom:25px;}.mb-sm-30{margin-bottom:30px;}.mb-sm-35{margin-bottom:35px;}.mb-sm-40{margin-bottom:40px;}.mb-sm-45{margin-bottom:45px;}.mb-sm-50{margin-bottom:50px;}.mb-sm-55{margin-bottom:55px;}.mb-sm-60{margin-bottom:60px;}.mb-sm-65{margin-bottom:65px;}.mb-sm-70{margin-bottom:70px;}.mb-sm-75{margin-bottom:75px;}.mb-sm-80{margin-bottom:80px;}.mb-sm-85{margin-bottom:85px;}.mb-sm-90{margin-bottom:90px;}.mb-sm-95{margin-bottom:95px;}.mb-sm-100{margin-bottom:100px;}.pt-sm-0{padding-top:0px;}.pt-sm-5{padding-top:5px;}.pt-sm-10{padding-top:10px;}.pt-sm-15{padding-top:15px;}.pt-sm-20{padding-top:20px;}.pt-sm-25{padding-top:25px;}.pt-sm-30{padding-top:30px;}.pt-sm-35{padding-top:35px;}.pt-sm-40{padding-top:40px;}.pt-sm-45{padding-top:45px;}.pt-sm-50{padding-top:50px;}.pt-sm-55{padding-top:55px;}.pt-sm-60{padding-top:60px;}.pt-sm-65{padding-top:65px;}.pt-sm-70{padding-top:70px;}.pt-sm-75{padding-top:75px;}.pt-sm-80{padding-top:80px;}.pt-sm-85{padding-top:85px;}.pt-sm-90{padding-top:90px;}.pt-sm-95{padding-top:95px;}.pt-sm-100{padding-top:100px;}.pb-sm-0{padding-bottom:0px;}.pb-sm-5{padding-bottom:5px;}.pb-sm-10{padding-bottom:10px;}.pb-sm-15{padding-bottom:15px;}.pb-sm-20{padding-bottom:20px;}.pb-sm-25{padding-bottom:25px;}.pb-sm-30{padding-bottom:30px;}.pb-sm-35{padding-bottom:35px;}.pb-sm-40{padding-bottom:40px;}.pb-sm-45{padding-bottom:45px;}.pb-sm-50{padding-bottom:50px;}.pb-sm-55{padding-bottom:55px;}.pb-sm-60{padding-bottom:60px;}.pb-sm-65{padding-bottom:65px;}.pb-sm-70{padding-bottom:70px;}.pb-sm-75{padding-bottom:75px;}.pb-sm-80{padding-bottom:80px;}.pb-sm-85{padding-bottom:85px;}.pb-sm-90{padding-bottom:90px;}.pb-sm-95{padding-bottom:95px;}.pb-sm-100{padding-bottom:100px;}.text-sm-left{text-align:left;}.text-sm-center{text-align:center;}.text-sm-right{text-align:right;}.text-sm-justify{text-align:justify;}}
       
            @media(min-width:992px){.container{max-width:960px}.col-lg{-ms-flex-preferred-size:0;flex-basis:0%;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-lg-auto{-webkit-box-flex:0;-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-lg-1{-webkit-box-flex:0;-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-lg-2{-webkit-box-flex:0;-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-lg-3{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-lg-4{-webkit-box-flex:0;-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-lg-5{-webkit-box-flex:0;-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-lg-6{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-lg-7{-webkit-box-flex:0;-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-lg-8{-webkit-box-flex:0;-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-lg-9{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-lg-10{-webkit-box-flex:0;-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-lg-11{-webkit-box-flex:0;-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-lg-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}
            
            @media(min-width:1200px){.container{max-width:1140px}.col-xl{-ms-flex-preferred-size:0;flex-basis:0%;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-xl-auto{-webkit-box-flex:0;-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-xl-1{-webkit-box-flex:0;-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-xl-2{-webkit-box-flex:0;-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-xl-3{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-xl-4{-webkit-box-flex:0;-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-xl-5{-webkit-box-flex:0;-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-xl-6{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-xl-7{-webkit-box-flex:0;-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-xl-8{-webkit-box-flex:0;-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-xl-9{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-xl-10{-webkit-box-flex:0;-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-xl-11{-webkit-box-flex:0;-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-xl-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}
			
			@font-face {
    font-family: 'HelveticaNeueLTStdRoman';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.svg#HelveticaNeueLTStdRoman') format('svg');
}
	   .HelveticaNeueLTStdRoman{ font-family : 'HelveticaNeueLTStdRoman'}
       .fs-sm-42px { font-size:42px;}
       .fs-sm-21px { font-size:21px;}
       .fs-sm-19px { font-size:19px;}

	@font-face {
    font-family: 'HelveticaNeueLTStdLt';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.svg#HelveticaNeueLTStdLt') format('svg');
}
		
	   .HelveticaNeueLTStdLt{ font-family : 'HelveticaNeueLTStdLt'}
       .fs-sm-18px { font-size:18px;}
       .fs-sm-12px { font-size:12px;}
     
	   
	@font-face {
    font-family: 'HelveticaNeueLTStdTh';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.svg#HelveticaNeueLTStdTh') format('svg');
}
	
	   .HelveticaNeueLTStdTh{ font-family : 'HelveticaNeueLTStdTh'}
       .fs-sm-46px { font-size:46px;}
       
	  @font-face {
    font-family: 'HelveticaNeueLTStdBd';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.svg#HelveticaNeueLTStdBd') format('svg');
} 
	   .HelveticaNeueLTStdBd{ font-family : 'HelveticaNeueLTStdBd'}
       .fs-sm-22px { font-size:22px;}
       .fs-sm-15px { font-size:15px;}
       .fs-sm-18px { font-size:18px;}
	   
	  @font-face {
    font-family: 'HelveticaNeueLTStdMd';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.svg#HelveticaNeueLTStdMd') format('svg');
}
    .HelveticaNeueLTStdMd{ font-family : 'HelveticaNeueLTStdMd'}
    .fs-sm-33px { font-size:33px;}
    .fs-sm-27px { font-size:27px;}
    .fs-sm-22px { font-size:22px;}
    .fs-sm-20px { font-size:20px;}
    .fs-sm-15px { font-size:15px;}
	
	@font-face {
    font-family: 'HelveticaBoldFont';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.svg#HelveticaBoldFont') format('svg');
}
    .HelveticaBoldFont{ font-family : 'HelveticaBoldFont'}
    .fs-sm-26px { font-size:26px;}
    .fs-sm-22px { font-size:22px;}
 
 @font-face {
    font-family: 'HelveticaLight';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.svg#HelveticaLight') format('svg');
}

    .HelveticaLight{ font-family : 'HelveticaLight'}
    .fs-sm-18px { font-size:18px;}
    

    @font-face {
    font-family: 'BebasNeueBold';
    src: url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.svg#BebasNeueBold') format('svg');
}

    .BebasNeueBold{ font-family : 'BebasNeueBold'}
    .fs-sm-24px { font-size:24px;}
    
    @font-face {
    font-family: 'BebasNeueRegular';
    src: url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueRegular.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueRegular.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueRegular.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueRegular.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueRegular.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueRegular.svg#BebasNeueRegular') format('svg');
}
    .BebasNeueRegular{ font-family : 'BebasNeueRegular'}
    .fs-sm-18px { font-size:18px;}
    .fs-sm-16px { font-size:16px;}



	    .experts h3{text-decoration: none;background: rgb(237, 22, 22);color: rgb(255, 255, 255);padding: 15px 26px 15px;
		 width: 38%;}	
		.experts h1{color:#ff0000; margin-top:0px;}
		.experts h3 a{text-decoration: none;background: rgb(237, 22, 22);color: rgb(255, 255, 255);padding: 4px 0px 9px;}
		.Diabetes h1{text-align:center; color:#fff;padding: 2% 0 0 0;}
		.diet p{color:#fff;}
		.diet h2{color:#fff;}
        .Diabetes{background: url(<?php echo get_template_directory_uri();?>/images/Cares-bg.jpg);background-repeat: no-repeat; background-size: cover;background-attachment: fixed; }		
		.mobile-speciality h1{text-align:center;}
		.book_appintment {background: rgb(72, 73, 83);float: left; margin: 0 auto;padding: 3%;width: 100%;}
		.book_appintment h1{text-align:left;color:#fff;padding: 0 5%;}
		.book_appintment p{text-align:left;color:#fff;padding: 0 5%;}
		.gravity_holder { border: 2px solid #ffffff;float: left;margin: 0 auto;width: 100%;padding: 1% 0% 0% 1%}
		 form input {display: inline-block;background-color: #ffffff; color: #d0d1d2;width:  70%;line-height: 30px; border: none;margin-bottom: 10px;padding: 0;padding-left: 10px;margin:3% 15%;}
		 form select#Location-list {display: inline-block;background-color: #ffffff; color: #86898c;width:  70%;line-height: 30px; border: none;margin-bottom: 10px;padding: 8px;padding-left: 10px;margin:3% 15%;}
		 form radio {display: inline-block;background-color: #ffffff; color: #d0d1d2;width:  70%;line-height: 30px; border: none;margin-bottom: 10px;padding: 8px;padding-left: 10px;margin:3% 15%;}
	     input#idComments {padding: 1% 0 13% 3%;}
         input.submit {cursor: pointer;padding-left: 0px;background-color: #f1151f;margin: 3% 2% 5% 15%;padding: 2% 0;
		 font-size:18px;}
	     input#radio { margin: 2% -1% 2% 11%;padding: 0px;width: 7%;}
		.service { margin: 9% 0;}
		.Centres { margin: 5% 0;}
		.Patients {margin: 0 0 0 20%;}
		.service p.paragraph {padding: 0 22%; text-align: center;}
		.whcolor h1{text-align:center;line-height: 39px ; margin-top: 12px ; color: #fff;}
		select#Location {display: inline-block;background-color: rgb(237, 22, 22);color: #fff;width: 40%;line-height: 30px;
        border: none;margin-bottom: 10px;padding: 12px;padding-left: 10px;margin: 3% 32%;font-family:HelveticaNeueLTStdRoman;font-size:21px;}
		section.journey option {background: #fff;color: black;}
		.knows_diabetes {background: url(<?php echo get_template_directory_uri();?>/images/forest-bg.jpg);background-repeat: no-repeat;background-position: bottom;
        background-size: cover;padding: 3% 0 1% 0;}
		.knows_diabetes h1{text-align:center;}
		.knows_diabetes .understands {border-bottom: 1px solid #ea4c4e;outline: 1px solid #c2c3cb;min-height: 254px;
         background: rgb(251, 251, 252,0.9); background-color: rgb(251, 251, 252,0.9);margin: 0 0% 0 7%;}
		.knows_diabetes .understands h2 { margin-bottom: 9%;}
		.knows_diabetes .understands p { margin-bottom: 13%;}
	    .knows_diabetes p.read {text-align: center;padding: 1% 1%; background: rgb(237, 22, 22);width: 17%;margin: 3% 41%;color: #fff;}
		.shops p { background: rgb(237, 22, 22);padding: 3% 7%;width: 89%;margin:0 0 4% 5%;color:#fff;text-align: center;}
		.shops .borders_stuff h2 {min-height: 175px;background: url(<?php echo get_template_directory_uri();?>/images/research.png) no-repeat center bottom;text-align: center;color: #fff;}
		.shops .research { border: 1px solid #ffffff;}
		.shops .research { background: rgb(72, 73, 83); padding: 10px;margin: 0 4% 0 8%;}
		.borders_stuff { border: 1px solid #fff;}
		.shops .tracker_app h2 {background: url(<?php echo get_template_directory_uri();?>/images/tracker-app.png) no-repeat center bottom;min-height: 175px;}
		.shops .tracker_app { background: rgb(72, 73, 83); padding: 10px;margin: 0 3% 0 4%;}
		.shops .cart { background: rgb(72, 73, 83); padding: 10px;margin: 0% 1% 0% 1%;}
		.shops .cart h2 {background: url(<?php echo get_template_directory_uri();?>/images/cart.png) no-repeat center bottom;min-height: 175px;}
		.knows_diabetes .understands span {border-bottom: 2px solid #ed1616; position: relative;}
		.visual { margin: 4% 0%;}
		.journey {background: url(<?php echo get_template_directory_uri();?>/images/journeys-bg.png); background-repeat: no-repeat;background-size: cover;}
		.footer {background: rgb(48,49, 49);}
		.footer h1 {color: rgb(179, 181, 181);text-transform: uppercase;}
		.footer p {color: rgb(179, 181, 181);margin: 0 auto; padding: 0;line-height: 25px;}
		.footer ul {list-style-type: none;margin: 0; padding: 0;}
		.footer ul li { color: rgb(179, 181, 181);line-height: 25px;}
		.last-section p {color: rgb(48,49, 49);font-family: 'HelveticaNeueLTStdLt';font-size: 14px;}
		.last-section .terms ul {list-style-type: none;padding: 0;display: inline-block;margin: 5px auto;}
		.last-section .terms ul li {border-right: 1px solid #303131;display: inline-block;line-height: 10px;list-style-type: none;margin: 0 auto;padding: 0 6px 0 6px;text-align: center;font-family: 'HelveticaNeueLTStd-Lt';font-size: 14px;}
        .last-section  .terms ul li:last-child {border-right: 0px none;}
        .last-section .terms ul li a {color: rgb(48,49, 49);font-family: 'HelveticaNeueLTStdLt';font-size: 14px;}
		.categories-widget a:after, .hover-border:after {content: '';position: absolute;width: 100%;height: 2px;bottom: -2px;left: 0;background-color: #E2061B;transform: scaleX(0);transform-origin: bottom right;transition: transform 0.4s;}
		.categories-widget a, .hover-border {position: relative;}
		.terms {text-align: center;margin: auto;}
	    .visual p a.whcolor { color: #fff;text-decoration:none;}
	    .last-section a.hover-border {text-decoration: none;color: #000;}
		.footer p a {text-decoration: none;color: #a9adad;}
		.knows_diabetes p.read a.whcolor {text-decoration: none;color: #fff;}
		.supported{margin: 4% 2%;}
		.video { padding: 0 4% 0% 4%;}
		.slides {padding: 6% 0;}
		.testing{margin-top: 1%;}
		.last-section{margin: 1% 4%;}
		.footer ul li a{text-decoration: none;color: rgb(179, 181, 181);}
		label { color: #fff;}
		.footer ul.socials {list-style-type: none;padding: 0;display: inline-block;text-align: center;}
		.footer ul.socials li {color: rgb(179, 181, 181);line-height: 25px; float:left;}
		.footer ul li a { text-decoration: none;color: rgb(179, 181, 181);}
		.footer-path { padding:1% 6% 2% 7%}
		.main-center{padding: 0% 2%;}
		.quik-links{padding: 0 4%;}
		.sbt {padding: 0 7%;}
	   .knows_diabetes .understands p a {border-bottom: 2px solid #ed1616;position: relative;}
	   .categories-widget a, .hover-border {position: relative;text-decoration: none;color: #000;}
	   .form {padding: 0 3%;}
	   .dieticians {padding: 0 0 0 5%;}
	
	   .menu {padding: 1% 0 0 6%;}
	   .header {position: absolute;width: 100%; top: 20px;background: #fff;padding: 5px 0 0 0;display:block;}
	   .header_menus p{color: #fff;background: rgb(237, 22, 22); padding:10px;margin-top:1%;text-align:center;}
	   .header_menus {padding: 0 0 0% 7%;;margin:2px 0 0 -10%;position: relative;}
	   .toggle { position:fixed;top: 30%;right: 0;}
	   .toggle  button {background: #34349c;color:#fff ;padding: 10% 4%;border:1px solid #34349c;}
	   .form-sidebar{width: 44%;}
	   .medications {margin: 0 0 0 -11%;}
	   .menu ul {list-style: none;padding: 0; margin: 0;}
       .menu ul li{display: block;position: relative;float: left;;padding: 0 20px; text-transform:uppercase;}
	   .menu li ul { display: none;padding: 7px 0px 0px 8px; }
	   .menu li li ul { display: none; left:100%;top:0 }
	   .menu ul li a {display: block;padding: 1px 3px;text-decoration: none;white-space: nowrap;color: #000;
		text-transform: uppercase;letter-spacing: 0px;font-family: 'BEBASNEUEREGULAR';line-height:25px;}
	    .menu li:hover > ul{display: block;position: absolute;background-color:rgba(255,255,255,0.8)}
	    .menu li:hover li { float: none; }
        .menu li:hover a { z-index: 100;padding-left: 15%;}
        
       .main-navigation li ul li { border-top: 0; }
	   
	  
	   li ul { display: none;}
	   .main-navigation li ul li {border-top: 0;}
		ul {display: inline-block;}
		ul li {float:none;}
		ul:before,ul:after {content: " ";  display: table;}
		ul:after { clear: both; margin:2% 0;}
       	ul.menu li a:hover { color: #fff;}  
		.main-navigation ul ul a {letter-spacing: 0;padding: 2px 0;position: relative;text-transform: none;}
		.clinics{margin: 0 6%;}
		.last-part {margin: 0 0 0 4%}
		.copyrights {margin: 0 -2%;}
		.mobile-header{display:none;}
		.medications a{text-decoration: none;}
		.journey button {background: red;border: 1px solid red;padding: 7px 8px;margin: 2% 40%;font-size: 21px;
			width: 30%;font-family:HelveticaNeueLTStdRoman;}
	    .amp-drop-down a{ display:block; padding:10px;text-decoration: none; color: #000;font-size: 19px;font-family:HelveticaNeueLTStdRoman;}
			.amp-drop-down{
				position: absolute;
				background: #fff;
				margin: 0;
				top: 56px;
				left: 318px;
				width: 226px;
			}
			.show{display:block}
			.hide{display:none}
	 body {
    
      margin: 0;
      padding: 0;
    }

    header {
      background: #ffffff;
      color: black;
	  border: 1px solid;
    }

    .headerbar {
      height: 40px;
      z-index: 999;
      top: 0;
      width: 30%;
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      justify-content: flex-start;
      align-items: flex-start;
	  padding: 0% 1%;
    margin: 1% 27%;
    }

    .hamburger,
    .search,
    .account,
    .basket {
      padding: 5px;
      flex: 0 1 60px;
      align-self: auto;
      cursor: pointer;
    }

    .site-name {
      flex: 1 1 auto;
     margin:9px 12px 4px 3px;
    }

    .tab-tooltip {
      position: absolute;
      top: -1000rem;
    }

    .tab-tooltip:focus {
      position: fixed;
      z-index: 99999;
      top: 0;
      left: 0;

   
      padding: 0.5rem 1rem;
      background: #f5f5f5;
      border: #a7a7a7 1px solid;
    }

    .submenu {
      width: 100%;
      height: 100%;
    }

    .main-menu,
     .submenu {
      overflow: auto;
    }

  .submenu {
      top: 0;
      left: 0;
      position: fixed;
    }

.hide-submenu {
      visibility: hidden;
      transform: translateX(-100%);
    }

    .show-submenu {
      visibility: visible;
      transform: translateX(0);
    }

     .hide-parent {
      visibility: hidden;
    }

     .truncate {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

   .link-container {
      display: block;
      height: 44px;
      line-height: 44px;
      border-bottom: 1px solid #f0f0f0;
      padding: 0 1rem;
    }

    #header-sidebar a {
      min-width: 44px;
      min-height: 44px;
      text-decoration: none;
      cursor: pointer;
    }


   .submenu-icon {
      padding-right: 44px;
    }

    .submenu-icon::after {
      position: absolute;
      right: 0;
      height: 44px;
      width: 44px;
      content: '';
      background-size: 1rem;
      background-image: url('data:image/svg+xml;utf8, <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z"/></svg>');
      background-repeat: no-repeat; background-position: center; }

   .controls {display: flex;height: 50px;background: #f0f0f0;}
   .controls a {display: flex;justify-content: center;align-items: center;  }
  .controls span {line-height: 50px;margin: 0 auto;}
    #header-sidebar nav>.controls>a:first-of-type {visibility: hidden;}
   .controls a svg {height: 1rem;width: 1rem;}
 .link-icon {float: left;height: 44px;margin-right: 0.75rem; }
	.link-icon>svg {height: 44px;}
    #header-sidebar {background: #fff;color: #232323;fill: #232323;text-transform: uppercase;letter-spacing: 0.18rem;font-size: 0.875rem;}
	#header-sidebar a {color: #232323;text-transform: none;letter-spacing: normal;}
	div[class*="-sidebar-mask"] {opacity: 0.8;}
	#header-sidebar a:hover {text-decoration: underline;fill: #232323;}
	 .view-all {font-style: italic;font-weight: bold;}
	.header_menus p a{text-decoration: none;color: white;}
	
    @media only screen and (max-width: 767px){
	   .video {padding: 5% 7% 0% 3%;}
	   .experts h1 {font-size: 37px;margin-top: 4%;}
	   .experts p {text-align: justify;padding: 2% 4%;}
	   .experts h3{    width: 77%;margin: 0 8%;}
	   .Diabetes h1{font-size: 27px;padding: 0 0;}
	   .dieticians{text-align:center;padding:0;}
	   .medications {text-align: center;margin:0;}
	   .Diabetes{background-repeat: repeat-y; background-position: center center;}
	   
	   .mobile-speciality h1.Book{font-size: 32px;}
	   .book_appintment p{font-size: 14px;}
	   input#radio {margin: 2% 0 3% 10%;}
	   .journey {background-repeat: repeat-y;background-position: center center;padding: 1em 0;margin: 10% 0 0 0;}
	   select#Location{width: 92%;margin: 3% 4%;}
	   .knows_diabetes h1 {font-size: 30px;}
	   .Exercise {padding: 0% 9%;}
	   .knows_diabetes .understands { height: auto;margin: 10px auto;}
	   .knows_diabetes p.read{width: 56%;margin: 6% 23%;}
	   .visual { margin: 8% 0% 8% 0%;padding: 0 10%;}
	   .shops .research{margin: 0 4% 0 1%;}
	   .shops .tracker_app{margin: 0 3% 0 1%;}
	   .footer ul{margin: 0; padding: 0;}
	   .footer-path { margin: 5% 0;padding: 2% 0 4% 0;}
	   .footer h1{padding: 0 5%;}
	   .footer p{padding: 0 5%;}
	   .footer ul li a{padding: 0 10%;}
	   .footer ul li{padding:0;}
	   .header{display:none;}
	   .mobile-header{display:block;}
	  .mohan-logo {width: 55%;margin: 3% 10%;}
	   .menu { padding: 1% 0 0 0%;}
	   .header_menus {padding: 0 2%;margin: 0 22%;max-width: 45%;}
	   section.Diabetes {padding: 1% 0 9% 0;}
	   .diet h2{font-size:20px;font-family:'HelveticaNeueLTStdMd';}
	   .diet p{font-size:15px;font-family:'HelveticaNeueLTStdMd';}
	   .slides { padding: 0% 0% 3% 0%;  margin: 1% 0 6% 0;}
	   .diet {padding: 0px 0 0 1px;margin: -9px 1px 5px 2px;}
	   .copyrights {padding: 0;text-align: center;}
	   .sbt{text-align:center;}
	   .journey button{margin: 2% 18% ;width: 69%;}
	   .amp-drop-down{top: 47px;left: 73px;width: 223px;}
	}
</style>
</head>
<body>
<amp-sidebar id='header-sidebar' side='left' layout='nodisplay'>
	 <nav>

		  <div class="controls" [class]="hidePrimary ? 'controls hide-parent' : 'controls'" aria-label="Menu controls">
			<a tabindex="0" role="button" aria-label="">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>                </a>
			<span class="truncate">Menu</span>
			<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close Menu">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                </a>
		  </div>

		<div class="main-menu items" role="menu" aria-label="Menu links" [class]="hidePrimary ? 'main-menu items hide-parent' : 'main-menu items'">

			<a class="link-container submenu-icon truncate  BebasNeueRegular fs-sm-18px" role="menuitem" tabindex="0" aria-label="New" aria-haspopup="true" on="tap:AMP.setState({level_1_0: !level_1_0, hidePrimary: !hidePrimary})" title="New"> Services </a>

				<div class="submenu hide-submenu" [class]="(level_1_0 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideSecondary ? 'hide-parent' : '')" role="menu" aria-label="New">
				<div class="controls" [class]="hideSecondary ? 'controls hide-parent' : 'controls'" aria-label="New controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_1_0: !level_1_0, hidePrimary: !hidePrimary})" aria-label="Return to Menu">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>                                </a>
						<span class="truncate">Services</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close New">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                </a>
				</div>

					  <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#" title="New">Diabetes Prevention</a>
					   <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Diabetes Diet</a>
					   <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Weight Loss</a>
					   <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#" title="New">Stress Management</a>
					   <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Precision Diabetest</a></li></a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Insulin Management</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Hypoglycemia</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Eye Care</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Foot Care</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Cardiac Care</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Kidney Care</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Diabetes Physiotherapy</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Orthopedic Care</a>
					   <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Surgeries</a>
					   <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Dental Care</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Pregnancy</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Fitness</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">Home Care</a>
					   <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#" title="New">Obesity Centre</a>
					   <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#" title="New">Insurance & Corporate Services</a>
					    <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="#"  title="New">International Patients</a>
				</div>
		
       
       
   
        <a class="link-container submenu-icon truncate BebasNeueRegular fs-sm-18px" role="menuitem" tabindex="0" aria-label="Brand Test" aria-haspopup="true" on="tap:AMP.setState({level_1_4: !level_1_4, hidePrimary: !hidePrimary})" title="Brand Test">Locate Clinics  </a>

        <div class="submenu hide-submenu" [class]="(level_1_4 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideSecondary ? 'hide-parent' : '')" role="menu" aria-label="Brand Test">
			<div class="controls" [class]="hideSecondary ? 'controls hide-parent' : 'controls'" aria-label="Brand Test controls">
            <a tabindex="0" role="button" on="tap:AMP.setState({level_1_4: !level_1_4, hidePrimary: !hidePrimary})" aria-label="Return to Menu">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>                                </a>
					<span class="truncate">Locate Clinics</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close Brand Test">
                           <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                               
						</a>
			</div>

			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="A-B" aria-haspopup="true" on="tap:AMP.setState({level_2_0: !level_2_0, hideSecondary: !hideSecondary})" title="A-B">
            Andhra Pradesh</a>

			<div class="submenu hide-submenu" [class]="(level_2_0 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="A-B">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="A-B controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_0: !level_2_0, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>                 
					</a>
					<span class="truncate"> Andhra Pradesh </span>
					<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close A-B">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                          
					</a>
                </div>

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Armani">Rajahmundry </a>                  
                                                                                                                            
																														
                <a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Armani"> Vijayawada </a>
                                                                                                                                                    

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Armani">isakhapatnamh  </a>
                                                                                                                                                   


			</div>
            
			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="C-F" aria-haspopup="true" on="tap:AMP.setState({level_2_1: !level_2_1, hideSecondary: !hideSecondary})" title="C-F">Odisha</a>
                                                                            

			<div class="submenu hide-submenu" [class]="(level_2_1 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="C-F">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="C-F controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_1: !level_2_1, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate"> Odisha </span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close C-F">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                            
						</a>  
				</div>

					<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Channel">Bhubaneswar </a> 
			</div>

			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="H-N" aria-haspopup="true" on="tap:AMP.setState({level_2_2: !level_2_2, hideSecondary: !hideSecondary})" title="H-N">
                                                                               Chennai</a>

			<div class="submenu hide-submenu" [class]="(level_2_2 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="H-N">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="H-N controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_2: !level_2_2, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate">Chennai</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close H-N">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                           
						</a>
				</div>

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Anna Nagar">Anna Nagar</a>
																																						 

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="HUGO">Avadi</a> 
																																					  

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Lacoste">Gopalapuram  </a>
																																					  

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Lacoste">OMR – Karapakkam  </a>
																																					

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Levis">Porur  </a>
																																						 

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Levis">Selaiyur  </a>    
																																				  

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Levis">Tambaram   </a>   
																																				 
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Levis">Vadapalani</a>   
																																			  
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Levis">Velachery  </a>	   
																																																										
																										
			</div>

			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="P-Z" aria-haspopup="true" on="tap:AMP.setState({level_2_3: !level_2_3, hideSecondary: !hideSecondary})" title="P-Z">
               Delhi </a>

			<div class="submenu hide-submenu" [class]="(level_2_3 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="P-Z">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="P-Z controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_3: !level_2_3, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate">Delhi </span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close P-Z">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                            </a>
				</div>

          

			</div>
			
			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="H-N" aria-haspopup="true" on="tap:AMP.setState({level_2_4: !level_2_4, hideSecondary: !hideSecondary})" title="H-N">
                                                                              Karnataka</a>

			<div class="submenu hide-submenu" [class]="(level_2_4 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="H-N">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="H-N controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_4: !level_2_4, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate">Karnataka</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close H-N">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                           
						</a>
				</div>

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Anna Nagar">Indira Nagar (Bengaluru)</a>
																																						 

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="HUGO">JP Nagar (Bengaluru)</a> 
																																					  

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Lacoste">Malleswaram (Bengaluru)  </a>
																																					  

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Lacoste">Mysuru  </a>
																																					

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Levis">Whitefield (Bengaluru)  </a>
																																																
			</div>
            
			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="H-N" aria-haspopup="true" on="tap:AMP.setState({level_2_5: !level_2_5, hideSecondary: !hideSecondary})" title="H-N">
                                                                             Kerala</a>

			<div class="submenu hide-submenu" [class]="(level_2_5 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="H-N">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="H-N controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_5: !level_2_5, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate">Kerala</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close H-N">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                           
						</a>
				</div>

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Anna Nagar">Kochi</a>
																																						 

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="HUGO">Thiruvananthapuram</a> 
															
			</div>
			
			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="H-N" aria-haspopup="true" on="tap:AMP.setState({level_2_6: !level_2_6, hideSecondary: !hideSecondary})" title="H-N">
                                                                            Kolkata</a>

			<div class="submenu hide-submenu" [class]="(level_2_6 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="H-N">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="H-N controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_6: !level_2_6, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate">Kolkata</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close H-N">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                           
						</a>
				</div>

				<a class="link-container truncate" tabindex="0" href="/link" title="Kaikhali">Kaikhali</a>										
			</div>
			
			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="H-N" aria-haspopup="true" on="tap:AMP.setState({level_2_7: !level_2_7, hideSecondary: !hideSecondary})" title="H-N">
                                                                            Puducherry</a>

			<div class="submenu hide-submenu" [class]="(level_2_7 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="H-N">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="H-N controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_7: !level_2_7, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate">Puducherry</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close H-N">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                           
						</a>
				</div>									
			</div>
			
			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="H-N" aria-haspopup="true" on="tap:AMP.setState({level_2_8: !level_2_8, hideSecondary: !hideSecondary})" title="H-N">
                                                                           Tamil Nadu</a>

			<div class="submenu hide-submenu" [class]="(level_2_8 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="H-N">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="H-N controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_8: !level_2_8, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate">Tamil Nadu</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close H-N">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                           
						</a>
				</div>

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Chunampet</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Coimbatore</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Gudiyatham</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Kancheepuram</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Madurai</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Salem</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Thanjavur</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Tiruchirappalli (Trichy)</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Thoothukudi</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Vellore</a>										
			</div>
			
			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="H-N" aria-haspopup="true" on="tap:AMP.setState({level_2_9: !level_2_9, hideSecondary: !hideSecondary})" title="H-N">
                                                                           Telanganau</a>

			<div class="submenu hide-submenu" [class]="(level_2_9 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="H-N">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="H-N controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_2_9: !level_2_9, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate">Telangana</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close H-N">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                           
						</a>
				</div>

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Dilsukh Nagar (Malakpet</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">A.S.Rao Nagar (Hyderabad)</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Domalguda (Hyderabad)</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Jubilee Hills (Hyderabad)</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Kukatpally (Hyderabad)</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Secunderabad</a>										
				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Tolichowki (Hyderabad)</a>																		
			</div>
			
			<a class="link-container submenu-icon truncate HelveticaNeueLTStdLt fs-sm-16px" role="menuitem" tabindex="0" aria-label="H-N" aria-haspopup="true" on="tap:AMP.setState({level_3_0: !level_3_0, hideSecondary: !hideSecondary})" title="H-N">
                                                                          Uttar Pradeshu</a>

			<div class="submenu hide-submenu" [class]="(level_3_0 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideTertiary ? 'hide-parent' : '')" role="menu" aria-label="H-N">
				<div class="controls" [class]="hideTertiary ? 'controls hide-parent' : 'controls'" aria-label="H-N controls">
					<a tabindex="0" role="button" on="tap:AMP.setState({level_3_0: !level_3_0, hideSecondary: !hideSecondary})" aria-label="Return to Brand Test">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg></a>
						<span class="truncate">Uttar Pradesh</span>
						<a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close H-N">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                           
						</a>
				</div>

				<a class="link-container truncate HelveticaNeueLTStdLt fs-sm-16px" tabindex="0" href="/link" title="Kaikhali">Diabetes Centre in Lucknow</a>																												
			</div>

        </div>

        <a class="link-container submenu-icon truncate BebasNeueRegular fs-sm-18px" role="menuitem" tabindex="0" aria-label="Sale" aria-haspopup="true" on="tap:AMP.setState({level_1_5: !level_1_5, hidePrimary: !hidePrimary})" title="Sale">
                                                       About Us                      </a>

        <div class="submenu hide-submenu" [class]="(level_1_5 ? 'submenu show-submenu' : 'submenu hide-submenu') + ' ' + (hideSecondary ? 'hide-parent' : '')" role="menu" aria-label="Sale">
          <div class="controls" [class]="hideSecondary ? 'controls hide-parent' : 'controls'" aria-label="Sale controls">
            <a tabindex="0" role="button" on="tap:AMP.setState({level_1_5: !level_1_5, hidePrimary: !hidePrimary})" aria-label="Return to Menu">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>                                </a>
            <span class="truncate">About Us</span>
            <a tabindex="0" role="button" on='tap:header-sidebar.toggle' aria-label="Close Sale">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>                                </a>
          </div>

			<a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="/link" title="Sale">
                                    Growth Story                                </a>
			<a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="/link" title="Sale">
                                    Recognition                              </a>
			<a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="/link" title="Sale">
                                   Testimonial                             </a>
			<a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="/link" title="Sale">
                                   Partner institutions                           </a>
			<a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="/link" title="Sale">
                                   Research                             </a>
			<a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="/link" title="Sale">
                                   CSR                             </a>
			<a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="/link" title="Sale">
                                 Homage                            </a>								   
            <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="/link" title="Sale">
                                 Careers                          </a>								   
            <a class="link-container view-all truncate HelveticaNeueLTStdLt fs-sm-16px" href="/link" title="Sale">
                               Investor Info                         </a>								   
                                                                               
        </div>

        <a class="link-container submenu-icon-defeat truncate BebasNeueRegular fs-sm-18px" role="menuitem" tabindex="0" aria-label="Sale" aria-haspopup="true" on="tap:AMP.setState({level_1_6: !level_1_6, hidePrimary: !hidePrimary})" title="Sale">
        Defeat Diabetes  </a>
													  
        <a class="link-container submenu-icon-defeat truncate BebasNeueRegular fs-sm-18px" role="menuitem" tabindex="0" aria-label="Sale" aria-haspopup="true" on="tap:AMP.setState({level_1_6: !level_1_6, hidePrimary: !hidePrimary})" title="Sale">
       News and Events  </a>
        
     </div>
    </nav>
  
  <!-- end nav -->
</amp-sidebar>

        <div class="mobile-header">
		    <div class="amp-container">
				<div class="col-lg-12 col-md-12 col-12 clinics">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-12">
							<a href="https://drmohans.com"><amp-img src="<?php echo get_template_directory_uri();?>/images/logo-new.png" width="194" height="43" layout="responsive" class="mohan-logo"
								alt="a sample image"></amp-img></a>
						</div>
						<div class="col-lg-7 col-md-7 col-12 menu">
							<div class="lists"> 
							
   

					  <header class="headerbar">
						<div class="hamburger" role="button" aria-label="mobile menu" on="tap:header-sidebar.toggle" tabindex="0">
						  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 6h-24v-4h24v4zm0 4h-24v4h24v-4zm0 8h-24v4h24v-4z"/></svg>
						</div>
					<div class="site-name">MENU</div> 

							
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-12 header_menus ">
								<p class="BebasNeueRegular fs-sm-18px"><a href="#10">Book Appointment</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
        
		
		<section class="slide">
		   
				<amp-carousel id="carousel-with-preview" width="1366" height="650" layout="responsive" type="slides">
				<amp-img src="<?php echo get_template_directory_uri();?>/images/banner-b (1).jpg" width="1366" height="650" layout="responsive" alt="a sample image"></amp-img>
				<amp-img src="<?php echo get_template_directory_uri();?>/images/bannerc (1).jpg" width="1366" height="650" layout="responsive"
				alt="a sample image"></amp-img>
				<amp-img src="<?php echo get_template_directory_uri();?>/images/banner-e (1).jpg" width="1366" height="650" layout="responsive"
				alt="a sample image"></amp-img>
				<amp-img src="<?php echo get_template_directory_uri();?>/images/banner-new-4.jpg" width="1366" height="650" layout="responsive"
				alt="a sample image"></amp-img>
				</amp-carousel>
		</section>
		
		<div class="header">
		    <div class="amp-container">
				<div class="col-lg-12 col-md-12 col-12 clinics">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-12">
							<a href="https://drmohans.com"><amp-img src="<?php echo get_template_directory_uri();?>/images/logo-new.png" width="194" height="43" layout="responsive" class="mohan-logo"
								alt="a sample image"></amp-img></a>
						</div>
						<div class="col-lg-7 col-md-7 col-12 menu">
							<div class="lists"> 
							<ul class="main-navigation">
								<li class="BebasNeueRegular fs-sm-18px "><a href="#">Services</a>
								    <ul>
									    <div class="row" style="width: 524px;">
											<div class="col-3"  style="display: inline-block;">
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Diabetes Prevention</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px" ><a href="#">Diabetes Diet</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Weight Loss</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Stress Management</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Precision Diabetest</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Insulin Management</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Hypoglycemia</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Eye Care</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Eye Care</a></li>
											</div>
											<div class="col-3" style="display: inline-block;">
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Foot Care</a></li>		<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Cardiac Care</a>
												</li>								
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Kidney Care</a>
												</li>								
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Diabetes Physiotherapy</a></li>								
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Orthopedic Care</a></li>								
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Surgeries</a>
												</li>								
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Dental Care</a>
												</li>								
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Pregnancy</a></li>								
											</div>
											<div class="col-3"  style="display: inline-block;">
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Fitness</a></li>	
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Home Care</a></li>	
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Obesity Centre</a></li>	
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Insurance & Corporate Services</a></li>	
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">International Patients</a></li>	
											</div>
										</div>
									    
                                    </ul>
								</li>
								<li class="BebasNeueRegular fs-sm-18px "><a href="#">Locate Clinics</a>
								    <ul>
									    <li class="HelveticaNeueLTStdLtr fs-sm-16px"><a href="#">Andhra Pradesh</a>
											<ul>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Rajahmundry</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Vijayawada</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Visakhapatnamh</a></li>
											</ul>
										</li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Odisha</a>
											<ul>
											    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Bhubaneswar</a></li>
											</ul>
										</li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Chennai</a>
											<ul>
											     <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Anna Nagar</a></li>
											     <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Avadi</a></li>
											     <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Gopalapuram</a></li>
											     <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">OMR – Karapakkam</a></li>
											     <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Porur</a></li>
											     <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Selaiyur</a></li>
											     <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Tambaram</a></li>
											     <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Vadapalani</a></li>
											     <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Velachery</a></li>
											</ul>
										</li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Delhi</a></li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Karnataka</a>
										    <ul>
												 <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Indira Nagar (Bengaluru)</a></li>
												 <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">JP Nagar (Bengaluru)</a></li>
												 <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Malleswaram (Bengaluru)</a></li>
												 <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Mysuru</a></li>
												 <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Whitefield (Bengaluru)</a></li>
											</ul>
										</li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Kerala</a>
										    <ul>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Kochi</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Thiruvananthapuram</a></li>
											</ul>
										</li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Kolkata</a>
										    <ul>
											    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Kaikhali</a></li>
											    
											</ul>
										</li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Puducherry</a></li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Tamil Nadu</a>
											<ul>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Chunampet</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Coimbatore</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Erode</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Gudiyatham</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Kancheepuram</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Madurai</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Salem</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Thanjavur</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Tiruchirappalli (Trichy)</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Thoothukudi</a></li>
												<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Vellore</a></li>
											</ul>
										</li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Telangana</a>
											<ul>
											    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Dilsukh Nagar (Malakpet)</a></li>
											    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">A.S.Rao Nagar (Hyderabad)</a></li>
											    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Domalguda (Hyderabad)</a></li>
											    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Jubilee Hills (Hyderabad)</a></li>
											    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Kukatpally (Hyderabad)</a></li>
											    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Secunderabad</a></li>
											    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Tolichowki (Hyderabad)</a></li>
											</ul>
										</li>
										<li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Uttar Pradesh</a>
											<ul>
												
											</ul>
										</li>
										
											
										
									</ul>
								</li>
								<li class="BebasNeueRegular fs-sm-18px "><a href="#">About Us</a>
								    <ul>
									    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Growth Story</a></li>
									    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Recognition</a></li>
									    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Testimonial</a></li>
									    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Partner institutions</a></li>
									    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Research</a></li>
									    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">CSR</a></li>
									    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Homage</a></li>
									    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Careers</a></li>
									    <li class="HelveticaNeueLTStdLt fs-sm-16px"><a href="#">Investor Info</a></li>
									</ul>
								</li>
								<li class="BebasNeueRegular fs-sm-18px "><a href="#">Defeat Diabetes</a></li>
								<li class="BebasNeueRegular fs-sm-18px "><a href="#">News and Events</a></li>
								
								
							</ul>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-12 header_menus ">
								<p class="BebasNeueRegular fs-sm-18px"><a href="#10">Book Appointment</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<section class="expert-care">
		    <div class="amp-container">
			    <div class="col-lg-12 col-md-12 col-sm-12 supported">
				    <div class="row">
					    <div class="col-lg-6 col-md-6 col-12 video">
						    <amp-youtube width="480"  height="270" layout="responsive" data-videoid="AVLTUptkHK0"></amp-youtube>
					
						</div>
						<div class="col-lg-6 col-md-6 col-12 experts">
						    <h1 class="HelveticaNeueLTStdRoman fs-sm-42px">Expert care <br> at your service</h1>
							<p class="HelveticaNeueLTStdLt fs-sm-18px">We have the country’s most skilled and experienced clinical and paraclinical personnel ably supported by state-of-the-art infrastructure to extend personalised services, expert advice on nutrition and diabetic products. At Dr Mohan’s, we understand every patient is unique and, therefore, we are committed to providing you with customised treatment solutions for your varied needs.</p>
							<h3 class="HelveticaNeueLTStdRoman fs-sm-21px"><a href="https://drmohans.com/videos-page/">Watch how we care</a></h3>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="Diabetes">
		    <div class="amp-container">
			    <h1 class="HelveticaNeueLTStdTh fs-sm-46px">Helping you know your Diabetes</h1>
					<amp-carousel id="carousel" width="1366" height="650" layout="intrinsic" type="slides" >
						<div class="col-lg-12 col-md-12 col-sm-12 slides">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Diabetes-Diet.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="https://drmohans.com/patient-care/diabetes-diet/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">Diabetes Diet</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">Our in-house nutritionists and dieticians help you maintain the right weight and food habits. Their services go hand in hand with the medications to help you achieve better results.</p>
											</div>
										</div>
								    </div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/fitnessIcon.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="https://drmohans.com/patient-care/fitness/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">Fitness</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We provide fitness training and counselling to help patients manage insulin levels, lower blood pressure and reduce stress.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Stress-Management.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="https://drmohans.com/patient-care/stress-management"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">Stress Management</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We undertake psychological evaluation and conduct patient-education, skills-development and motivation through counseling, as well as relaxation techniques.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
					
				
							<div class="col-lg-12 col-md-12 col-sm-12 testing">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-12 diet">
										<div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Precision-Diabetes.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<a href="https://drmohans.com/precision-diabetes"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">Precision Diabetes</h2></a>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We customize health care by tailoring testing, decisions and treatments to the individual.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Diabetes-Preventive-Care.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<a href="https://drmohans.com/patient-care/preventive-care/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px"> Preventive Care</h2></a>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">With regular check-ups and timely intervention, we help you prevent diabetes and the complications due to diabetes.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Weight-Loss.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<a href="https://drmohans.com/patient-care/weight-loss/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px"> Weight Loss</h2></a>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We assist you in achieving your optimal body weight, thereby preventing the development of diabetes and making control easier if you have already been diagnosed with diabetes.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					    </div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 slides">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/eyeIcon.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="https://drmohans.com/patient-care/diabetes-eye-care"><h2 class="HelveticaNeueLTStdMd fs-sm-22px"> Eye Care</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">Our retinal specialists ensure that your eyes are protected from the ill-effects of diabetes</p>
											</div>
										</div>
								    </div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Foot-Care.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="https://drmohans.com/patient-care/diabetes-footcare"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">Foot Care</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We provide comprehensive facilities for early detection and  treatment of diabetic foot complications</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/kidneyIcon.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="https://drmohans.com/patient-care/kidney-care/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">Kidney Care</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We ensure that your kidneys remain in good health through prevention, early detection and prompt treatment of diabetic kidney disease.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
					
				
							<div class="col-lg-12 col-md-12 col-sm-12 testing">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-12 diet">
										<div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/cardiacIcon.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<a href=" https://drmohans.com/patient-care/diabetes-cardiac-care/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">Cardiac Care</h2></a>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We provide preventive, diagnostic and therapeutic services for heart disease due to diabetes.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Surgeries.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<a href="https://drmohans.com/patient-care/surgeries"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">  Surgeries & Treatments</h2></a>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">With timely check-ups and specific treatment options for every part of your body, we help you manage diabetes with ease.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Orthopaedic.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<a href="https://drmohans.com/patient-care/orthopedic"><h2 class="HelveticaNeueLTStdMd fs-sm-22px"> Orthopedic Care</h2></a>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">Right from mobilisation of joints to surgeries and preventive care, our orthopedic specialists offer expert treatment for bone and joint problems.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					    </div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 slides">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Pregnancy---Gestational-Diabetes.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="https://drmohans.com/patient-care/pregnancy/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px"> Pregnancy</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We ensure the best possible outcome for your pregnancy through a multidisciplinary approach to achieve optimal blood glucose control.</p>
											</div>
										</div>
								    </div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Physiotherapy.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="https://drmohans.com/patient-care/diabetes-physiotherapy/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">Physiotherapy</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">Our therapists are well-acquainted with promoting mobility of joints and tissues for increased circulation and strength conditioning.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Home-Care.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="https://drmohans.com/home-care"><h2 class="HelveticaNeueLTStdMd fs-sm-22px"> Home Care</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We put the well-being of our patients at the centre of everything we do. Get the best treatment services from the comfort of your home.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
					
				
							<div class="col-lg-12 col-md-12 col-sm-12 testing">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-12 diet">
										<div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/lab.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<a href="https://drmohans.com/patient-care/diabetes-laboratory/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px"> Lab</h2></a>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">Our state-of-the-art labs are equipped with cutting edge tchnology to deliver accurate results.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Insurance-&-Corporate-Services.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<a href="https://drmohans.com/corporate-and-insurance-services/"><h2 class="HelveticaNeueLTStdMd fs-sm-22px">Insurance & Corporate Services</h2></a>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We offer exclusive benefits tailor-made for Corporate Employees and Insurance options created for your needs.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/International-Patients.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<a href="https://drmohans.com/international-patients"><h2 class="HelveticaNeueLTStdMd fs-sm-22px"> International Patients</h2></a>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We extend a seamless service right to our overseas patients right from the first enquiry to the post-treatment follow-up.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					    </div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 slides">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 diet">
									<div class="col-12">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Shop-Online.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<a href="http://diabetes.ind.in/" target="_blank"><h2 class="HelveticaNeueLTStdMd fs-sm-22px"> Shop Online</h2></a>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">From special dietary needs to orthopedic footwear that is beneficial and fashionable, all our products are available online with easy payment and delivery options.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				    </amp-carousel>	
			</div>
		</section>
		
		<section class="mobile-speciality">
		    <div class="amp-container">
			    <h1 class="HelveticaNeueLTStdTh fs-sm-46px">4,50,000+ Patients Trust Us</h1>
				    <div class="col-lg-12 col-md-12 col-sm-12">
					    <div class="row">
						    <div class="col-lg-8 col-md-8 col-12 service">
								<div class="col-lg-12 col-md-12 col-sm-12 management">
								    <div class="row">
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon1.png" width="162" height="103" layout="intrinsic" class="Patients" ></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">More than 25 Years of Service in prevention and  management of  diabetes</p>
										</div>
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon2.png" width="162" height="103" layout="intrinsic" class="Patients" ></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">ONLY diabetes care centre to have an exclusive app for diabetes management</p>
										</div>
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon3.png" width="162" height="103" layout="intrinsic"  class="Patients" ></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">State of art facilities comparable to the best in the world</p>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 Centres">
								    <div class="row">
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon4-1.png" width="162" height="103" layout="intrinsic"  class="Patients"></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">40+ Diabetes Care Centres across India</p>
										</div>
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon5.png" width="162" height="103" layout="intrinsic"  class="Patients"></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">Exclusive range of Diabetes Health Products</p>
										</div>
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon6.png" width="162" height="103" layout="intrinsic"  class="Patients"></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">Customized  Diabetes  treatment</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-12 form">
							    <div class="spacing mt-50 mt-xs-20 mb-50 mt-xs-20">
								    <div class="book_appintment">
										<div class="gravity_holder">
											<h1 class="HelveticaNeueLTStdBd fs-sm-22px Book">Book an Appointment</h1>
											<p class="HelveticaNeueLTStdBd fs-sm-15px">Let's get you set up with an appointment</p>

											<form action="https://drmohans.com/sendtogravityamp.php" method="GET" target="_top" id="10">
												<input type="text" name="Name" id="idEmail" class="emailfield" required pattern="[a-zA-Z][a-zA-Z\s]*" placeholder="Name">
												<span visible-when-invalid="valueMissing" validation-for="idEmail">Your Name please!</span>
												<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Name!</span>
												<input type="email" name="Email" id="idEmail" class="emailfield" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email">
												<span visible-when-invalid="valueMissing" validation-for="idEmail">Your email please!</span>
												<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Email!</span>
												<input type="text" name="Phone" id="idEmail" class="emailfield" required pattern="\d{10}" placeholder="Phone">
												<span visible-when-invalid="valueMissing" validation-for="idEmail">Your Number please!</span>
												<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Number!</span>
												<label for="Location"></label>
													<select name="Location" id="Location-list">
														<option value="volvo">Location</option>
														<option value="Gopalapuram - Chennai">Gopalapuram - Chennai</option>
														<option value="Anna Nagar - Chennai">Anna Nagar - Chennai</option>
														<option value="Avadi - Chennai">Avadi - Chennai</option>
														<option value="Tambaram - Chennai">Tambaram - Chennai</option>
														<option value="Karapakkam - Chennai">Karapakkam - Chennai</option>
														<option value="Vadapalani - Chennai">Vadapalani - Chennai</option>
														<option value="Velachery - Chennai">Velachery - Chennai</option>
														<option value="Porur - Chennai">Porur - Chennai</option>
														<option value="Selaiyur - Chennai">Selaiyur - Chennai</option>
														<option value="Coimbatore">Coimbatore</option>
														<option value="Madurai">Madurai</option>
														<option value="Gudiyatham - Vellore">Gudiyatham - Vellore</option>
														<option value="Thanjavur">Thanjavur</option>
														<option value="Tuticorin">Tuticorin</option>
														<option value="Chunampet">Chunampet</option>
														<option value="Kancheepuram">Kancheepuram</option>
														<option value="Pondicherry">Pondicherry</option>
														<option value="Domalguda,Indira Park Road - Hyderabad">Domalguda,Indira Park Road - Hyderabad</option>
														<option value="Jubilee Hills - Hyderabad">Jubilee Hills - Hyderabad</option>
														<option value="Kukatpally - Hyderabad">Kukatpally - Hyderabad</option>
														<option value="Dilsukh Nagar - Hyderabad">Dilsukh Nagar - Hyderabad</option>
														<option value="Secunderabad - Hyderabad">Secunderabad - Hyderabad</option>
														<option value="Tolichowki - Hyderabad">Tolichowki - Hyderabad</option>
														<option value="A.S.Rao Nagar - Hyderabad">A.S.Rao Nagar - Hyderabad</option>
														<option value="Vijayawada - Andhra Pradesh">Vijayawada - Andhra Pradesh</option>
														<option value="Visakhapatnam - Andhra Pradesh">Visakhapatnam - Andhra Pradesh</option>
														<option value="Bhubaneswar - Odisha">Bhubaneswar - Odisha</option>
														
														<option value="Delhi - Kirti Nagar">Delhi - Kirti Nagar</option>
														<option value="Indira Nagar - Bengaluru">Indira Nagar - Bengaluru</option>
														<option value="JP Nagar - Bengaluru">JP Nagar - Bengaluru</option>
														<option value="Malleshwaram - Bengaluru">Malleshwaram - Bengaluru</option>
														<option value="Whitefield - Bengaluru">Whitefield - Bengaluru</option>
														
														<option value="Mysuru">Mysuru</option>
														<option value="Trivandrum">Trivandrum</option>
														<option value="Kochi">Kochi</option>
														<option value="Kaikhali - Kolkata - West Bengal">Kaikhali - Kolkata - West Bengal</option>
													</select>
												<amp-date-picker id="simple-date-picker" type="single" mode="overlay"
												layout="container" on="select:AMP.setState({date1: event.date, dateType1: event.id})" format="YYYY-MM-DD" open-after-select input-selector="[name=date1]"
												class="example-picker space-between">
												<div class="icon-input"></div>
												<input name="date1" placeholder="Preferred Date">
												<template type="amp-mustache" info-template>
												<span [text]="date1 != null ? 'You picked ' + date1 + '.' : 'You will see your chosen date here.'">You will see your chosen date here.</span>
												</template>
												</amp-date-picker>
												<input type="radio" id="radio" name="Patient Type" value="New Patient" >
												<label for="New Patient" class="HelveticaNeueLTStdLt fs-sm-15px">New Patient</label>
												<input type="radio" id="radio" name="Patient Type" value="Review Patient">
												<label for="Review Patient"  class="HelveticaNeueLTStdLt fs-sm-15px">Review Patient</label>
												<input type="text" name="Name" id="idComments" class="Comments" required pattern="[a-zA-Z][a-zA-Z\s]*" placeholder="Comments">
											   
												<input type="hidden" name="URL" value="<?php  echo $actual_link; ?>" />
												<div class="tc pos"> 
													<input type="submit" value="Submit" id="idSubmit" class="submit">
												</div>
										   </form>
										</div>
									</div>
                                </div>
							</div>
						</div>
					</div>
			</div>
		</section>
		
		<section class="journey">
		    <div class="amp-container">
			    <div class="col-lg-12 col-md-12 col-sm-12">
				    <div class="row">
						<div class="col-lg-5 col-md-5 col-12 ">
						</div>
						<div class="col-lg-7 col-md-7 col-12 whcolor">
						    <h1 class="HelveticaNeueLTStdMd fs-sm-33px">Want to learn more about <br>
							treatment and prevention of diabetes? <br>
							Learn more.</h1>
						</div>
			        </div>
			    </div> 

                <div class="col-lg-12 col-md-12 col-12">
				    <div class="row">
					    <div class="col-lg-5 col-md-5 col-12">
						</div>
						<div class="col-lg-7 col-md-7 col-12">
							<button on="tap:AMP.setState({visible: !visible})">Choose Your Journey</button>
							<p [class]="visible ? 'show' : 'hide'" class="hide amp-drop-down">
								<a href="https://drmohans.com/screening/" class="text-decoration-none">Should you get Screened?</a>
								<a href="https://drmohans.com/category/blogs/" class="text-decoration-none">Defeat Diabetes</a>
								<a href="https://drmohans.com/home-care/" class="text-decoration-none">Home care Service</a>
							</p>
						    <!--select name="Location" id="Location">
								<option value="Choose Your Journey" class="choose">Choose Your Journey</option>
								<option value="https://drmohans.com/screening/" class="Should you get Screened?">Should you get Screened?</option>
								<option value="https://drmohans.com/category/blogs/" class="Defeat Diabetes">Defeat Diabetes</option>
								<option value="https://drmohans.com/precision-diabetes/" class="Precision Diabetes>Precision Diabetes</option>
								<option value="https://drmohans.com/home-care/" class="Home care Service">Home care Service</option>
							</select-->
						</div>
					</div>
                </div>				
			</div>     
		</section>
		
		<section class="knows_diabetes">
		    <div class="amp-container">
			    <h1 class="HelveticaNeueLTStdTh fs-sm-46px lbl-title">Understand Your Diabetes</h1>
				    <div class="col-lg-12 col-md-12 col-12 Exercise ">
					    <div class="row">
						    <div class="col-lg-3 col-md-3 col-12 understands ">
							    <h2 class="HelveticaBoldFont fs-sm-22px">Prevention and Management of Diabetes</h2>
								<p class="HelveticaNeueLTStdLt fs-sm-18px">To prevent diabetes-related visual impairment.</p>
								<span class="HelveticaLight fs-sm-18px"><a href="https://drmohans.com/prevention-and-management-of-diabetes/" class="fs-18 Helvetica_Light focolor hover-border">Read More</a></span>
							</div>
							<div class="col-lg-3 col-md-3 col-12 understands ">
							    <h2 class="HelveticaBoldFont fs-sm-22px">Control Diabetes – Easy through HbA1c testing</h2>
								<p class="HelveticaNeueLTStdLt fs-sm-18px">HbA1c test is a simple and effective tool in diabetes monitoring.</p>
								<span class="HelveticaLight fs-sm-18px"><a href="https://drmohans.com/control-diabetes-it-is-easy-through-hba1c-testing/" class="fs-18 Helvetica_Light focolor hover-border">Read More</a></span>
							</div>
							<div class="col-lg-3 col-md-3 col-12 understands ">
							    <h2 class="HelveticaBoldFont fs-sm-22px">Importance of Exercise for Diabetes</h2>
								<p class="HelveticaNeueLTStdLt fs-sm-18px">Exercise delays the onset of diabetes, helps in control of diabetes….</p>
								<span class="HelveticaLight fs-sm-18px"><a href="https://drmohans.com/exercise-for-diabetes/" class="fs-18 Helvetica_Light focolor hover-border">Read More</a></span>
							</div>
						</div>
						    <p class="read HelveticaNeueLTStdBd fs-m-18px"><a href="https://drmohans.com/category/blogs" class="whcolor">READ ALL BLOGS</a></p>
					</div>
			</div>
		</section>
		
		<section class="shops">
		    <div class="amp-container">
			    <div class="col-lg-12 col-md-12 col-12 visual  ">
				    <div class="row">
					    <div class="col-lg-3 col-md-3 col-12 research">
				            <div class="borders_stuff ">
						        <h2 class="HelveticaBoldFont fs-sm-6px">Research</h2>
							    <p class="HelveticaNeueLTStdBd fs-sm-18px"><a href="https://drmohans.com/research" class="whcolor">Know More</a></p>
							</div>	
						</div>
						<div class="col-lg-3 col-md-3 col-12 cart">
				            <div class="borders_stuff ">
						        <h2 class="HelveticaBoldFont fs-sm-26px">Shop</h2>
							    <p class="HelveticaNeueLTStdBd fs-sm-18px"><a href="http://diabetes.ind.in/" class="whcolor">Shop Online</a></p>
							</div>	
						</div>
						<div class="col-lg-3 col-md-3 col-12 tracker_app">
				            <div class="borders_stuff ">
						        <h2 class="HelveticaBoldFont fs-sm-26px">Diabetes App</h2>
							    <p class="HelveticaNeueLTStdBd fs-sm-18px">Know More</p>
							</div>	
						</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="footer">
		    <div class="amp-container">
			    <div class="col-lg-12 col-md-12 col-12 footer-path">
				    <div class="row">
					    <div class="col-1g-4 col-md-4 col-12 main-center">
						    <h1 class="BebasNeueBold fs-sm-24px">Main centre address</h1>
							<p class="HelveticaNeueLTStdLt fs-sm-14px">No.6-B, Conran Smith Road,</p>
							<p class="HelveticaNeueLTStdLt fs-sm-14px">Near Satyam Cinemas, Gopalapuram,</p>
							<p class="HelveticaNeueLTStdLt fs-sm-14px">Chennai, Tamil Nadu 600086</p>
							<p class="HelveticaNeueLTStdLt fs-sm-14px">Chennai, Tamil Nadu 600086</p>
							<p class="HelveticaNeueLTStdLt fs-sm-14px">Email :<a href="mailto:appointments-gop@drmohans.com"> appointments-gop@drmohans.com</a></p>
						</div>
						<div class="col-lg-3 col-md-3 col-12 quik-links">
							<h1 class="BebasNeueBold fs-sm-24px">quick links</h1>
							<ul>
							    <li><a href="https://drmohans.com/home-care/" class="hover-border HelveticaNeueLTStdLt fs-sm-14px ">Home Care</a></li>
							    <li><a href="https://drmohans.com/patient-care/diabetes-preventive-care/" 
								class="hover-border HelveticaNeueLTStdLt fs-sm-14px">Diabetes Prevention</a></li>
							    <li><a href="https://drmohans.com/patient-care/diabetic-diet/" class="hover-border HelveticaNeueLTStdLt fs-sm-14px">Diabetes Diet</a></li>
							    <li><a href="https://drmohans.com/patient-care/weight-loss/" class="hover-border HelveticaNeueLTStdLt fs-sm-14px">Weight Loss</a></li>
							    <li><a href="https://drmohans.com/patient-care/stress-management/" class="hover-border HelveticaNeueLTStdLt fs-sm-14px">Stress Management</a></li>
							    <li><a href="https://drmohans.com/precision-diabetes/" class="hover-border HelveticaNeueLTStdLt fs-sm-14px">Precision Diabetes</a></li>
							</ul>
						</div>
						<div class="col-lg-3 col-md-3 col-12 quik-links">
						    <h1 class="BebasNeueBold fs-sm-24px">Locate Clinics</h1>
							    <ul>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/tamilnadu/" class="hover-border">Clinics in Tamil Nadu</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/chennai/" class="hover-border">Clinics in Chennai</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/karnataka/" class="hover-border">Clinics in Karnataka</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/andhra-pradesh/" class="hover-border">Clinics in Andhra Pradesh</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/pondicherry/" class="hover-border">Clinics in Puducherry</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/odisha/" class="hover-border">Clinics in Odisha</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/delhi/" class="hover-border">Clinics in Delhi</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/kerala/" class="hover-border">Clinics in Kerala</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/telangana/" class="hover-border">Clinics in Hyderabad</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/uttar-pradesh/" class="hover-border">Clinics in Uttar Pradesh</a></li>
								    <li class="HelveticaNeueLTStdLt fs-sm-14px"><a href="https://drmohans.com/diabetes-centre-locations/kolkata/" class="hover-border">Clinic in Kolkata</a></li>
								</ul>
						</div>
						<div class="col-lg-2 col-md-2 col-12">
						    <h1 class="BebasNeueBold fs-sm-24px">Connect with us</h1>
								<ul class="socials">
								<li><a href="https://www.facebook.com/drmohansdiabetesinstitutions" target="_blank">
								<amp-img src="<?php echo get_template_directory_uri();?>/images/FaceBook.png" width="25" height="40" layout="intrinsic"  class="Patients"></amp-img></a></li>
								<li><a href="https://twitter.com/dmdsc" target="_blank"><amp-img src="<?php echo get_template_directory_uri();?>/images/Twitter.png" width="35" height="40" layout="intrinsic"  class="Patients"></amp-img></a></li>
								<li><a href="https://www.youtube.com/user/DrMohansGroup" target="_blank"><amp-img src="<?php echo get_template_directory_uri();?>/images/youtube.png" width="30" height="30" layout="intrinsic"  class="Patients"></amp-img></a></li>
								</ul>
								    
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="last-section">
			<div class="amp-container">
			    <div class="col-lg-12 col-md-12 col-12 last-part">
				    <div class="row">
					    <div class="col-lg-4 col-md-4 col-12 copyrights">
							<p class="HelveticaNeueLTStdLt fs-sm-14px">© Copyright 2019 Dr. Mohan's Diabetes Specialities Centre</p>
						</div>
						<div class="col-lg-4 col-md-4 col-12 terms">
						    <ul>
							    <li><a href="https://drmohans.com/contact-us/" class="hover-border HelveticaNeueLTStdLt fs-sm-14px">Contact Us</a>
								</li>
							</ul>
						</div>
						<div class="col-lg-4 col-md-4 col-12 sbt">
						    <p class="HelveticaNeueLTStdLt fs-sm-14px">Designed & developed by <a href="https://socialbeat.in" target="_blank" class="hover-border" rel="nofollow">Social Beat</a></p>
						</div>
					</div>
				</div>
			</div>
		</section>
</body>
</html>