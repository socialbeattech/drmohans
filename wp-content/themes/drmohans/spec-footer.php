<div style="display:none !important">
<?php gravity_form( 9, $display_title = false, $display_description = false, $tabindex, $ajax = true, $echo = true ); ?>
</div>
</div><!-- #content -->

	<footer class="fullwidth">
		<div class="fullwidth footer">
			<div class="container">
				<div class="row pt-4 pb-3">
					<div class="col-12 col-sm-4">
						<h1 class="col-12 col-sm-12 text-uppercase no-gutters">Main centre address</h1>
						<p>No.6-B, Conran Smith Road,</p>
						<p>Near Satyam Cinemas, Gopalapuram,</p>
						<p>Chennai, Tamil Nadu 600086</p>
						<p>Ph: 044 43968888</p>
						<p>Email : <a href="mailto:appointments-gop@drmohans.com">appointments-gop@drmohans.com</a></p>
					</div>
					<div class="col-12 col-sm-3">
						<h1 class="col-12 col-sm-12 text-uppercase no-gutters sm-mt-10">quick links</h1>	
						<ul class="col-12 col-sm-12 no-gutters list-none">	
							<li><a href="<?php echo get_home_url();?>/home-care/" class="hover-border">Home Care</a></li>
							<li><a href="<?php echo get_home_url();?>/patient-care/preventive-care/" class="hover-border"> Diabetes Prevention</a></li>
							<li><a href="<?php echo get_home_url();?>/patient-care/diabetes-diet/" class="hover-border">Diabetes Diet</a></li>
							<li><a href="<?php echo get_home_url();?>/patient-care/weight-loss/"class="hover-border">Weight Loss</a></li>
							<li><a href="<?php echo get_home_url();?>/patient-care/stress-management/" class="hover-border">Stress Management</a></li>
							<li><a href="<?php echo get_home_url();?>/precision-diabetes/" class="hover-border">Precision Diabetes</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-3">
						<h1 class="col-12 col-sm-12 text-uppercase no-gutters">Locate Clinics</h1>
							<ul class="col-12 col-sm-12 no-gutters list-none">
								<li><a href="<?php echo get_home_url();?>/locate-clinics/tamilnadu/" class="hover-border">Clinics in Tamil Nadu</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/chennai/" class="hover-border">Clinics in Chennai</a></li>
								<li><a href="<?php echo get_home_url();?>/locate-clinics/Karnataka" class="hover-border">Clinics in Karnataka</a></li>
								<li><a href="<?php echo get_home_url();?>/locate-clinics/andhra-pradesh/" class="hover-border">Clinics in Andhra Pradesh</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/pondicherry/" class="hover-border">Clinics in Puducherry</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/odisha/" class="hover-border">Clinics in Odisha</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/delhi/" class="hover-border">Clinics in Delhi</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/kerala/" class="hover-border">Clinics in Kerala</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/telangana/" class="hover-border">Clinics in Hyderabad</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/uttar-pradesh/" class="hover-border">Clinics in Uttar Pradesh</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/kolkata/" class="hover-border">Clinic in Kolkata</a></li>

							</ul>
					</div>
					<div class="col-12 col-sm-2 no-guttersleft social_icons">
						<h1 class="col-12 col-sm-12 text-uppercase text-left no-gutters" style=" padding-left: 14px;">Connect with us</h1>
						<ul class="socials">
							<li><a href="https://www.facebook.com/drmohansdiabetesinstitutions" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
							<li><a href="https://twitter.com/dmdsc" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<!--li><a href="https://twitter.com/dmdsc" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="https://twitter.com/dmdsc" target="_blank"><i class="fa fa-google-plus"></i></a></li-->
							<li><a href="https://www.youtube.com/user/DrMohansGroup" target="_blank"><i class="fa fa-youtube"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="fullwidth footer_last">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-4 pt-4 copyrights no-gutters">
						<p>&copy; Copyright <?php echo date("Y"); ?> Dr. Mohan's Diabetes Specialities Centre</p>
					</div>
					<div class="col-12 col-sm-4 text-center pt-3 terms">
						<ul class="col-12 col-sm-12 text-center">
							<!--li><a href="">Terms</a></li>
							<li><a href="">Privacy</a></li-->
							<li><a href="<?php echo get_home_url();?>/contact-us/" class="hover-border">Contact Us</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-4 text-right pt-4 sbt no-gutters">
						<p>Designed & developed by <a href="https://socialbeat.in" target="_blank" rel="nofollow" class="hover-border">Social Beat</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="modal" id="newsletter-signup"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
				<div class="gravity_holder">
					<span class="text-right close colorfff" data-dismiss="modal">X</span>
					<span class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title" style="color:#fff">Newsletter Signup</span>
					<?php gravity_form( 8, $display_title = false, $display_description = false, $tabindex, $ajax = true, $echo = true ); ?>
				</div>							
			</div>							
		</div>	
	</div>
	<script>
	jQuery(document).ready(function(){
	wow = new WOW({
		boxClass: 'wow', // default
		animateClass: 'flipInX fadeOut fade fadeInUp bounceInDown bounceInUp fadeInUp', // default
		offset: 1, // default
		mobile: true, // default
		live: true // default
	})
	wow.init();
	});
	</script>
</div><!-- #page -->
<?php wp_footer(); ?>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 967380966;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/967380966/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0019/1047.js" async="async"></script>
</body>
</html>