<?php
/**
 *  Template Name: CSR Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>

		<?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
			<img src="<?php echo $image[0]; ?>" alt="Banner" class="banner d-none d-md-block"/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) {?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	<!-- Banner Text-->
	<div class="wow zoomIn csr-banner-caption carousel-caption">
	<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Corporate Social<br class="sm-disp-none"> Responsibility </h1>
	<h3 class="wow zoomIn text-left fs-30">Commited to improving the <br> world around us</h3>
	</div>
	<section class="breadcrumb">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light knows_diabetes padd-top-bottom-70" style="background : url('<?php echo get_template_directory_uri();?>/images/understands-new-bg.jpg');background-repeat: no-repeat; background-position: bottom; background-size: cover;">
		<div class="container">
			<label class="text-center Helvetica_Roman color-red fs-46 lbl-title">About DIRECT</label>
			<div class="row">
				<div class="col-12 col-sm-12 text-center">
					<p>“DIRECT” is a charitable trust which helps the poor and the needy patients with diabetes who cannot afford to pay for their out-patient treatment, in-patient admission, surgeries they may have to undergo or for emergencies which can arise at any moment. We identify such individuals and ensure that treatment is made available to the right person irrespective of caste, creed or religion.</p>
					<!--a href="#schemes" class="Helvetica_Bold colorfff bg-red" style="padding:10px 15px;">View Schemes</a-->
				</div>
			</div>
		</div>
	</section>
	
	<section class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 lbl-title">Our Goals</label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-4 d-sm-none text-center" style="padding-bottom:20px">
					<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/goals-img.jpg" alt="goals" />
				</div>
				<div class="col-12 col-sm-12 col-md-8">
					<ul class="primary-ul">
						<li>To sponsor total free or heavily subsidized treatment to poor and needy patients with diabetes.</li>
						<li style="    margin: 49px 0;">To support educational activities on diabetes both for the health-care personnel and for the lay public.</li>
						<li>To organize large-scale free screening for diabetes in the country.</li>
					</ul>
				</div>
				<div class="col-12 col-sm-12 col-md-4 d-none d-md-block">
					<img class="img-fluid" src="<?php echo get_template_directory_uri();?>/images/goals-img.jpg" alt="goals" />
				</div>
			</div>
		</div>
	</section>
	
	<section id="schemes" class="fullwidth Helvetica_Light padd-top-bottom-70 bg-dark-grey ">
		<div class="container">
			<label class="lbl-title text-center Helvetica_Thin fs-46 colorfff">Our Initiatives</label>
			<div id="accordion">
				<?php if( have_rows('our_initiatives') ){ ?>
				<?php $count =1; ?>
				<?php while ( have_rows('our_initiatives') ) : the_row();  ?>
				  <div class="card">
					<div class="growth-acc-heading" id="heading<?php echo $count; ?>">
					  <h5 class="mb-0">
						<button class="btn-acc <?php if ($count != 1 ) { ?>collapsed <?php } ?>" data-toggle="collapse" data-target="#collapse<?php echo $count; ?>" aria-expanded="true" aria-controls="collapse<?php echo $count; ?>">
						<?php the_sub_field('initiative_title'); ?>
						</button>
					  </h5>
					</div>

					<div id="collapse<?php echo $count; ?>" class="collapse <?php if ($count == 1) { ?> show <?php } ?>" aria-labelledby="heading<?php echo $count; ?>" data-parent="#accordion">
					  <div class="card-body p-50">
						<?php the_sub_field('initiative_story_text'); ?>
					  </div>
					</div>
				  </div>
				  <?php $count++; ?>
				  <?php endwhile; ?>
				<?php } ?>
			</div>
		</div>
	</section>
	<section id="schemes" class="fullwidth Helvetica_Light padd-top-bottom-70 bg-light-blue">
		<div class="container">
			<label class="lbl-title text-center Helvetica_Thin fs-46">Other Initiatives</label>
			<p class="text-center">Dr Mohan’s Diabetes Specialties Centre (DMDSC) has adopted 25 villages in and around Kanchipuram district for providing complete diabetes care.
DMDSC will extent its services to these 25 villages of Kanchipuram district and offer them affordable diabetes care. Basic medical assistance is still a faraway dream for many remote areas of the country and keeping this mind, we have taken the effort to provide diabetes care at every doorstep”. <br>
 Apart from distributing blood glucose meters and strips to 25 children with Type 1 diabetes, over 100 diabetes patients from various parts of the city were made as diabetes ambassadors who will spread the awareness on diabetes to the society.</p>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 ">
		<?php 
		$images = get_field('csr_slider');
		if( $images ){ ?>
		<label class="text-center Helvetica_Thin fs-46 color000 lbl-title">CSR Gallery</label>
		<div class="container">
			<div class="row">
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/prev.png" class="spec-prev img-responsive" alt="prev"/>
				</div>
				<div class="col-8 col-sm-10 speciality-slider">
					<div class="swiper-container" id="speciality-slider">
						<div class="swiper-wrapper">
						<?php foreach( $images as $image ){ ?>
							<div class="swiper-slide">
								<img src="<?php echo $image['url']; ?>" alt="speciality" />
							</div>
						<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/next.png" alt="Next" class="spec-next img-responsive"/>
				</div>
			</div>
		</div>
		<?php } ?>
	</section>
<?php include('spec-footer.php'); ?>