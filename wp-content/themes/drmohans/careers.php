<?php
/**
 *  Template Name: Careers Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner <?php if(get_field('mobile_banner',get_the_ID())) { ?>  banner d-none d-md-block <?php } ?> "/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) { ?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	<!-- Banner Text-->
	<div class="wow zoomIn recognition-banner-caption carousel-caption">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Careers</h1>
	</div>
	<section class="">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light services-list">
		<div class="container">
			<p class="color-red fs-22 Helvetica_Roman text-center fw-ligher lh52 color-red">List of Job Openings:</p>
			<div class="row bottom-red" style="margin-top:20px">
				<div class="col-12">
			
					<?php
$count1 = 1;
					if( have_rows('Jobs') ):
						// loop through the rows of data
						while ( have_rows('Jobs') ) : the_row(); ?>
							<label class="text-center"> <?php the_sub_field('location');  ?></label>
							<?php
							if( have_rows('job_details') ):
								$count = 1;
								?> <div class="row careers" style="margin-top:20px"><?php
								
								while( have_rows('job_details') ): the_row(); ?>
									<div class="col-12 col-sm-4 careers-seperator"> 
									<b>Position :  </b><?php the_sub_field('position');  ?><br>
									<b>Qualification :  </b><?php the_sub_field('qualification');  ?><br>
									<b>Experience:  </b><?php the_sub_field('experience');  ?><br>
									</div> <?php if(($count % 3) == 0){ ?> </div> <div class="row careers" style="margin-top:20px"><?php }
									$count++;
								endwhile; ?>
								</div>
								<?php 
							endif;	
							?><?php if($count1 != count(get_field('Jobs'))) { ?></div></div><div class="row bottom-red" style="margin-top:20px"><div class="col-12">  <?php } 	$count1++;						
						endwhile;
					endif;
					?>
				</div>
			</div>
			<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 lbl-title" style="padding-left: 0;">For further details contact:</label>
			<p class="fs-16 Helvetica_Roman fw-ligher">To The Associate Vice President / Sr. Executive – Recruitment Dr. Mohan’s Diabetes Specialities Centre <br>6B, Conran Smith Road,<br> Gopalapuram, <br>Chennai–600 086. <br>Ph (91-44) 43968888, 28359048, 28359051 <br>Email: <a href="mailto:hrd@drmohans.com">hrd@drmohans.com</a></p>
			<p class="fs-16 Helvetica_Roman fw-ligher">POSITION IN MADRAS DIABETES RESEARCH FOUNDATION (MDRF)

MDRF is recruiting for the post of Research Assistant – Dept. of Physical activity <a target="_blank" class="color-red" href="https://web.archive.org/web/20160119173858/http://mdrf.in/Research-Asst.html">(Click for more details)</a></p>
<p class="fs-16 Helvetica_Roman fw-ligher">
Invites applications for a Junior Research Fellow <a target="_blank" href="https://web.archive.org/web/20180102151051/http://mdrf.in/Junior-Research-Fellow(BRNS%20).html"  class="color-red">(Click for more details)</a></p>
<p class="fs-16 Helvetica_Roman fw-ligher">
The MDRF is an autonomous, non-profitable, R&D organization, recognized by the Department of Scientific and Industrial Research, Govt. of India. MDRF is affiliated to both University of Madras and the Tamilnadu Dr. MGR Medical University, Chennai for offering Ph.D programs under the supervision of independent scientists. The Foundation is looking for Scientists/Post Doctoral Fellows to join our team investigating the Genetics of Diabetes dealing with genomics, proteomics and pharmacogenomic aspects. Applicants should have completed their Ph.D and have knowledge in genetics, proteomics and cell signaling work. Candidates possessing experience in molecular techniques especially rDNA technology, eukaryotic expression systems and functional analysis of disease processes will be preferred.</p>
<p class="fs-16 Helvetica_Roman fw-ligher">
Application including CV, list of publications, and 3 letters of reference should be sent to:</p>
<p class="fs-16 Helvetica_Roman fw-ligher">
The Director, Madras Diabetes Research Foundation (MDRF),<br> 4, Conran Smith Road, <br>Gopalapuram,<br> Chennai–600 086. <br>Ph : (91-44) 28359048, 28359051 <br>Email: <a href="mailto:drmohans@diabetes.ind.in">drmohans@diabetes.ind.in</a> <br/>Contact Dr. M. Balasubramanyam for further details</p>
		</div>
	</section>
<?php include('spec-footer.php'); ?>