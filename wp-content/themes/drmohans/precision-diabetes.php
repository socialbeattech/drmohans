<?php
/**
 *  Template Name: Precision Diabetes Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<div class="container-fluid padd0"><!-- Banner -->
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class="banner d-none d-md-block"/>
	<?php endif; ?>
	<?php if(get_field('mobile_banner',get_the_ID())) {?>
		<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
	<?php } ?>
	</div><!-- Banner Text-->
	<div class="wow zoomIn precision-banner-caption carousel-caption">
		<h3 class="wow zoomIn text-left fs-30">Because each one needs</h3>
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">special care</h1>
	</div>
	<section class=" bg-light-grey">
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
				  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
			?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-light-grey padd-top-bottom-70">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-6 home_videos ">
					<div class="">	
						<iframe width="560" height="370" src="https://www.youtube.com/embed/yvjTlJ4K9_8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>					
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-6 marginauto">
					<p class="sevices-subtitle text-left m-b0 fs-24">Dr. Mohan’s launches </p>
					<!--p class="sevices-subtitle text-left m-b0 fs-24">Dr. Mohan’s launches Precision Diabetes</p-->
					<?php /* Edited by Sowmya on 16-07-2018 Client change */ ?>
					<label class="lbl-title services-section-title HelveticaNeueLTStd-Md m-b0 text-left pt-0 pb-20">
						Precision Diabetes Treatment in India
					</label>
					<ul class="primary-ul pl-18">
						<li class="">
							A revolutionary approach to managing diabetes.
						</li>
						<li class="">
							Medical practices, testing, decisions and treatments tailored to individual patient level.
						</li>
						<li class="">
							Tests and therapies made based on specific risk factor profile and health history obtained from genetic profiling.			
						</li>
					</ul>			
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-light-blue padd-top-bottom-70">
		<div class="container">
			<ul class="nav nav-tabs bordernone xs-txt-left justify-content-center"> 
				<li class="border-r list-specialities color-red"><a data-toggle="tab" href="#tab1" class="active color-red">What is Precision Diabetes</a></li>
				<li class="border-r list-specialities color-red"><a data-toggle="tab" href="#tab2"  class="color-red">Need for Precision Diabetes</a></li>
				<li class=" list-specialities color-red"><a data-toggle="tab" href="#tab3"  class="color-red">Benefits</a></li>
			</ul>
			<div class="tab-content">
				<div id="tab1" class="tab-pane in active col-sm-12 text-center">
					<p>
						At its core, precision is a model that proposes customization of health care with medical practices, testing, decisions and treatments tailored to the individual patient level. Diagnostic tests and therapies are selected not only on the basis of generic symptoms but also by the specific risk factor profile and health history obtained from genetic profiling.
					</p>
					<p>
						<!--a><button class="Helvetica_Roman btn-primary-speciality p-btn">Read more</button></a-->
					</p>
				</div>
				<div id="tab2" class="tab-pane fade col-sm-12 text-center ">
					<p>
						Diabetes is a complex health condition where symptoms differ from person to person and so does the diagnosis. Present day refinements in technology and advancements in genomics and other fields has proved that there are at least 20 types of diabetes that exists. It is important to diagnose and understand an individual’s body type, genes, and other factors before prescribing any sort of treatment. An individual’s diagnosis can only be prescribed after assessing and conducting the essential tests. Thus, the precision method provides tailor made diagnosis and treatment for people with diabetes.
					</p>
					<p>
						<!--a><button class="btn-primary-speciality">Read more</button></a-->
					</p>
				</div>
				<div id="tab3" class="tab-pane fade col-sm-12 text-center ">
					<p>
						‘Precision Diabetes’ will make it possible to accurately classify a patient and determine the type of diabetes any individual is suffering from depending on which the treatment for the patient will be prescribed.<br>
At Dr. Mohan’s, we constantly try to innovate and provide the best diabetes care to our patients, launch of ‘Precision Diabetes’ is just another humble step towards it. We believe this approach will not only provide accurate and relevant treatment, but also have a huge impact on the cost of managing diabetes.
					</p>
					<!--p>
						<a><button class="btn-primary-speciality">Read more</button></a>
					</p-->
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 diabetes_care">
		<div class="container">
			<label class="lbl-title text-center Helvetica_Thin fs-46"><?php the_field('patient_count_text', 'options'); ?></label>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-8 text-center" style="margin:auto">
						<div class="row sm-p-b0" style="padding-bottom:40px;">		
							<?php $count=1;
							$numrows = count( get_field('we_care_icons','option') ); ?>
							
							<?php while ( have_rows('we_care_icons','option') ) : the_row(); ?>	
							<div class="col-12 col-sm-4">
								<img src="<?php the_sub_field('icon'); ?>"/>
								<label class="title-40k"><?php the_sub_field('sub_text'); ?></label>
							</div>
							<?php if($count%3 ==0 && $count != $numrows){ ?>
							</div>							
							<div class="row">	
							<?php } ?>
							<?php $count++; ?>
							<?php endwhile; ?>
						</div>			
					</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="book_appintment">
						<div class="gravity_holder">
							<label class="lbl-title col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters ">Book an Appointment</label>
							<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
							<?php gravity_form( 1, $display_title = false, $display_description = false,$tabindex, $ajax = false, $echo = true ); ?>
						</div>							
					</div>							
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>