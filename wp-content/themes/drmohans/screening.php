<?php
/**
 *  Template Name: Screening Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
			<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner"/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) {?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	<!-- Banner Text-->
	<div class="wow zoomIn screening-banner-caption carousel-caption">
	<h3 class="wow zoomIn text-left fs-30">Should you get</h3>
	<h1 class="wow zoomIn text-left Helvetica_Roman fs-48"> Screened for <br class="sm-disp-none"> Diabetes? </h1>
	</div>
	<section class="bg-light-grey">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list Helvetica_Light bg-light-grey">
		<div class="container">
			<p class="color1d2e34 text-center m-b0">Diabetes is termed as the 'silent killer' because it gradually affects every system of the body particularly the eyes, kidneys, heart, feet and nerves. Unfortunately, many people do not even discover they have diabetes until complications start to appear. Diabetes thus does not set in overnight; it picks up at a steady pace.</p>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list Helvetica_Light">
		<div class="container">
			<label class="lbl-title red-title HelveticaNeueLTStd-Md m-b0 text-center">
				Diabetes Starts in Silence
			</label>
			<p class="red-subtitle text-center">Know the symptoms </p>
			<ul class="homecare-services-offered-ul text-center">
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/screening-icon1.png" alt="Silence"/>
					<label class="service-title">Frequent<br>Urination </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/screening-icon2.png" alt="Silence"/>
					<label class="service-title">Excessive<br>Thirst</label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/screening-icon3.png" alt="Silence"/>
					<label class="service-title">Increased<br> Hunger</label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/screening-icon4.png" alt="Silence"/>
					<label class="service-title">Weight<br>Loss  </label>
				</li>
			</ul>
			<ul class="homecare-services-offered-ul text-center">
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/screening-icon5.png" alt="Silence"/>
					<label class="service-title">Blurry<br>Vision </label>
				</li>
				<li class="homecare-services-offered-li text-center"> 
					<img src="<?php echo get_template_directory_uri();?>/images/screening-icon6.png" alt="Silence"/>
					<label class="service-title">Numbness in<br> the Hands </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/screening-icon7.png" alt="Silence"/>
					<label class="service-title">Unexplained <br>Fatigue  </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/screening-icon8.png" alt="Silence"/>
					<label class="service-title">Wounds That <br>Don't Heal  </label>
				</li>
			</ul>
			
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-dark-grey padd-top-bottom-70 section-speciality-form">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 colorfff lbl-title">Specialising in Your Diabetes</label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 speciality-video">
				<iframe width="793" height="282" class="spec-main-video" src="https://www.youtube.com/embed/q5GmOyyZe0w?rel=0" frameborder="0" allow="autoplay; encrypted-media" title="q5GmOyyZe0w" allowfullscreen></iframe>
				<ul class="ul-sub-video">
					<li class="sub-video">
						<img src="<?php echo get_template_directory_uri();?>/images/youtube-play-button-transparent-png-15.png" class="youtube-play-button cur-pointer" title="bL_t50YMn9s" style="z-index: 99;" alt="Specialising"/>	
						<img src="https://img.youtube.com/vi/bL_t50YMn9s/hqdefault.jpg" width="240" height="200" class="video-thumbnail" title="bL_t50YMn9s" style="position:relative"/>	
					</li>
					<li class="sub-video">
						<img src="<?php echo get_template_directory_uri();?>/images/youtube-play-button-transparent-png-15.png" class="youtube-play-button cur-pointer" title="Kn_kIuinTtk" style="z-index: 99;" alt="youtube"/>	
						<img src="https://img.youtube.com/vi/Kn_kIuinTtk/hqdefault.jpg" width="240" height="200" class="video-thumbnail" title="Kn_kIuinTtk" style="position:relative" alt="youtube"/>	
					</li>
				</ul>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="book_appintment">
						<div class="gravity_holder">
							<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
							<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
							<?php gravity_form( 1, $display_title = false, $display_description = false,$tabindex, $ajax = true, $echo = true ); ?>
						</div>							
					</div>							
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>