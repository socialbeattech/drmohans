<?php
/**
 *  Template Name: Internation Patients Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
			<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner <?php if(get_field('mobile_banner',get_the_ID())) { ?> d-none d-md-block <?php } ?>"/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) {?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	<!-- Banner Text-->
	<div class="wow zoomIn screening-banner-caption carousel-caption">
	<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">International Patient<br class="sm-disp-none"> Service </h1>
	<h3 class="wow zoomIn text-left fs-30"> We are One for Everyone</h3>
	</div>
	<section>
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list Helvetica_Light ">
		<div class="container text-center">
			<p class="color1d2e34 text-center">We provide world class patient care services for international patients at an affordable cost. The entire health care program is led by a dedicated team of Consultants under the tutelage of our Chairman – Prof. Dr. V. Mohan.</p>
			<p class="color1d2e34 text-center ip-p">All the Diabetic healthcare solutions are provided under one roof. The scope of services varies from small procedures to complicated cases.</p>
			<a class="text-center fs-18 " href="#service"><button class="btn-international-patients-1 Helvetica_Bold">Our Service</button></a>
			<a class="text-center fs-18 Helvetica_Bold" href="#speciality"><button class="btn-international-patients-2 Helvetica_Bold">Our Specialisation</button></a>
			<a class="text-center fs-18 Helvetica_Bold" href="#why_dr_mohan"><button class="btn-international-patients-3 Helvetica_Bold">Why Choose Dr Mohans</button></a>
		</div>
	</section>
	<section id="service" class="fullwidth Helvetica_Light bg-light-blue padd-top-bottom-70">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 lbl-title">Corporate Services</label>
			<p class="text-center">The hospital is accredited by the internationally recognized ISO 9001:2015 and the NABH entry level standards. The laboratory services also adhere to NABH entry level requirements with fully automated equipments, Sample Barcode facility etc.</p>
			<p class="text-center">Quality of care, infection control and patient safety are our priority.  Patient satisfaction is regularly monitored and given utmost importance. Rights of patients are respected and protected.</p>
			<p class="text-center" style="margin-top:3.5em;margin-bottom:0">
			<!--button class="btn-primary-speciality"> Know More</button-->
			</p>
			<div class=" padd-top-bottom-70" style="padding-bottom:0 !important;">
					<div class="row Helvetica_Bold">
					<?php if( have_rows('services') ){ 
						$count = 1; ?>
						<?php while ( have_rows('services') ) : the_row();  ?>
						<div class="col-12 col-sm-4 text-center">
							<div class="col-12 cp-services-keys p-tb-50">
								<img src="<?php the_sub_field('service_icon'); ?>" alt="services"/>
							</div>
							<label class="international-patients-icon-label fs-18 Helvetica_Light lh30"><?php the_sub_field('service_text'); ?></label>
						</div>
						<?php if($count == 3){ ?>
							</div>
							<div class="row cp_row" >
						<?php } ?>
						<?php $count++; ?>
						<?php endwhile; ?>
					<?php } ?>
					</div>
			</div>
		</div>
	</section>
		
	<div id="speciality" class="fullwidth specialied_care d-none d-md-block" style=""><!--main section-->
			<div class="container"><!--main container-->
				<label class="lbl-title text-center Helvetica_Thin fs-46 colorfff">Helping you know your Diabetes</label>
				<div id="demo" class="carousel slide" data-interval="false" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="row pt-0">
							<div class="cols5 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Diabetes-Diet.png" alt="speciality" />		
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/diabetes-diet/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Diabetes Diet</h2></a>
										<p class="Helvetica_Light fs-16">Our in-house nutritionists and dieticians help you maintain the right weight and food habits. Their services go hand in hand with the medications to help you achieve better results.</p>
									</div>
								</div>
							</div>
							
							<div class="cols1 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/fitnessIcon.png" alt="speciality" />			
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/fitness/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Fitness</h2></a>
										<p class="Helvetica_Light fs-16">We provide fitness training and counselling to help patients manage insulin levels, lower blood pressure and reduce stress.</p>
									</div>
								</div>
							</div>
							<div class="cols1 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Stress-Management.png">						
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/stress-management"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Stress Management</h2></a>
											<p class="Helvetica_Light fs-16">We undertake psychological evaluation and conduct patient-education, skills-development and motivation through counseling, as well as relaxation techniques.</p>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row pt-0"><!-- Slide 1 row 2 -->
							<div class="cols2 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Precision-Diabetes.png" alt="speciality" />				
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/precision-diabetes"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Precision Diabetes</h2></a>
										<p class="Helvetica_Light fs-16">We customize health care by tailoring testing, decisions and treatments to the individual.</p>
									</div>
								</div>
							</div>
							<div class="cols2 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Diabetes-Preventive-Care.png" alt="speciality" />						
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/preventive-care/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Preventive Care</h2></a>
										<p class="Helvetica_Light fs-16">With regular check-ups and timely intervention, we help you prevent diabetes and the complications due to diabetes.</p>
									</div>
								</div>
							</div>
							<div class="cols2 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Weight-Loss.png" alt="speciality" />				
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/weight-loss/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Weight Loss</h2></a>
										<p class="Helvetica_Light fs-16">We assist you in achieving your optimal body weight, thereby preventing the development of diabetes and making control easier if you have already been diagnosed with diabetes.</p>
									</div>
								</div>
							</div>
						</div><!-- row -->
						</div>
						<!-- Slide one ends -->
						<div class="carousel-item">
							<div class="row pt-0">
								<div class="cols4 col-12 col-sm-12 col-md-4"><!-- 2 1 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/eyeIcon.png" alt="speciality" />			
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/diabetes-eye-care"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Eye Care</h2></a>
											<p class="Helvetica_Light fs-16">Our retinal specialists ensure that your eyes are protected from the ill-effects of diabetes</p>
										</div>
									</div>
								</div>
								<div class="cols1 col-12 col-sm-12 col-md-4"><!-- 2 2 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Foot-Care.png" alt="speciality" />					
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/diabetes-footcare"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Foot Care</h2></a>
											<p class="Helvetica_Light fs-16">We provide comprehensive facilities for early detection and  treatment of diabetic foot complications </p>
										</div>
									</div>
								</div>
						
								<div class="cols1 col-12 col-sm-12 col-md-4"><!-- 2 3 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/kidneyIcon.png" alt="speciality" />			
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/kidney-care/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Kidney Care</h2></a>
											<p class="Helvetica_Light fs-16">We ensure that your kidneys remain in good health through prevention, early detection and prompt treatment of diabetic kidney disease.</p>
										</div>
									</div>
								</div>
							</div><!-- row -->
							<div class="row pt-0">
								<div class="cols4 col-12 col-sm-12 col-md-4"><!-- 2 4 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/cardiacIcon.png" alt="speciality" />					
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href=" <?php echo get_home_url();?>/patient-care/diabetes-cardiac-care/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Cardiac Care</h2></a>
											<p class="Helvetica_Light fs-16">We provide preventive, diagnostic and therapeutic services for heart disease due to diabetes.</p>
										</div>
									</div>
								</div>
								
								<div class="cols1 col-12 col-sm-12 col-md-4"><!-- 2 5 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">			
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Surgeries.png" alt="speciality" />
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/surgeries"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Surgeries & Treatments</h2></a>
											<p class="Helvetica_Light fs-16">With timely check-ups and specific treatment options for every part of your body, we help you manage diabetes with ease.</p>
										</div>
									</div>
								</div>								
								<div class="cols6 col-12 col-sm-12 col-md-4"><!-- 2 6 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Orthopaedic.png" alt="speciality" />					
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/orthopedic"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Orthopedic Care </h2></a>
											<p class="Helvetica_Light fs-16">Right from mobilisation of joints to surgeries and preventive care, our orthopedic specialists offer expert treatment for bone and joint problems.</p>
										</div>
									</div>
								</div>
							</div><!--row-->
						</div><!-- item -->
					 <div class="carousel-item">
						<div class="row pt-0"><!-- 3 1 -->
							<div class="cols3 col-12 col-sm-12 col-md-4"><!-- 3 3 -->
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Pregnancy---Gestational-Diabetes.png" alt="speciality" />					
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url(); ?>/patient-care/pregnancy/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Pregnancy </h2></a>
										<p class="Helvetica_Light fs-16">We ensure the best possible outcome for your pregnancy through a multidisciplinary approach to achieve optimal blood glucose control. </p>
									</div>
								</div>
							</div>
							<div class="cols5 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Physiotherapy.png" alt="speciality" />			
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/diabetes-physiotherapy/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Physiotherapy</h2></a>
										<p class="Helvetica_Light fs-16">Our therapists are well-acquainted with promoting mobility of joints and tissues for increased circulation and strength conditioning.</p>
									</div>
								</div>
							</div>
							<div class="cols3 col-12 col-sm-12 col-md-4"><!-- 3 5 -->
							
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Home-Care.png" alt="speciality" />					
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/home-care"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Home Care</h2></a>
										<p class="Helvetica_Light fs-16">We put the well-being of our patients at the centre of everything we do. Get the best treatment services from the comfort of your home.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row pt-0"><!-- 3 4 -->
							<div class="cols4 col-12 col-sm-12 col-md-4"><!-- 3 6 -->
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/lab.png" alt="speciality" />					
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/diabetes-laboratory/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Lab</h2></a>
										<p class="Helvetica_Light fs-16">Our state-of-the-art labs are equipped with cutting edge tchnology to deliver accurate results.</p>
									</div>
								</div>
							</div>
							<div class="cols4 col-12 col-sm-12 col-md-4"><!-- 4 1 -->
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Insurance-&-Corporate-Services.png" alt="speciality" />
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/corporate-and-insurance-services/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Insurance & Corporate Services</h2></a>
										<p class="Helvetica_Light fs-16">We offer exclusive benefits tailor-made for Corporate Employees and Insurance options created for your needs.</p>
									</div>
								</div>
							</div>
							
							<div class="cols2 col-12 col-sm-12 col-md-4"><!-- 4 2 -->
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/International-Patients.png" alt="speciality" />			
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/international-patients"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">International Patients </h2></a>
										<p class="Helvetica_Light fs-16">We extend a seamless service right to our overseas patients right from the first enquiry to the post-treatment follow-up.</p>
									</div>
								</div>
							</div>
						</div>
					</div><!-- Carousel - item -->
					
				<div class="carousel-item">
					<div class="row pt-0">
						<div class="cols2 col-12 col-sm-12 col-md-4"><!-- 4 3 -->
							<div class="row">
								<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
									<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Shop-Online.png" alt="speciality" />			
								</div>
								<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
									<a href="http://diabetes.ind.in/" target="_blank"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Shop Online</h2></a>
									<p class="Helvetica_Light fs-16">From special dietary needs to orthopedic footwear that is beneficial and fashionable, all our products are available online with easy payment and delivery options. </p>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div><!-- Inner -->
				<i style="opacity: 1;color:rgb(237, 22, 22);font-size: 36px;" class="fa fa-chevron-left carousel-control-prev "></i>
				<i style="opacity: 1;color:rgb(237, 22, 22); font-size: 36px;" class="fa fa-chevron-right carousel-control-next "></i>
				<?php /*
			<img class="carousel-control-prev" src="<?php echo get_template_directory_uri();?>/images/Arrows-Previous-icon.png"/>
			<img class="carousel-control-next" src="<?php echo get_template_directory_uri();?>/images/Arrows-Next-icon.png"/>*/?>
			</div><!-- #demo -->
		</div><!-- Main Container-->
	</div><!-- Main Container-->
    	<div id="mobile-speciality" class="fullwidth specialied_care d-md-none"><!--main section-->
		<div class="container"><!--main container-->
			<label class="lbl-title text-center Helvetica_Thin fs-46 colorfff">Helping You Know Your Diabetes</label>
			<div id="mobile_demo" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="row pt-0">
					<?php if( have_rows('specialising_in_your_diabetes',2) ){ 
						$spec_count = 1; ?>
						<?php while ( have_rows('specialising_in_your_diabetes',2) ) : the_row();  ?>
							<div class="cols1 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="col-12 text-center">	
										<img src="<?php the_sub_field('speciality_icon',2); ?>" alt="speciality" />			
									</div>
									<div class="col-12 text-center">
										<a href="<?php the_sub_field('speciality_url',2); ?>"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"><?php the_sub_field('speciality_title'); ?></h2></a></a>
										<p class="Helvetica_Light fs-16"><?php the_sub_field('speciality_description',2); ?></p>
									</div>
								</div>
							</div>
							<?php if($spec_count % 2 == 0){ ?>
							</div>
						</div>
						<div class="carousel-item">
							<div class="row pt-0">
							<?php  } ?>
						<?php $spec_count++; ?>
						<?php endwhile; ?>
					<?php } ?>
						</div> <!-- row -->
					</div> <!-- slider item -->
				</div> <!-- slider inner -->
				<i style="opacity: 1;color:rgb(237, 22, 22)" class="fa fa-chevron-left mobile-carousel-control-prev"></i>
				<i style="opacity: 1;color:rgb(237, 22, 22)" class="fa fa-chevron-right mobile-carousel-control-next"></i>
			</div> <!-- Carousel -->
		</div> <!-- main container -->
	</div> <!-- main section -->

	<section id="why_dr_mohan" class="fullwidth Helvetica_Light padd-top-bottom-70 section-speciality-form">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 lbl-title">4,50,000+ Patients trust Us</label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 speciality-video">
				<iframe width="793" height="515" src="https://www.youtube.com/embed/AVLTUptkHK0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="book_appintment" style="padding:3% !important">
						<div class="gravity_holder">
							<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
							<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
							<?php gravity_form( 1, $display_title = false, $display_description = false, $tabindex, $ajax = true, $echo = true ); ?>
						</div>							
					</div>							
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>