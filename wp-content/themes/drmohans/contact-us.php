<?php
/**
 *  Template Name: Contact us Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>

	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner <?php if(get_field('mobile_banner',get_the_ID())) { ?>  banner d-none d-md-block <?php } ?> "/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) { ?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	<!-- Banner Text-->
	<div class="wow zoomIn recognition-banner-caption carousel-caption">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Contact Us</h1>
	</div>
	<section class="">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list">
		<div class="container">
			<p class="color-red fs-22 Helvetica_Roman text-center fw-ligher rec-title">Thanks for contacting Dr. Mohan’s Diabetes Institutions! Our team will get in touch with you shortly. In the meantime please feel free to email us at appointments-web@drmohans.com.<br> Our working hours 9.00 am-6.00 pm Mon- Sat and Sunday holiday. </p>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 text-center">
				<p class="color-red fs-22 Helvetica_Roman text-center fw-ligher lh52 rec-title">You can also reach out to the nearest branch from the links below:</p>
				<?php $args = array(
						'post_type'      => 'page',
						'posts_per_page' => -1,
						'post_parent'    => 10,
						'orderby'=> 'title', 
						'order' => 'ASC'
					);

					$parent = new WP_Query( $args );
					if ( $parent->have_posts() ) :
						while ( $parent->have_posts() ) : $parent->the_post();?>
							<div class="row" style="margin-bottom:50px">
								<div class="col-4 contact-loc-split">
									<label><a href="<?php echo get_permalink($parent->ID) ?>" ><?php echo get_the_title() ?> </a></label>
								</div>
								<div class="col-8 text-left cp-loc">
									<?php 
									$sub_args = array(
										'post_type'      => 'page',
										'posts_per_page' => -1,
										'post_parent'    => get_the_ID($parent->ID),
										'order'          => 'ASC',
										'orderby'        => 'menu_order'
									 );

									$sub_loctaions = new WP_Query( $sub_args );
									
									if ( $sub_loctaions->have_posts() ) {
										while ( $sub_loctaions->have_posts() ) : $sub_loctaions->the_post();
											?> <label><a href=" <?php echo get_permalink($sub_loctaions->ID) ?>" ><?php echo get_the_title() ?> </a></label> <?php
										endwhile; 
									}else{ ?>
										<label><a href="<?php echo get_permalink($parent->ID) ?>" ><?php echo get_the_title() ?> </a></label>
									<?php } ?>
								</div>
						   </div>
					<?php 	endwhile; 
					endif; 
					wp_reset_postdata();	?>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="book_appintment">
						<div class="gravity_holder">
							<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
							<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
							<?php gravity_form( 1, $display_title = false, $display_description = false,$tabindex, $ajax = true, $echo = true ); ?>
						</div>							
					</div>							
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>
