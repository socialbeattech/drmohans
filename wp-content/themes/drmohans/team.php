<?php
/**
 *  Template Name: Team Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<div class="container-fluid padd0"><!-- Banner -->
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	<?php endif; ?>
	</div><!-- Banner Text-->
	<section class="Helvetica_Light experts_care1 Helvetica_Light">
	</section>
	<section class="breadcrumb">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '</p><p id="breadcrumbs">','</p><p>' );
			}
		?>
		</div>
	</section>
<section>
<p class="text-center Helvetica_Light"><b>DMDSC TEAM (CHENNAI)</b></p>
<div class="container backlav teamshow">
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drvmohan.jpg" style="width:250px; height:166px;" class="bordimg"/></p>
<p class="text-center Helvetica_Light lh30">Dr. V. Mohan, M.D., FRCP(London,Edinburgh,Glasgow <br>& Ireland), Ph.D., D.Sc., D.Sc (Hon. Causa), FNASc, FASc, FNA,<br> FACP, FACE, FTWAS, MACP<br>
<b>Chairman and Chief Consultant</b></p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/rema.jpg" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Late Dr. Rema Mohan, M.B.B.S., D.O., Ph.D., FABMS<br>
<b>Co-Founder, Managing Director & Chief Ophthalmologist and wife of<br> Dr.V. Mohan</b></p>
</div>
</div>
</div><br>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6 backlav">
<p class="text-center Helvetica_Light lh30"><b>Vice Chairman</b>
</p>
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.Ranjit_web1.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. I. Ranjit Unnikrishnan<br>
Vice Chairman & Consultant</p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><b>Managing Director</b>
</p>
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/dr-anjana-dr-mohans.png" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. R. M. Anjana<br>
Managing Director & Consultant
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Mr.prabakaran.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Mr. N. Prabakaran<br>
Vice President - Accounts & Finances</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/S.J.Parvathi.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Ms. S. J. Parvathi<br>
Associate Vice President - Quality System<br>
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Mr.Appadurai.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Mr. R.P. Appadurai<br>
Associate Vice President - Human Resource
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Mr.-Ebenezer_Anand_Dhanaraj.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Mr. Ebenezer Anand Dhanaraj<br>
Operation Projects <br>Associate Vice President
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Gopalapuram Main Centre</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drramu.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. M. Ramu<br>
Consultant </p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/dr.kumar.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Brijendra Kumar<br> Srivatsava<br>
Consultant
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/chandru.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Chandru<br>
Consultant
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drUthra.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. S. Uthra<br>
Consultant
</p>
</div>
</div>
</div>
<div class="container backlav teamshow">
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr. J. P. Vignesh.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. J.P. Vignesh<br>
Consultant </p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR. JAGDISH. P.S.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. P.S. Jagdish<br>
Consultant
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drparthasarathy.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. B. Parthasarathy<br>
Consultant
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drvidhya.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Vidhya Jaydeep<br>
Consultant
</p>
</div>
</div>
</div>
<div class="container backlav teamshow">
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.SAMUEL-SATHWEEK-RAYAPATI1.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Samuel Sathweek<br> Rayapati<br>
Consultant</p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drLovelena_Munawar.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Lovelena Munawar<br>
Consultant
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Annanagar Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drkayal.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. S. Kayal Vizhi<br>
Consultant</p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drmuthukumar.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Muthukumaran<br>
Consultant
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Tambaram Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drprasanna.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Prasanna Kumar Gupta<br>
Consultant</p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drselvakumar.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. J. Selvakumar<br>
Consultant
</p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.KasthuriSelvam.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Kasthuri Selvam<br>
Consultant
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Vadapalani Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drramu1.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. M. Ramuu<br>
Consultant</p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.-V.-Rathika.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. V. Rathika<br>
Consultant
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">OMR Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/dr-prasad-jason.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.Prasath Jaison Jacob<br>
Consultant </p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Porur Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.Honey-Evangelin.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.Honey Evangelin<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Avadi Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.tariq_.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Syed Tariq<br>
Dr. Syed Tariq
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Selaiyur Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drprasanna1.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Prasanna Kumar Gupta<br>
Consultant</p>
</div>

<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.Aishwarya.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.Aishwarya<br>
Consultant</p>
</div>
</div>
</div>
</section class="padtb">
<section>
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol">Diabetologist Velachery Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/hemalatha.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Hemalatha<br>
Consultant </p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Vellore & Gudiyatham Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.M.Sudharkar.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.M.Sudhakar<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Chunampet Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR. H. RAKESH.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. H. Rakesh<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Coimbatore Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR. SUGUNA PRIYA.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. S. Sugunapriya<br>
Consultant</p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/RaviKanna.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Ravi Kannan<br>
Consultant
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Coimbatore Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr. A. Kannan.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. A. Kannan<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol">Thanjavur Branch (Karups Nagar)</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.D.Killivallavann.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. D. Killivallavan<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Tuticorin Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.T.ARUN-KUMAR-CONSULTANT.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.T. ARUN KUMAR<br>
Consultant </p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Salem Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DrE.Vidya_.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. E. Vidya<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Erode Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.-Sudharshan-Consultant.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. V. Sudarsan<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Puducherry Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/vinod.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. G.R. Vinod Kumar<br>
Consultant </p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR. H. RAKESH.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. H. Rakesh<br>
Consultan
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol">Telangana Branch (Domalguda, Jubilee Hills & Kukatpally, Dilsukh Nagar, Secunderabad, Tolichowki, A.S.Rao Nagar)</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.-NG-SASTRY-director.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. N.G. Sastry<br>
Director & Consultant </p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.-KAVITHA-RAO.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Kavitha Rao<br>
Consultant<br>
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.Pavan-Kumar-consultant.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Pavan Kumar<br>
Consultant
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.-Kareemuddin-Consultant-Ophthalmologist.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Kareemuddin<br>
Sr.Consultant<br> Ophthalmologist
</p>
</div>
</div>
</div>
<div class="container backlav teamshow">
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr-PAYNENI-SIVANI.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Paymeni Sivani<br>
Consultant</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr-Aawar-Dipika-Narayan.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Aawar Dipika Narayan<Br>
Consultant
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr-Shakera-Mubeen.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Shakera Mubeen<br>
Consultant
</p>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr-Ridhika.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Ridhika<br>
Consultant
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Andhra Pradesh (Vijayayawada) Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.Saritha-Kakani.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.Saritha-Kakani<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol">Andhra Pradesh (Vijayayawada) Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.T.Sameer-Nandan-Consultant.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.T. Sameer Nandan<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Andhra Pradesh (Rajahmundry) Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Thillari-neeraja.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.Thillari Neeraja<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol">Diabetologist Bomikhal, Bhubaneswar Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.PHILIPS ROUTRAY.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Philips Routray<br>
Consultant </p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist Karnataka,Bengaluru Branch ( Indiranagar)</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.Chaithanya.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Chaithanya<br>
Consultant</p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.Poonkodi.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Poonkodi<br>
Opthalmologist
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist Karnataka,Bengaluru Branch (JP Nagar)</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.S.Preeti02.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Preeti. S.<br>
Consultant </p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist Karnataka,Bengaluru Branch (Malleshwaram)</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.-PRAVEEN-.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">DR. Praveen Gangadhara<br>
Consultant </p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist Karnataka,Bengaluru Branch (Whitefield)</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr-Ramya.B.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Ramya<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist Karnataka, Mysuru Branch (V.V. Mohalla) </p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.RENUKAPRASAD.A.R.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.Renukaprasad.A.R.<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist Karnataka, Mangalore Branch (Hampankatta)</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.-SHREEKANTH-HEGDE.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Shreekanth Hegde<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist New Delhi Branch (Kirti Nagar)</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr.Shalini-Jaggi.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr.Shalini Jaggi<br>
Consultant </p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist Lucknow Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drjyoti.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Jyoti sah<br>
Consultant </p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist Trivandrum Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.RAJMOHAN-CONSULTANT-DIABETOLOGIST-CUM-ENDOCRINOLOGIST-TRIVANDRUM.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. L. Raj Mohan<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Diabetologist Kochi Branch</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DR.-JYOTHISH-R-NAIR-CONSULTANT-DIABETOLOGIST-COCHIN.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Jyotish R. Nair<br>
Consultant</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Ophthalmologists</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drpradeepa.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. V. Pradeepa<br>
Consultant Ophthalmologist </p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drrajalakshmi.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. R. Rajalakshmi<br>
Consultant Ophthalmologist
</p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drkannanbala.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. S. Kannan Bala<br>
Consultant Ophthalmologist
</p>
</div>
</div>
</div>
<div class="container backlav teamshow">
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr_Arul Malar.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. S. Arul Malar<br>
Consultant Ophthalmologist</p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/Dr_Usha.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Usha<br>
Consultant Ophthalmologist
</p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drgeetha.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. M. Geetha<br>
Consultant Ophthalmologist <br>(Coimbatore)
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Surgeons / Intensivisit</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/vasudevan.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. R. Vasudevan<br>
General Surgeon </p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drpalaniyappan.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. T. Palaniappan<br>
Consultant intensivist
</p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drsivakumar.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Siva Kumar<br>
Ophthalmic Surgeon
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Radiologist/Sonalogist</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/drshreen.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Dr. Shireen<br>
Sonologist </p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Paramedicals</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/IVY.JPG" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Mrs. S. Ivy<br>
HOD-Assistant General Manager -Nursing </p>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/jayshree.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Ms. N. S. Jayashree<br>
HOD & Sr. Manager- Patient Liaison
</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">HOD Clinical Laboratory</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-12 col-md-12">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/jayashree_lab.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Ms. R. Jayashri<br>
AGM & HOD -Clinical Lab</p>
</div>
</div>
</div>
</section>
<section class="padtb">
<div class="container backlav teamshow">
<p class="text-center Helvetica_Light lh30 padt25 marcol marosho">Administration</p>
<div class="row pl-0 pr-0 ml-0 mr-0 padt25">
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/DGM-A-S-Saravanan1-300x300.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Mr. A.S. Saravanan<br>
General Manager - <br>Marketing </p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/sundar.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Mr. K.B. Sundarraj<br>
AGM & HOD - IT<br>
</p>
</div>
<div class="col-xs-12 col-sm-4 col-md-4">
<p class="text-center Helvetica_Light lh30"><img src="<?php echo get_template_directory_uri();?>/images/suresh.jpg" width="82px" height="98px" class="bordimg"/></p>	
<p class="text-center Helvetica_Light lh30">Mr. S.Suresh Babu<br>
Hod & Manager - Pharmacy
</p>
</div>
</div>
</div>
</section>
	<!--<section class="fullwidth Helvetica_Light padd-top-bottom-70 bg-grey-cloud">
		<div class="container text-center">
			<label class="vision-title Helvetica_Roman">Our Vision Towards Better Health</label>
			<div class="row">
				<div class="col-9 col-sm-3 ov-holder marginauto">
					<div class="our-vision-content ">
						<p class="m-b0 colorfff">To provide world-class health care to patients with diabetes at affordable costs.</p>
					</div>
				</div>
				<div class="col-9 col-sm-3 ov-holder marginauto">
					<div class="our-vision-content ">
						<p class="m-b0 colorfff">To carry out research on diabetes which will ultimately improve the treatment of patients with diabetes. </p>
					</div>
				</div>
				<div class="col-9 col-sm-3 ov-holder marginauto">
					<div class="our-vision-content">
					<p class="m-b0 colorfff"> To train doctors, paramedical personnel, scientists and students on various aspects of diabetes care and research in order to build expertise in the field of diabetes in India. </p>
					</div>
				</div>
			</div>
		</div>
	</section>-->
	<!--div class="fullwidth diabetes_care wow fadeInUp">
		<div class="container">
			<h1 class="text-center Helvetica_Thin fs-46">4,00,000+ patients trust us</h1>
			<div class="row">
				<div class="col-12">
					<ul class="col-12 col-sm-12 float-left lg-pd-0 clearfix services_icons services_icons1">
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center curing"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">26 years of experience in the prevention and management of diabetes </span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center tag"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Only diabetes centre with double international recognitions from WHO and IDF</span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center surgeons"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Well-trained physicians in the management of diabetes </span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center bottle"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Comprehensive check-ups for diabetes in a short duration</span></li>
						<li class="fs-14 col-12 col-sm-12 col-md-2 float-left text-center allcenters"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">3 state-of-the-art facilities comparable to the best in the world</span></li>
					</ul>
					<ul class="col-12 col-sm-12 float-left clearfix services_icons">
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center hp-hand"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">37 diabetes care centres across India and growing </span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center hp-notepad"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">4,20,000 registered diabetes patients from all over</span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center bg-ham"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Customised treatment of diabetes for every patient </span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center mobile-icon"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Only specialities chain with an exclusive app for diabetes management</span></li>
						<li class="fs-14 col-12 col-sm-12 col-md-2 float-left text-center hp-milk"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Exclusive range of proven diabetes health products</span></li>
					</ul>				
					<!--h2 class="Helvetica_Light col-12 col-sm-12 float-left clearfix pt-4 pb-3 fs-18 focolor">At Dr.Mohans we are dedicated to support you in all your requirements during your patient journey</h2>
					<ul class="col-12 col-sm-12 float-left clearfix services_lists">
						<li class="Helvetica_Light fs-18 focolor ml-3">Specialized Diabetic Consultation</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Surgical Expertise</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Express Checkup and Diagnosis</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Nutrition and Fitness Advisory</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Home Care for you your diabetes</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Guidance to buy Diabetic Products</li>
					</ul- ->
					
				</div>
			</div>
		</div>
	</div-->
	<!--<section class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container text-center">
			<label class="text-center Helvetica_Thin fs-46 title-4l">4,00,000+ Patients Trust Us</label>
			<p class="lh30 abt-us-p">Today with over 4,20,000 registered diabetes patients spread across 37 centres in India, DMDSC is one of the fastest growing diabetes centers in the world and is making rapid strides in its march towards �Total Quality Management under one roof� in letter and spirit. </p>
			<ul class="abt-us-services-offered-ul text-center">
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-1.png"/>
					<label class="title-40k">26 years of experience in the prevention and management of diabetes</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-2.png"/>
					<label class="title-40k">Only diabetes centre with double international recognitions from WHO and IDF</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-3.png"/>
					<label class="title-40k">Well-trained physicians in the management of diabetes </label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-4.png"/>
					<label class="title-40k">Comprehensive check-ups for diabetes in a short duration</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-5.png"/>
					<label class="title-40k">State-of-the-art facilities comparable to the best in the world</label>
				</li>
			</ul>
			<ul class="abt-us-services-offered-ul text-center">
				<li class="abt-us-services-offered-li text-center" style="vertical-align:top">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-6.png"/>
					<label class="title-40k">37 diabetes care centres across India and growing</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-7.png"/>
					<label class="title-40k">4,20,000 registered diabetes patients from across the globe</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-8.png"/>
					<label class="title-40k">Customised treatment of diabetes for every patient</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-9.png"/>
					<label class="title-40k">An exclusive app for diabetes management</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-10.png"/>
					<label class="title-40k">Exclusive range of proven diabetes health products</label>
				</li>
			</ul>
		</div>
	</section>-->
	<!--<section class="fullwidth Helvetica_Light padd-top-bottom-70 bg-light-grey">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 title-4l">We are on a Mission</label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-4">
					<img class="img-fluid sm-w-100p" src="<?php echo get_template_directory_uri();?>/images/about-us-(1)_03.jpg"/>
				</div>
				<div class="col-12 col-sm-12 col-md-8 md-p-l-40px">
					<label class="abt-banner-section Helvetica_Bold">mission</label>
					<ul class="primary-ul lh30 mb-45">
						<li>To help individuals with diabetes live a full and healthy life.</li>
						<li>To provide world-class care for diabetes and its complications at reasonable costs</li>
						<li>To set up centres of excellence in diabetes in all parts of India and abroad</li>
					</ul>
					<label class="abt-banner-section Helvetica_Bold">Objectives</label>
					<ul class="primary-ul lh30">
						<li>Holistic care of diabetes and its complications using cutting-edge technology.</li>
						<li>To increase awareness of diabetes and its complications</li>
						<li>Prevention, management and control of diabetes and its complications</li>
						<li>To conduct research that meets international standards</li>
					</ul>
				</div>
			</div>
		</div>
	</section>-->
	<!--<section class="abt-bg fullwidth Helvetica_Light padd-top-bottom-70 abt-us-links">
		<div class="container">
		<p class="text-center Helvetica_Thin fs-18 colorfff">To achieve these objectives, the following institutions were established by Dr. V. Mohan and Late Dr. Rema Mohan</p>
			<div class="row">
				<div class="col-12">
					<ul class="abt-link-ul">
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-1.jpg"/></a></li>
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-2.jpg"/></a></li>
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-3.jpg"/></a></li>
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-4.jpg"/></a></li>
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-5.jpg"/></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>-->
<?php include('spec-footer.php'); ?>