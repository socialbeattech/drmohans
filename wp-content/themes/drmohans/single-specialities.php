<?php
/**
 * The template for displaying Speciality archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?>
 <?php include('spec-header.php'); ?>
	<div class="container-fluid padd0">
		<img src="<?php the_field('desktop_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-none d-md-block"/>
		<?php if( get_field('mobile_banner',get_the_ID())){ ?>
		<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	</div>
	<div class="wow zoomIn spec-banner-caption carousel-caption">
		<?php the_field('banner_text',get_the_ID()); ?>
	</div>
	<section class="breadcrumb">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<div class="site-content-contain">
		<div id="content" class="fullwidth">
			<section class="padd-top-bottom-70 Helvetica_Light">
				<div class="container text-center">
					<p class="color-red fs-42 margin-btm30 speciality-intro"><?php the_field('speciality_section1_title',get_the_ID()); ?></p>
					<p class="fs-18 color-41 lheight35"><?php the_field('speciality_description',get_the_ID()); ?></p>
					<?php /*if( have_rows('specialities_buttons') ): ?>
						<p class="paddtop20">
							<?php while ( have_rows('specialities_buttons') ) : the_row(); ?>
								<a href="<?php the_sub_field('btn_url'); ?>"> <button  class="btn-primary-speciality"><?php the_sub_field('description_button_text'); ?></button></a>
							<?php endwhile; ?>
						</p>
					<?php endif; */?>
					<p class="paddtop20">
							<a href="#tab_spec"> <button  class="btn-primary-speciality">Know More</button></a>
							<a href="<?php echo get_home_url();?>/videos-page/"> <button  class="btn-primary-speciality">Watch Video</button></a>
					</p>
					
				</div>
			</section>
	<section id="tab_spec" class="bg-light-blue padd-top-bottom-70 Helvetica_Light">
		<div class="container text-center">
		<label class="fs-46 lbl-title color000 tab-section-title">Dr. Mohan's - Specialising in Your Diabetes<?php /* the_field('specialising_tabs_title',get_the_ID()); */ ?></label><br/>
		<?php if( have_rows('specialities_tab_title') ): ?>
			<ul class="nav nav-tabs bordernone xs-txt-left justify-content-center">
			<?php $count = 1; $tot=0; ?>
			<?php $tot = count(get_field('specialities_tab_title')); ?>
			<?php while ( have_rows('specialities_tab_title') ) : the_row(); ?>
				<li class="<?php if($count != $tot ){ ?> border-r <?php } ?> list-specialities color-red"><a data-toggle="tab" href="#<?php echo 'd'.$count; ?>"  class="<?php if($count == 1 ){ ?> active <?php } ?> color-red "><?php the_sub_field('tab_title'); ?></a></li>
				<?php $count++; ?>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
		<?php if( have_rows('specialities_tab_title') ): ?>
		  <div class="tab-content">
		  <?php $count = 1; ?>
		  <?php while ( have_rows('specialities_tab_title') ) : the_row(); ?>
			<div id="<?php echo 'd'.$count; ?>" class="tab-pane <?php if($count == 1 ){ ?> in active <?php }else{ ?>fade <?php } ?> col-sm-12 text-left treatments">
				<?php the_sub_field('tab_content'); ?>
			</div>
			<?php $count++; ?>
			<?php endwhile; ?>
			</div>
		<?php endif; ?>
		</div>
		<?php 
		$images = get_field('slider');
		if( $images ){ ?>
		<div class="container">
			<div class="row">
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/prev.png" class="spec-prev img-responsive" alt="speciality"/>
				</div>
				<div class="col-8 col-sm-10 speciality-slider">
					<div class="swiper-container" id="speciality-slider">
						<div class="swiper-wrapper">
						<?php foreach( $images as $image ){ ?>
							<div class="swiper-slide">
								<img src="<?php echo $image['url']; ?>" alt="speciality"/>
							</div>
						<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/next.png" class="spec-next img-responsive" alt="speciality"/>
				</div>
			</div>
		</div>
		<?php } ?>
	</section>
	<section class="fullwidth Helvetica_Light bg-dark-grey padd-top-bottom-70 section-speciality-form">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 lbl-title colorfff"><?php the_field('patient_count_text', 'options'); ?></label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 speciality-video">
				<?php if( have_rows('sub_videos') ){ ?>
							<iframe width="793" height="282" class="spec-main-video" src="https://www.youtube.com/embed/<?php the_field('know_more_video',get_the_ID()); ?>?rel=0" frameborder="0" allow="autoplay; encrypted-media" title="<?php the_field('know_more_video',get_the_ID()); ?>" allowfullscreen></iframe>
						<ul class="ul-sub-video">
							<?php while ( have_rows('sub_videos') ) : the_row(); ?>
							<li class="sub-video">
								<img src="<?php echo get_template_directory_uri();?>/images/youtube-play-button-transparent-png-15.png" class="youtube-play-button cur-pointer" title="<?php the_sub_field('video_id'); ?>" style="z-index: 99;" alt="speciality"/>	
								<img src="https://img.youtube.com/vi/<?php the_sub_field('video_id'); ?>/hqdefault.jpg" width="240" height="200" class="video-thumbnail" title="<?php the_sub_field('video_id'); ?>" style="position:relative" alt="speciality"/>	
							</li>
							<?php endwhile; ?>
						</ul>
				<?php } else { ?>
					<iframe width="793" height="472" src="https://www.youtube.com/embed/<?php the_field('know_more_video',get_the_ID()); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				<?php } ?>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="book_appintment"  style="padding:0 3%">
						<div class="gravity_holder">
							<label class="lbl-title col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters">Book an Appointment</label>
							<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
							<?php gravity_form( 1, $display_title = false, $display_description = false, $tabindex, $ajax = true, $echo = true ); ?>
						</div>							
					</div>							
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth shops padd-top-bottom-70">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-4 float-left">								
					<div class="col-12 col-sm-12 locate-center">	
						<div class="col-12 col-sm-12 borders_stuff wow zoomIn">	
							<h2 class="col-12 col-sm-12 text-center fs-26 whcolor pt-4 Helvetica_Bold">Locate Centre</h2>
							<select class="select_box" id="spec_select_cites">
								<?php echo wpb_list_child_pages(); //picks up page title and link from functions.php ?>
							</select>
							<p class="col-12 col-sm-12 text-center mt-3 cur-pointer spec-view-centers fs-18 whcolor text-none Helvetica_Bold ">View centres</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-4 float-left">								
					<div class="col-12 col-sm-12 defeat-diabetes">	
						<div class="col-12 col-sm-12 borders_stuff wow zoomIn">	
							<h2 class="col-12 col-sm-12 text-center fs-26 whcolor pt-4 Helvetica_Bold">Defeat Diabetes</h2>
							<select class="select_box" id="spec_blogs_cat">
							<?php $categories = get_categories( array(
								'orderby' => 'name',
								'order'   => 'ASC'
							) );
							 
							foreach( $categories as $category ) { ?>
								<option value="<?php echo get_category_link( $category->term_id ); ?>"><?php echo $category->name; ?> </option>
							<?php } ?>
							</select>
							<p class="col-12 col-sm-12 text-center mt-3 fs-18 whcolor text-none cur-pointer Helvetica_Bold spec_read_blogs">Read Blogs</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-4 float-left">								
					<div class="col-12 col-sm-12 diabetes-products">	
						<div class="col-12 col-sm-12 borders_stuff wow zoomIn">	
							<h2 class="col-12 col-sm-12 text-center fs-26 whcolor pt-4 Helvetica_Bold">Diabetes Products</h2>
							<p class="colorfff bg-none margin-bm-1em">All the essentials from food products to books on health care, to help manage your Diabetes</p>
							<p class="col-12 col-sm-12 text-center mt-3"><a href="http://diabetes.ind.in/" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold">Know More</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth knows_diabetes padd-top-bottom-70">
			<div class="container">
				<label class="text-center fs-46 lbl-title Helvetica_Thin understand-ur-diabetes">Understand Your Diabetes</label>
				<div class="row my-5">
				<?php
				global $post;
				$category_name = $post->post_title;
				$args = array( 'category_name' => $category_name, 'post_type' => 'post', 'posts_per_page' => '3');
				$spec_blogs = new WP_Query( $args );
				if ( $spec_blogs->have_posts() ) {
					while($spec_blogs->have_posts()): $spec_blogs->the_post(); ?>
					<div class="col-12 col-sm-4">
						<div class="col-12 col-sm-12 pl-4 pt-4 Helvetica_Light focolor understands wow slideInDown">
							<h2 class="text-left fs-22 Helvetica_Bold focolor"><?php the_title(); ?></h2>
							<?php the_excerpt(); ?>
							<p><span><a href="<?php the_permalink(); ?>" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
				<?php endwhile;
				wp_reset_postdata();
				}else{ 
					$args_default = array( 'category_name' => 'Generic', 'post_type' => 'post', 'posts_per_page' => '3');
					$spec_blogs_default = new WP_Query( $args_default );
					if ( $spec_blogs_default->have_posts() ) { 
						while($spec_blogs_default->have_posts()): $spec_blogs_default->the_post(); ?>
							<div class="col-12 col-sm-4">
								<div class="col-12 col-sm-12 pl-4 pt-4 Helvetica_Light focolor understands wow slideInDown">
									<h2 class="text-left fs-22 Helvetica_Bold focolor"><?php the_title(); ?></h2>
									<?php the_excerpt(); ?>
									<p><span><a href="<?php the_permalink(); ?>" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
								</div>
							</div>
						<?php endwhile;
					} 
				} 
				wp_reset_postdata(); ?>
				</div>
				<p class="text-center">
				<a href="<?php echo get_home_url();?>/category/blogs" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold" style="padding: 12px 23px;background: rgb(237, 22, 22);">READ ALL BLOGS</a></p>
				<!--div class="row">
					<div class="col-12 col-sm-4">
						<div class="col-12 col-sm-12 pl-4 pt-4 understands wow bounceInUp">
							<h2 class="text-left fs-22 Helvetica_Bold focolor">FOOD & DIET</h2>
							<p class="text-left fs-18 Helvetica_Light focolor margin-0">Top Power Foods for Diabetes</p>
							<p><span><a href="" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
					<div class="col-12 col-sm-4">		
						<div class="col-12 col-sm-12 pl-4 pt-4 understands wow bounceInUp">		
							<h2 class="text-left fs-22 Helvetica_Bold focolor">MANAGING DIABETES</h2>
							<p class="text-left fs-18 Helvetica_Light focolor margin-0">10 things Diabetics can do to better self-manage</p>
							<p><span><a href="" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="col-12 col-sm-12 pl-4 pt-4 understands wow bounceInUp">
							<h2 class="text-left fs-22 Helvetica_Bold focolor">DIABETIC PRODUCTS</h2>
							<p class="text-left fs-18 Helvetica_Light focolor margin-0">Did you know how healthy Brown rice is for you</p>
							<p><span><a href="" class="fs-18 Helvetica_Light focolor">Read More</a></span></p>
						</div>
					</div>
				</div-->
				
			</div>
		</section>
	 <?php include('spec-footer.php'); ?>