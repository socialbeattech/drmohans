<?php
/**
 *  Template Name: Videos Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>

	<div class="container-fluid padd0"><!-- Banner -->
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
			<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner <?php if(get_field('mobile_banner',get_the_ID())) { ?> d-none d-md-block <?php } ?>"/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) {?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	</div><!-- Banner Text-->
	<div class="wow zoomIn video-banner-caption carousel-caption">
		<h3 class="wow zoomIn text-left fs-30">Let’s Defeat Diabetes</h3>
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Together</h1>
	</div>
	<div class="container-fluid bg-lgrey">
		<div class="row">
			<div class="col-12 col-md-8 videos-leftbar">			
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','<p>' );
					}
				?>
				<h2 class="video-section-title" style="padding-top:0 !important">Understand Diabetes</h2>
				<div class="latestnews row">
					<div class="col-12 col-sm-6 video_div">
						<iframe width="356" height="201" src="https://www.youtube.com/embed/3J7k611GQ-g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						<h4 class="video-title"><a href="#">Is there a cure for Type 1 Diabetes?</a></h4>
					</div>
					<div class="col-12 col-sm-6 video_div">
						<iframe width="356" height="201" src="https://www.youtube.com/embed/yvjTlJ4K9_8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						<h4 class="video-title">Why is Total Diabetic Care important?</h4>
					</div>
				</div>
				
				<h2 class="video-section-title">Fitness and Nutrition</h2>
				<div class="latestnews row">
					<div class="col-12 col-sm-6 video_div">
						<iframe width="356" height="201" src="https://www.youtube.com/embed/Lan4CeWFbms" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						<h4 class="video-title">What kind of exercise should diabetes patients do?</h4>
					</div>
					<div class="col-12 col-sm-6 video_div">
						<iframe width="356" height="201" src="https://www.youtube.com/embed/iablgVLU7rE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						<h4 class="video-title">What fruits can a diabetes patient eat?</h4>
					</div>
				</div>
				
				<h2 class="video-section-title">Frequently Asked Questions</h2>
				<div class="latestnews row">
					<div class="col-12 col-sm-6 video_div">
						<iframe width="356" height="201" src="https://www.youtube.com/embed/AVLTUptkHK0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						<h4 class="video-title">What's special about Dr Mohan's Diabetes Specialities Centre?</h4>
					</div>
					<div class="col-12 col-sm-6 video_div">
						<iframe width="356" height="201" src="https://www.youtube.com/embed/rQU4BuRfcTY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						<h4 class="video-title">Do people with diabetes have a higher risk of heart attacks?</h4>
					</div>
				</div>
				<h2 class="video-section-title">News and Events</h2>
				<div class="latestnews row">
					<div class="col-12 col-sm-6 video_div">
						<iframe width="356" height="201" src="https://www.youtube.com/embed/ySz37hgtNg4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						<h4 class="video-title">Let's defeat diabetes - Actress Revathi</h4>
					</div>
					<div class="col-12 col-sm-6 video_div">
						<iframe width="356" height="201" src="https://www.youtube.com/embed/Dq0AAmOlJZ4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						<h4 class="video-title">Let’s defeat Diabetes | An initiative by Dr Mohan’s</h4>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4 no-guttersright bg-dl-grey Helvetica_Light leftbar-videopage">
				<label class=" lbl-title featured-post-title">FEATURED POSTS</label>
				<?php
					$args = array(
							'posts_per_page' => 5,
							'meta_key' => 'meta-checkbox',
							'meta_value' => 'yes'
						);
						$featured = new WP_Query($args);
					 
					if ($featured->have_posts()): 
						while($featured->have_posts()): $featured->the_post(); ?>
						<div class="featured-post">
							<label class="featured-title"><?php the_title(); ?></label>
							<p class="featured-content"><?php the_excerpt(); ?></p>
							<a href="<?php the_permalink(); ?>" class=" fs-18 Helvetica_Light focolor hover-border border-bottom-red">Read More</a>
						</div>
							<?php
						endwhile;
					endif;
					wp_reset_postdata();
				?>
				<p class="para-feature">
				At Dr.Mohans we are dedicated to support you in all your requirements during your patient journey
				</p>
				<div class="book_appintment row sidebar-form">
					<div class="gravity_holder gravity_holder-sidebar">
						<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
						<?php gravity_form( 1, $display_title = false, $display_description = false,$tabindex, $ajax = true, $echo = true ); ?>
					</div>							
				</div>
			</div>
		</div>
	</div>
	<div class="fullwidth diabetes_care blogs-icons-part">
		<div class="container">
			<label class="lbl-title text-center Helvetica_Thin fs-46">4,50,000+ Patients Trust Us</label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 text-center" style="margin:auto">
					<div class="row sm-p-b0" style="padding-bottom:40px;">		
						<?php $count=1;
						$numrows = count( get_field('we_care_icons','option') ); ?>
						
						<?php while ( have_rows('we_care_icons','option') ) : the_row(); ?>	
						<div class="col-12 wow zoomIn col-sm-4">
							<img class="icon-40k" src="<?php the_sub_field('icon'); ?>"/>
							<label class="title-40k wow zoomIn"><?php the_sub_field('sub_text'); ?></label>
						</div>
						<?php if($count%3 ==0 && $count != $numrows){ ?>
						</div>							
						<div class="row">	
						<?php } ?>
						<?php $count++; ?>
						<?php endwhile; ?>
					</div>			
				</div>
			</div>
		</div>
	</div>
<?php include('spec-footer.php'); ?>
