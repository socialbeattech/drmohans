<?php
/**
 *  Template Name: About us Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<div class="container-fluid padd0"><!-- Banner -->
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class="banner d-none d-md-block"/>
	<?php endif; ?>
	<?php if(get_field('mobile_banner',get_the_ID())) {?>
		<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
	<?php } ?>
	</div><!-- Banner Text-->
	<div class="wow zoomIn aboutus-banner-caption carousel-caption">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">India's Most</h1>
		<h3 class="wow zoomIn text-left fs-30">Respected and Trusted Diabetes Centre</h3>
	</div>
	<section class="breadcrumb">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 experts_care Helvetica_Light">
		<div class="container">
			<div class="row">
				<h2 class="col-12 col-sm-12 col-md-12 col-lg-12 fs-42 pt-4 pt-sm-4 pt-md-0 color-red text-center Helvetica_Roman lh44" style="margin-bottom: 54px;">At the Forefront of Diabetes Care Since 1991</h2>
				<div class="col-12 col-sm-12 col-md-12 col-lg-6 about-us-videos">
					<div class="">	
						<iframe width="560" height="365" src="https://www.youtube.com/embed/AVLTUptkHK0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>					
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-6 home_experts">
					<p class="col-12 col-sm-12 col-md-10 col-lg-12 text-left fs-18 focolor Helvetica_Light lh30">Dr. Mohan's Group is one of the largest Healthcare group in Asia devoted to the treatment of diabetes and its complications. The various verticals in the group are healthcare services, pharmacy (includes Dr. Mohan’s Health products), education and charity which comes under our Corporate Social responsibility (CSR). Besides these, a separate foundation for diabetes related research called Madras Diabetes Research Foundation was established in September 1996. A highly committed team of over 1200 dedicated personnel is responsible for the growth and success of DMDSC over the past 27 years. </p>   
					<div class="row">
					<h3 class="col-5 text-left py-0 Helvetica_Roman" style="display:inline-block"><a href="<?php echo get_home_url();?>/videos-page/" class="text-uppercase fs-21 Helvetica_Roman">Know more</a></h3>
					<h3 class="col-6 text-left py-0 Helvetica_Roman" style="display:inline-block"><a href="http://drvmohan.com/" class="text-uppercase fs-21 Helvetica_Roman">About Dr Mohan</a></h3>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 bg-grey-cloud">
		<div class="container text-center">
			<label class="vision-title Helvetica_Roman">Our Vision Towards Better Health</label>
			<div class="row">
				<div class="col-9 col-sm-3 ov-holder marginauto">
					<div class="our-vision-content ">
						<p class="m-b0 colorfff">To provide world-class health care to patients with diabetes at affordable costs.</p>
					</div>
				</div>
				<div class="col-9 col-sm-3 ov-holder marginauto">
					<div class="our-vision-content ">
						<p class="m-b0 colorfff">To carry out research on diabetes which will ultimately improve the treatment of patients with diabetes. </p>
					</div>
				</div>
				<div class="col-9 col-sm-3 ov-holder marginauto">
					<div class="our-vision-content">
					<p class="m-b0 colorfff"> To train doctors, paramedical personnel, scientists and students on various aspects of diabetes care and research in order to build expertise in the field of diabetes in India. </p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container text-center">
			<label class="text-center Helvetica_Thin fs-46 title-4l">4,50,000 Patients Trust Us</label>
			<p class="lh30 abt-us-p">Today with over 4,50,000 registered diabetes patients spread across 40+ centres in India, DMDSC is one of the fastest growing diabetes centres in the world and is making rapid strides in its march towards ‘Total Quality Management under one roof’ in letter and spirit. </p>
			
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 text-center" style="margin:auto">
					<div class="row sm-p-b0" style="padding-bottom:40px;">		
						<?php $count=1;
						$numrows = count( get_field('we_care_icons','option') ); ?>
						
						<?php while ( have_rows('we_care_icons','option') ) : the_row(); ?>	
						<div class="col-12 col-sm-4 wow zoomIn">
							<img class="icon-40k" src="<?php the_sub_field('icon'); ?>" alt="Banner"/>
							<label class="title-40k"><?php the_sub_field('sub_text'); ?></label>
						</div>
						<?php if($count%3 ==0 && $count != $numrows){ ?>
						</div>							
						<div class="row">	
						<?php } ?>
						<?php $count++; ?>
						<?php endwhile; ?>
					</div>			
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 bg-light-grey">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 title-4l">We are on a Mission</label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-4">
					<img class="img-fluid sm-w-100p" src="<?php echo get_template_directory_uri();?>/images/about-us-(1)_03.jpg" alt="Banner"/>
				</div>
				<div class="col-12 col-sm-12 col-md-8 md-p-l-40px">
					<label class="abt-banner-section Helvetica_Bold">mission</label>
					<ul class="primary-ul lh30">
						<li>To set up centres of excellence in diabetes in all parts of India and abroad</li>
						<li>To help individuals with diabetes to live a full and healthy life, despite diabetes</li>
					</ul>
					<label class="abt-banner-section Helvetica_Bold">Quality Policy</label>
					<p> We dedicate ourselves “Towards Excellence in Diabetes Care”  by continuously improving our people, processes and technology</p>
					<label class="abt-banner-section Helvetica_Bold">Quality Objectives</label>
					<ul class="primary-ul lh30">
						<li>To continuously improve customer satisfaction</li>
						<li>To upgrade technology continuously</li>
						<li>To motivate, train and develop staff</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section class="abt-bg fullwidth Helvetica_Light padd-top-bottom-70 abt-us-links">
		<div class="container">
		<p class="text-center Helvetica_Thin fs-18 colorfff">To achieve these objectives, the following institutions were established by Dr. V. Mohan and Late Dr. Rema Mohan</p>
			<div class="row">
				<div class="col-12">
					<ul class="abt-link-ul">
						<li class="abt-link"><a href="<?php echo get_home_url();?>/patient-care/diabetes-obesity-centre/"><img src="<?php echo get_template_directory_uri();?>/images/about-us-1.jpg"/></a></li>
						<li class="abt-link"><a href="http://dmhcp.in/"><img src="<?php echo get_template_directory_uri();?>/images/about-us-2.jpg" alt="Banner" /></a></li>
						<li class="abt-link"><a href="http://diabetescourses.in/"><img src="<?php echo get_template_directory_uri();?>/images/about-us-3.jpg"/></a></li>
						<li class="abt-link"><a href="http://mdrf.in/"><img src="<?php echo get_template_directory_uri();?>/images/about-us-4.jpg" alt="Banner"/></a></li>
						<li class="abt-link"><a href="<?php echo get_home_url();?>/csr/"><img src="<?php echo get_template_directory_uri();?>/images/about-us-5.jpg" alt="Banner"/></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>