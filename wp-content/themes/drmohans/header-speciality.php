<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if (is_page('specialities')) { ?>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/style-speciality.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/custom-styles.css">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/swiper.min.css">
<?php } ?>
<?php wp_head(); ?>
<?php if (is_page('specialities')) { ?>
<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/swiper.min.js'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/custom.js'></script>
<?php } ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NGXBRHZ');</script>
<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGXBRHZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="site">
	<header id="masthead">
		<div class="header">
			<div class="container">
				<div class="row">
				<div class="col-12 col-sm-6 col-md-12 col-lg-3 no-gutters header_logo">
					<a href="https://drmohans.com"><img src="<?php echo get_template_directory_uri();?>/images/Drmohans-logo.png" alt="Drmohans" width="194" height="43" /></a>
				</div>
				<div class="col-12 col-sm-6 col-md-11 col-lg-8 no-gutters header_menus" style="float:none !important;">
					<?php if ( has_nav_menu( 'top' ) ) : ?>
						<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-12 col-sm-12 col-md-1 col-lg-1 no-gutters ">
					<span id="b_app" class=""><img src="<?php echo get_template_directory_uri();?>/images/book-appointment.png"/></span>
				</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	<div class="container-fluid">
	<img src="<?php echo get_template_directory_uri();?>/images/banner-specaility.jpg" alt="Banner" class="img-responsive"/>
	</div>
	<div class="spec-banner-caption carousel-caption">
		<h1 class="Helvetica_Light footcare">Diabetes<br/>FootCare</h1>
		<div class="text-center Helvetica_Light fs-36 header-speciality">
			<label class=" fs-24 header-foot-clinic">Foot Clinic at</label>
			<div class="text-center box-content"> Dr. Mohan’s Diabetes <br> Specialities Centre</div>
		</div>
	</div>
	<?php if(is_home() || is_front_page()): ?>
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<div class="swiper-slide"><img src="<?php echo get_template_directory_uri();?>/images/Banner-1.png" alt="Banner" class="img-fluid" width="1366" height="650" />
				<div class="carousel-caption">
					<h1 class="animated bounceInLeft text-left Helvetica_Roman fs-24">Everything you need to</h1>
					<h2 class="animated bounceInRight text-left Helvetica_Medium fs-60">Fight Diabetes</h2>
					<h3 class="animated bounceInLeft text-right Helvetica_Roman fs-24">is right here, at</h3>
					<h4 class="animated bounceInRight text-right Helvetica_Medium fs-60">Dr Mohans</h4>
					<h5 class="animated bounceIn"><a href="" class="fs-26 whcolor text-lowercase">Explore</a></h5>
				</div>
			</div>
			<div class="swiper-slide"><img src="<?php echo get_template_directory_uri();?>/images/Banner-2.png" alt="Banner" class="img-fluid" width="1366" height="650" /></div>
			<div class="swiper-slide"><img src="<?php echo get_template_directory_uri();?>/images/Banner-3.png" alt="Banner" class="img-fluid" width="1366" height="650" /></div>
			<div class="swiper-slide"><img src="<?php echo get_template_directory_uri();?>/images/Banner-4.png" alt="Banner" class="img-fluid" width="1366" height="650" /></div>
		</div>
			<!-- Add Arrows -->
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
	</div>
	<?php endif; ?>
	<div class="site-content-contain">
		<div id="content" class="fullwidth">