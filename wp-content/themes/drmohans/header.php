<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<meta name="google-site-verification" content="fgVdCpAdWUlTZ21m1HQ6LNF6KU62JxYOnsjCz6pFtp0" />
<?php echo the_field('critical_css', get_the_ID() ); ?>
<?php wp_head(); ?>
<script type='text/javascript' src='<?php echo get_template_directory_uri();?>/js/custom.js'></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/swiper.min.css">
<script src="<?php echo get_template_directory_uri();?>/js/swiper.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NGXBRHZ');</script>
<script src="https://wchat.freshchat.com/js/widget.js"></script>

<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGXBRHZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<script>
  window.fcWidget.init({
    token: "dd74a188-6777-4cee-9215-d2cf141186a5",
    host: "https://wchat.freshchat.com"
  });
</script>
<div id="out"></div>
<div id="page" class="site">
	<header id="masthead">
		<div class="header">
			<div class="container">
<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-2 no-gutters header_logo">
					<a href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri();?>/images/logo-new.png" alt="Drmohans"/></a>
					<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false"> 
						<?php
						echo twentyseventeen_get_svg( array( 'icon' => 'bars' ) );
						echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );
						_e( '', 'twentyseventeen' );
						?> 
					</button>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 no-gutters header_menus" style="float:none !important;">
					<?php if ( has_nav_menu( 'top' ) ) : ?>
						<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
					<?php endif; ?>
				</div>
				<div class="  d-none d-sm-none d-md-block d-lg-block col-lg-2 no-gutters header_menus m-auto" style="padding: 0 34px;">
					<div id="b_app" class="cur-pointer text-center color-fff fs-16 " style="color: #fff;  background: rgb(237, 22, 22);padding: 10px;font-family: 'BEBASNEUEREGULAR';">Book Appointment</div>
				</div>
</div>
		</div>
	</header><!-- #masthead -->
	
	<?php if(is_home() || is_front_page()){ ?>
	<div class="swiper-container hidden-sm-down" id="home-swiper-slider">
		<div class="swiper-wrapper">
				<!--<div class="swiper-slide">
			<a href="http://drmohans.com/90-day-challenge/ "><img src="<?php echo get_template_directory_uri();?>/images/BAN-01.jpg" alt="Banner" class="img-fluid" style="width:100%;"/></a>
			 <div class="carousel-caption"> 
					<h1 class="wow zoomIn text-left fs-24" style="font-size:30px">Welcome to</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:30px">Dr. Mohan’s Diabetes <br> Specialities Centre</h3>
					<h5 class="wow zoomIn text-left " style="float:left;margin-top:10px"><a href="#speciality" class="fs-26 whcolor ">Explore</a></h5>
				</div> 
			</div>-->
			<div class="swiper-slide">
			<img src="<?php echo get_template_directory_uri();?>/images/bannerc (1).jpg" alt="Banner" class="img-fluid" style="width:100%;"/>
				<div class="carousel-caption"> 
					<h1 class="wow zoomIn text-left fs-24" style="font-size:30px">Welcome to</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:30px">Dr. Mohan’s Diabetes <br> Specialities Centre</h3>
					<h5 class="wow zoomIn text-left " style="float:left;margin-top:10px"><a href="#speciality" class="fs-26 whcolor ">Explore</a></h5>
				</div>
			</div>
			<div class="swiper-slide">
			<img src="<?php echo get_template_directory_uri();?>/images/banner-b (1).jpg" alt="Banner" class="img-fluid" style="width:100%;"/>
				<div class="carousel-caption"> 
					<h1 class="wow zoomIn text-left fs-24" style="font-size:30px">India’s most trusted</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:30px">diabetes centre</h3>
					<h5 class="wow zoomIn text-left " style="float:left;margin-top:10px"><a href="#speciality" class="fs-26 whcolor ">Explore</a></h5>
				</div>
			</div>
			<div class="swiper-slide">
			<img src="<?php echo get_template_directory_uri();?>/images/banner-e (1).jpg" alt="Banner" class="img-fluid" style="width:100%;"/>
				<div class="carousel-caption"> 
					<h1 class="wow zoomIn text-left fs-24" style="font-size:30px">We are committed</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:30px">to providing comprehensive care<br>for all your needs</h3>
					<h5 class="wow zoomIn text-left " style="float:left;margin-top:10px"><a href="#speciality" class="fs-26 whcolor ">Explore</a></h5>
				</div>
			</div>
			<div class="swiper-slide">
				<img src="<?php echo get_template_directory_uri();?>/images/banner-new-4.jpg" alt="Banner" class="img-fluid" style="width:100%;"/>
				<div class="carousel-caption"> 
					<h1 class="wow zoomIn text-left fs-24" style="font-size:30px">Providing excellence in diabetes care since 1991</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:30px">Dr. Mohan’s Diabetes<br>Specialities Centre</h3>
				</div>
			</div>
		</div> 
		<div class="swiper-pagination"></div>
		<!--i class="fa fa-chevron-left hp-slide-arr home-swiper-button-prev colorfff"></i>
		<i class="fa fa-chevron-right hp-slide-arr home-swiper-button-next colorfff"></i-->
			<!-- Add Arrows -->
	</div>
	<div class="swiper-container d-sm-block d-md-none" id="home-mobile-swiper-slider">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<a href="http://drmohans.com/90-day-challenge/ "><img src="<?php echo get_template_directory_uri();?>/images/MOB-01.jpg" alt="Banner" class="img-fluid" style="width:100%;"/></a>
				<div class="carousel-caption" style="top:5% !important">
					<!-- <h1 class="wow zoomIn text-left fs-24" style="font-size:23px; line-height: 20px !important;">Welcome to</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:23px;line-height: 32px !important;">Dr. Mohan’s Diabetes<br> Specialities Centre</h3>!-->
					<h5 class="wow zoomIn text-left " style="float:left;margin-top:10px"><a href="http://drmohans.com/90-day-challenge/" class="fs-26 whcolor ">Explore</a></h5> 
				</div>
			</div>
			<div class="swiper-slide">
				<img src="<?php echo get_template_directory_uri();?>/images/m-banner1.jpg" alt="Banner" class="img-fluid" style="width:100%;"/>
				<div class="carousel-caption" style="top:5% !important">
					<h1 class="wow zoomIn text-left fs-24" style="font-size:23px; line-height: 20px !important;">Welcome to</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:23px;line-height: 32px !important;">Dr. Mohan’s Diabetes<br> Specialities Centre</h3>
					<h5 class="wow zoomIn text-left " style="float:left;margin-top:10px"><a href="#mobile-speciality" class="fs-26 whcolor ">Explore</a></h5>
				</div>
			</div>
			<div class="swiper-slide">
				<img src="<?php echo get_template_directory_uri();?>/images/m-banner2.jpg" alt="Banner" class="img-fluid" style="width:100%;"/>
				<div class="carousel-caption" style="top:5% !important">
					<h1 class="wow zoomIn text-left fs-24" style="font-size:23px; line-height: 20px !important;">India’s most</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:23px;line-height: 32px !important;">trusted diabetes centre</h3>
					<h5 class="wow zoomIn text-left " style="float:left;margin-top:10px"><a href="#mobile-speciality" class="fs-26 whcolor ">Explore</a></h5>
				</div>
			</div>
			<div class="swiper-slide">
				<img src="<?php echo get_template_directory_uri();?>/images/m-banner3.jpg" alt="Banner" class="img-fluid" style="width:100%;"/>
				<div class="carousel-caption" style="top:5% !important">
					<h1 class="wow zoomIn text-left fs-24" style="font-size:23px; line-height: 20px !important;">Our team spells commitment</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:23px;line-height: 32px !important;">to provide comprehensive care<br> for all your needs</h3>
					<h5 class="wow zoomIn text-left " style="float:left;margin-top:10px"><a href="#mobile-speciality" class="fs-26 whcolor ">Explore</a></h5>
				</div>
			</div>
			<div class="swiper-slide">
				<img src="<?php echo get_template_directory_uri();?>/images/banner-d-mobile-bannernew.jpg" alt="Banner" class="img-fluid" style="width:100%;"/>
				<div class="carousel-caption" style="top:5% !important">
					<h1 class="wow zoomIn text-left fs-24" style="font-size:23px; line-height: 20px !important;">Providing excellence in Diabetes Care Since 1991</h1>
					<h3 class="wow zoomIn text-left Helvetica_Roman fs-24" style="font-size:23px;line-height: 32px !important;">Dr. Mohan’s Diabetes <br> Specialties Centre</h3>
				</div>
			</div>
		</div> 
		
		<div class="mobile-swiper-pagination text-center"></div>
		<!--i class="fa fa-chevron-left hp-slide-arr home-mobile-swiper-button-prev colorfff"></i>
		<i class="fa fa-chevron-right hp-slide-arr home-mobile-swiper-button-next colorfff"></i-->
			<!-- Add Arrows -->
	</div>
	<?php }else{ ?>
	<div class="swiper-container">
		<img src="<?php echo get_template_directory_uri();?>/images/readers_blog-Banner.jpg" alt="blog" class="banner  d-none d-md-block" style="width:100%;"/>
		<img src="<?php echo get_template_directory_uri();?>/images/Mobile-banner.png" alt="blog" class="banner d-sm-block d-md-none" />
		<div class="carousel-caption readers-blog" style="top:60%">
			<h1 class="wow zoomIn text-left fs-24" style="font-size: 53px;">Let's Defeat Diabetes</h1>
		</div>
		<div class="car-blogs col-11 col-sm-4 text-center col-md-4 col-lg-2">
			<div class="">
				<p>To be your most trusted ally in your pursuit of health and well-being</p>
				<p class="btn-blog cur-pointer" data-toggle="modal" data-target="#newsletter-signup">Newsletter Signup</p>
			</div>	
		</div>
	</div>
	<?php } ?>
	<div class="site-content-contain">
		<div id="content" class="fullwidth">