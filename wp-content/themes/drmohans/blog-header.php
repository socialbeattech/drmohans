<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead">
		<div class="header">
			<div class="container">
				<div class="col-12 col-sm-6 col-md-4 col-lg-2 no-gutters header_logo">
					<a href="https://drmohans.com"><img src="<?php echo get_template_directory_uri();?>/images/Drmohans-logo.png" alt="Drmohans" width="194" height="43" /></a>
				</div>
				<div class="col-12 col-sm-6 col-md-8 col-lg-10 float-right no-gutters header_menus">
					<?php if ( has_nav_menu( 'top' ) ) : ?>
						<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
		<img src="<?php echo get_template_directory_uri();?>/images/blog-banner.jpg" alt="blog"/>

	<div class="blog-bannertext">
		<div class="col-sm-6">
			<p>To be your most trusted ally in your pursuit of health and well-being</p>
			<button class="btn-blog">Newsletter Signup</button>
		</div>	
	</div>
	<div class="site-content-contain">
		<div id="content" class="fullwidth">