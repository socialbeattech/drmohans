<?php
/**
 *  Template Name: Homage Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<div class="container-fluid padd0"><!-- Banner -->
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	<?php endif; ?>
	</div><!-- Banner Text-->

	<section class="fullwidth Helvetica_Light experts_care Helvetica_Light" style="background-color:lavender;">
	<section class="breadcrumb">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-6 about-us-videos ">
					<div class="" style="background-color:white;">	<br><br>
					<p class="col-12 col-sm-12 col-md-10 col-lg-12 text-center fs-18 focolor Helvetica_Light lh30"><b>A tribute to the memory of </b></p> 
						<p class="text-center"><img src="<?php echo get_template_directory_uri();?>/images/homeage1.png"/></p>	
                    <p class="col-12 col-sm-12 col-md-10 col-lg-12 text-center fs-18 focolor Helvetica_Light lh30" style="padding-bottom:30px;"><b>LATE PROF.M.VISWANATHAN (1923-1996)</b><br>

(Father of Dr. V. Mohan)<br>

The relentless crusader against Diabetes and<br>

a pioneer in Diabetology in India</p>						
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-6 about-us-videos">
					<div class="" style="background-color:white;">	<br><br>
					<p class="col-12 col-sm-12 col-md-10 col-lg-12 text-center fs-18 focolor Helvetica_Light lh30"><b>A tribute to the memory of </b></p> 
						<p class="text-center"><img src="<?php echo get_template_directory_uri();?>/images/homeage2.png"/></p>	
                    <p class="col-12 col-sm-12 col-md-10 col-lg-12 text-center fs-18 focolor Helvetica_Light lh30" style="padding-bottom:30px;">
					<b>Late Dr. Rema Mohan, M.B.B.S., D.O., Ph.D., FABMS (1954-2011)</b><br>

Co-Founder, Managing Director & Chief Ophthalmologist<br> and wife of Dr.V. Mohan<br>
</p>						
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--<section class="fullwidth Helvetica_Light padd-top-bottom-70 bg-grey-cloud">
		<div class="container text-center">
			<label class="vision-title Helvetica_Roman">Our Vision Towards Better Health</label>
			<div class="row">
				<div class="col-9 col-sm-3 ov-holder marginauto">
					<div class="our-vision-content ">
						<p class="m-b0 colorfff">To provide world-class health care to patients with diabetes at affordable costs.</p>
					</div>
				</div>
				<div class="col-9 col-sm-3 ov-holder marginauto">
					<div class="our-vision-content ">
						<p class="m-b0 colorfff">To carry out research on diabetes which will ultimately improve the treatment of patients with diabetes. </p>
					</div>
				</div>
				<div class="col-9 col-sm-3 ov-holder marginauto">
					<div class="our-vision-content">
					<p class="m-b0 colorfff"> To train doctors, paramedical personnel, scientists and students on various aspects of diabetes care and research in order to build expertise in the field of diabetes in India. </p>
					</div>
				</div>
			</div>
		</div>
	</section>-->
	<!--div class="fullwidth diabetes_care wow fadeInUp">
		<div class="container">
			<h1 class="text-center Helvetica_Thin fs-46">4,00,000+ patients trust us</h1>
			<div class="row">
				<div class="col-12">
					<ul class="col-12 col-sm-12 float-left lg-pd-0 clearfix services_icons services_icons1">
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center curing"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">26 years of experience in the prevention and management of diabetes </span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center tag"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Only diabetes centre with double international recognitions from WHO and IDF</span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center surgeons"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Well-trained physicians in the management of diabetes </span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center bottle"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Comprehensive check-ups for diabetes in a short duration</span></li>
						<li class="fs-14 col-12 col-sm-12 col-md-2 float-left text-center allcenters"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">3 state-of-the-art facilities comparable to the best in the world</span></li>
					</ul>
					<ul class="col-12 col-sm-12 float-left clearfix services_icons">
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center hp-hand"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">37 diabetes care centres across India and growing </span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center hp-notepad"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">4,20,000 registered diabetes patients from all over</span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center bg-ham"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Customised treatment of diabetes for every patient </span></li>
						<li class="fs-14 col-12 col-sm-6 col-md-2 float-left text-center mobile-icon"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Only specialities chain with an exclusive app for diabetes management</span></li>
						<li class="fs-14 col-12 col-sm-12 col-md-2 float-left text-center hp-milk"><span class="col-12 col-sm-12 float-left text-center pt-2 focolor fs-14 Helvetica_Light lg-pd-0">Exclusive range of proven diabetes health products</span></li>
					</ul>				
					<!--h2 class="Helvetica_Light col-12 col-sm-12 float-left clearfix pt-4 pb-3 fs-18 focolor">At Dr.Mohans we are dedicated to support you in all your requirements during your patient journey</h2>
					<ul class="col-12 col-sm-12 float-left clearfix services_lists">
						<li class="Helvetica_Light fs-18 focolor ml-3">Specialized Diabetic Consultation</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Surgical Expertise</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Express Checkup and Diagnosis</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Nutrition and Fitness Advisory</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Home Care for you your diabetes</li>
						<li class="Helvetica_Light fs-18 focolor ml-3">Guidance to buy Diabetic Products</li>
					</ul- ->
					
				</div>
			</div>
		</div>
	</div-->
	<!--<section class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container text-center">
			<label class="text-center Helvetica_Thin fs-46 title-4l">4,00,000+ Patients Trust Us</label>
			<p class="lh30 abt-us-p">Today with over 4,20,000 registered diabetes patients spread across 37 centres in India, DMDSC is one of the fastest growing diabetes centers in the world and is making rapid strides in its march towards �Total Quality Management under one roof� in letter and spirit. </p>
			<ul class="abt-us-services-offered-ul text-center">
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-1.png"/>
					<label class="title-40k">26 years of experience in the prevention and management of diabetes</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-2.png"/>
					<label class="title-40k">Only diabetes centre with double international recognitions from WHO and IDF</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-3.png"/>
					<label class="title-40k">Well-trained physicians in the management of diabetes </label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-4.png"/>
					<label class="title-40k">Comprehensive check-ups for diabetes in a short duration</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-5.png"/>
					<label class="title-40k">State-of-the-art facilities comparable to the best in the world</label>
				</li>
			</ul>
			<ul class="abt-us-services-offered-ul text-center">
				<li class="abt-us-services-offered-li text-center" style="vertical-align:top">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-6.png"/>
					<label class="title-40k">37 diabetes care centres across India and growing</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-7.png"/>
					<label class="title-40k">4,20,000 registered diabetes patients from across the globe</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-8.png"/>
					<label class="title-40k">Customised treatment of diabetes for every patient</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-9.png"/>
					<label class="title-40k">An exclusive app for diabetes management</label>
				</li>
				<li class="abt-us-services-offered-li text-center">
					<img src="<?php echo get_template_directory_uri();?>/images/abt-icon-10.png"/>
					<label class="title-40k">Exclusive range of proven diabetes health products</label>
				</li>
			</ul>
		</div>
	</section>-->
	<!--<section class="fullwidth Helvetica_Light padd-top-bottom-70 bg-light-grey">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 title-4l">We are on a Mission</label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-4">
					<img class="img-fluid sm-w-100p" src="<?php echo get_template_directory_uri();?>/images/about-us-(1)_03.jpg"/>
				</div>
				<div class="col-12 col-sm-12 col-md-8 md-p-l-40px">
					<label class="abt-banner-section Helvetica_Bold">mission</label>
					<ul class="primary-ul lh30 mb-45">
						<li>To help individuals with diabetes live a full and healthy life.</li>
						<li>To provide world-class care for diabetes and its complications at reasonable costs</li>
						<li>To set up centres of excellence in diabetes in all parts of India and abroad</li>
					</ul>
					<label class="abt-banner-section Helvetica_Bold">Objectives</label>
					<ul class="primary-ul lh30">
						<li>Holistic care of diabetes and its complications using cutting-edge technology.</li>
						<li>To increase awareness of diabetes and its complications</li>
						<li>Prevention, management and control of diabetes and its complications</li>
						<li>To conduct research that meets international standards</li>
					</ul>
				</div>
			</div>
		</div>
	</section>-->
	<!--<section class="abt-bg fullwidth Helvetica_Light padd-top-bottom-70 abt-us-links">
		<div class="container">
		<p class="text-center Helvetica_Thin fs-18 colorfff">To achieve these objectives, the following institutions were established by Dr. V. Mohan and Late Dr. Rema Mohan</p>
			<div class="row">
				<div class="col-12">
					<ul class="abt-link-ul">
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-1.jpg"/></a></li>
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-2.jpg"/></a></li>
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-3.jpg"/></a></li>
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-4.jpg"/></a></li>
						<li class="abt-link"><a href=""><img src="<?php echo get_template_directory_uri();?>/images/about-us-5.jpg"/></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>-->
<?php include('spec-footer.php'); ?>