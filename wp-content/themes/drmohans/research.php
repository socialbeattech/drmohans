<?php
/**
 *  Template Name: Research Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class="img-responsive banner d-none d-md-block"/>
	<?php endif; ?>
	<?php if(get_field('mobile_banner',get_the_ID())) {?>
		<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
	<?php } ?>
	<!-- Banner Text-->
	<div class="wow zoomIn research-banner-caption carousel-caption">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Detailing Everything</h1>
		<h3 class="wow zoomIn text-left fs-30">that supports you and <br/>your diabetes</h3>
	</div>
	<section style="background:#f4f4f9 !important">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list Helvetica_Light bg-light-grey research-intro">
		<div class="container">
			<p class="subtitle-p">A journey through the world of</p>
			<label class="lbl-title red-title HelveticaNeueLTStd-Md text-center">
				Diabetes Research
			</label>
			<p class="text-center research-intro-content">
				Recognizing the need for a world-class research institute to conduct studies on diabetes and its complications in India, Dr. V. Mohan and Dr. M. Rema established the Madras Diabetes Research Foundation (MDRF) in 1996. From its humble beginnings in a small room, MDRF has gone from strength to strength and is now recognized worldwide as a premier research institute for diabetes and its complications. Come join us on a journey through the world of diabetes research!
			</p>
			<div class="row">
				<div class="col-sm-6 text-center">
					<div class="bg-dark-grey research-intro-box">
						MDRF and DEAKIN University, Australia to collaborate in Diabetes Research & Education<br>
						<a href="http://www.mdrf.in/publications/current_year_pub.html" target="_blank"><button class="btn-primary-wider">Read More</button></a>
					</div>
				</div>
				<div class="col-sm-6 text-center">
					<div class="bg-dark-grey research-intro-box">
						Global Diabetes Research Centre (GDRC) for epidemiological research of diabetes and its complications<br>
						<a href="http://globaldiabetes.org/" target="_blank"><button class="btn-primary-wider">Read More</button></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list Helvetica_Light">
		<div class="container">
			<label class="m-b0 red-title lbl-title HelveticaNeueLTStd-Md text-center">
				Advanced Research
			</label>
			<p class="red-subtitle text-center">in diabetes </p>
			<ul class="homecare-services-offered-ul research-services text-center">
				<li class="homecare-services-offered-li text-center" style="vertical-align:top">
					<img class="wow zoomIn" src="<?php echo get_template_directory_uri();?>/images/research-icon1.png" alt="Research"/>
					<label class="service-title wow zoomIn">Diabetology </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img class="wow zoomIn" src="<?php echo get_template_directory_uri();?>/images/research-icon2.png" alt="Research"/>
					<label class="service-title wow zoomIn">Ocular <br/>Research </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img class="wow zoomIn" src="<?php echo get_template_directory_uri();?>/images/research-icon3.png" alt="Research"/>
					<label class="service-title wow zoomIn">Cell and<br> Molecular Biology </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img class="wow zoomIn" src="<?php echo get_template_directory_uri();?>/images/research-icon4.png" alt="Research"/>
					<label class="service-title wow zoomIn">Molecular<br>Genetics  </label>
				</li>
			</ul>
			<ul class="homecare-services-offered-ul research-services text-center">
				<li class="homecare-services-offered-li text-center">
					<img class="wow zoomIn" src="<?php echo get_template_directory_uri();?>/images/research-icon5.png" alt="Research"/>
					<label class="service-title wow zoomIn">Research<br>Operations </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img class="wow zoomIn" src="<?php echo get_template_directory_uri();?>/images/research-icon6.png" alt="Research"/>
					<label class="service-title wow zoomIn">Clinical<br> Trials </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img class="wow zoomIn" src="<?php echo get_template_directory_uri();?>/images/research-icon7.png" alt="Research"/>
					<label class="service-title wow zoomIn">Food & Nutrition <br>Research  </label>
				</li>
				<li class="homecare-services-offered-li text-center">
					<img class="wow zoomIn" src="<?php echo get_template_directory_uri();?>/images/research-icon8.png" alt="Research"/>
					<label class="service-title wow zoomIn">Physical Activity <br>Research  </label>
				</li>
			</ul>
			
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-light-grey padd-top-bottom-70 section-resource">
		<div class="container">
			<label class="text-center Helvetica_Thin lbl-title fs-46 color000"> Helpful Resources</label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-4 text-center resource-holder">
				<img src="<?php echo get_template_directory_uri();?>/images/research-resource-1.jpg" alt="Research"/>
				<a href="<?php echo get_home_url();?>/precision-diabetes/"><span class="resource-text">Precision Diabetes</span></a>
				</div>
				<div class="col-12 col-sm-12 col-md-4 text-center resource-holder">
				<img src="<?php echo get_template_directory_uri();?>/images/research-resource-2.jpg" alt="Research"/>
				<a href="<?php echo get_template_directory_uri();?>/images/Foot_atlas_cover page.jpg" download><span class="resource-text">Food Atlas</span></a>
				</div>
				<div class="col-12 col-sm-12 col-md-4 text-center resource-holder">
				<img src="<?php echo get_template_directory_uri();?>/images/research-resource-3.jpg" alt="Research"/>
				<a href="http://www.mdrf.in/publications/current_year_pub.html" target="_blank"><span class="resource-text">Research at MDRF</span></a>
				</div>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>