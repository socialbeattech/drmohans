<?php
/**
 *  Template name: popup
 *
 * 
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  id="onload">
    <div class="modal-dialog popup_form">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close formclose_popup" data-dismiss="modal">X</button>
        </div>
        <div class="modal-body">
			<div class="book_appintment">
				<div class="gravity_holder">
					<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
					<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
					<?php gravity_form( 13, $display_title = false, $display_description = false,$tabindex, $ajax = true, $echo = true ); ?>
				</div>							
			</div>	
        </div>
      </div>

    </div>
</div>

<style type="text/css">
.popup_form .modal-header { padding: 0px; margin: 0px auto; position: absolute; z-index: 999; border: 0px none; top: -20px; right: 10px; }
.popup_form .formclose_popup { margin: 0px auto; font-size: 15px; font-weight: normal; opacity: 1;filter: alpha(opacity=100); background: white; color: black; padding: 10px; border-radius: 26px; height: 30px; width: 30px; }
.popup_form .modal-body { padding: 0px; margin: 0px auto; }
.popup_form .modal-content { background : transparent !important; }
#onload { z-index : 9999 !important; }
.gravity_holder .gform_wrapper #input_13_9 {  display: inline-block;  width: 87% !important; }
</style>
<script type="text/javascript">
jQuery.noConflict();
jQuery(window).load(function(){
   setTimeout(function(){
       jQuery('#onload').modal('show');
   }, 5000);
});
</script>

<?php get_footer(); ?>