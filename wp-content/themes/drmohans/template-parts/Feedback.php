<?php
/**
 *  Template name: Feedbacks
 *
 * 
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<div class="fullcols">
	<div class="container">
		<div class="col-12 col-sm-12 col-md-12 float-left p-o m-0">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>			
		</div>
		<div class="feedback_holder col-12 col-sm-12 float-left p-0 m-0">
		<h2 class="col-12 col-sm-12 float-left p-0 m-0 text-center">OP/IP feedback form</h2>
			<form action="https://drmohans.com/feedback-sendtogravity.php" method="POST">
				<div class="col-12 col-sm-12 float-left p-0 my-3">
					<div class="col-12 col-sm-12 col-md-6 float-left field_groups">
						<div class="labels">Patient / Attender Name*:</div>
						<div class="fields"><input name="attender_name" type="text" value="" id="attender_name" required></div>
						<div id="anamerror" class="required_error"></div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 float-left field_groups">
						<div class="labels">M. No.*:</div>
						<div class="fields"><input name="mnno" id="mnno" type="text" value="" required></div>
						<div class="fields required_error"><span id="mnoError"></span></div>
						<div id="mnerror" class="required_error"></div>						
					</div>
				</div>
				
				<div class="col-12 col-sm-12 float-left p-0 my-3">
					<div class="col-12 col-sm-12 col-md-6 float-left field_groups">
						<div class="labels">Branch*:</div>
						<div class="fields">
							<select name="feedback_branches" id="feedback_branches" required>
								<option value="">Please Select Location</option>                  
								<option value="Gopalapuram - Chennai">Gopalapuram - Chennai</option>                    
								<option value="Anna nagar - Chennai">Anna nagar - Chennai</option>                    
								<option value="Avadi - Chennai">Avadi - Chennai</option>                    
								<option value="Tambaram - Chennai">Tambaram - Chennai</option>                    
								<option value="Karapakkam - Chennai">Karapakkam - Chennai</option>                   
								<option value="Vadapalani - Chennai">Vadapalani - Chennai</option>                   
								<option value="Velachery - Chennai">Velachery - Chennai</option>                   
								<option value="Porur - Chennai">Porur - Chennai</option>                   
								<option value="Selaiyur - Chennai">Selaiyur - Chennai</option>                  
								<option value="Coimbatore">Coimbatore</option>                    
								<option value="Madurai">Madurai</option>                   
								<option value="Salem">Salem</option>                   
								<option value="Gudiyatham">Gudiyatham</option>                    
								<option value="Vellore">Vellore</option>                    
								<option value="Thanjavur">Thanjavur</option>                   
								<option value="Tuticorin">Tuticorin</option>                    
								<option value="Erode">Erode</option>                    
								<option value="Trichy">Trichy</option>                    
								<option value="Chunampet">Chunampet</option>                    
								<option value="Kancheepuram">Kancheepuram</option>                   
								<option value="Pondicherry">Pondicherry</option>                   
								<option value="Domalguda,Indira Park Road - Hyderabad">Domalguda,Indira Park Road - Hyderabad</option>                 
								<option value="Jubilee Hills - Hyderabad">Jubilee Hills - Hyderabad</option>                   
								<option value="Kukatpally - Hyderabad">Kukatpally - Hyderabad</option>                    
								<option value="Dilsukh nagar - Hyderabad">Dilsukh nagar - Hyderabad</option>                   
								<option value="Secunderabad - Hyderabad">Secunderabad - Hyderabad</option>                  
								<option value="Tolichowki - Hyderabad">Tolichowki - Hyderabad</option>                   
								<option value="A.S.Rao nagar - Hyderabad">A.S.Rao nagar - Hyderabad</option>                 
								<option value="Kadapa - Andhra Pradesh">Kadapa - Andhra Pradesh</option>                   
								<option value="Nellore - Andhra Pradesh">Nellore - Andhra Pradesh</option>                   
								<option value="Tirupati - Andhra Pradesh">Tirupati - Andhra Pradesh</option>                   
								<option value="Rajahmundry - Andhra Pradesh">Rajahmundry - Andhra Pradesh</option>             
								<option value="Vijayawada - Andhra Pradesh">Vijayawada - Andhra Pradesh</option>               
								<option value="Visakhapatnam - Andhra Pradesh">Visakhapatnam - Andhra Pradesh</option>  
                                <option value="Guntur - Andhra Pradesh">Visakhapatnam - Andhra Pradesh</option>
                                <option value="Bhubaneswar - Odisha">Bhubaneswar - Odisha</option>                   
								<option value="Lucknow - Uttar Pradesh">Lucknow - Uttar Pradesh</option>                    
								<option value="Delhi - Kirti nagar">Delhi - Kirti nagar</option>                   
								<option value="Indira nagar - Bengaluru">Indira nagar - Bengaluru</option>                   
								<option value="JP nagar - Bengaluru">JP nagar - Bengaluru</option>                   
								<option value="Malleshwaram - Bengaluru">Malleshwaram - Bengaluru</option>                    
								<option value="Whitefield - Bengaluru">Whitefield - Bengaluru</option>                   
								<option value="Mangalore">Mangalore</option>                    
								<option value="Mysuru">Mysuru</option>                   
								<option value="Trivandrum">Trivandrum</option>                   
								<option value="Kochi">Kochi</option>                   
								<option value="Kaikhali - Kolkata - West Bengal">Kaikhali - Kolkata - West Bengal</option>                    
								<option value="Rash Behari Avenue - Kolkata - West Bengal">Rash Behari Avenue - Kolkata - West Bengal</option> 
                                <option value="Basavanagudi-Bengaluru">Basavanagudi-Bengaluru</option> 
							</select>						
						</div>
						<div id="fbserror" class="required_error"></div>						
					</div>
					<div class="col-12 col-sm-12 col-md-6 float-left field_groups">
						<div class="labels">Visit date*:</div>
						<div class="fields"><input name="visitdate" type="text" id="datepickers" required readonly></div>	
						<div id="visiterror" class="required_error"></div>						
					</div>
				</div>
				
				<div class="col-12 col-sm-12 float-left p-0 my-3">
					<div class="col-12 col-sm-12 col-md-6 float-left field_groups">
						<div class="labels">Email:</div>
						<div class="fields"><input name="email" id="email" type="email" value="" ></div>
					</div>
                    
                    <div class="col-12 col-sm-12 col-md-6 float-left field_groups">
						<div class="labels">Phone Number*:</div>
						<div class="fields"><input name="phone" id="phone" type="tel" value="" required ></div>
						<div id="contacterror" class="required_error"></div>
						<div id="phoneerror" class="required_error"></div>
						<div id="patternerror" class="required_error"></div>
					</div>
					
				</div>
				<div id="basefielderror" class="required_error"></div>
				<div class="col-12 col-sm-12 float-left mt-5 mb-0"><h3>Kindly rate our services for the below mentioned attributes:</h3></div>
				<div id="service_ratings" class="required_error"></div>
				<div class="col-12 col-sm-12 float-left my-4 field_groups register_admission">
					<div class="labels">1. Registration / Admission formalities:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" name="admission_formalities" id="admission_formalities" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="admission_formalities"  id="admission_formalities" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="average"><input type="radio" name="admission_formalities"  id="admission_formalities" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="poor"><input type="radio" name="admission_formalities"  id="admission_formalities" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="admission_formalities"  id="admission_formalities" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="admission_formalities_remarks"><textarea name="admission_formalities_remarks_detailed" id="admission_formalities_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value="" ></textarea></div>
					<div id="admission_formalities_req" class="required_error"></div>	
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups phone_call">
					<div class="labels">2. Phone call handling (Call Centre / Appointment):</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="phone_handling" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="phone_handling" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="phone_handling" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="phone_handling" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="phone_handling" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="phone_handling_remarks"><textarea name="phone_handling_remarks_detailed" id="phone_handling_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					<div id="phone_handling_req" class="required_error"></div>	
				</div>
					
				<div class="col-12 col-sm-12 float-left my-4 field_groups billing">
					<div class="labels">3. Billing:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="billing" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="billing" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="billing" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="billing" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="billing" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="billing_remarks"><textarea name="billing_remarks_detailed" id="billing_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					<div id="billing_req" class="required_error"></div>					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups labs">
					<div class="labels">4. Laboratory & Blood collection:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="laboratory" name="laboratory" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="laboratory" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="laboratory" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="laboratory" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="laboratory" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="laboratory_remarks"><textarea name="laboratory_remarks_detailed" id="laboratory_remarks_detailed" rows="2" cols="50" placeholder="Kindly give your feedback" value=""></textarea></div>
					<div id="laboratory_req" class="required_error"></div>
				</div>
					
				<div class="col-12 col-sm-12 float-left my-4 field_groups investics">
					<div class="labels">5. Investigations (ECG/ Doppler / Biothesiometry / Foot examination/ X-ray):</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="investigatons" name="investigatons" value="Excellent" />Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="investigatons" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="investigatons" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="investigatons" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="investigatons" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="investigatons_remarks"><textarea name="investigatons_remarks_detailed" id="investigatons_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups dietics">
					<div class="labels">6. Diabetes class & Diet advice:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="diet_advice" name="diet_advice" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="diet_advice" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="diet_advice" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="diet_advice" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="diet_advice" value="NA"/>NA</label>
						</span>										
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="diet_advice_remarks"><textarea name="diet_advice_remarks_detailed" id="diet_advice_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups diagnosis">
					<div class="labels">7. Diagnosis & Treatment:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="diagnosis_treatment" name="diagnosis_treatment" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="diagnosis_treatment" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="diagnosis_treatment" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="diagnosis_treatment" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="diagnosis_treatment" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="diagnosis_treatment_remarks"><textarea name="diagnosis_treatment_remarks_detailed" id="diagnosis_treatment_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups nursing">
					<div class="labels">8. Nursing Care (IP/OT/Surgical Dressing):</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="nursing_care" name="nursing_care" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="nursing_care" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="nursing_care" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="nursing_care" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="nursing_care" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="nursing_care_remarks"><textarea name="nursing_care_remarks_detailed" id="nursing_care_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left mt-5 mb-0"><h3>Other Specialities:</h3></div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups eye_depart">
					<div class="labels">1. Eye:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" name="eyechecks" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="eyechecks" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="average"><input type="radio" name="eyechecks" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="poor"><input type="radio" name="eyechecks" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="eyechecks" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="eyechecks_remarks"><textarea name="eyechecks_remarks_detailed" id="eyechecks_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups fitness">
					<div class="labels">2. Fitness/Physiotherapy:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="fitness_phy" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="fitness_phy" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="fitness_phy" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="fitness_phy" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="fitness_phy" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="fitness_phy_remarks"><textarea name="fitness_phy_remarks_detailed" id="fitness_phy_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left mt-5 mb-0"><h3>Your ratings on:</h3></div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups guidance">
					<div class="labels">1. Adherence to appointment time:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" name="guidance_care" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="guidance_care" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="average"><input type="radio" name="guidance_care" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="poor"><input type="radio" name="guidance_care" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="guidance_care" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="guidance_care_remarks"><textarea name="guidance_care_remarks_detailed" id="guidance_care_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups clinical">
					<div class="labels">2. Homecare:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="clinical_secs" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="clinical_secs" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="clinical_secs" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="clinical_secs" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="clinical_secs" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="clinical_secs_remarks"><textarea name="clinical_secs_remarks_detailed" id="clinical_secs_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups catering">
					<div class="labels">3. Catering services:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="caterings" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="caterings" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="caterings" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="caterings" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="caterings" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="caterings_remarks"><textarea name="caterings_remarks_detailed" id="caterings_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups pharmacy">
					<div class="labels">4. Pharmacy service:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="pharmacys" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="pharmacys" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="pharmacys" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="pharmacys" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="pharmacys" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="pharmacys_remarks"><textarea name="pharmacys_remarks_detailed" id="pharmacys_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
				
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups footwear">
					<div class="labels">5. Footwear:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="footwears" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="footwears" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="footwears" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="footwears" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="footwears" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="footwears_remarks"><textarea name="footwears_remarks_detailed" id="footwears_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups cleanliness">
					<div class="labels">6. Cleanliness:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="cleanhos" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="cleanhos" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="cleanhos" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="cleanhos" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="cleanhos" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="cleanhos_remarks"><textarea name="cleanhos_remarks_detailed" id="cleanhos_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups insurance">
					<div class="labels">7. Insurance:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="insurances" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="insurances" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="insurances" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="insurances" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="insurances" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="insurances_remarks"><textarea name="insurances_remarks_detailed" id="insurances_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups security">
					<div class="labels">8. Security:</div>
					<div class="fields">
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="excellent"><input type="radio" class="excellent" name="securitys" value="Excellent"/>Excellent</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">
							<label class="good"><input type="radio" name="securitys" value="Good"/>Good</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="average"><input type="radio"  name="securitys" value="Average"/>Average</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">		
							<label class="poor"><input type="radio"  name="securitys" value="Poor"/>Poor</label>
						</span>
						<span class="col-12 col-sm-12 col-md-2 float-left p-0">							
							<label class="NA"><input type="radio" name="securitys" value="NA"/>NA</label>
						</span>					
					</div>
					<div class="col-12 col-sm-12 float-left mt-3" id="securitys_remarks"><textarea name="securitys_remarks_detailed" id="securitys_remarks_detailed" rows="2" cols="50" placeholder="Please give your inputs for improvement" value=""></textarea></div>
					
				</div>
				
				<div class="col-12 col-sm-12 float-left my-4 field_groups suggestions">
					<textarea name="suggestions" id="suggestions" rows="2" cols="50" placeholder="Your suggestions/appreciations please" value=""></textarea>
				</div>
				<?php $sourceurl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" .$_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']; ?> 
				<input type="hidden" value="<?php echo $sourceurl; ?>" name="sourceurl">
				<input type="hidden" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" name="remoteip">
				<div class="col-12 col-sm-12 float-left field_groups submissions text-center">
					<input type="submit" value="Submit" id="submit">
				</div>
			</form>	
			
		</div>
	</div>
</div>
<style type="text/css">
.feedback_holder .required_error { float : left; width : 100%; }
.feedback_holder .required_error span { color: red; float: left; font-family: Verdana;  font-size: 15px;  padding: 0 0 5px; width: 100%; font-weight : bold; }
h2 { color: #000; font-family: verdana; font-size: 25px; }
h3 { color: #000; font-family: verdana; font-size: 22px; padding :0; margin : 0 auto; }
form { float : left; width : 100%; }
.field_groups { }
.labels { color: #000; float: left; font-family: Verdana;  font-size: 16px;  padding: 0 0 5px; width: 100%; }
.fields label { color: #000; float: left; font-family: Verdana;  font-size: 15px;  padding: 0 0 5px; width: 100%; font-weight : normal; }
.fields label.active {  font-weight : bold; color: green;  }
.fields { }
.fields input[type="text"], .fields input[type="email"], .fields input[type="tel"], .fields input[type="date"]  { float: left;  margin: 0;  padding: 5px 0;  width: 100%; }
.fields { float : left; width : 100%; padding :0; margin : 0 auto; }
.fields input[type="radio"] { }
.fields input[type="radio"]:hover { }
.fields input[type="radio"] {  display: none; margin: 0;  padding: 0; }
.fields input[type="radio"]:checked  { }
.fields label {display: inline-block; float: left;  height: auto;  margin: 0 auto;  padding: 0; text-align: center;  width: 80%; cursor : pointer; }
.fields label.excellent { background : url("<?php echo site_url();?>/wp-content/themes/drmohans/images/excellent.png") no-repeat 7px 0px; }
.fields label.good { background : url("<?php echo site_url();?>/wp-content/themes/drmohans/images/good.png") no-repeat 7px 0px; }
.fields label.average { background : url("<?php echo site_url();?>/wp-content/themes/drmohans/images/average.png") no-repeat 7px 0px; }
.fields label.poor { background : url("<?php echo site_url();?>/wp-content/themes/drmohans/images/poor.png") no-repeat 7px 0px; }
.fields label.NA { background : url("<?php echo site_url();?>/wp-content/themes/drmohans/images/na.png") no-repeat 7px 0px; }
.fields label:hover, .fields label:active { font-style : italic; color: green !important; }
.fields input[type=radio]:checked + label { color: green; font-style: italic; } 
.submissions #submit { background : #ed1616; color : #fff; font-family: VerdaNA;  font-size: 15px; border-radius : 10px; }
select { width  :100%; }
.page-id-5660 #masthead { background: #dddddd; }
.page-id-5660 .fullcols { float: left; width: 100%; position: relative; margin: 0px auto; padding: 6em 0px 0px; }
.page-id-5660 .swiper-container { display : none !important; }
</style>


<script type="text/javascript">

var $j = jQuery.noConflict();
jQuery(document).ready(function($j){

	
$j("#mnno").keypress(function (e) {
	var keyCode = e.keyCode || e.which; 
	$j("#mnoError").html("");
	var regex = /^[A-Za-z0-9]+$/;
	var isValid = regex.test(String.fromCharCode(keyCode));
	if (!isValid) {
		$j("#mnoError").html("Only Alphabets and Numbers allowed.");
	}
	return isValid;
});


	
$j("#datepickers").datepicker({ dateFormat: "dd-mm-yy", maxDate: '0' });
$j("#admission_formalities_remarks").css("display","none");
$j("#phone_handling_remarks").css("display","none");
$j("#billing_remarks").css("display","none");
$j("#laboratory_remarks").css("display","none");
$j("#investigatons_remarks").css("display","none");
$j("#diet_advice_remarks").css("display","none");
$j("#diagnosis_treatment_remarks").css("display","none");
$j("#nursing_care_remarks").css("display","none");
$j("#eyechecks_remarks").css("display","none");
$j("#fitness_phy_remarks").css("display","none");
$j("#guidance_care_remarks").css("display","none");
$j("#clinical_secs_remarks").css("display","none");
$j("#caterings_remarks").css("display","none");
$j("#pharmacys_remarks").css("display","none");
$j("#footwears_remarks").css("display","none");
$j("#cleanhos_remarks").css("display","none");
$j("#insurances_remarks").css("display","none");
$j("#securitys_remarks").css("display","none");

	$j('.register_admission label').click(function()
	{
		$j('.register_admission label').removeClass("active");
		$j(this).addClass("active");
		var field1  = $j("input[name=admission_formalities]:checked").val();
		if(field1=="Poor" || field1=="Average") { $j("#admission_formalities_remarks_detailed").attr("required", ""); $j("#admission_formalities_remarks").slideDown("fast"); }
		else { $j("#admission_formalities_remarks").css("display","none"); $j("#admission_formalities_remarks_detailed").removeAttr("required", "");  $j("#admission_formalities_remarks").slideUp("fast"); }
		if(field1!='') { $j('#admission_formalities_req').empty(); }		
	});
	
	$j('.phone_call label').click(function()
	{
		$j('.phone_call label').removeClass("active");
		$j(this).addClass("active");
		var field2  = $j("input[name=phone_handling]:checked").val();
		if(field2=="Poor" || field2=="Average") { $j("#phone_handling_remarks_detailed").attr("required", ""); $j("#phone_handling_remarks").slideDown("fast"); }
		else { $j("#phone_handling_remarks").css("display","none"); $j("#phone_handling_remarks_detailed").removeAttr("required", ""); $j("#phone_handling_remarks").slideUp("fast"); }
		if(field2!='') { $j('#phone_handling_req').empty(); }
	});
	

	$j('.billing label').click(function()
	{
		$j('.billing label').removeClass("active");
		$j(this).addClass("active");
		var fieldnew  = $j("input[name=billing]:checked").val();
		if(fieldnew=="Poor" || fieldnew=="Average") { $j("#billing_remarks_detailed").attr("required", ""); $j("#billing_remarks").slideDown("fast"); }
		else { $j("#billing_remarks").css("display","none"); $j("#billing_remarks_detailed").removeAttr("required", ""); $j("#billing_remarks").slideUp("fast"); }
		if(fieldnew!='') { $j('#billing_req').empty(); }
	});
	
	$j('.labs label').click(function()
	{
		$j('.labs label').removeClass("active");
		$j(this).addClass("active");
		var field3  = $j("input[name=laboratory]:checked").val();
		if(field3=="Poor" || field3=="Average") { $j("#laboratory_remarks_detailed").attr("required", ""); $j("#laboratory_remarks").slideDown("fast"); }
		else { $j("#laboratory_remarks").css("display","none"); $j("#laboratory_remarks_detailed").removeAttr("required", ""); $j("#laboratory_remarks").slideUp("fast"); }
		if(field3!='') { $j('#laboratory_req').empty(); }
	});

	$j('.investics label').click(function()
	{
		$j('.investics label').removeClass("active");
		$j(this).addClass("active");
		var field4  = $j("input[name=investigatons]:checked").val();
		if(field4=="Poor" || field4=="Average") { $j("#investigatons_remarks_detailed").attr("required", ""); $j("#investigatons_remarks").slideDown("fast");  }
		else { $j("#investigatons_remarks").css("display","none"); $j("#investigatons_remarks_detailed").removeAttr("required", ""); $j("#investigatons_remarks").slideUp("fast"); }
		if(field4!='') { $j('#investigatons_req').empty(); }
	});
	
	$j('.dietics label').click(function()
	{
		$j('.dietics label').removeClass("active");
		$j(this).addClass("active");
		var field5  = $j("input[name=diet_advice]:checked").val();
		if(field5=="Poor" || field5=="Average") { $j("#diet_advice_remarks_detailed").attr("required", ""); $j("#diet_advice_remarks").slideDown("fast"); }
		else { $j("#diet_advice_remarks").css("display","none"); $j("#diet_advice_remarks_detailed").removeAttr("required", ""); $j("#diet_advice_remarks").slideUp("fast"); }
		if(field5!='') { $j('#diet_advice_req').empty(); }
	});
	
	$j('.diagnosis label').click(function()
	{
		$j('.diagnosis label').removeClass("active");
		$j(this).addClass("active");
		var field6  = $j("input[name=diagnosis_treatment]:checked").val();
		if(field6=="Poor" || field6=="Average") { $j("#diagnosis_treatment_remarks_detailed").attr("required", ""); $j("#diagnosis_treatment_remarks").slideDown("fast"); }
		else { $j("#diagnosis_treatment_remarks").css("display","none"); $j("#diagnosis_treatment_remarks_detailed").removeAttr("required", ""); $j("#diagnosis_treatment_remarks").slideUp("fast"); }
		if(field6!='') { $j('#diagnosis_treatment_req').empty(); }
	});
	
	$j('.nursing label').click(function()
	{
		$j('.nursing label').removeClass("active");
		$j(this).addClass("active");
		var field7  = $j("input[name=nursing_care]:checked").val();
		if(field7=="Poor" || field7=="Average") { $j("#nursing_care_remarks_detailed").attr("required", ""); $j("#nursing_care_remarks").slideDown("fast"); }
		else { $j("#nursing_care_remarks").css("display","none"); $j("#nursing_care_remarks_detailed").removeAttr("required", ""); $j("#nursing_care_remarks").slideUp("fast"); }
		if(field7!='') { $j('#nursing_care_req').empty(); }
	});
	
	$j('.eye_depart label').click(function()
	{
		$j('.eye_depart label').removeClass("active");
		$j(this).addClass("active");
		var field8  = $j("input[name=eyechecks]:checked").val();
		if(field8=="Poor" || field8=="Average") {$j("#eyechecks_remarks_detailed").attr("required", "");  $j("#eyechecks_remarks").slideDown("fast"); }
		else { $j("#eyechecks_remarks").css("display","none"); $j("#eyechecks_remarks_detailed").removeAttr("required", ""); $j("#eyechecks_remarks").slideUp("fast"); }
		if(field8!='') { $j('#eyechecks_req').empty(); }
	});
	
	$j('.fitness label').click(function()
	{
		$j('.fitness label').removeClass("active");
		$j(this).addClass("active");
		var field9  = $j("input[name=fitness_phy]:checked").val();
		if(field9=="Poor" || field9=="Average") { $j("#fitness_phy_remarks_detailed").attr("required", "");  $j("#fitness_phy_remarks").slideDown("fast"); }
		else { $j("#fitness_phy_remarks").css("display","none"); $j("#fitness_phy_remarks_detailed").removeAttr("required", ""); $j("#fitness_phy_remarks").slideUp("fast"); }
		if(field9!='') { $j('#fitness_phy_req').empty(); }
	});
	
	$j('.guidance label').click(function()
	{
		$j('.guidance label').removeClass("active");
		$j(this).addClass("active");
		var field10  = $j("input[name=guidance_care]:checked").val();
		if(field10=="Poor" || field10=="Average") { $j("#guidance_care_remarks").slideDown("fast"); }
		else { $j("#guidance_care_remarks").css("display","none"); $j("#guidance_care_remarks").slideUp("fast"); }
		if(field10!='') { $j('#guidance_care_req').empty(); }
	});
	
	$j('.clinical label').click(function()
	{
		$j('.clinical label').removeClass("active");
		$j(this).addClass("active");
		var field11  = $j("input[name=clinical_secs]:checked").val();
		if(field11=="Poor" || field11=="Average") { $j("#clinical_secs_remarks_detailed").attr("required", ""); $j("#clinical_secs_remarks").slideDown("fast"); }
		else { $j("#clinical_secs_remarks").css("display","none"); $j("#clinical_secs_remarks_detailed").removeAttr("required", ""); $j("#clinical_secs_remarks").slideUp("fast"); }
		if(field11!='') { $j('#clinical_secs_req').empty(); }
	});

	$j('.catering label').click(function()
	{
		$j('.catering label').removeClass("active");
		$j(this).addClass("active");
		var field12  = $j("input[name=caterings]:checked").val();
		if(field12=="Poor" || field12=="Average") { $j("#caterings_remarks_detailed").attr("required", ""); $j("#caterings_remarks").slideDown("fast"); }
		else { $j("#caterings_remarks").css("display","none");  $j("#caterings_remarks_detailed").removeAttr("required", ""); $j("#caterings_remarks").slideUp("fast"); }
		if(field12!='') { $j('#caterings_req').empty(); }
	});
	
	$j('.pharmacy label').click(function()
	{
		$j('.pharmacy label').removeClass("active");
		$j(this).addClass("active");
		var field13  = $j("input[name=pharmacys]:checked").val();
		if(field13=="Poor" || field13=="Average") { $j("#pharmacys_remarks_detailed").attr("required", ""); $j("#pharmacys_remarks").slideDown("fast"); }
		else { $j("#pharmacys_remarks").css("display","none"); $j("#pharmacys_remarks_detailed").removeAttr("required", "");
		$j("#pharmacys_remarks").slideUp("fast"); }
		if(field13!='') { $j('#pharmacys_req').empty(); }
	});
	
	$j('.footwear label').click(function()
	{
		$j('.footwear label').removeClass("active");
		$j(this).addClass("active");
		var field14  = $j("input[name=footwears]:checked").val();
		if(field14=="Poor" || field14=="Average") { $j("#footwears_remarks_detailed").attr("required", ""); $j("#footwears_remarks").slideDown("fast"); }
		else { $j("#footwears_remarks").css("display","none"); $j("#footwears_remarks_detailed").removeAttr("required", ""); 
		$j("#footwears_remarks").slideUp("fast"); }
		if(field14!='') { $j('#footwears_req').empty(); }
	});
	
	$j('.cleanliness label').click(function()
	{
		$j('.cleanliness label').removeClass("active");
		$j(this).addClass("active");
		var field15  = $j("input[name=cleanhos]:checked").val();
		if(field15=="Poor" || field15=="Average") { $j("#cleanhos_remarks_detailed").attr("required", ""); $j("#cleanhos_remarks").slideDown("fast"); }
		else { $j("#cleanhos_remarks").css("display","none"); $j("#cleanhos_remarks_detailed").removeAttr("required", ""); 
		$j("#cleanhos_remarks").slideUp("fast"); }
		if(field15!='') { $j('#cleanhos_req').empty(); }
	});
	
	$j('.insurance label').click(function()
	{
		$j('.insurance label').removeClass("active");
		$j(this).addClass("active");
		var field16  = $j("input[name=insurances]:checked").val();
		if(field16=="Poor" || field16=="Average") { $j("#insurances_remarks_detailed").attr("required", ""); $j("#insurances_remarks").slideDown("fast"); }
		else { $j("#insurances_remarks").css("display","none"); $j("#insurances_remarks_detailed").removeAttr("required", "");$j("#insurances_remarks").slideUp("fast"); }
		if(field16!='') { $j('#insurances_req').empty(); }
	});
	
	$j('.security label').click(function()
	{
		$j('.security label').removeClass("active");
		$j(this).addClass("active");
		var field17  = $j("input[name=securitys]:checked").val();
		if(field17=="Poor" || field17=="Average") { $j("#securitys_remarks_detailed").attr("required", ""); $j("#securitys_remarks").slideDown("fast"); }
		else { $j("#securitys_remarks").css("display","none"); $j("#securitys_remarks_detailed").removeAttr("required", ""); 
		$j("#securitys_remarks").slideUp("fast"); }
	});	
	
$j('#submit').click(function(){
	
var af1  = $j("input[name=attender_name]").val();
var af2  = $j("input[name=mnno]").val();
var af3  = $j("select[name=feedback_branches]").val();
var af4  = $j("input[name=visitdate]").val();
var af5  = $j("input[name=phone]").val();
var afs = $j("input[type='radio']:checked").val();

if(af1=="")  {  $j('#anamerror').slideDown().html('<span id="error">Please enter Patient / Attender Name</span>'); $j('html, body').animate({scrollTop: $j("#anamerror").offset().top-60}, 800);  return false; } else { $j('#anamerror').empty(); 	}

if (af2=="")  {  $j('#mnerror').slideDown().html('<span id="error">Please enter M.No</span>'); $j('html, body').animate({scrollTop: $j("#mnerror").offset().top-60}, 800); return false; } else  { $j('#mnerror').empty();  }

if (af3=="")  {  $j('#fbserror').slideDown().html('<span id="error">Please select branch</span>'); $j('html, body').animate({scrollTop: $j("#fbserror").offset().top-60}, 800); return false; } else { $j('#fbserror').empty();   }

if (af4=="")  {  $j('#visiterror').slideDown().html('<span id="error">Please select date</span>'); $j('html, body').animate({scrollTop: $j("#visiterror").offset().top-60}, 800); return false; } else  { $j('#visiterror').empty();  }

if (af5=="")  {  $j('#contacterror').slideDown().html('<span id="error">Please enter phone</span>'); $j('html, body').animate({scrollTop: $j("#contacterror").offset().top-60}, 800); return false; } else { $j('#contacterror').empty();  }
 
if ( (af5.length < 6 ) || (af5.length > 13) || (isNaN(af5)) ) { $j('#patternerror').slideDown().html('<span id="error">Please enter valid phone number(without any spaces)</span>'); $j('html, body').animate({scrollTop: $j("#patternerror").offset().top-60}, 800);  return false; } else { $j('#patternerror').empty();  }


if(afs==undefined) {  $j('#service_ratings').slideDown().html('<span id="error">Kindly rate some of our services</span>'); $j('html, body').animate({scrollTop: $j("#service_ratings").offset().top-60}, 800); return false; } else {  $j('#service_ratings').empty();  }


	
});
	
});

</script>
<?php get_footer(); ?>