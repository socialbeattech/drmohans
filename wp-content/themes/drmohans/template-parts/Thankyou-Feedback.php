<?php
/**
 *  Template name: Thankyou Feedback
 *
 * 
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="fullcols">
	<div class="container">
		<div class="col-12 col-sm-12 col-md-12 float-left p-o m-0">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>			
		</div>
		<div class="feedback_holder col-12 col-sm-12 float-left p-0 m-0">
			<p class="col-12 col-sm-12 float-left p-0 mt-3 text-center" style="color : #0054a6;font-weight : bold;">THANK YOU FOR YOUR VALUABLE FEEDBACK</p>
		</div>
	</div>
</div>
<?php get_footer(); ?>