<?php
/**
 *  Template Name: Blogs
 *
 * 
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="blogs_sidebar">
	<div class="container-fluid padd0">
		<div class="col-12 col-sm-8  float-left blogs no-guttersleft">  
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
				  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
			?>
			<div class="latestnews">
			<?php $args_default1 = array( 'category_name' => 'Diebetic Diet', 'post_type' => 'post', 'posts_per_page' => '2');
			   $spec_blogs_default1 = new WP_Query( $args_default1 ); 
			   if ( $spec_blogs_default1->have_posts() ) {  ?>
				<h2 class=" lbl-title"><a class=" lbl-title" href="<?php echo get_site_url(); ?>/category/diabetic-diet/">Eating Right & Staying Fit</a></h2>
				<?php while($spec_blogs_default1->have_posts()): $spec_blogs_default1->the_post(); ?>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft">
					<a href="<?php the_permalink(); ?>" class="col-sm-10 no-gutters"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><?php the_title(); ?></h3>
					<h4><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h4>
				</div>
				<?php endwhile; ?>
			   <?php } ?>
			  <?php wp_reset_postdata(); ?>
			</div>
			
			<div class="latestnews">
			<?php $args_default2 = array( 'category_name' => 'Preventive Care', 'post_type' => 'post', 'posts_per_page' => '2');
			   $spec_blogs_default2 = new WP_Query( $args_default2 ); 
			   if ( $spec_blogs_default2->have_posts() ) {  ?>
				<h2 class=" lbl-title"><a class=" lbl-title" href="<?php echo get_site_url(); ?>/category/preventive-care/">Preventive Care</a></h2>
				<?php while($spec_blogs_default2->have_posts()): $spec_blogs_default2->the_post(); ?>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft">
					<a href="<?php the_permalink(); ?>" class="col-sm-10 no-gutters"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><?php the_title(); ?></h3>
					<h4><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h4>
				</div>
				<?php endwhile; ?>
			   <?php } ?>
			  <?php wp_reset_postdata(); ?>
			</div>
			
			<div class="latestnews">
			<?php $args_default3 = array( 'category_name' => 'Fitness', 'post_type' => 'post', 'posts_per_page' => '2');
			   $spec_blogs_default3 = new WP_Query( $args_default3 ); 
			   if ( $spec_blogs_default3->have_posts() ) {  ?>
				<h2 class=" lbl-title"><a class=" lbl-title" href="<?php echo get_site_url(); ?>/category/fitness/">Staying Fit</a></h2>
				<?php while($spec_blogs_default3->have_posts()): $spec_blogs_default3->the_post(); ?>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft">
					<a href="<?php the_permalink(); ?>" class="col-sm-10 no-gutters"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><?php the_title(); ?></h3>
					<h4><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h4>
				</div>
				<?php endwhile; ?>
			   <?php } ?>
			  <?php wp_reset_postdata(); ?>
			</div>
			<div class="latestnews">
			<?php $args_default3 = array( 'category_name' => 'Diabetic Diet', 'post_type' => 'post', 'posts_per_page' => '2');
			   $spec_blogs_default3 = new WP_Query( $args_default3 ); 
			   if ( $spec_blogs_default3->have_posts() ) {  ?>
				<h2 class=" lbl-title"><a class=" lbl-title" href="<?php echo get_site_url(); ?>/category/diabetic-diet">Diabetic Diet</a></h2>
				<?php while($spec_blogs_default3->have_posts()): $spec_blogs_default3->the_post(); ?>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft">
					<a href="<?php the_permalink(); ?>" class="col-sm-10 no-gutters"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><?php the_title(); ?></h3>
					<h4><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h4>
				</div>
				<?php endwhile; ?>
			   <?php } ?>
			  <?php wp_reset_postdata(); ?>
			</div>
			<div class="latestnews">
			<?php $args_default3 = array( 'category_name' => 'Pregnancy', 'post_type' => 'post', 'posts_per_page' => '2');
			   $spec_blogs_default3 = new WP_Query( $args_default3 ); 
			   if ( $spec_blogs_default3->have_posts() ) {  ?>
				<h2 class=" lbl-title"><a class=" lbl-title" href="<?php echo get_site_url(); ?>/category/pregnancy">Pregnancy</a></h2>
				<?php while($spec_blogs_default3->have_posts()): $spec_blogs_default3->the_post(); ?>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft">
					<a href="<?php the_permalink(); ?>" class="col-sm-10 no-gutters"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><?php the_title(); ?></h3>
					<h4><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h4>
				</div>
				<?php endwhile; ?>
			   <?php } ?>
			  <?php wp_reset_postdata(); ?>
			</div>

			<div class="latestnews">
			<?php $args_default3 = array( 'category_name' => 'Stress Management', 'post_type' => 'post', 'posts_per_page' => '2');
			   $spec_blogs_default3 = new WP_Query( $args_default3 ); 
			   if ( $spec_blogs_default3->have_posts() ) {  ?>
				<h2 class=" lbl-title"><a class=" lbl-title" href="<?php echo get_site_url(); ?>/category/stress-management">Stress Management</a></h2>
				<?php while($spec_blogs_default3->have_posts()): $spec_blogs_default3->the_post(); ?>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft">
					<a href="<?php the_permalink(); ?>" class="col-sm-10 no-gutters"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><?php the_title(); ?></h3>
					<h4><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h4>
				</div>
				<?php endwhile; ?>
			   <?php } ?>
			  <?php wp_reset_postdata(); ?>
			</div>
			<div class="latestnews">
			<?php $args_default3 = array( 'category_name' => 'Counselling', 'post_type' => 'post', 'posts_per_page' => '6');
			   $spec_blogs_default3 = new WP_Query( $args_default3 ); 
			   if ( $spec_blogs_default3->have_posts() ) { $c=1; ?>
				<h2 class=" lbl-title"><a class=" lbl-title" href="<?php echo get_site_url(); ?>/category/counselling">Counselling</a></h2>
<div class="row">
				<?php while($spec_blogs_default3->have_posts()): $spec_blogs_default3->the_post(); ?>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft">
					<a href="<?php the_permalink(); ?>" class="col-sm-10 no-gutters"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><?php the_title(); ?></h3>
					<h4><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h4>
				</div><?php if($c % 2 == 0){ ?></div> <div class="row"><?php } ?>
				<?php $c++; endwhile; ?>
</div>
			   <?php } ?>
			  <?php wp_reset_postdata(); ?>
			</div>
			<div class="latestnews">
				<h2 class=" lbl-title"><a class=" lbl-title" href="<?php echo get_site_url(); ?>/news-and-events/">News and Events</a></h2>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft">
					<a target="_blank" href=" http://www.thehindu.com/news/cities/chennai/precision-diabetes-department-launched/article19848664.ece" class="col-sm-10 no-gutters"><img src="<?php echo get_template_directory_uri();?>/images/precision.jpg" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><a href=" http://www.thehindu.com/news/cities/chennai/precision-diabetes-department-launched/article19848664.ece">Precision diabetes department launched</a></h3>
				</div>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft">
					<a target="_blank" href="https://www.google.com/url?q=https://timesofindia.indiatimes.com/city/chennai/cashews-can-improve-good-cholesterol-levels-says-study/articleshow/62782132.cms&sa=D&source=hangouts&ust=1527676001971000&usg=AFQjCNGRBmUlm29BOBotYfzXW1CQr7_I_g" class="col-sm-10 no-gutters"><img src="<?php echo get_template_directory_uri();?>/images/cashews.jpg" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><a href="https://www.google.com/url?q=https://timesofindia.indiatimes.com/city/chennai/cashews-can-improve-good-cholesterol-levels-says-study/articleshow/62782132.cms&sa=D&source=hangouts&ust=1527676001971000&usg=AFQjCNGRBmUlm29BOBotYfzXW1CQr7_I_g">Cashews can improve good cholesterol levels, says study</a></h3>
				</div>
				<!--div class="col-12 col-sm-6 blogs_article no-guttersleft" style="padding-top:20px">
					<a target="_blank" href="https://www.google.com/url?q=https://timesofindia.indiatimes.com/city/hyderabad/form-of-genetic-diabetes-often-wrongly-diagnosed/articleshow/63104950.cms&sa=D&source=hangouts&ust=1527676001972000&usg=AFQjCNGsJFG3UXeycH6KlNKte5VxcdkNiQ" class="col-sm-10 no-gutters"><img src="<?php echo get_template_directory_uri();?>/images/wrongly.jpg" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="text-uppercase"><a href="https://www.google.com/url?q=https://timesofindia.indiatimes.com/city/hyderabad/form-of-genetic-diabetes-often-wrongly-diagnosed/articleshow/63104950.cms&sa=D&source=hangouts&ust=1527676001972000&usg=AFQjCNGsJFG3UXeycH6KlNKte5VxcdkNiQ">Form of genetic diabetes often wrongly diagnosed</a></h3>
				</div-->
			</div>
		</div>
		<div class="col-12 col-sm-4 float-right blogs_sidebar pb-4 no-guttersright bg-dl-grey blog-rightpanel Helvetica_Light">
			<label class=" lbl-title featured-post-title" style="padding-top: 50px;">FEATURED POSTS</label>
				<?php
					$args = array(
							'posts_per_page' => 5,
							'meta_key' => 'meta-checkbox',
							'meta_value' => 'yes'
						);
						$featured = new WP_Query($args);
					 
					if ($featured->have_posts()): 
						while($featured->have_posts()): $featured->the_post(); ?>
							<div class="featured-post">
								<label class="featured-title"><?php the_title(); ?></label>
								<p class="featured-content"><?php the_excerpt(); ?></p>
								<a href="<?php the_permalink(); ?>" class="fs-18 Helvetica_Light focolor hover-border border-bottom-red">Read More</a>
							</div>
							<?php
						endwhile;
					endif;
					wp_reset_postdata();
				?>
				
				<ul class="categories-widget pt-1 m-b0">
					<?php wp_list_categories(); ?>
				</ul>
			<p class="para-feature">
			At Dr.Mohans we are dedicated to support you in all your requirements during your patient journey
			</p>
			<div class="book_appintment">
					<div class="gravity_holder">
						<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
						<?php gravity_form( 1, $display_title = false, $display_description = false, $ajax = true, $echo = true ); ?>
					</div>							
			</div>
		</div>
	</div>
</div>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container text-center">
			<label class="text-center Helvetica_Thin fs-46 title-4l"><?php the_field('patient_count_text', 'options'); ?></label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 text-center" style="margin:auto">
					<div class="row sm-p-b0" style="padding-bottom:40px;">		
						<?php $count=1;
						$numrows = count( get_field('we_care_icons','option') ); ?>
						
						<?php while ( have_rows('we_care_icons','option') ) : the_row(); ?>	
						<div class="col-12 col-sm-4 wow zoomIn">
							<img class="icon-40k" src="<?php the_sub_field('icon'); ?>"/>
							<label class="title-40k"><?php the_sub_field('sub_text'); ?></label>
						</div>
						<?php if($count%3 ==0 && $count != $numrows){ ?>
						</div>							
						<div class="row">	
						<?php } ?>
						<?php $count++; ?>
						<?php endwhile; ?>
					</div>			
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>