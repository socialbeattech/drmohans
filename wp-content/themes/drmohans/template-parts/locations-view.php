<?php
/**
 *  Template Name: Locations View
 *
 * 
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<style>.book_appintment h2{color: rgb(255, 255, 255);
    font-size: 15px;font-family:HelveticaNeueLTStd-Lt;font-weight:300;text-transform:none}</style>
<?php 
global $post;    
global $wp_query; $page_id = $wp_query->post->ID;
while ( have_posts() ) : the_post(); 
?>


<div class="fullwidth locations_banner">
	<a href="<?php the_permalink(); ?>" class="col-12 col-sm-12 no-gutters"><img src="<?php echo get_template_directory_uri();?>/images/locations-inner.png" alt="Drmohans" width="1366" height="450" /></a>
	<h2 class="Helvetica_Bold">Dr. Mohan's Diabetes</br>Specialities Centre</h2>
	<?php $title = get_the_title(); 
	$title1 = substr($title, 19); ?>
	<h3 class="Helvetica_Light"><?php echo $title1; ?></h3>
</div>

<div class="locationsview_sidebar">	
	<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>		
			<div class="centers_count">
				<p class="fs-18 Helvetica_Light focolor text-center pb-2" style="padding-top:40px">Dr. Mohan's Diabetes Specialities Centre is one of the most trusted diabetes speciality hospitals in India. With 40+ clinics across the country, we provide world class healthcare facilities for patients with diabetes, and help them live a happy, healthy life. Our <?php if( $title == 'Diabetes Centre in Kaikhali'){ echo 'Diabetes Centre in Kolkata'; } else{ the_title(); } ?> centre is well-equipped with cutting edge technology to provide holistic care for diabetes and it complications.</p>
			</div>
			<div class="col-12 col-sm-6 float-left locations_view">							
				<div class="fullwidth address">	
					<h1><?php the_title(); ?></h1>
					<?php if( have_rows('address') ):?>
						<h2>LOCATION ADDRESS</h2>
						<?php while ( have_rows('address') ) : the_row(); ?>
						<p><?php the_sub_field('address_lines');?></p>
						<?php endwhile; ?> 
					<?php endif; ?>
					
	            	
					<?php if((get_field('phone_number')) && (get_field('phone_number_tel_link'))): ?>
					<p>Phone: <a href="tel:+91<?php the_field('phone_number_tel_link'); ?>"><?php the_field('phone_number'); ?></a>, <?php if((get_field('alternative_number')) && (get_field('alternative _number_tel_link'))): ?><a href="tel:+91<?php the_field('alternative _number_tel_link'); ?>"><?php the_field('alternative_number'); ?></a><?php endif; ?></p>
					<?php endif; ?>
					
					<?php if(get_field('email')): ?><p>Email: <a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p><?php endif; ?>
					
					<?php if(get_field('fax')): ?><p>Fax: <?php the_field('fax'); ?></p><?php endif; ?>
				</div>
				
				<div id="mobile-form" class="fullwidth"></div>
				<div class="fullwidth timings">
					<?php if( have_rows('landmark') ):?>
						<h2>Landmark</h2>
						<?php while ( have_rows('landmark') ) : the_row(); ?>
						<p><?php the_sub_field('landmark_lines');?></p>
						<?php endwhile; ?> 
					<?php endif; ?>
				</div>
				
				<div class="fullwidth nearby_places">
					<?php if( have_rows('nearby_places') ):?>
						<h2>Nearby Places</h2>
						<?php while ( have_rows('nearby_places') ) : the_row(); ?>
						<p><?php the_sub_field('nearby_places_lines');?></p>
						<?php endwhile; ?> 
					<?php endif; ?>
				</div>
				
				<?php if(get_field('doctors')):?>
				<div class="fullwidth doctors_locations nearby_places">
					<h2>Doctors</h2>											
					<?php the_field('doctors');?>					
				</div>
				<?php endif; ?>
				
				<?php if(get_field('facilities')):?>
				<div class="fullwidth facilities_locations nearby_places">
					<h2>Facilities</h2>											
					<?php the_field('facilities');?> 
				</div>
				<?php endif; ?>
			
			</div>
			<div class="col-12 col-sm-6 float-left locview_sidebar">
				<?php if(get_field('locate_map')): ?>
				<div class="acf-map">
					<?php the_field('locate_map'); ?>
				</div>
				<?php endif; ?>
			</div>			
		</article>
	</div>
</div>


<div class="fullwidth diabetes_care fadeInUp locationsview_icons">
	<div class="container">
		<label class="lbl-title text-center Helvetica_Thin fs-46"><?php the_field('patient_count_text','option'); ?></label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 text-center" style="margin:auto">
					<div class="row sm-p-b0" style="padding-bottom:40px;">		
						<?php $count=1;
						$numrows = count( get_field('we_care_icons','option') ); ?>
						
						<?php while ( have_rows('we_care_icons','option') ) : the_row(); ?>	
						<div class="col-12 col-sm-4">
							<img src="<?php the_sub_field('icon'); ?>"/>
							<label class="title-40k"><?php the_sub_field('sub_text'); ?></label>
						</div>
						<?php if($count%3 ==0 && $count != $numrows){ ?>
						</div>							
						<div class="row">	
						<?php } ?>
						<?php $count++; ?>
						<?php endwhile; ?>
					</div>			
				</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 margin-lr-auto" id="remove-mobile-form">
				<div class="book_appintment">
					<div class="gravity_holder">
						<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
						<?php gravity_form( 1, $display_title = false, $display_description = false, $ajax = true, $echo = true ); ?>
					</div>							
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; ?>
<script>jQuery(document).ready(function($){ if($(window).width()<769){var element=document.getElementById('remove-mobile-form');element.parentNode.removeChild(element);var element1 = document.getElementById("mobile-form");
element1.appendChild(element);} });</script>
<style type="text/css">
.swiper-container { display : none; }
.locationsview_sidebar { float : left; width : 100%; position : relative; clear : both; }
.locations_banner { top:0px; padding :0;}
.locations_banner img, .locations_banner a  { cursor : pointer; }
.locations_banner img { margin: 0px auto; max-width: 100%; text-align: center; width: 100%; }
.locations_banner h2 { bottom: 5rem; left: 10%; position: absolute; width: 90%; font-size : 38px; line-height: 36px; color : #1d2e34; }
.locations_banner h3 { bottom: 2rem; left: 10%; position: absolute; width: 90%; font-size : 30px; color : #1d2e34; }
.locations_banner .book_appintment { padding : 3%; }
.locations_view ul { float : left; width : 100%; padding :0; margin : 0 auto; }
.locations_view ul li { list-style-type: none; }
.locations_view .doctors_locations { padding: 1rem 0 1.5rem; }
.doctors_locations ul{ padding: 0 0 4% 0; }
.facilities_locations ul{ padding: 2% 0 0 0; }
</style>
<?php get_footer(); ?>