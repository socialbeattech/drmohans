<?php
/**
 *  Template Name: Locations List
 *
 * 
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php 
global $post;    
global $wp_query; $page_id = $wp_query->post->ID;
while ( have_posts() ) : the_post(); 

$children = get_pages( array( 'child_of' => $post->ID ) );
if ( count( $children ) > 0 ) : 		
$children = get_pages( array( 'sort_column' => 'menu_order', 'parent' => $post->ID, ));		
?>
<style>
@media (max-width:768px){.book_appintment{margin:30px auto}}
</style>
<div class="fullwidth locations_banner">
	<a href="<?php the_permalink(); ?>" class="col-12 col-sm-12 no-gutters"><img src="<?php echo get_template_directory_uri();?>/images/locationslist-banner.png" alt="Drmohans" width="1366" height="650" /></a>
	<h2 class="Helvetica_Roman">Everything you need and more....</br>with a personal touch</h2>
	<h3 class="Helvetica_Light"><?php the_title(); ?></h3>
</div>
	<section class="fullwidth Helvetica_Light">
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
				  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
			?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 text-center title-4l"><?php the_field('patient_count_text','option'); ?></label>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 text-center" style="margin:auto">
					<div class="row sm-p-b0" style="padding-bottom:40px;">		
						<?php $count=1;
						$numrows = count( get_field('we_care_icons','option') ); ?>
						
						<?php while ( have_rows('we_care_icons','option') ) : the_row(); ?>	
						<div class="col-12 col-sm-4">
							<img src="<?php the_sub_field('icon'); ?>"/>
							<label class="title-40k"><?php the_sub_field('sub_text'); ?></label>
						</div>
						<?php if($count%3 ==0 && $count != $numrows){ ?>
						</div>							
						<div class="row">	
						<?php } ?>
						<?php $count++; ?>
						<?php endwhile; ?>
					</div>			
				</div>
			</div>
		</div>
	</section>

<?php //start of locations list page ?>
<div class="locationslist_sidebar fullwidth">
	<div class="container">	
	<div id="mobile-form" class="fullwidth  m-t-sm-20 m-b-sm-20"></div>
		<div class="row">
			<div class="col-12 text-center">
				<label style="margin-bottom:20px;font-size: 22px; color: rgb(41, 41, 41); font-family: 'HelveticaNeueLTStd-Bd'; text-transform: uppercase; padding: 0 0 1% 0;    margin: 0 auto;">Diabetes Centers in <?php the_title(); ?></label>
			</div>
		</div>
		<div class="col-12 col-sm-8 float-left locationslist">
			<?php foreach( $children as $post ) : setup_postdata( $post ); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
					<div class="addresslist fullwidth">	
						<h1><?php the_title(); ?></h1>						
						<?php if( have_rows('address') ):?>						
							<?php while ( have_rows('address') ) : the_row(); ?>
							<p><?php the_sub_field('address_lines');?></p>
							<?php endwhile; ?> 
						<?php endif; ?>						
						<?php if((get_field('phone_number')) && (get_field('phone_number_tel_link'))): ?>
						<p>Phone: <a href="tel:+91<?php the_field('phone_number_tel_link'); ?>"><?php the_field('phone_number'); ?></a>, <?php if((get_field('alternative_number')) && (get_field('alternative _number_tel_link'))): ?><a href="tel:+91<?php the_field('alternative _number_tel_link'); ?>"><?php the_field('alternative_number'); ?></a><?php endif; ?></p>
						<?php endif; ?>						
						<?php if(get_field('email')): ?><p>Email: <a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p><?php endif; ?>						
						<?php if(get_field('fax')): ?><p>Fax: <?php the_field('fax'); ?></p><?php endif; ?>					
						<p class="readmore"><a href="<?php echo get_permalink( $post->ID ); ?>">View Map</a></p>
					</div>							
				</article>
			<?php endforeach; ?>
		</div>	
		<div class="col-12 col-sm-4 float-left loclist_sidebar" id="remove-mobile-form">
			<div class="book_appintment">
				<div class="gravity_holder">
					<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters">Book an Appointment</h1>
					<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
					<?php gravity_form( 1, $display_title = false, $display_description = false, $ajax = true, $tabindex, $echo = true ); ?>
				</div>							
			</div>
		</div>	
	</div>	
</div>
<?php //End of locations list page ?>

<?php endif; ?>
<?php endwhile; ?>
<script>jQuery(document).ready(function($){ if($(window).width()<769){var element=document.getElementById('remove-mobile-form');element.parentNode.removeChild(element);var element1 = document.getElementById("mobile-form");
element1.appendChild(element);} });</script>
<style>
.swiper-container { display : none; }
.locationsview_sidebar { float : left; width : 100%; position : relative; clear : both; }
.locations_banner { top:0px; }
.locations_banner img, .locations_banner a  { cursor : pointer; }
.locations_banner img { margin: 0px auto; max-width: 100%; text-align: center; width: 100%; }
.locations_banner h2 { bottom: 15rem; left: 10%; position: absolute; width: 90%; font-size : 38px; line-height: 36px; color : #1d2e34; }
.locations_banner h3 { bottom: 12rem; left: 10%; position: absolute; width: 90%; font-size : 30px; color : #1d2e34; }
.locationslist_sidebar .book_appintment { padding : 3%; }
</style>
<?php get_footer(); ?>