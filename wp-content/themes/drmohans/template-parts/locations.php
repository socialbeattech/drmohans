<?php
/**
 *  Template Name: Locations
 *
 * 
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php 
global $post;    
global $wp_query; $page_id = $wp_query->post->ID;
while ( have_posts() ) : the_post(); 

$children = get_pages( array( 'child_of' => $post->ID ) );
if ( count( $children ) > 0 ) : 		
$children = get_pages( array( 'sort_column' => 'menu_order', 'parent' => $post->ID, ));		
?>

<div class="fullwidth locations_banner">
	<a href="<?php the_permalink(); ?>" class="col-12 col-sm-12 no-gutters"><img src="<?php echo get_template_directory_uri();?>/images/locationslist-banner.png" alt="Drmohans" width="1366" height="650" /></a>
	<h2 class="Helvetica_Roman">Everything you need and more....</br>with a personal touch</h2>
	<h3 class="Helvetica_Light"><?php the_title(); ?></h3>
</div>
	<section class="fullwidth Helvetica_Light locationslist_sidebar" style="padding:0">
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
				  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
				}
			?>
		</div>
	</section>
<?php //start of locations list page ?>
<div class="locationslist_sidebar fullwidth">
	<div class="container">		
		<div class="col-12 col-sm-8 float-left locationslist">
			<?php foreach( $children as $post ) : setup_postdata( $post ); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
					<div class="addresslist fullwidth" style="padding-bottom:10px"> 	
						<a href="<?php echo get_permalink( $post->ID ); ?>"><h1><?php the_title(); ?></h1>	</a>
					</div>							
				</article>
			<?php endforeach; ?>
		</div>	
		<div class="col-12 col-sm-4 float-left loclist_sidebar">
			<div class="book_appintment">
				<div class="gravity_holder">
					<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters">Book an Appointment</h1>
					<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
					<?php gravity_form( 1, $display_title = false, $display_description = false, $ajax = true, $tabindex, $echo = true ); ?>
				</div>							
			</div>
		</div>	
	</div>	
</div>
<?php //End of locations list page ?>

<?php endif; ?>
<?php endwhile; ?>
<style>
.swiper-container { display : none; }
.locationsview_sidebar { float : left; width : 100%; position : relative; clear : both; }
.locations_banner { top:0px; }
.locations_banner img, .locations_banner a  { cursor : pointer; }
.locations_banner img { margin: 0px auto; max-width: 100%; text-align: center; width: 100%; }
.locations_banner h2 { bottom: 15rem; left: 10%; position: absolute; width: 90%; font-size : 38px; line-height: 36px; color : #1d2e34; }
.locations_banner h3 { bottom: 12rem; left: 10%; position: absolute; width: 90%; font-size : 30px; color : #1d2e34; }
.locationslist_sidebar .book_appintment { padding : 3%; }
</style>
<?php get_footer(); ?>