<?php
/**
 *  Template Name: News & Events
 *
 * 
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="blogs_sidebar">
	<div class="container-fluid padd0">
		<div class="col-12 col-sm-8  float-left blogs no-guttersleft">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
			<div class="latestnews">
				<h2 class=" lbl-title">News and Events</h2>
				<div class="row" style="margin:0;padding-bottom:35px;">
				<?php $count = 1; ?>
				<?php while ( have_rows('events_list') ) : the_row(); ?>
				<div class="col-12 col-sm-6 blogs_article no-guttersleft" style="padding-top:20px">
					<a <?php if( get_sub_field('want_it_as_popup') ){ ?> href="javascript:void(0)" data-toggle="modal" data-target="#modal<?php echo $count; ?> " target="_blank"  <?php }else { ?> href="<?php the_sub_field('event_url'); ?>" target="_blank" <?php } ?> class="col-sm-10 no-gutters"><img src="<?php the_sub_field('event_image'); ?>" alt="Drmohans" width="350" height="190" /></a>
					<h3 class="" style="max-width: 350px;margin:0;font-family: 'HelveticaNeueLTStd';"><a <?php if( get_sub_field('want_it_as_popup') ){ ?> href="javascript:void(0)" data-toggle="modal" data-target="#modal<?php echo $count; ?>"   <?php } else { ?> href="<?php the_sub_field('event_url'); ?>" target="_blank" <?php } ?>><?php the_sub_field('event_title'); ?></a></h3>
				</div>
				 <?php 
					if($count % 2 == 0){ ?>
						</div>
						<div class="row" style="margin:0;padding-bottom:35px;">
						<?php
					} ?>
				<?php if( get_sub_field('want_it_as_popup') ){ ?>
				<div class="modal" id="modal<?php echo $count;?>"> 
					<div class="modal-dialog">
						<div class="modal-content" style="padding:20px">
							<span class="text-right close color29" data-dismiss="modal" style="z-index:99">X</span>
							<div style="padding-bottom:5%;position: relative;" class="row">
								<h3 class="news-title text-center pb-3" style="margin:auto;font-family: 'HelveticaNeueLTStd';"><?php the_sub_field('event_title'); ?></h3>
								<?php if(get_sub_field('popup_text')) { ?>
									<p><?php the_sub_field('popup_text'); ?></p>
								<?php } ?>
								<?php if( get_sub_field('popup_carousel') ){ 
									$images = get_sub_field('popup_carousel'); ?>
									<?php if( sizeof($images) > 1 ){ ?>
										<div class="col-2 marginauto">
											<img src="<?php echo get_template_directory_uri();?>/images/prev.png" alt="News and Events" class="event-prev<?php echo $count; ?> img-fluid cur-pointer" style="border:none;outline:none"/>	
										</div>
									<?php } ?>
										<div class="<?php if( sizeof($images) > 1 ){ ?> col-8  <?php }else{ ?> col-12 <?php } ?>text-center">
											<div class="swiper-container" id="news_and_events<?php echo $count; ?>">
												<div class="swiper-wrapper">
												<?php foreach( $images as $image ){ ?>
													<div class="swiper-slide">
														<img src="<?php echo $image['url']; ?>" style="border:none;outline:none" alt="News and Events"/>
													</div>
												<?php } ?>	
												</div>
											</div>
											<?php /* <div class="swiper-pagination news-pagination<?php echo $count; ?>"></div> */ ?>
										</div>
										<?php if( sizeof($images) > 1 ){ ?>
										<div class="col-2 marginauto">
											<img src="<?php echo get_template_directory_uri(); ?>/images/next.png" class="event-next<?php echo $count; ?> img-fluid cur-pointer" style="border:none;outline:none" alt="News and Events"/>
										</div>
										<?php } ?>
									<?php if( sizeof($images) > 1 ){ ?>
									<script>
										var news_and_events<?php echo $count; ?> = new Swiper("#news_and_events<?php echo $count; ?>",{
											loop:true,
											slidesPerView:1,
											slidesPerGroup:1,
											spaceBetween:30,
											navigation: {
												nextEl: '.event-next<?php echo $count; ?>',
												prevEl: '.event-prev<?php echo $count; ?>', 
											} /* 
											pagination: {
												el: '.news-pagination<?php echo $count; ?>',
												type: 'bullets',
												clickable: true,
											}, */
										});
										jQuery('#modal<?php echo $count;?>').on('shown.bs.modal', function (e) {
											news_and_events<?php echo $count; ?>.update();
										})
									</script>
									<?php } ?>
								<?php } else { ?>
								<div class="">
									<img src="<?php the_sub_field('event_image'); ?>" alt="News and Events"/>
								</div>
								<?php } ?>
							</div>						
						</div>							
					</div>	
				</div>	
				<?php } ?>
				<?php $count++; ?>
				<?php endwhile; ?>
			</div>
			</div>
		</div>
		<div class="col-12 col-sm-4 float-right blogs_sidebar pb-4 no-guttersright bg-dl-grey blog-rightpanel Helvetica_Light">
			<label class=" lbl-title featured-post-title" style="padding-top: 50px;">FEATURED POSTS</label>
				<?php
					$args = array(
							'posts_per_page' => 5,
							'meta_key' => 'meta-checkbox',
							'meta_value' => 'yes'
						);
						$featured = new WP_Query($args);
					 
					if ($featured->have_posts()): 
						while($featured->have_posts()): $featured->the_post(); ?>
							<div class="featured-post">
								<label class="featured-title"><?php the_title(); ?></label>
								<p class="featured-content"><?php the_excerpt(); ?></p>
								<a href="<?php the_permalink(); ?>" class="fs-18 Helvetica_Light focolor hover-border border-bottom-red">Read More</a>
							</div>
							<?php
						endwhile;
					endif;
					wp_reset_postdata();
				?>
				
				<ul class="categories-widget pt-1 m-b0">
					<?php wp_list_categories(); ?>
				</ul>
			<p class="para-feature">
			At Dr.Mohans we are dedicated to support you in all your requirements during your patient journey
			</p>
			<div class="book_appintment">
				<div class="gravity_holder">
					<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
					<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
					<?php gravity_form( 1, $display_title = false, $display_description = false, $ajax = true, $echo = true ); ?>
				</div>							
			</div>
		</div>
	</div>
</div>
<section class="fullwidth Helvetica_Light padd-top-bottom-70">
	<div class="container text-center">
		<label class="text-center Helvetica_Thin fs-46 title-4l">4,50,000+ Patients Trust Us</label>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 text-center" style="margin:auto">
				<div class="row sm-p-b0" style="padding-bottom:40px;">		
					<?php $count=1;
					$numrows = count( get_field('we_care_icons','option') ); ?>
					
					<?php while ( have_rows('we_care_icons','option') ) : the_row(); ?>	
					<div class="col-12 col-sm-4 wow zoomIn">
						<img src="<?php the_sub_field('icon'); ?>" class="icon-40k" alt="News and Events"/>
						<label class="title-40k"><?php the_sub_field('sub_text'); ?></label>
					</div>
					<?php if($count%3 ==0 && $count != $numrows){ ?>
					</div>							
					<div class="row">	
					<?php } ?>
					<?php $count++; ?>
					<?php endwhile; ?>
				</div>			
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>