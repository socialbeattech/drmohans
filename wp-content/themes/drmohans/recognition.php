<?php
/**
 *  Template Name: Recognition Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>

	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class="banner d-none d-md-block"/>
		<?php endif; ?>
		<?php if(get_field('mobile_banner',get_the_ID())) { ?>
			<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
		<?php } ?>
	<!-- Banner Text-->
	<div class="recognition-banner-caption carousel-caption">
	<h3 class="wow zoomIn text-left fs-48 colorfff Helvetica_Thin">Our recent</h3>
	<h1 class="wow zoomIn text-left Helvetica_Roman colorfff fs-48">Accomplishments</h1>
	</div>
	<section>
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list">
		<div class="container">
			<label class="color-red fs-42 Helvetica_Roman text-center fw-ligher lh52 rec-title">One of the largest Healthcare groups in Asia<br class="sm-disp-none"> devoted to the treatment of diabetes</label>
			<?php if( have_rows('acheivements') ){ ?>
			<?php while ( have_rows('acheivements') ) : the_row();  ?>
			<div class="row" style="margin-bottom:37px;margin-top:37px;">
				<div class="col-12 col-sm-3">
					<label class="rec-year Helvetica_Light fs-32"><?php the_sub_field('year'); ?></label>
				</div>
				<?php if( have_rows('awards') ){ ?>
					<div class="col-12 col-sm-9 color29 sm-center sm-pl-15" style="padding-left:42px">
					<?php while ( have_rows('awards') ) : the_row();  ?>
						<p class="fs-18 Helvetica_Bold lh30 m-b0"><?php the_sub_field('award_name'); ?></p>
						<p class="" style="margin-bottom: 40px;"><?php the_sub_field('award_description'); ?></p>
					<?php endwhile; ?>
					</div>
				<?php } ?>
			</div>
			<?php endwhile; 
			}?>
		</div>
	</section>
<?php include('spec-footer.php'); ?>
