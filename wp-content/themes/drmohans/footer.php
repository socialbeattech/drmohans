<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<div style="display:none !important">
<?php gravity_form( 9, $display_title = false, $display_description = false, $tabindex, $ajax = true, $echo = true ); ?>
</div>
</div><!-- #content -->
<div id="form-slideout" class="form-slideout floating-btns">
		<a href="javascript:void(0)" class="fixed-btn text-uppercase text-center padd5 book-appointment">
		</a>
		<div class="div-to-extend" id="extend_bookapp">		
			<div class="extend-content">
				<div class="book_appintment">
					<div class="gravity_holder"> 
						<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">Book an Appointment</h1>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
						<?php gravity_form( 3, $display_title = false, $display_description = false, $tabindex, $ajax = false, $echo = true ); ?>
					</div>							
				</div>	
			</div>
			<a class="close-extend">X</a>
		</div>
		<a href="javascript:void(0)" class="fixed-btn text-uppercase text-center padd5 location"></a>
		<div class="div-to-extend">		
			<div class="extend-content" style="top:-16%">
				<div class="book_appintment">
					<div class="gravity_holder">
						<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">We are located across<br>40+ centres in India.</h1>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Locate us for consultation</h2>
						<select id="id_states1" name="state" class="id_states1 location-select sticky-select">
							<option value="0">States</option>						
							<?php echo wpb_list_child_pages(); //picks up page title and link from functions.php ?>
							</select>
						<select id="id_cities1" name="city" class="id_cities1 location-select sticky-select">
							<option value="0">Cities</option>
						</select><br>
						<label class="alert1"></label>
						<button id="submit_inv1" class="submit_inv1 speciality-b">Search</button>
					</div>							
				</div>	
			</div>
			<a class="close-extend">X</a>
		</div>
		<a href="javascript:void(0)" class="fixed-btn text-uppercase text-center padd5 enquire"></a>
		<div class="div-to-extend" id="extend_bookapp">		
			<div class="extend-content">	
				<div class="book_appintment">
					<div class="gravity_holder">
						<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">Enquire</h1>
						<?php gravity_form( 2, $display_title = false, $display_description = false,$tabindex, $ajax = false, $echo = true ); ?>
					</div>							
				</div>	
			</div>
			<a class="close-extend">X</a>
		</div>
		<a href="https://drmohans.com/feedback/" class="fixed-btn text-uppercase text-center padd5 feedback" style="padding:0;"></a>
		<a href="javascript:void(0)" class="fixed-btn text-uppercase text-center padd5 search"></a>
		<div class="div-to-extend" style="top:0">		
			<div class="book_appintment">
				<div class="gravity_holder" style="padding-bottom:5%;">
					<input type="text" class="search-input padd0" name="" value="" placeholder="Enter your search keyword" style="padding:3px !important"/>
					<img class="search-submit" src="<?php echo get_template_directory_uri();?>/images/search-icon.png"/>
					<label class="err_search colorfff"></label>
					<div class="search-result">
					</div>
				</div>							
			</div>	
			<a class="close-extend">X</a>
		</div> 
	</div>
	<div class="modal" id="modalbookapp"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
				<span class="text-right close colorfff" data-dismiss="modal">X</span>
				<div class="gravity_holder">
					<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters">Book an Appointment</h1>
					<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
					<?php gravity_form( 4, $display_title = false, $display_description = false,$tabindex, $ajax = false, $echo = true ); ?>
				</div>							
			</div>							
		</div>	
	</div>	
	<div class="modal" id="modalenq"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
				<span class="text-right close colorfff" data-dismiss="modal">X</span>
				<div class="gravity_holder">
					<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">Enquire</h1>
					<?php gravity_form( 5, $display_title = false, $display_description = false,$tabindex, $ajax = false, $echo = true ); ?>	
				</div>							
			</div>							
		</div>	
	</div>	
	<div class="modal" id="modallocation"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
			<span class="text-right close colorfff" data-dismiss="modal">X</span>
				<div class="gravity_holder" style="padding-bottom:5%">
					<h1 class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters Helvetica_Bold">We are located across<br>35+ centres in India.</h1>
					<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Locate us for consultation</h2>
					<select id="id_states" name="state" class="id_states location-select sticky-select">
						<option value="0">States</option>
							<?php echo wpb_list_child_pages(); //picks up page title and link from functions.php ?>
					</select>
						<?php //echo wpb_list_child_pages(); ?>
					<select id="id_cities" name="city" class="id_cities location-select sticky-select">
						<option value="0">Cities</option>
					</select><br>
					<label class="alert"></label>
					<button id="submit_inv" class="submit_inv speciality-b">Search</button>
				</div>							
			</div>							
		</div>	
	</div>	
	<div class="modal" id="modalsearch"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
			<span class="text-right close colorfff" data-dismiss="modal">X</span>
			<div class="book_appintment">
				<div class="gravity_holder" style="padding-bottom:5%;position: relative;margin-top: 20px;">
					<input type="text" class="search-input1 padd0" name="" value="" placeholder="Enter your search keyword" style="padding: 4px;"/>
					<img class="search-submit1" src="<?php echo get_template_directory_uri();?>/images/search-icon.png"/ style="bottom: 45%;right: 32px;">
					<label class="err_search1 colorfff"></label>
					<div class="search-result1">
						
					</div>
				</div>							
			</div>						
			</div>							
		</div>	
	</div>	
	<div id="form-slideout-mobile" class="form-slideout-mobile">
	<a href=""  class="fixed-btn text-uppercase  text-center padd5 book-appointment" data-toggle="modal" data-target="#modalbookapp" style="padding:0;"></a>
	
	<a href=""  class="fixed-btn text-uppercase text-center padd5 location" style="padding:0;" data-toggle="modal" data-target="#modallocation"></a> 
	
	<a href=""  class="fixed-btn text-uppercase text-center padd5 enquire" data-toggle="modal" data-target="#modalenq" style="padding:0;"></a>
	<a href="https://drmohans.com/feedback/" class="fixed-btn text-uppercase text-center padd5 feedback" style="padding:0;"></a>
	<a href="" class="fixed-btn text-uppercase text-center search padd5" style="padding:0;" data-toggle="modal" data-target="#modalsearch"></a>
	</div>

	<footer class="fullwidth">
		<div class="fullwidth footer">
			<div class="container">
				<div class="row pt-4 pb-3">
					<div class="col-12 col-sm-4">
						<h1 class="col-12 col-sm-12 text-uppercase no-gutters">Main centre address</h1>
						<p>No.6-B, Conran Smith Road,</p>
						<p>Near Satyam Cinemas, Gopalapuram,</p>
						<p>Chennai, Tamil Nadu 600086</p>
						<p>Ph: 044 43968888</p>
						<p>Email : <a href="mailto:appointments-gop@drmohans.com">appointments-gop@drmohans.com</a></p>
					</div>
					<div class="col-12 col-sm-3">
						<h1 class="col-12 col-sm-12 text-uppercase no-gutters sm-mt-10">quick links</h1>	
						<ul class="col-12 col-sm-12 no-gutters list-none">				
							<li><a href="<?php echo get_home_url();?>/home-care/" class="hover-border">Home Care</a></li>
							<li><a href="<?php echo get_home_url();?>/patient-care/diabetes-preventive-care/" class="hover-border"> Diabetes Prevention</a></li>
							<li><a href="<?php echo get_home_url();?>/patient-care/diabetic-diet/" class="hover-border">Diabetes Diet</a></li>
							<li><a href="<?php echo get_home_url();?>/patient-care/weight-loss/" class="hover-border">Weight Loss</a></li>
							<li><a href="<?php echo get_home_url();?>/patient-care/stress-management/" class="hover-border">Stress Management</a></li>
							<li><a href="<?php echo get_home_url();?>/precision-diabetes/" class="hover-border">Precision Diabetes</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-3">
						<h1 class="col-12 col-sm-12 text-uppercase no-gutters">Locate Clinics</h1>
							<ul class="col-12 col-sm-12 no-gutters list-none">
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/tamilnadu/" class="hover-border">Clinics in Tamil Nadu</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/chennai/" class="hover-border">Clinics in Chennai</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/karnataka/" class="hover-border">Clinics in Karnataka</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/andhra-pradesh/" class="hover-border">Clinics in Andhra Pradesh</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/pondicherry/" class="hover-border">Clinics in Puducherry</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/odisha/" class="hover-border">Clinics in Odisha</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/delhi/" class="hover-border">Clinics in Delhi</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/kerala/" class="hover-border">Clinics in Kerala</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/telangana/" class="hover-border">Clinics in Hyderabad</a></li>
								<li><a href="<?php echo get_home_url();?>/diabetes-centre-locations/kolkata/" class="hover-border">Clinic in Kolkata</a></li>
							</ul>
					</div>
					<div class="col-12 col-sm-2 no-guttersleft social_icons">
						<h1 class="col-12 col-sm-12 text-uppercase text-left no-gutters" style=" padding-left: 14px;">Connect with us</h1>
						<ul class="socials">
							<li><a href="https://www.facebook.com/drmohansdiabetesinstitutions" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
							<li><a href="https://twitter.com/dmdsc" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://www.youtube.com/user/DrMohansGroup" target="_blank"><i class="fa fa-youtube"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="fullwidth footer_last">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-4 pt-4 copyrights no-gutters">
						<p>&copy; Copyright <?php echo date("Y"); ?> Dr. Mohan's Diabetes Specialities Centre</p>
					</div>
					<div class="col-12 col-sm-4 text-center pt-3 terms">
						<ul class="col-12 col-sm-12 text-center">
							<!--li><a href="">Terms</a></li>
							<li><a href="">Privacy</a></li-->
							<li><a href="<?php echo get_home_url();?>/contact-us/" class="hover-border">Contact Us</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-4 text-right pt-4 sbt no-gutters">
						<p>Designed & developed by <a href="https://socialbeat.in" target="_blank" class="hover-border" rel="nofollow">Social Beat</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="modal" id="newsletter-signup"> 
		<div class="modal-dialog">
			<div class="modal-content book_appintment">
				<div class="gravity_holder">
					<span class="text-right close colorfff" data-dismiss="modal">X</span>
					<span class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title" style="color:#fff">Newsletter Signup</span>
					<?php gravity_form( 8, $display_title = false, $display_description = false, $tabindex, $ajax = true, $echo = true ); ?>
				</div>							
			</div>							
		</div>	
	</div>
	
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  id="onload">
		<div class="modal-dialog popup_form">
		  <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close formclose_popup" data-dismiss="modal">X</button>
			</div>
			<div class="modal-body">
				<div class="book_appintment">
					<div class="gravity_holder">
						<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
						<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
						<?php gravity_form( 13, $display_title = false, $display_description = false,$tabindex, $ajax = true, $echo = true ); ?>
					</div>							
				</div>	
			</div>
		  </div>

		</div>
	</div>


<script>
jQuery(document).ready(function(){
wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'flipInX fadeOut fade fadeInUp bounceInDown bounceInUp fadeInUp', // default
    offset: 1, // default
    mobile: true, // default
    live: true // default
})
wow.init();
});
</script>


	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

<style type="text/css">
.popup_form .modal-header { padding: 0px; margin: 0px auto; position: absolute; z-index: 999; border: 0px none; top: -20px; right: 10px; }
.popup_form .formclose_popup { margin: 0px auto; font-size: 15px; font-weight: normal; opacity: 1;filter: alpha(opacity=100); background: white; color: black; padding: 10px; border-radius: 26px; height: 30px; width: 30px; }
.popup_form .modal-body { padding: 0px; margin: 0px auto; }
.popup_form .modal-content { background : transparent !important; }
#onload { z-index : 9999 !important; }
.gravity_holder .gform_wrapper #input_13_9 {  display: inline-block;  width: 87% !important; }
.gravity_holder #gf_13 ul li#field_13_11 img.ui-datepicker-trigger { height: auto !important; width: auto !important; }
</style>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 967380966;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/967380966/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0019/1047.js" async="async"></script>

</body>
</html>