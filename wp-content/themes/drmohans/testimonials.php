<?php
/**
 *  Template Name: Testimonial Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class="banner d-none d-md-block"/>
	<?php endif; ?>
	<?php if(get_field('mobile_banner',get_the_ID())) {?>
		<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
	<?php } ?>
	<!-- Banner Text-->
	<div class="wow zoomIn  screening-banner-caption carousel-caption">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Testimonials</h1>
		<h3 class="wow zoomIn text-left fs-30">Hear from our happy patients</h3>
	</div>
	<section class="breadcrumb">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 services-list" style="padding-bottom:0 !important;">
		<div class="container text-center">
			<p class="p-b1">Every person's diabetes is unique and we have developed a way to customize your treatment based on your needs </p>
			<div class="row" style="padding-bottom: 39px;padding-top: 40px;">
				<div class="col-12 col-sm-4">
					<span class="fs-42 Helvetica_Roman color-red">4,80,000+</span>
					<p class="fs-22">Patients Trust us</p>
				</div>
				<div class="col-12 col-sm-4">
					<span class="fs-42 Helvetica_Roman color-red">53+ Centres </span>
					<p class="fs-22">across India and counting</p>
				</div>
				<div class="col-12 col-sm-4">
					<span class="fs-42 Helvetica_Roman color-red">29 years</span>
					<p class="fs-22">of service for Diabetes</p>
				</div>
			</div>
			<!--p class="m-b0 ins-banner-a text-center"><a href="#video_testimonials" class="text-uppercase Helvetica_Roman fs-16" style="margin-right:12px">Video testimonials</a><span class="color-red Helvetica_Roman" style="font-weight:bold"> | </span><a href="#written_testimonials" class="text-uppercase Helvetica_Roman fs-16" style="margin-left: 12px;">Written testimonials</a></p-->
		</div>
	</section>
	
	<?php /* section id="video_testimonials" class="fullwidth Helvetica_Light padd-top-bottom-70 bg-dark-grey ">
		<div class="container">
			<label class="lbl-title text-center Helvetica_Thin fs-46 colorfff">Video Testimonials</label>
			<div class="row">
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/prev-slide.png" class="client-prev img-responsive"/>
				</div>
				<div class="col-8 col-sm-10 corporate-slider">
					<div class="swiper-container" id="video-testimonial-slider">
						<div class="swiper-wrapper">						
					<?php if( have_rows('video_testimonial') ){ ?>
						<?php while ( have_rows('video_testimonial') ) : the_row();  ?>
							<div class="swiper-slide">
								<iframe width="356" height="201" src="https://www.youtube.com/embed/<?php the_sub_field('video_id'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</div>
						<?php endwhile; ?>
						<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-2 col-sm-1 marginauto">
					<img src="<?php echo get_template_directory_uri();?>/images/next-slide-white.png" class="client-next img-responsive"/>
				</div>
			</div>
		</div>
	</section */ ?>
	
	<section id="written_testimonials" class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container">
			<label class="lbl-title text-center Helvetica_Thin fs-46 ">Written Testimonials</label>
			<?php if( have_rows('written_testimonials') ){ 
				$w_count = 1; ?>
				<?php while ( have_rows('written_testimonials') ) : the_row();  ?>
				<?if($w_count %2 == 0){ ?>
				<div class="row" style="padding-bottom:50px">				
					<div class="col-12 col-sm-3 text-center m-tb-auto testimonial bl-red d-sm-none">
						<span class="cust_name fs-22 Helvetica_bold color000"><?php the_sub_field('customer_name'); ?></span>
						<p class="cust_location fs-18 m-b0"><?php the_sub_field('location'); ?></p>
					</div>
					<div class="col-12 col-sm-9 m-tb-auto testimonial_text2" >
						"<?php the_sub_field('testimonial_text'); ?>"
					</div>
					<div class="col-12 col-sm-3 text-center m-tb-auto testimonial bl-red  d-none d-md-block">
						<span class="cust_name fs-22 Helvetica_bold color000"><?php the_sub_field('customer_name'); ?></span>
						<p class="cust_location fs-18 m-b0"><?php the_sub_field('location'); ?></p>
					</div>
				</div>
				<?php }else{ ?>
				<div class="row" style="padding-bottom:50px">
					<div class="col-12 col-sm-3 text-center m-tb-auto testimonial br-red">
						<span class="cust_name fs-22 Helvetica_bold color000"><?php the_sub_field('customer_name'); ?></span>
						<p class="cust_location fs-18 m-b0"><?php the_sub_field('location'); ?></p>
					</div>
					<div class="col-12 col-sm-9 m-tb-auto testimonial_text1">
						"<?php the_sub_field('testimonial_text'); ?>"
					</div>
				</div>
				<?php }
			$w_count++;
			endwhile; ?>
			<?php } ?>
		</div>
	</section>
<?php include('spec-footer.php'); ?>