<?php
/**
 *  Template Name: Our Growth Page
  *
 * @link https://codex.wordpress.org/Template_Hierarchy
 
   @package Drmohans
   
 */
 ?><?php include('spec-header.php'); ?>
	<div class="container-fluid padd0"><!-- Banner -->
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>" alt="Banner" class="banner d-none d-md-block"/>
	<?php endif; ?>
	<?php if(get_field('mobile_banner',get_the_ID())) {?>
		<img src="<?php the_field('mobile_banner',get_the_ID()); ?>" alt="Banner" class="img-responsive banner d-sm-block d-md-none"/>
	<?php } ?>
	</div><!-- Banner Text-->
	<div class="wow zoomIn growth-banner-caption carousel-caption">
		<h1 class="wow zoomIn text-left Helvetica_Roman fs-48">Our Growth Story </h1>
		<h3 class="wow zoomIn text-left fs-30">India's Number 1 Diabetes Centre</h3>
	</div>
	<section class="" style="background:#e9eaf3 !important">
		<div class="container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
		?>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light bg-light-blue padd-top-bottom-70">
		<div class="container">
			<div class="row">
				<div class="column pos-relative">
					<img class="pos-relative" src="<?php echo get_template_directory_uri();?>/images/Growth-page_03.png" style="height:100%" alt="Banner" />
					<span class="gallery-txt-1 lh30 fs-24 colorfff">Dr. Mohan’s DSC (or M.V. Diabetes Specialities Centre as it was called then) was inaugurated by Prof. M. Viswanathan.</span>
				</div>
				<div class="column2 pos-relative">
					<div class="row">	
						<div class="col-sm-6 padd0">
							<img src="<?php echo get_template_directory_uri();?>/images/Growth-page_04.png" alt="Banner" />
						</div>
						<div class="col-sm-6 padd0">
							<img class="pos-relative" src="<?php echo get_template_directory_uri();?>/images/Growth-page_05.png" style="width:100%" alt="Banner" />
							<span class="gallery-txt-2 Helvetica_Bold colorfff" style="line-height:1.2 !important;">India's Number 1 Diabetes Centre.<br> <span class="Helvetica_Light gallerytxt2-sub">
Leading the way for the future. </span><br> <span class="Helvetica_Light gallerytxt2-sub">
more than 25 years of experience in the </span><span class="Helvetica_Light gallerytxt2-sub">
prevention and management of diabetes </span></span>
						</div>
					</div> 
					<div class="row Helvetica_Bold">
						<div class="colorfff col-sm-4 bg-red fs-24 lh32 growth-gal-text">
							more than 25 years of <br>experience in the <br>prevention and <br>management of diabetes 
						</div>
						<div class="col-sm-4 padd0">
							<img src="<?php echo get_template_directory_uri();?>/images/Growth-page_08.jpg" style="width:100%" alt="Banner" />
						</div>
						<div class="growth-gal-text col-sm-4 bg-red colorfff fs-24 lh32">
							4,50,000 registered diabetes patients from across 40 centres in India 

						</div>
					</div>  
			  </div>  
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center col-sm-5 Helvetica_Bold" style="margin-top: 20px;">
					<div class="div-first-growth text-uppercase">
						<span class="color-red line" >"</span>Firsts at<span class="color-red">"</span><br/>
						Dr. Mohan's DSC
					</div>
				</div>
				<div class="col-12 col-sm-7">
				<?php if( have_rows('list_first') ){ ?>
				<ul class="primary-ul lh30">
				<?php while ( have_rows('list_first') ) : the_row();  ?>
					<li>
						<?php the_sub_field('list'); ?>
					</li>
				<?php endwhile; ?>
				</ul>
				<?php } ?>
				</div>
			</div>
		</div>
	</section>
	<section class="fullwidth Helvetica_Light padd-top-bottom-70 bg-dark-grey ">
		<div class="container">
			<label class="text-center Helvetica_Thin fs-46 colorfff lbl-title">Our Growth Story</label>
			<div id="accordion">
				<?php if( have_rows('growth_story') ){ ?>
				<?php $count =1; ?>
				<?php while ( have_rows('growth_story') ) : the_row();  ?>
				  <div class="card">
					<div class="growth-acc-heading" id="heading<?php echo $count; ?>">
					  <h5 class="mb-0">
						<button class="btn-acc <?php if ($count != 1 ) { ?>collapsed <?php } ?>" data-toggle="collapse" data-target="#collapse<?php echo $count; ?>" aria-expanded="true" aria-controls="collapse<?php echo $count; ?>">
						<?php the_sub_field('growth_story_year'); ?>
						</button>
					  </h5>
					</div>

					<div id="collapse<?php echo $count; ?>" class="collapse <?php if ($count == 1) { ?> show <?php } ?>" aria-labelledby="heading<?php echo $count; ?>" data-parent="#accordion">
					  <div class="card-body p-50">
						<?php the_sub_field('growth_story_text'); ?>
					  </div>
					</div>
				  </div>
				  <?php $count++; ?>
				  <?php endwhile; ?>
				<?php } ?>
			</div>
		</div>
	</section>
<?php include('spec-footer.php'); ?>