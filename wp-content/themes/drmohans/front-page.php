<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<style>
.carousel-caption{top:25%;}
.carousel-caption h1 { font-weight:bold; line-height: 37px;}
.carousel-caption h3{
	 line-height: 37px;
    margin-top: 7px;
}
.gform_wrapper .gform_footer input.button, .gform_wrapper .gform_footer input[type=submit], .gform_wrapper .gform_page_footer input.button, .gform_wrapper .gform_page_footer input[type=submit]{
	margin:0 !important;
}
.swiper-pagination-bullet-active{
	background: rgb(237, 22, 22) !important;
}
</style>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">		
	<div class="fullwidth experts_care">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-6 home_videos">
					<div class="">	
						<iframe width="560" height="370" src="https://www.youtube.com/embed/AVLTUptkHK0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>					
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-6 home_experts">
					<h2 class="col-12 col-sm-12 col-md-12 col-lg-12 text-left fs-42 pt-4 pt-sm-4 pt-md-0 Helvetica_Roman">Expert care</br> at your service</h2>
					<p class="col-12 col-sm-12 col-md-10 col-lg-12 text-left pt-3 fs-18 focolor Helvetica_Light no-guttersright">We have the country’s most skilled and experienced clinical and paraclinical personnel ably supported by state-of-the-art infrastructure to extend personalised services, expert advice on nutrition and diabetic products. At Dr Mohan’s, we understand every patient is unique and, therefore, we are committed to providing you with customised treatment solutions for your varied needs.</p>  
					<h3 class="col-12 col-sm-12 col-md-12 col-lg-12 text-left py-0 Helvetica_Roman"><a href="<?php echo get_home_url();?>/videos-page/" class="text-uppercase fs-21 Helvetica_Roman">Watch how we care</a></h3>
				</div>
			</div>
		</div>
	</div>	
	
		<div id="speciality" class="fullwidth specialied_care d-none d-md-block" style=""><!--main section-->
			<div class="container"><!--main container-->
				<label class="lbl-title text-center Helvetica_Thin fs-46 colorfff">Helping you know your Diabetes</label>
				<div id="demo" class="carousel slide" data-interval="false" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="row pt-0">
							<div class="cols5 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Diabetes-Diet.png" alt="Diabetes Diet" />		
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/diabetes-diet/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Diabetes Diet</h2></a>
										<p class="Helvetica_Light fs-16">Our in-house nutritionists and dieticians help you maintain the right weight and food habits. Their services go hand in hand with the medications to help you achieve better results.</p>
									</div>
								</div>
							</div>
							
							<div class="cols1 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/fitnessIcon.png" alt="fitness" />			
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/fitness/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Fitness</h2></a>
										<p class="Helvetica_Light fs-16">We provide fitness training and counselling to help patients manage insulin levels, lower blood pressure and reduce stress.</p>
									</div>
								</div>
							</div>
							<div class="cols1 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Stress-Management.png" alt="Stress Management" />						
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/stress-management"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Stress Management</h2></a>
											<p class="Helvetica_Light fs-16">We undertake psychological evaluation and conduct patient-education, skills-development and motivation through counseling, as well as relaxation techniques.</p>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row pt-0"><!-- Slide 1 row 2 -->
							<div class="cols2 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Precision-Diabetes.png" alt="Precision Diabetes" />				
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/precision-diabetes"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Precision Diabetes</h2></a>
										<p class="Helvetica_Light fs-16">We customize health care by tailoring testing, decisions and treatments to the individual.</p>
									</div>
								</div>
							</div>
							<div class="cols2 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Diabetes-Preventive-Care.png" alt="Preventive Care" />  						
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/preventive-care/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Preventive Care</h2></a>
										<p class="Helvetica_Light fs-16">With regular check-ups and timely intervention, we help you prevent diabetes and the complications due to diabetes.</p>
									</div>
								</div>
							</div>
							<div class="cols2 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Weight-Loss.png" alt="Weight Loss" />				
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/weight-loss/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Weight Loss</h2></a>
										<p class="Helvetica_Light fs-16">We assist you in achieving your optimal body weight, thereby preventing the development of diabetes and making control easier if you have already been diagnosed with diabetes.</p>
									</div>
								</div>
							</div>
						</div><!-- row -->
						</div>
						<!-- Slide one ends -->
						<div class="carousel-item">
							<div class="row pt-0">
								<div class="cols4 col-12 col-sm-12 col-md-4"><!-- 2 1 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/eyeIcon.png" alt="eye" />			
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/diabetes-eye-care"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Eye Care</h2></a>
											<p class="Helvetica_Light fs-16">Our retinal specialists ensure that your eyes are protected from the ill-effects of diabetes</p>
										</div>
									</div>
								</div>
								<div class="cols1 col-12 col-sm-12 col-md-4"><!-- 2 2 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Foot-Care.png" alt="Foot Care" />					
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/diabetes-footcare"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Foot Care</h2></a>
											<p class="Helvetica_Light fs-16">We provide comprehensive facilities for early detection and  treatment of diabetic foot complications </p>
										</div>
									</div>
								</div>
						
								<div class="cols1 col-12 col-sm-12 col-md-4"><!-- 2 3 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/kidneyIcon.png" alt="Kidney" />			
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/kidney-care/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Kidney Care</h2></a>
											<p class="Helvetica_Light fs-16">We ensure that your kidneys remain in good health through prevention, early detection and prompt treatment of diabetic kidney disease.</p>
										</div>
									</div>
								</div>
							</div><!-- row -->
							<div class="row pt-0">
								<div class="cols4 col-12 col-sm-12 col-md-4"><!-- 2 4 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/cardiacIcon.png" alt="Cardiac">					
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href=" <?php echo get_home_url();?>/patient-care/diabetes-cardiac-care/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Cardiac Care</h2></a>
											<p class="Helvetica_Light fs-16">We provide preventive, diagnostic and therapeutic services for heart disease due to diabetes.</p>
										</div>
									</div>
								</div>
								
								<div class="cols1 col-12 col-sm-12 col-md-4"><!-- 2 5 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">			
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Surgeries.png" alt="Surgeries" />
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/surgeries"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Surgeries & Treatments</h2></a>
											<p class="Helvetica_Light fs-16">With timely check-ups and specific treatment options for every part of your body, we help you manage diabetes with ease.</p>
										</div>
									</div>
								</div>								
								<div class="cols6 col-12 col-sm-12 col-md-4"><!-- 2 6 -->
									<div class="row">
										<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
											<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Orthopaedic.png" alt="Orthopedic" />					
										</div>
										<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
											<a href="<?php echo get_home_url();?>/patient-care/orthopedic"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Orthopedic Care </h2></a>
											<p class="Helvetica_Light fs-16">Right from mobilisation of joints to surgeries and preventive care, our orthopedic specialists offer expert treatment for bone and joint problems.</p>
										</div>
									</div>
								</div>
							</div><!--row-->
						</div><!-- item -->
					 <div class="carousel-item">
						<div class="row pt-0"><!-- 3 1 -->
							<div class="cols3 col-12 col-sm-12 col-md-4"><!-- 3 3 -->
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Pregnancy---Gestational-Diabetes.png" alt="Pregnancy" />					
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url(); ?>/patient-care/pregnancy/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Pregnancy </h2></a>
										<p class="Helvetica_Light fs-16">We ensure the best possible outcome for your pregnancy through a multidisciplinary approach to achieve optimal blood glucose control. </p>
									</div>
								</div>
							</div>
							<div class="cols5 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Physiotherapy.png" alt="Physiotherapy" />			
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/diabetes-physiotherapy/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Physiotherapy</h2></a>
										<p class="Helvetica_Light fs-16">Our therapists are well-acquainted with promoting mobility of joints and tissues for increased circulation and strength conditioning.</p>
									</div>
								</div>
							</div>
							<div class="cols3 col-12 col-sm-12 col-md-4"><!-- 3 5 -->
							
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Home-Care.png" alt="Home Care" />					
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/home-care"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Home Care</h2></a>
										<p class="Helvetica_Light fs-16">We put the well-being of our patients at the centre of everything we do. Get the best treatment services from the comfort of your home.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row pt-0"><!-- 3 4 -->
							<div class="cols4 col-12 col-sm-12 col-md-4"><!-- 3 6 -->
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/lab.png" alt="lab" />					
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/patient-care/diabetes-laboratory/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Lab</h2></a>
										<p class="Helvetica_Light fs-16">Our state-of-the-art labs are equipped with cutting edge tchnology to deliver accurate results.</p>
									</div>
								</div>
							</div>
							<div class="cols4 col-12 col-sm-12 col-md-4"><!-- 4 1 -->
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Insurance-&-Corporate-Services.png" alt="Insurance" />
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/corporate-and-insurance-services/"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">Insurance & Corporate Services</h2></a>
										<p class="Helvetica_Light fs-16">We offer exclusive benefits tailor-made for Corporate Employees and Insurance options created for your needs.</p>
									</div>
								</div>
							</div>
							
							<div class="cols2 col-12 col-sm-12 col-md-4"><!-- 4 2 -->
								<div class="row">
									<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
										<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/International-Patients.png" alt="International" />			
									</div>
									<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
										<a href="<?php echo get_home_url();?>/international-patients"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters">International Patients </h2></a>
										<p class="Helvetica_Light fs-16">We extend a seamless service right to our overseas patients right from the first enquiry to the post-treatment follow-up.</p>
									</div>
								</div>
							</div>
						</div>
					</div><!-- Carousel - item -->
					
				<div class="carousel-item">
					<div class="row pt-0">
						<div class="cols2 col-12 col-sm-12 col-md-4"><!-- 4 3 -->
							<div class="row">
								<div class="spec-img col-12 col-sm-4 col-md-4 col-lg-3">	
									<img src="<?php echo get_template_directory_uri();?>/images/reactivitiesfordrmohans/Shop-Online.png" alt="Shop Online" />			
								</div>
								<div class="spec-content col-12 col-sm-8 col-md-8 col-lg-9">
									<a href="http://diabetes.ind.in/" target="_blank"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"> Shop Online</h2></a>
									<p class="Helvetica_Light fs-16">From special dietary needs to orthopedic footwear that is beneficial and fashionable, all our products are available online with easy payment and delivery options. </p>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div><!-- Inner -->
				<i style="opacity: 1;color:rgb(237, 22, 22);font-size: 36px;" class="fa fa-chevron-left carousel-control-prev "></i>
				<i style="opacity: 1;color:rgb(237, 22, 22); font-size: 36px;" class="fa fa-chevron-right carousel-control-next "></i>
				<?php /*
			<img class="carousel-control-prev" src="<?php echo get_template_directory_uri();?>/images/Arrows-Previous-icon.png"/>
			<img class="carousel-control-next" src="<?php echo get_template_directory_uri();?>/images/Arrows-Next-icon.png"/>*/?>
			</div><!-- #demo -->
		</div><!-- Main Container-->
	</div><!-- Main Container-->
	<div id="mobile-speciality" class="fullwidth specialied_care d-md-none"><!--main section-->
		<div class="container"><!--main container-->
			<label class="lbl-title text-center Helvetica_Thin fs-46 colorfff">Helping You Know Your Diabetes</label>
			<div id="mobile_demo" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="row pt-0">
					<?php if( have_rows('specialising_in_your_diabetes') ){ 
						$spec_count = 1; ?>
						<?php while ( have_rows('specialising_in_your_diabetes') ) : the_row();  ?>
							<div class="cols1 col-12 col-sm-12 col-md-4">
								<div class="row">
									<div class="col-12 text-center">	
										<img src="<?php the_sub_field('speciality_icon'); ?>" alt="specializations" />			
									</div>
									<div class="col-12 text-center">
										<a href="<?php the_sub_field('speciality_url'); ?>"><h2 class="Helvetica_Medium pb-1 sm-center fs-24 col-12 col-sm-12 col-md-12 col-lg-12 no-gutters"><?php the_sub_field('speciality_title'); ?></h2></a></a>
										<p class="Helvetica_Light fs-16"><?php the_sub_field('speciality_description'); ?></p>
									</div>
								</div>
							</div>
							<?php if($spec_count % 2 == 0){ ?>
							</div>
						</div>
						<div class="carousel-item">
							<div class="row pt-0">
							<?php  } ?>
						<?php $spec_count++; ?>
						<?php endwhile; ?>
					<?php } ?>
						</div> <!-- row -->
					</div> <!-- slider item -->
				</div> <!-- slider inner -->
				<i style="opacity: 1;color:rgb(237, 22, 22)" class="fa fa-chevron-left mobile-carousel-control-prev"></i>
				<i style="opacity: 1;color:rgb(237, 22, 22)" class="fa fa-chevron-right mobile-carousel-control-next"></i>
			</div> <!-- Carousel -->
		</div> <!-- main container -->
	</div> <!-- main section -->

		<div class="fullwidth diabetes_care">
			<div class="container">
				<label class="lbl-title text-center Helvetica_Thin fs-46"><?php the_field('patient_count_text','option'); ?></label>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-8 text-center" style="margin:auto">
						<div class="row sm-p-b0" style="padding-bottom:40px;">		
							<?php $count=1;
							$numrows = count( get_field('we_care_icons','option') ); ?>
							
							<?php while ( have_rows('we_care_icons','option') ) : the_row(); ?>	
							<div class="col-12 col-sm-4">
								<img src="<?php the_sub_field('icon'); ?>" alt="Patients"/>
								<label class="title-40k"><?php the_sub_field('sub_text'); ?></label>
							</div>
							<?php if($count%3 ==0 && $count != $numrows){ ?>
							</div>							
							<div class="row">	
							<?php } ?>
							<?php $count++; ?>
							<?php endwhile; ?>
						</div>			
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-4">
						<div class="book_appintment">
							<div class="gravity_holder">
								<label class="col-12 col-sm-12 text-left pb-2 my-auto fs-22 whcolor no-gutters lbl-title">Book an Appointment</label>
								<h2 class="col-12 col-sm-12 text-left pt-0 my-auto fs-15 237color no-gutters">Let's get you set up with an appointment</h2>
								<?php gravity_form( 1, $display_title = false, $display_description = false,$tabindex, $ajax = true, $echo = true ); ?>
							</div>							
						</div>							
					</div>
				</div>
			</div>
		</div>
		<div class="fullwidth journey">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12 wow fadeInUp">
						<h1 class="col-12 col-sm-7 float-right text-center whcolor Helvetica_Medium">Want to learn more about </br>treatment and prevention of diabetes? </br>Learn more.</h1>
						<div class="col-12 col-sm-7 float-right text-center choose_journey">
							<button class="" type="button" data-toggle="dropdown">Choose Your Journey<span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li><a href="<?php echo get_home_url();?>/screening/">Should you get Screened?</a></li>
								<li><a href="<?php echo get_home_url();?>/category/blogs/">Defeat Diabetes</a></li>
								<li><a href="<?php echo get_home_url();?>/precision-diabetes/">Precision Diabetes</a></li>
								<li><a href="<?php echo get_home_url();?>/home-care/">Home care Service</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $args_default = array( 'category_name' => 'Preventive Care', 'post_type' => 'post', 'posts_per_page' => '3');
			   $spec_blogs_default = new WP_Query( $args_default ); 
			   if ( $spec_blogs_default->have_posts() ) {  ?>
				<section class="fullwidth knows_diabetes">
					<div class="container" style=" margin-bottom: 5.5em;margin-top: 2.5em;">
						<label class="text-center fs-46 lbl-title Helvetica_Thin understand-ur-diabetes">Understand Your Diabetes</label>
						<div class="row my-5">
						<?php while($spec_blogs_default->have_posts()): $spec_blogs_default->the_post(); ?>
							<div class="col-12 col-sm-4">
								<div class="col-12 col-sm-12 pl-4 pt-4 Helvetica_Light focolor understands wow slideInDown">
									<h2 class="text-left fs-22 Helvetica_Bold focolor"><?php the_title(); ?></h2>
									<?php the_excerpt(); ?>
									<p><span><a href="<?php the_permalink(); ?>" class="fs-18 Helvetica_Light focolor hover-border">Read More</a></span></p>
								</div>
							</div>
						<?php endwhile; ?>
						</div>
						<p class="text-center">
						<a href="<?php echo get_home_url();?>/category/blogs" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold" style="padding: 12px 23px;background: rgb(237, 22, 22);">READ ALL BLOGS</a></p> 
					</div>
				</section>
		<?php } 
		wp_reset_postdata(); ?>		
		<div class="fullwidth shops">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-4 float-left">								
						<div class="col-12 col-sm-12 research">	
							<div class="col-12 col-sm-12 borders_stuff wow zoomIn">	
								<h2 class="col-12 col-sm-12 text-center fs-26 whcolor pt-4 Helvetica_Bold">Research</h2>
								<p class="col-12 col-sm-12 text-center mt-3"><a href="<?php echo get_home_url();?>/research" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold">Know More</a></p>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-4 float-left">								
						<div class="col-12 col-sm-12 cart">	
							<div class="col-12 col-sm-12 borders_stuff wow zoomIn">	
								<h2 class="col-12 col-sm-12 text-center fs-26 whcolor pt-4 Helvetica_Bold">Shop</h2>
								<p class="col-12 col-sm-12 text-center mt-3"><a href="http://diabetes.ind.in/" target="_blank" class="col-12 col-sm-12 text-center fs-18 whcolor text-none Helvetica_Bold">Shop Online</a></p>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-4 float-left">								
						<div class="col-12 col-sm-12 tracker_app">	
							<div class="col-12 col-sm-12 borders_stuff wow zoomIn">	
								<h2 class="col-12 col-sm-12 text-center fs-26 whcolor pt-4 Helvetica_Bold">Diabetes App</h2>
								<p class="col-12 col-sm-12 whcolor text-center mt-3 fs-18 whcolor text-none Helvetica_Bold">Know More</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</main><!-- #main -->
</div><!-- #primary -->

<script>/*
var swiper = new Swiper('.swiper-container', {
	//loop: true,
	autoplay: true,
	speed: 300,
  navigation: {
	nextEl: '.swiper-button-next',
	prevEl: '.swiper-button-prev',
  },
});*/
</script>          

<?php get_footer();