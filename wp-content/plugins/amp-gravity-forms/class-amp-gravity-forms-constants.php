<?php

//start form f=of gravity form

function ampforwp_start_gvform( $actionXhrUrl ){
	$actionXhrUrl = $actionXhrUrl."&ampsubmit=1";
		return '<form method="post" enctype="multipart/form-data" id="gform_2" action-xhr="' . $actionXhrUrl . '" target="_top">';

}

//Created heading og form

function ampforwp_gv_heading($form, $atts){	
	$title = '';
	if( 'true' == $atts['title'] ){
		$title = '<h3 class="gform_title">'.$form['title'].'</h3>';
	}
	return '<div class="gform_heading">
			'.$title.'
            <span class="gform_description">'.$form['description'].'</span>

        </div>';  

}

//Main Label

function ampforwp_gv_label($label,$fields){

	$required_field = '';

	if($fields['isRequired'] == true){
			$required_field = '<span class="gfield_required">*</span>';
	}

	return '<label class="gfield_label">'.$label.' '. $required_field .' </label>';

}
// description option
function ampforwp_gf_description($description){
	return '<div class="gfield_description">'. $description .'</div>';
}

//Create radio button of gravity

function ampforwp_gv_radio($choice,$fieldData ,$choicekey){

	//$get_tabindex = get_tabindex();
	$select = '';
	if(isset($optvalue['isSelected'])){
		$select = "checked";
	}

	return '<input

				 name="input_'.$fieldData['id'].'" 

				 type="radio" value="'.$choice['value'].'" 

				 id="choice_'.$fieldData['formId'].'_'.$fieldData['id'].'_'.$choicekey.'" 

				 tabindex="'.get_tabindex().'"   
				 '.$select.'

				 on="change:AMP.setState({ampforwp_gravity_data_'.$fieldData['formId'].':{fieldId_'.$fieldData['id'].': event.value}})"

			/>

			<label for="choice_'.$fieldData['formId'].'_'.$fieldData['id'].'_'.$choicekey.'" id="label_'.$fieldData['formId'].'_'.$fieldData['id'].'_'.$choicekey.'">'.$choice['text'].'</label>';

}



function get_tabindex() {

		return GFCommon::$tab_index > 0 ?  GFCommon::$tab_index ++ : '';

	}

function ampforwp_gv_checkbox($choice,$fieldData ,$choicekey){  

	$choicekey ++;
	$checked = '';	  // var_dump($choice); die;
	if( 1 ==  $choice['isSelected'] ){
		$checked = 'checked';
	}
	//$get_tabindex = get_tabindex();

	return '<input

				 name="input_' .$fieldData['inputs'][0]['id']. '" 

				 type="checkbox" value=   \''. $choice['value'].'\'  

				 id="choice_'.$fieldData['formId'].'_'.$fieldData['id'].'_'.$choicekey.'" 

				 tabindex="'.get_tabindex().'" 
				 '.$checked.'


				on="change:AMP.setState({ ampforwp_gravity_data_'.$fieldData['formId'].': {fieldId_'.$fieldData['id'].': (event.checked == true ? event.value : false ) }})"

			/>
		
			<label for="choice_'.$fieldData['formId'].'_'.$fieldData['id'].'_'.$choicekey.'" id="label_'.$fieldData['formId'].'_'.$fieldData['id'].'_'.$choicekey.'">'.$choice['text'].'</label>';

}

//Create input form

function ampforwp_gv_input($fields,$tab){

	$required_field = '';
 
	$fields['defaultValue'] = GFCommon::replace_variables( $fields['defaultValue'], $fields, array(), false, true,true );

	/*// $vari = GFFormDisplay::get_calculations_init_script($fields);
	$ampforwp_read_only   = '';

	$amp_gf_field = new GF_Field;


			if ( $amp_gf_field->has_calculation() ) {

				// calculation-enabled fields should be read only
				$ampforwp_read_only = 'readonly="readonly"';

			//	var_dump($ampforwp_read_only); die;

			} 
	 */
	if($fields['isRequired'] == true){
			$required_field = '<span class="gfield_required">*</span>';
	}
	return '<label class="gfield_label" for="input_'.$fields['formId'].'_'.$fields['id'].'">'.$fields['label'].' '.$required_field.' </label>

         <div class="ginput_container ginput_container_text"><input name="input_'.$fields['id'].'" id="input_'.$fields['formId'].'_'.$fields['id'].'" type="'.$fields['type'].'" value="'.$fields['defaultValue'].'" class="medium" tabindex="'.get_tabindex().'" role="input" aria-invalid="false" on="change:AMP.setState({ampforwp_gravity_data_'.$fields['formId'].': {fieldId_'.$fields['id'].': event.value}})" placeholder="'. $fields['placeholder'].'" /></div>';

}

//for phone field  

function ampforwp_gv_tel($fields,$tab){

	$patt = '';
	$required_field = '';
	$amp_placeholder = 'placeholder="Enter 10 digits number"';

	if($fields['phoneFormat'] == 'standard') {

		$patt = 'pattern="[0-9]{10}(-[0-9]{4})?"';

		$amp_placeholder = 'placeholder="Enter 10 digits number"';

	}
	if($fields['isRequired'] == true){
			$required_field = '<span class="gfield_required">*</span>';
	}

	return '<label class="gfield_label" for="input_'.$fields['formId'].'_'.$fields['id'].'">'.$fields['label'].' '.$required_field.'</label>

         <div class="ginput_container ginput_container_phone"><input name="input_'.$fields['id'].'" id="input_'.$fields['formId'].'_'.$fields['id'].'" type="tel" value="'.$fields['defaultValue'].'" class="medium" tabindex="'.get_tabindex().'"  '.$patt.' '.$amp_placeholder.' aria-invalid="false" placeholder="'.$fields['placeholder'].'" /> </div>';  

}

//Create multiselect box

function ampforwp_gv_multiselect($fields,$tab){

	$opt ='';
	$required_field = '' ;
	if($fields['choices'] && !isset($fields['choices'])){
		$fields['choices'] = array();
	}


		if(isset($fields['choices']) && count(array($fields['choices']))>0 && $fields['choices']!=""){

			foreach ($fields['choices'] as $optKey => $optvalue) {

				$select ='';

				if($optvalue['isSelected']){

					$select = "selected='selected'";

				}

				$opt .= '<option value="'.$optvalue['value'].'" '.$select.'>'.$optvalue['text'].'</option>';

			}

		}
	if($fields['isRequired'] == true){
			$required_field = '<span class="gfield_required">*</span>';
	}

	return '<label class="gfield_label" for="input_'.$fields['formId'].'_'.$fields['id'].'">'.$fields['label'].' '.$required_field.'</label>

			<div class="ginput_container ginput_container_multiselect">

				<select multiple="multiple" size="7" name="input_'.$fields['id'].'[]" id="input_'.$fields['formId'].'_'.$fields['id'].'" class="medium gfield_select" tabindex="'.get_tabindex().'" on="change:AMP.setState({ampforwp_gravity_data_'.$fields['formId'].': {fieldId_'.$fields['id'].': event.value}})" >

					'.$opt.'

				</select>

			</div>';

}

// Create select dropdown

function ampforwp_gv_select($fields,$tab){

	$opt = '';
	$required_field = '';

		if($fields['placeholder']!= ''){
         	$opt .= '<option value="" selected>'.$fields['placeholder'].'</option>';
        
         }
		if(isset($fields['choices']) && count($fields['choices'])>0){
			$defaultValue = $fields['defaultValue'] ;

			foreach ($fields['choices'] as $optKey => $optvalue) {

				$select ='';
				if(!empty($defaultValue) &&  $optvalue['value'] == $defaultValue){
					$select = "selected='selected'";
				}elseif($optvalue['isSelected']){
					$select = "selected='selected'";
				}

				$opt .= '<option value="'.$optvalue['value'].'" '.$select.'>'.$optvalue['text'].'</option>';

			}

		}

	if($fields['isRequired'] == true){
			$required_field = '<span class="gfield_required">*</span>';
	}

	return '<label class="gfield_label" for="input_'.$fields['formId'].'_'.$fields['id'].'">'.$fields['label'].''.$required_field.'</label>

			<div class="ginput_container ginput_container_select">

				<select name="input_'.$fields['id'].'" id="input_'.$fields['formId'].'_'.$fields['id'].'" class="medium gfield_select" tabindex="'.get_tabindex().'" aria-invalid="false" on="change:AMP.setState({ampforwp_gravity_data_'.$fields['formId'].': {fieldId_'.$fields['id'].': event.value}})" >
					'.$opt.'
 				</select>
			</div>';

}

//Create textarea

function ampforwp_gv_textarea($fields,$tab){

	$opt = '';
	$required_field = '';


		if(isset($fields['choices']) && count ($fields['choices'])>0 && $fields['choices']!=""){

			error_log($fields['choices']);

			foreach($fields['choices'] as $optKey => $optvalue){

				$select ='';

				if($optvalue['isSelected']){

					$select = "selected='selected'";

				}

				$opt .= '<option value="'.$optvalue['value'].'" '.$select.'>'.$optvalue['text'].'</option>';

			}

		}
	if($fields['isRequired'] == true){
			$required_field = '<span class="gfield_required">*</span>';
	}

	return '<label class="gfield_label" for="input_'.$fields['formId'].'_'.$fields['id'].'">'.$fields['label'].''.$required_field.'</label>

		<div class="ginput_container ginput_container_textarea">

			<textarea name="input_'.$fields['id'].'" id="input_'.$fields['formId'].'_'.$fields['id'].'" class="textarea medium" tabindex="'.get_tabindex().'" placeholder="'.$fields['placeholder'].'" aria-invalid="false" rows="10" cols="50"></textarea>

			<div class="charleft ginput_counter">0 of 500 max characters</div>

		</div>';

}
 
//create page bottom

function ampforwp_gv_section_type($fieldData, $form){

	if($fieldData['type'] == 'section'){
		return 
		 	'<div id="gform_page_form_id_'.$fieldData['formId'].'" class="gform_section" >
		 	 <ul>';
	}

}


function ampforwp_gv_page_bottom($fieldData, $key, $form,$currentPage){


	$showPreviousPage ='';
	$sec_cls_div_li = '';
	global $sec_check;

	if($currentPage>2){

		$showPreviousPage = '<button type="button" id="gform_next_button_'.$fieldData['formId'].'_'.$fieldData['id'].'" class="gform_next_button button" tabindex="'.get_tabindex().'" on="tap:AMP.setState({ampforwp_gravity_data_'.$form['id'].': {showpage: '.($currentPage-2).'}})"><span>'.$fieldData['previousButton']['text'].'</span></button> ';



	}  
	if($sec_check == 'true' ) {
		$sec_cls_div_li = '</div></li>';
	}
	return '</li>

		 </ul>
		 	 '.$sec_cls_div_li.'
			 <div class="gform_page_footer">

			 	'.$showPreviousPage.'

	             <button type="button" id="gform_next_button_'.$fieldData['formId'].'_'.$fieldData['id'].'" class="gform_next_button button" tabindex="'.get_tabindex().'" on="tap:AMP.setState({ampforwp_gravity_data_'.$form['id'].': {showpage: '.($currentPage).'}}),AMP.scrollTo(\'id\'=\'gf_page_steps_'.$fieldData['formId'].'\');"><span>'.$fieldData['nextButton']['text'].'</span></button> 

	        </div>

        </div>
        
     <div id="gform_page_'.$form['id'].'_'.( $currentPage+1) .'" class="gform_page hide" :openbrack:class:closebrack:="ampforwp_gravity_data_'.$form['id'].'.showpage==\''.($currentPage).'\'? \'gform_page show\': \'gform_page hide\'">
        <ul>';
}

//create page of pagebreake

function ampforwp_gv_page($fieldData, $headingType){
 
			$pageTitle ='';

			if(isset($fieldData['pagination']['pages'])){

				foreach ($fieldData['pagination']['pages'] as $key => $page) {

					$pageno = $key+1;

					$activeClass = '';

					if($pageno==1){

						$activeClass = 'gf_step_active';

					}
					if(empty($page)){
						$page = '<span class="gf_step_number">'.$pageno.'</span>';
					}

					$pageTitle .= '<div id="gf_step_'.$fieldData['id'].'_'.$pageno.'" class="gf_step  '.$activeClass.'" :openbrack:class:closebrack:="ampforwp_gravity_data_'.$fieldData['id'].'.showpage=='.$pageno.'? \'gf_step gf_step_active\': \'gf_step\'">

							&nbsp;<span class="gf_step_label">'.$page.'</span>

							&nbsp;<span [text]="ampforwp_gravity_data.showpage"></span>

						</div>';
				}

			}

			return '<div id="gf_page_steps_'.$fieldData['id'].'" class="gf_page_steps">

					'.$pageTitle.'

					<div class="gf_step_clear"></div>

				</div>';	

}



function ampforwp_gv_submit($form){
	global $defaultValues;
	$showPreviousPage ='';
	$defaultShowField = $defaultLiClass = 'gform_footer top_label ';

	if(isset($form['pagination']) && count($form['pagination']['pages'])>0){

		$showPreviousPage = '<button type="button" id="gform_next_button_'.$form['id'].'" class="gform_button gform_next_button button" tabindex="'.count($form['fields']).'" on="tap:AMP.setState({ampforwp_gravity_data_'.$form['id'].': {showpage: '.(count($form['pagination']['pages'])-1).'}})"><span>'.$form['lastPageButton']['text'].'</span></button> ';

	}

	if(!class_exists('GFFormDisplay')){

		require_once( GFCommon::get_base_path() . '/form_display.php' );

	}

	if(!class_exists('GFFormsModel')){

		require_once( GFCommon::get_base_path() . '/forms_model.php' );

	}

	$buttonCondition = $loadClass = ""; 

	if(isset($form['pagination']['pages'])){

		$buttonCondition = ':openbrack:class:closebrack:="ampforwp_gravity_data_'.$form['id'].'.showpage==\''.(count($form['pagination']['pages'])).'\'? \'show\': \'hide\' "';

		$loadClass = "hide";

	}

	//compatible with gravity-forms-zero-spam
	$spamKeyField = '';
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if(is_plugin_active('gravity-forms-zero-spam/gravityforms-zero-spam.php')){
		$gf_zero_spam = new GF_Zero_Spam;
		
		$spamKeyField = '<input type="hidden" name="gf_zero_spam_key" value="'.$gf_zero_spam->get_key().'">';
	} // gravity-forms-zero-spam end here...

       //compatible with Wordpress Zero Spam plugin
	    $zero_spamKeyField = '';
		 if(is_plugin_active('zero-spam/zero-spam.php')){
		  if(function_exists('zerospam_get_key')){
		    $zero_spam_key = zerospam_get_key();
	       }
		 $zero_spamKeyField = '<input type="hidden" name="zerospam_key" value="'.$zero_spam_key.'">';
	    } // Wordpress Zero Spam end here...


	if(isset($form['button']['conditionalLogic']) && is_array($form['button']['conditionalLogic'])){
		
			if($form['button']['conditionalLogic']['actionType']=='show'){

				$conditionExpression = '';
				$conditions = '';
				$defaultShow =  '';
				foreach ($form['button']['conditionalLogic']['rules'] as $condition) {

					$condition['fieldId'] =  str_replace(".", "_", $condition['fieldId']);
					//Get the operator
					$operator = str_replace(array('isnot','is','contains'), array("!=",'==','indexOf'), $condition['operator']);					

					if($conditionExpression!=""){
						$conditionExpression .= ' && ';
					}
					$conditionExpression = 'ampforwp_gravity_data_'.$form['id'].'.fieldId_'.$condition['fieldId'].$operator.'\''.$condition['value'].'\'';

					if( isset($defaultValues['fieldId_'.$condition['fieldId']]) ){ 

						switch($operator){ 
							case '!=':  
								if( $defaultValues['fieldId_'.$condition['fieldId']] != $condition['value'] ){
									$defaultShow = "show";
								}else{
									$defaultShow = "hide";
								}
							break;
							case ' <':
								if( $defaultValues['fieldId_'.$condition['fieldId']] < $condition['value'] ){
									$defaultShow = "show";
								}else{
									$defaultShow = "hide";
								}
							break;
							case " >":
								if( $defaultValues['fieldId_'.$condition['fieldId']] > $condition['value'] ){
									$defaultShow = "show";
								}else{
									$defaultShow = "hide";
								}
							break;
							case 'indexOf':
								if( strpos($defaultValues['fieldId_'.$condition['fieldId']], $condition['value']) !== false ){
									$defaultShow = "show";
								}else{
									$defaultShow = "hide";
								}
							break;
							case '==': 
							default:
								if( $defaultValues['fieldId_'.$condition['fieldId']] == $condition['value'] ){ 
									$defaultShow = "show";

								}else{
									$defaultShow = "hide";
								}
							
								
							break;
						}
					}
					if($conditions != ''){
					if($form['button']['conditionalLogic']['logicType'] == 'all'){
						$conditions .= '&&';
					}else{
							$conditions .= '||';
					  }
					}
				
 					if($operator == 'indexOf'){
						$conditions .= 'ampforwp_gravity_data_'.$form['id'].'.fieldId_'.$condition['fieldId'].".".$operator.'(\''.$condition['value'].'\')!=-1';
					}else{
						$conditions .= 'ampforwp_gravity_data_'.$form['id'].'.fieldId_'.$condition['fieldId'].$operator.'\''.$condition['value'].'\'';
					}

				} 

				$defaultShowField .= $defaultShow;
				$conditionClass = ':openbrack:class:closebrack:="'.$conditions.' ? \''. $defaultLiClass .' show\': \''. $defaultLiClass .' hide\' " ';
			}elseif($form['button']['conditionalLogic']['actionType']=='hide'){

				$conditionExpression = '';
				$conditions = '';
				$defaultShow =  '';
				foreach ($form['button']['conditionalLogic']['rules'] as $condition) {

					$condition['fieldId'] =  str_replace(".", "_", $condition['fieldId']);
					//Get the operator
					$operator = str_replace(array('isnot','is','contains'), array("!=",'==','indexOf'), $condition['operator']);
					

					if($conditionExpression!=""){
						$conditionExpression .= ' && ';
					}
					$conditionExpression = 'ampforwp_gravity_data_'.$form['id'].'.fieldId_'.$condition['fieldId'].$operator.'\''.$condition['value'].'\'';

					if( isset($defaultValues['fieldId_'.$condition['fieldId']]) ){ 

						switch($operator){ 
							case '!=':  
								if( $defaultValues['fieldId_'.$condition['fieldId']] != $condition['value'] ){
									$defaultShow = "hide";
								}else{
									$defaultShow = "show";
								}
							break;
							case ' <':
								if( $defaultValues['fieldId_'.$condition['fieldId']] < $condition['value'] ){
									$defaultShow = "hide";
								}else{
									$defaultShow = "show";
								}
							break;
							case " >":
								if( $defaultValues['fieldId_'.$condition['fieldId']] > $condition['value'] ){
									$defaultShow = "hide";
								}else{
									$defaultShow = "show";
								}
							break;
							case 'indexOf':
								if( strpos($defaultValues['fieldId_'.$condition['fieldId']], $condition['value']) !== false ){
									$defaultShow = "hide";
								}else{
									$defaultShow = "show";
								}
							break;
							case '==': 
							default:
								if( $defaultValues['fieldId_'.$condition['fieldId']] == $condition['value'] ){ 
									$defaultShow = "hide";

								}else{
									$defaultShow = "show";
								}
							
								
							break;
						}
					}
					if($conditions != ''){
					if($form['button']['conditionalLogic']['logicType'] == 'all'){
						$conditions .= '&&';
					}else{
							$conditions .= '||';
					  }
					}
				
 					if($operator == 'indexOf'){
						$conditions .= 'ampforwp_gravity_data_'.$form['id'].'.fieldId_'.$condition['fieldId'].".".$operator.'(\''.$condition['value'].'\')!=-1';
					}else{
						$conditions .= 'ampforwp_gravity_data_'.$form['id'].'.fieldId_'.$condition['fieldId'].$operator.'\''.$condition['value'].'\'';
					}

				} 
				$defaultShowField .= $defaultShow;
				$conditionClass = ':openbrack:class:closebrack:="'.$conditions.' ? \''. $defaultLiClass .' hide\': \''. $defaultLiClass .' show\' " ';
			} 
		}
		if(!isset($conditionClass)){
			$conditionClass = "";
		}

	return '<div class="'.$defaultShowField.''.$loadClass.'" id= form_id_'.$form['id'].'  '.$buttonCondition.' '.($conditionClass!='' ? $conditionClass: '').'>
			'.$showPreviousPage.'

			<button type="submit" id="gform_submit_button_'.$form['id'].'" class="gform_button button" tabindex="8" ><span>'.$form['button']['text'].'</span></button>
 
             <input type="hidden" class="gform_hidden" name="is_submit_'.$form['id'].'" value="1" />

            <input type="hidden" class="gform_hidden" name="gform_submit" value="'.$form['id'].'" />
            '.$spamKeyField.'
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="" />

            <input type="hidden" class="gform_hidden" name="state_'.$form['id'].'" value="'.GFFormDisplay::get_state( $form, array() ).'" />

            

            <input type="hidden" class="gform_hidden" name="gform_target_page_number_'.$form['id'].'" id="gform_target_page_number_'.$form['id'].'" value="0" />

         <input type="hidden" class="gform_hidden" name="gform_source_page_number_'.$form['id'].'" id="gform_source_page_number_'.$form['id'].'" value="1" :openbrack:value:closebrack:="ampforwp_gravity_data_'.$form['id'].'.showpage"/>

            <input type="hidden" name="gform_field_values" value="" />
             '.$zero_spamKeyField.'

           </div>';

}

?>