<?php

$submitClass = realpath( __DIR__ . '/class-amp-gravity-forms-submission.php' );
if ( file_exists( $submitClass ) ) {
	include_once $submitClass;
} else {
	@header( 'HTTP/1.1 500 INTERNAL ERROR' );
	exit;
}

class AMP_Gravity_Form_Submit extends AMP_Form_Submit {
	protected $action  = '';
	protected $formId  = 0;
	
	public function ampforwp_custom_confirmation_action_Save($confirmation,$form,$entry,$is_ajax){
		if(is_array($confirmation) && isset( $confirmation['redirect']) ) {
			$headers = $this->cors_Headers;
			$confirmation = $confirmation['redirect'];
			$this->cors_Headers['AMP-Redirect-To'] = $confirmation;
			$this->cors_Headers['Access-Control-Expose-Headers'] = "AMP-Redirect-To, ".$headers['Access-Control-Expose-Headers'];
			$this->response['message'] = "Thank you, form has been submitted. please wait..";
			$this->outputResponse()
		     ->pushResponse();
			exit;
		}else{
			$this->response['message'] = $confirmation;
			add_action( 'gform_after_submission', array( $this, 'amp_gravity_send_json' ),99, 2 );
		}
	}
	
	public function amp_gravity_send_json( $items, $result ) { 
		
		$formId = $result['id'];
		if(isset($result['confirmation'])){
			$status = 'success';

			$new_result = $this->response['message'];
			
				$new_result = strip_tags($new_result,'<td>');
				$new_result = str_replace('<td>', " ", $new_result); 
				$new_result = str_replace('<td colspan="2">', '<i>', $new_result);
				$new_result = str_replace('<td width="20">', " ", $new_result);
				//$new_result = str_replace('<td>', " ", $new_result);
				//$new_result = str_replace('</td>', " ", $new_result);
				$new_result = str_replace('</td>', "</i>", $new_result);
				$new_result = str_replace('</td>', " ", $new_result);
				$new_result = preg_replace('/<td colspan="2" style=".*">/', " ", $new_result);

				$this->response['message'] = isset( $new_result ) ? $new_result : '';

			//	$result = $result['confirmation'];
			//	$this->response['message'] = isset( $result['message'] ) ? $result['message'] : '';
			
			
		}else{
			$status = 'error';
		}
		
		
		switch ( $status ) {
			case 'error':
				$this->response['status'] = 'error';
				$this->outputResponse()
				     ->pushResponse( 'HTTP/1.1 500 FORBIDDEN' );
				break;
			case 'success':
				$this->response['status'] = 'success';
				$this->response['form_id'] = $formId;
				$this->outputResponse()->pushResponse();
				break;
			default:
				$this->response['status'] = 'error';
				$this->outputResponse()
				     ->pushResponse( 'HTTP/1.1 403 FORBIDDEN' );
				break;
		}

	}

	protected function preProcess() {
		parent::preProcess();
		if ( ! empty( $this->request['action'] ) ) {
			$this->action = $this->request['action'];
		}
		return $this;
	}

	protected function setHooks() {
		parent::setHooks(); 
		add_filter( 'gform_validation', array( $this, 'amp_gravity_form_validation' ),30, 1 );
		add_filter( 'gform_confirmation', array($this,'ampforwp_custom_confirmation_action_Save'),21,4);
		//add_filter( 'get_js_redirect_confirmation', array($this,'normal'),30,4);

		
		return $this;
	}

	function amp_gravity_form_validation($form){
		$errors = array();
		if(isset($form['is_valid']) && !$form['is_valid'] && count($form['form']['fields'])>0){
			foreach ($form['form']['fields'] as $key => $err) {

				if ( ! empty( $err['validation_message'] ) ) {

					$errors[] = array(
								'error_label' => $err['label'],
								'error_detail' => $err['validation_message'] );
				}
			}
		}
		if ( ! empty( $errors ) ) {
			$this->response['errors']     = $errors;
			$this->response['status']     = 'error';
			$this->response['has_errors'] = true;
			$this->outputResponse()
			     ->pushResponse( 'HTTP/1.1 400 Bad Request' );
		}
		return $form;
	} 

	protected function submitForm() {
		$form_id = isset( $_POST['gform_submit'] ) ? absint( $_POST['gform_submit'] ) : 0;
		if ( empty( $form_id ) ) {
			$this->response['status'] = 'error';
			$this->outputResponse()
			     ->pushResponse( 'HTTP/1.1 404 NOT FOUND' );
		}
		
		if ( $form_id ) {
			//add_action('gform_post_submission_'.$form_id, array($this, 'submitCalles'));

			//add_action( 'gform_after_submission_'.$form_id, array( $this, 'amp_gravity_send_json' ),10, 2 );
			$form_info     = RGFormsModel::get_form( $form_id );
			$is_valid_form = $form_info && $form_info->is_active;

			if ( $is_valid_form ) {
				// require_once( GFCommon::get_base_path() . '/form_display.php' );
				// GFFormDisplay::process_form( $form_id );
				$this->ampforwp_gravity_submission( $form_id );
			}else{
				$this->response['status'] = 'error';
				$this->response['message'] = 'Form not valid';
				$this->outputResponse()
			    	 ->pushResponse( 'HTTP/1.1 404 NOT FOUND' );
			}
		}
	}

	protected function run() {
		switch ( $this->action ) {
			case 'gravity_form_submission'://'submit':
				$this->submitForm();
				break;
			default:
				$this->response['status'] = 'error';
				$this->outputResponse()
				     ->pushResponse( 'HTTP/1.1 404 NOT FOUND' );
				break;
		}
		return $this;
	}

	protected function getGfVersion() {
		$version = '';
		//if ( empty( $version ) && has_class( 'GFCommon' ) ) {
			$version = GFCommon::$version;
		//}
		return $version;
	}

	public function ampforwp_gravity_submission($form_id){


		require_once( GFCommon::get_base_path() . '/form_display.php' );
		GFCommon::log_debug( "GFFormDisplay::process_form(): Starting to process form (#{$form_id}) submission." );

		$form = GFAPI::get_form( $form_id ); 


		foreach ($form['fields'] as $form_key => $form_value) { 
		//	print_r($form_value->type);
			if($form_value->type == 'captcha'){
	          unset($form['fields'][$form_key]);
	       }
		}
	//echo "hello";	print_r($form['fields']);die;
		/**
		 * Filter the form before GF begins to process the submission.
		 *
		 * @param array $form The Form Object
		 */
		$filtered_form = gf_apply_filters( array( 'gform_pre_process', $form['id'] ), $form );
		if ( $filtered_form !== null ) {
			$form = $filtered_form;
		}

		//reading form metadata
		$form = GFFormDisplay::maybe_add_review_page( $form );

		if ( ! $form['is_active'] || $form['is_trash'] ) {
			return;
		}

		if ( rgar( $form, 'requireLogin' ) ) {
			if ( ! is_user_logged_in() ) {
				return;
			}
			check_admin_referer( 'gform_submit_' . $form_id, '_gform_submit_nonce_' . $form_id );
		}

		$lead = array();

		$field_values = RGForms::post( 'gform_field_values' );

		$confirmation_message = '';

		$source_page_number = GFFormDisplay::get_source_page( $form_id );
		$page_number        = $source_page_number;
		$target_page        = GFFormDisplay::get_target_page( $form, $page_number, $field_values );

		GFCommon::log_debug( "GFFormDisplay::process_form(): Source page number: {$source_page_number}. Target page number: {$target_page}." );

		//Loading files that have been uploaded to temp folder
		$files = GFCommon::json_decode( stripslashes( RGForms::post( 'gform_uploaded_files' ) ) );
		if ( ! is_array( $files ) ) {
			$files = array();
		}

		RGFormsModel::$uploaded_files[ $form_id ] = $files;

		$saving_for_later = rgpost( 'gform_save' ) ? true : false;

		$is_valid = true;

		$failed_validation_page = $page_number;

		//don't validate when going to previous page or saving for later
		if ( ! $saving_for_later && ( empty( $target_page ) || $target_page >= $page_number ) ) {
			$is_valid = GFFormDisplay::validate( $form, $field_values, $page_number, $failed_validation_page );
		}

		$log_is_valid = $is_valid ? 'Yes' : 'No';
		GFCommon::log_debug( "GFFormDisplay::process_form(): After validation. Is submission valid? {$log_is_valid}." );

		// Upload files to temp folder when going to the next page or when submitting the form and it failed validation
		if ( $target_page > $page_number || $target_page == 0 ) {
			if ( ! empty( $_FILES ) && ! $saving_for_later ) {
				// When saving, ignore files with single file upload fields as they have not been validated.
				GFCommon::log_debug( 'GFFormDisplay::process_form(): Uploading files...' );
				// Uploading files to temporary folder.
				//print_r($files);die;
				$files = self::upload_files( $form, $files );

				RGFormsModel::$uploaded_files[ $form_id ] = $files;
			}
		}

		// Load target page if it did not fail validation or if going to the previous page
		if ( ! $saving_for_later && $is_valid ) {
			$page_number = $target_page;
		} else {
			$page_number = $failed_validation_page;
		}
 
         global $redux_builder_amp;
         if(isset($redux_builder_amp['amp-gf-Recaptcha']) && $redux_builder_amp['amp-gf-Recaptcha'] == 1){
         $post_response = $_POST['g-recaptcha-response'];
         $gglcptch_remote_addr = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP );
         $secretkey = $redux_builder_amp['amp-gf-Recaptcha-secerete']; 
		    $args = array(
		          'body' => array(
		            'secret'   => $secretkey,
		            'response' => stripslashes( esc_html( $post_response ) ),
		            'remoteip' => $gglcptch_remote_addr,
		          ),
		          'sslverify' => false
		        );
         $resp = wp_remote_post( 'https://www.google.com/recaptcha/api/siteverify', $args );


         $response =  json_decode( wp_remote_retrieve_body( $resp ), true );
         
        $expected_Response_Msg = array(
          'missing-input-secret' => 'The secret parameter is missing.',
          'invalid-input-secret'=> 'The secret parameter is invalid or malformed.',
          'missing-input-response'=> 'The response parameter is missing.',
          'invalid-input-response'=> 'The response parameter is invalid or malformed.',
          'bad-request'=> 'The request is invalid or malformed.',
          'timeout-or-duplicate'=> 'The response is no longer valid: either is too old or has been used previously.',

            );

			    $result = array(
			              'response' => false,
			              'reason' => 'My changes'
			          );

			    if ( isset( $response['success'] ) ) {
			      if ($response['score'] > 0.5 ) {
			                    $result = array(
			                        'response' => true,
			                        'reason' => 'RECAPTCHA_SUCCESS'
			                    );
			       }
			    }else {
			        $result = array(
			              'response' => false,
			              'reason' => $response['error-codes']
			          );
			       }

			       if( $result['response'] == false){

				  //return new WP_Error('RECAPCTHA_INVALID', 'invalid recaptcha value',403 );
				  header("access-control-allow-credentials:true");
				  header("access-control-allow-headers:Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token");
				  header("Access-Control-Allow-Origin:".$_SERVER['HTTP_ORIGIN']);
				  $siteUrl = parse_url(  get_site_url() );
				  header("AMP-Access-Control-Allow-Source-Origin:".$siteUrl['scheme'] . '://' . $siteUrl['host']);
				  header("access-control-expose-headers:AMP-Access-Control-Allow-Source-Origin");
				  header("Content-Type:application/json;charset=utf-8");
				  $comment_status = array('response' => $expected_Response_Msg[$response['error-codes']] );
				  echo json_encode($comment_status);
				 $sapi_type = php_sapi_name();
				   if (substr($sapi_type, 0, 3) == 'cgi')
				    header("Status: 404 Not Found");
				  else
				    header("HTTP/1.1 404 Not Found");
				   die;
                }
       }

		$confirmation = '';
		if ( ( $is_valid && $page_number == 0 ) || $saving_for_later ) {

			$ajax = isset( $_POST['gform_ajax'] );

			//adds honeypot field if configured
			/*if ( rgar( $form, 'enableHoneypot' ) ) {
				$form['fields'][] = '';//GFFormDisplay::get_honeypot_field( $form );
			}*/

			$failed_honeypot = false;//rgar( $form, 'enableHoneypot' ) && ! GFFormDisplay::validate_honeypot( $form );

			if ( $failed_honeypot ) {

				GFCommon::log_debug( 'GFFormDisplay::process_form(): Failed Honeypot validation. Displaying confirmation and aborting.' );

				//display confirmation but doesn't process the form when honeypot fails
				$confirmation = GFFormDisplay::handle_confirmation( $form, $lead, $ajax );
				$is_valid     = false;
			} elseif ( ! $saving_for_later ) {

				GFCommon::log_debug( 'GFFormDisplay::process_form(): Submission is valid. Moving forward.' );

				$form = GFFormDisplay::update_confirmation( $form );

				//pre submission action
                /**
                 * Fires before form submission is handled
                 *
                 * Typically used to modify values before the submission is processed.
                 *
                 * @param array $form The Form object
                 */
				gf_do_action( array( 'gform_pre_submission', $form['id'] ), $form );

				//pre submission filter
				$form = gf_apply_filters( array( 'gform_pre_submission_filter', $form_id ), $form );

				//handle submission
				$confirmation = GFFormDisplay::handle_submission( $form, $lead, $ajax );

				//after submission hook
				if ( has_filter( 'gform_after_submission' ) || has_filter( "gform_after_submission_{$form['id']}" ) ) {
					GFCommon::log_debug( __METHOD__ . '(): Executing functions hooked to gform_after_submission.' );
				}
                /**
                 * Fires after successful form submission
                 *
                 * Used to perform additional actions after submission
                 *
                 * @param array $lead The Entry object
                 * @param array $form The Form object
                 */
				gf_do_action( array( 'gform_after_submission', $form['id'] ), $lead, $form );

			} elseif ( $saving_for_later ) {
				GFCommon::log_debug( 'GFFormDisplay::process_form(): Saving for later.' );
				$lead = GFFormsModel::get_current_lead();
				$form = GFFormDisplay::update_confirmation( $form, $lead, 'form_saved' );

				$confirmation = rgar( $form['confirmation'], 'message' );
				$nl2br        = rgar( $form['confirmation'], 'disableAutoformat' ) ? false : true;
				$confirmation = GFCommon::replace_variables( $confirmation, $form, $lead, false, true, $nl2br );

				$form_unique_id = GFFormsModel::get_form_unique_id( $form_id );
				$ip             = GFFormsModel::get_ip();
				$source_url     = GFFormsModel::get_current_page_url();
				$source_url     = esc_url_raw( $source_url );
				$resume_token   = rgpost( 'gform_resume_token' );
				$resume_token   = sanitize_key( $resume_token );
				$resume_token   = GFFormsModel::save_incomplete_submission( $form, $lead, $field_values, $page_number, $files, $form_unique_id, $ip, $source_url, $resume_token );

				$notifications_to_send = GFCommon::get_notifications_to_send( 'form_saved', $form, $lead );

				$log_notification_event = empty( $notifications_to_send ) ? 'No notifications to process' : 'Processing notifications';
				GFCommon::log_debug( "GFFormDisplay::process_form(): {$log_notification_event} for form_saved event." );

				foreach ( $notifications_to_send as $notification ) {
					if ( isset( $notification['isActive'] ) && ! $notification['isActive'] ) {
						GFCommon::log_debug( "GFFormDisplay::process_form(): Notification is inactive, not processing notification (#{$notification['id']} - {$notification['name']})." );
						continue;
					}
					$notification['message'] = GFFormDisplay::replace_save_variables( $notification['message'], $form, $resume_token );
					GFCommon::send_notification( $notification, $form, $lead );
				}
				GFFormDisplay::set_submission_if_null( $form_id, 'saved_for_later', true );
				GFFormDisplay::set_submission_if_null( $form_id, 'resume_token', $resume_token );
				GFCommon::log_debug( 'GFFormDisplay::process_form(): Saved incomplete submission.' );

			}

			if ( is_array( $confirmation ) && isset( $confirmation['redirect'] ) ){
				//header( "Location: {$confirmation["redirect"]}" );
                /**
                 * Fires after submission, if the confirmation page includes a redirect
                 *
                 * Used to perform additional actions after submission
                 *
                 * @param array $lead The Entry object
                 * @param array $form The Form object
                 */
				gf_do_action( array( 'gform_post_submission', $form['id'] ), $lead, $form );
				//exit;
			}
		}



		if ( ! isset( GFFormDisplay::$submission[ $form_id ] ) ) {
			GFFormDisplay::$submission[ $form_id ] = array();
		}

		GFFormDisplay::set_submission_if_null( $form_id, 'is_valid', $is_valid );
		GFFormDisplay::set_submission_if_null( $form_id, 'form', $form );
		GFFormDisplay::set_submission_if_null( $form_id, 'lead', $lead );
		GFFormDisplay::set_submission_if_null( $form_id, 'confirmation_message', $confirmation );
		GFFormDisplay::set_submission_if_null( $form_id, 'page_number', $page_number );
		GFFormDisplay::set_submission_if_null( $form_id, 'source_page_number', $source_page_number );

		/**
		 * Fires after the form processing is completed. Form processing happens when submitting a page on a multi-page form (i.e. going to the "Next" or "Previous" page), or
		 * when submitting a single page form.
		 *
		 * @param array $form               The Form Object
		 * @param int   $page_number        In a multi-page form, this variable contains the current page number.
		 * @param int   $source_page_number In a multi-page form, this parameters contains the number of the page that the submission came from.
		 *                                  For example, when clicking "Next" on page 1, this parameter will be set to 1. When clicking "Previous" on page 2, this parameter will be set to 2.
		 */
		gf_do_action( array( 'gform_post_process', $form['id'] ), $form, $page_number, $source_page_number );

	
}

public static function upload_files( $form, $files ) {

		$form_upload_path = GFFormsModel::get_upload_path( $form['id'] );
		GFCommon::log_debug( "GFFormDisplay::upload_files(): Upload path {$form_upload_path}" );

		//Creating temp folder if it does not exist
		$target_path = $form_upload_path . '/tmp/';
		wp_mkdir_p( $target_path );
		GFCommon::recursive_add_index_file( $form_upload_path );

		foreach ( $form['fields'] as $field ) {
			$input_name = "input_{$field->id}";

			//skip fields that are not file upload fields or that don't have a file to be uploaded or that have failed validation
			$input_type = RGFormsModel::get_input_type( $field );
			if ( ! in_array( $input_type, array( 'fileupload', 'post_image' ) ) || $field->multipleFiles ) {
				continue;
			}

			/*if ( $field->failed_validation || empty( $_FILES[ $input_name ]['name'] ) ) {
				GFCommon::log_debug( "GFFormDisplay::upload_files(): Skipping field: {$field->label}({$field->id} - {$field->type})." );
				continue;
			}*/

			if ( $field->failed_validation ) {
				GFCommon::log_debug( "GFFormDisplay::upload_files(): Skipping field because it failed validation: {$field->label}({$field->id} - {$field->type})." );
				continue;
			}

			if ( empty( $_FILES[ $input_name ]['name'] ) ) {
				GFCommon::log_debug( "GFFormDisplay::upload_files(): Skipping field because a file could not be found: {$field->label}({$field->id} - {$field->type})." );
				continue;
			}

			$file_name = $_FILES[ $input_name ]['name'];
			if ( GFCommon::file_name_has_disallowed_extension( $file_name ) ) {
				GFCommon::log_debug( __METHOD__ . "(): Illegal file extension: {$file_name}" );
				continue;
			}

			$allowed_extensions = ! empty( $field->allowedExtensions ) ? GFCommon::clean_extensions( explode( ',', strtolower( $field->allowedExtensions ) ) ) : array();

			if ( ! empty( $allowed_extensions ) ) {
				if ( ! GFCommon::match_file_extension( $file_name, $allowed_extensions ) ) {
					GFCommon::log_debug( __METHOD__ . "(): The uploaded file type is not allowed: {$file_name}" );
					continue;
				}
			}

			/**
			 * Allows the disabling of file upload whitelisting
			 *
			 * @param bool false Set to 'true' to disable whitelisting.  Defaults to 'false'.
			 */
			$whitelisting_disabled = apply_filters( 'gform_file_upload_whitelisting_disabled', false );

			if ( empty( $allowed_extensions ) && ! $whitelisting_disabled ) {
				// Whitelist the file type

				$valid_file_name = GFCommon::check_type_and_ext( $_FILES[ $input_name ], $file_name );

				if ( is_wp_error( $valid_file_name ) ) {
					GFCommon::log_debug( __METHOD__ . "(): The uploaded file type is not allowed: {$file_name}" );
					continue;
				}
			}

			$file_info = RGFormsModel::get_temp_filename( $form['id'], $input_name );
			GFCommon::log_debug( 'GFFormDisplay::upload_files(): Temp file info: ' . print_r( $file_info, true ) );

			if ( $file_info && move_uploaded_file( $_FILES[ $input_name ]['tmp_name'], $target_path . $file_info['temp_filename'] ) ) {
				GFFormsModel::set_permissions( $target_path . $file_info['temp_filename'] );
				$files[ $input_name ] = $file_info['uploaded_filename'];
				GFCommon::log_debug( "GFFormDisplay::upload_files(): File uploaded successfully: {$file_info['uploaded_filename']}" );
			} else {
				GFCommon::log_error( "GFFormDisplay::upload_files(): File could not be uploaded: tmp_name: {$_FILES[ $input_name ]['tmp_name']} - target location: " . $target_path . $file_info['temp_filename'] );
			}
		}
		return $files;
	}

}
$amp_gravity_form_instantiate = new AMP_Gravity_Form_Submit( $_GET, $_POST );
$amp_gravity_form_instantiate->activate();