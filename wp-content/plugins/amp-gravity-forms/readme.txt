=== AMP Gravity Forms ===
Contributors:  Mohammed Kaludi, Ahmed Kaludi
Tags: gravity forms, amp, forms, contact, plugin
Requires at least: 3.0
Tested up to: 5.2.2
Stable tag: 2.9
License: GPLv2

Gravity Forms support for AMP.

== Description ==
You can now enable Gravity Forms plugin support in AMP with just one click! This will work with the form builder and all kinds of fields with proper form validation.


== Changelog ==

= 2.9 (04 Oct 2019) =
== Feature Added ==
* Recaptcha support (V3) Added.
== Bugs Fixed ==
 * Conditional Logic not working in Pages/Posts resolved.
 * Caption error when Gallery used with Gutenberg Editor issue resolved.

= 2.8 (28 Sep 2019) =
== Bugs Fixed ==
 * Multi-Select Field not working, issue resolved.
 * Conditional Forms Pagination not working in Footer resolved.
 * Gallery not displaying while inserting through Gutenberg resolved.

= 2.7 (13 Sep 2019) =
== Bug Fixed ==
 * Form not submitting, gives one or more fields required issue resolved.

= 2.6 (30 Aug 2019) =

 * Minor bugs Fixed.

= 2.5 (30 Aug 2019) =

= Bugs Fixed: =
* Resolved Fatal error: Uncaught ArgumentCountError: Too few arguments to function
  ampforwp_gv_label().
* Resolved Gravity form CSS loading issue even if it's not used.

= 2.4 (12 Jul 2019) =

= Feature added: =
* Added support of input type="file".
* Section Field compatibility added.

= Bugs Fixed: =
* Resolved the issue of gravity form not submitting with amp by automatic plugin.
* Resolved the issue of form not redirecting to other urls.
* Resolved Wp-debug warning undefined variable:conditionClass.
* Custom Classes will now render in AMP and Scrolling issue resolved.

= 2.3 (24 May 2019) =
= Feature added: =
* Compatibility with GP Post Content Merge Tags improved(Redirect feature added).

= 2.2 (17 May 2019) =
= Features added: =
* Wordpress Zero spam plugin compatibility added for gravity forms.
* Added description for email field.

= 2.1 (10 May 2019) =
= Bugs Fixed : =
*  Properly improved submit button match of "all" and "any" conditional logic.
*  Notice errors have been resolved.

= 2.0 (26 Apr 2019) =
= Feature added: =
* Product field support added.

= 1.9 (19 Apr 2019) =
= Feature added: =
* Gravity Form widget will work in AMP.

= Bugs Fixed: = 
* Validation errors fixed.
* AMP_DIR warning issue resolved.
* Fatal error resolved when only Gravity forms for AMP is active.

= 1.8 (11 Aug 2018) =
* Dynamic populated via shortcode feature added.
* Some Debug & Warning issue resolved.

= 1.7 (31 July 2018) =
* Latest version of amp-mustache extension .js added.

= 1.6 (26 May 2018) =
*  select field issue fixed.
*  section field with pagination issue fixed.

= 1.5 (20 April 2018) =
*  Upgrade Constants

1.4 (19 April 2018) =
*  Change some 'Update Proces' Code for better enhancement.

= 1.3 (10 April 2018) =
* Date picker feature Compatibility Added.
* WP AMP by teamdev Compatibility Added.
* Website form field Added.
* required field added in the form fields.
* Section field added.
* Place holder for Select field added.
* Conditional logic for Submit Button added.
* placeholder added for type=text & type=tel.
* description function created.
* Better Update Process Integrated
