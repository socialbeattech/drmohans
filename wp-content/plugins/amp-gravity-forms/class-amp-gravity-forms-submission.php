<?php

class AMP_Form_Submit {
	protected $posted = array();
	protected $request = array();
	protected $response = array();
	protected $AllowOrigin = '';
	protected $amp_gravity_Site = '';
	protected $amp_gravity_Source = '';
	protected $siteHost = '';
	protected $allowedSites = array(
		'*.ampproject.org',
		'*.amp.cloudflare.com'
	);

	protected $cors_Headers = array(
		'Content-Type'	=> 'application/json',
		'Access-Control-Allow-Origin'  => '',
		'AMP-Access-Control-Allow-Source-Origin'=> '',
		'Access-Control-Expose-Headers' => 'AMP-Access-Control-Allow-Source-Origin',

	);

	protected $more_Headers = array(
		'Access-Control-Allow-Credentials' => 'true',
	); 

	public function __construct( $request, $data ) {
		$this->posted   = $data;
		$this->request  = $request;
	  $forms = GFFormsModel::get_form_meta_by_id( $data['gform_submit'] );
      $form = ampforwp_gravity_prepare_forms_for_export( $forms ) ;
      $form = $form[0];
      //print_r($form['confirmations'][0]['type']); die;
      $this->redirect_type = $form['confirmations'][0]['type'];
      if($this->redirect_type == 'redirect'){ 
      $this->redirect_url = $form['confirmations'][0]['url'];
      }
	  $this->response = array(
			'status' => 'ok', 'message' => '', 'link' => '', 'debug' => ''
		);
	}

	public function activate() {
		$this->checkMethodType()
		     ->preProcess()
		     //->CheckWordPressEnviorment()
		     ->validation()
		     ->setHooks()
		     ->run()
		     ->outputResponse()
		     ->pushResponse();
	}
 
	protected function checkMethodType() {
		return $this;
	}
 
	protected function validation() {
		foreach ( $_SERVER as $key => $value ) {
			switch ( true ) {
				case ( 'http_origin' == strtolower( $key ) ):
					$this->origin = $value;
					break;
				case ( 'http_amp_same_origin' == strtolower( $key ) ):
					$ampSameOrigin = $value;
					break;
			}
		}
		$this->amp_gravity_Source = empty( $this->request['__amp_source_origin'] ) ? '' : $this->request['__amp_source_origin'];
		$siteUrl = parse_url(
			get_site_url()
		);
		$this->siteHost = $siteUrl['host'];
 		$this->amp_gravity_Site = $siteUrl['scheme'] . '://' . $siteUrl['host'];
		array_unshift(
			$this->allowedSites,
			$this->amp_gravity_Site
		); 
		if ( empty( $this->origin ) ) {
			if ( 'true' !== $ampSameOrigin ) { 
				$this->pushResponse( 'HTTP/1.1 403 FORBIDDEN' );
			}
		} else { 
			if ( ! $this->AllowOrigin( $this->origin ) ) {
				$this->pushResponse( 'HTTP/1.1 403 FORBIDDEN' );
			} 
			if (
				(empty( $this->amp_gravity_Source )
				||
				$this->amp_gravity_Site != $this->amp_gravity_Source) && 
				!function_exists('amp_activate')
			) {
				$this->pushResponse( 'HTTP/1.1 403 FORBIDDEN ' );
			}  
		}
		return $this;
	}

	protected function AllowOrigin( $AllowOrigin ) {
		return true;
	}

	protected function preProcess() {
		return $this;
	}

	protected function CheckWordPressEnviorment() {

		$rootPath = dirname( dirname( dirname( dirname( __FILE__ ) ) ) );
		require( $rootPath . '/wp-load.php' );
		return $this;
	}

	protected function setHooks() {
		return $this;
	}

	protected function run() {
		return $this;
	}

	protected function outputResponse() {
		nocache_headers();
		$headers = $this->getHeaders();
		foreach ( $headers as $name => $value ) {
			@header( $name . ': ' . $value );
		}
		echo json_encode( $this->response );
		return $this;
	}

	protected function getHeaders() {
		$headers = $this->cors_Headers;
		$headers['Access-Control-Allow-Origin']  = $this->origin;
		$headers['AMP-Access-Control-Allow-Source-Origin'] = $this->amp_gravity_Site;
	  if(!empty($this->redirect_url) && $this->redirect_url != 'page' && $this->redirect_url != 'message'){
       $headers['AMP-Redirect-To'] = $this->redirect_url;
       $headers['Access-Control-Expose-Headers'] = 'AMP-Redirect-To';
      }
		$headers = array_merge(
			$headers,
			$this->more_Headers
		);
		return $headers;
	}

	protected function pushResponse( $header = '' ) {
		if ( ! empty( $header ) ) {
			header( $header );
		} exit;
	}
}