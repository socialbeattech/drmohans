<?php
/*
Plugin Name: AMP Gravity Forms
Plugin URI: https://ampforwp.com/gravity-forms
Description: Gravity Forms support for AMP.
Version: 2.9
Author:  Mohammed Kaludi, Ahmed Kaludi
Author URI: https://ampforwp.com/extensions
License: GPL2
*/
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'AMP_GRAVITY_FORMS_VERSION' ) ) {
    define( 'AMP_GRAVITY_FORMS_VERSION', '2.9' );
}
// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'AMP_GRAVITY_FORMS_STORE_URL', 'https://accounts.ampforwp.com/' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of your product. This should match the download name in EDD exactly
define( 'AMP_GRAVITY_FORMS_ITEM_NAME', 'Gravity Forms' );

// the download ID. This is the ID of your product in EDD and should match the download ID visible in your Downloads list (see example below)
//define( 'AMPFORWP_ITEM_ID', 2502 );
// the name of the settings page for the license input to be displayed
define( 'AMP_GRAVITY_FORMS_LICENSE_PAGE', 'gravity-forms' );
if(! defined('AMP_GF_ITEM_FOLDER_NAME')){
    $folderName = basename(__DIR__);
    define( 'AMP_GF_ITEM_FOLDER_NAME', $folderName );
}

define('AMP_GRAVITY_PLUGIN_URI', plugin_dir_url(__FILE__));
define('AMP_GRAVITY_PLUGIN_PATH', plugin_dir_path(__FILE__));

add_action('init', 'amp_gravity_forms_plugin_init');
function amp_gravity_forms_plugin_init() {
	if(!defined('AMPFORWP_AMP__DIR__')){
    if ( defined('AMP__DIR__') ){
        define( 'AMPFORWP_AMP__DIR__', AMP__DIR__);
    }elseif( defined('AMP__VENDOR__DIR__')){
        define( 'AMPFORWP_AMP__DIR__', AMP__VENDOR__DIR__);
    }
}

    require_once AMP_GRAVITY_PLUGIN_PATH .'/class-amp-gravity-forms-blacklist.php';
    require_once AMP_GRAVITY_PLUGIN_PATH .'/class-amp-gravity-forms-constants.php';
  // require_once AMP_GRAVITY_PLUGIN_PATH .'/class-ampforwp-tag-and-attribute-sanitizer.php';

}

add_action('plugins_loaded','amp_gf_initiate_plugin');
function amp_gf_initiate_plugin(){


	if ( class_exists('RGForms')) {
	
		add_action('wp', 'amp_gf_start_generating_code', 100);
		
	}
}
add_action( 'gform_pre_handle_confirmation', 'call_tracking_plugin_js_removal');
function call_tracking_plugin_js_removal($lead){
if(class_exists('CallTrackingMetrics')){
	if (strpos($lead['source_url'], '__amp')) {
		ampforwp_remove_filters_for_class('gform_confirmation', 'CallTrackingMetrics', 'gf_confirmation', 10,1);
	}
}
}

function gravity_form_admin_notice() {
   if ( ! class_exists('RGForms')) { ?>
	    <div class="notice notice-warning is-dismissible">
	        <p><?php _e( 'Please activate the parent plugin of Gravity forms for AMP', 'accelerated-mobile-pages-gravity-forms-support' ); ?></p>
	    </div>
	    <?php
	}
}

add_action( 'admin_notices', 'gravity_form_admin_notice' );

add_filter('amp_content_sanitizers','ampforwp_gravity_blacklist_sanitizer', 99);
function ampforwp_gravity_blacklist_sanitizer($data){
	global $redux_builder_amp;
	$post_id = '';
	$gravity_form_checker  = '';
	$post_id = get_the_ID();

	if( is_home() && $redux_builder_amp['amp-frontpage-select-option'] ) {
		$post_id = $redux_builder_amp['amp-frontpage-select-option-pages'] ;
	}
	$gravity_form_checker = get_post_meta($post_id, 'amp-gravity-form-checker', true); 

	// This will make sure the sanitizer will only run 
	// When post has CF7 form
	if ( ! empty( $gravity_form_checker )  || ( class_exists('\Elementor\Plugin') && \Elementor\Plugin::instance()->db->is_built_with_elementor( $post_id ) ) ) {
		 
 		unset($data['AMP_Tag_And_Attribute_Sanitizer']); 
		unset($data['AMP_Blacklist_Sanitizer']);
		unset($data['AMPFORWP_Blacklist_Sanitizer']);
		$data[ 'AMP_GRAVITY_Blacklist' ] = array();
		update_post_meta($post_id, 'amp-gravity-form-checker', 1);

	}
	
	return $data;
}
add_filter('the_content','amp_check_gravity_shortcode',99);
function amp_check_gravity_shortcode($content){
	global $redux_builder_amp;
	$post_id = '';
	$gravity_form_checker  = '';

	$post_id = get_the_ID();

	if( is_home() && $redux_builder_amp['amp-frontpage-select-option'] ) {
		$post_id = $redux_builder_amp['amp-frontpage-select-option-pages'] ;
	}
	$gravity_form_checker = get_post_meta($post_id, 'amp-gravity-form-checker', true); 
	 

	if ( has_shortcode($content, 'gravityforms' ) && has_shortcode($content, 'gravityform' ) && empty($gravity_form_checker) ) {
		update_post_meta($post_id, 'amp-gravity-form-checker', 1);
	}
	return $content;
}

function amp_gf_start_generating_code() {

	 if ( function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint() || function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() || function_exists('is_wp_amp') && 	is_wp_amp()) {

		global $gravitydata;
		global $shortcode_tags;
		
		// store current handler
		$gravitydata = amp_get_array( $shortcode_tags, 'gravityform' );
		// replace original handler with our
		add_shortcode('gravityform','amp_generate_amp_gravity_form');
		add_shortcode('gravityforms', 'amp_generate_amp_gravity_form' );
	 }
}
$defaultValues = array();
$ampforwp_AMPState_check = array();

function amp_generate_amp_gravity_form($atts, $content = null, $code = ''){
	// print_r($atts);
	// die;
	global $gravitydata, $defaultValues, $ampforwp_AMPState_check, $sec_check ;
	$post_id 	= '';
	$post_id 	= (int) $atts['id'];
	$handler    = $gravitydata;
	//$submit_url =  AMP_GRAVITY_PLUGIN_URI . "class-amp-gravity-forms.php?action=submit";
	$submit_url =  admin_url('admin-ajax.php?action=gravity_form_submission');
	$actionXhrUrl = preg_replace('#^https?:#', '', $submit_url);

	// for dynamic populated value 
	$explode_value = array();
	$explode_temp = '';
	if(isset($atts['field_values'])){
		$explode = explode('&', $atts['field_values']);
		foreach ($explode as $key => $value) {			 
			$explode_temp = explode('=', $value);
			$explode_value[$explode_temp[0]] = $explode_temp[1];
		}	 
	}
	// for dynamic populated value code end here
	/*
	$gravityContent = $handler( $atts, $content, $code );
	$submit_url =  AMP_GRAVITY_PLUGIN_URI . "class-amp-gravity-forms.php?action=submit";
	$actionXhrUrl = preg_replace('#^https?:#', '', $submit_url);

	// insert updated URL
	$gravityContent = preg_replace( '/target=\'(.*?)\'/', '', $gravityContent ); 
	$gravityContent = preg_replace( '/action=\'(.*)\'>/', 'action-xhr="' . $actionXhrUrl . '" target="_blank">', $gravityContent ); 
	
	// Generate Submit Button
	$submit_button =  amp_after_submit_notice();
	$gravityContent = preg_replace( '#(</form>)#iu', $submit_button . '$1', $gravityContent );
	//$gravityContent = preg_replace( '#(<input\s+type=\'submit\')#iu', $submit_button . '$1', $gravityContent );
	$gravityContent = preg_replace( '/<script (.*)<\/script>/si', '', $gravityContent );
	$gravityContent = preg_replace( '/<iframe (.*)<\/iframe>/si', '', $gravityContent );
	$gravityContent = preg_replace( '/<iframe (.*)<\/iframe>/si', '', $gravityContent );
	//$gravityContent .= add_gravity_json();
	$content = $gravityContent;
 	$content .= "<br/>---------------------------------------------------<br/>";
 	*/
 	$forms = GFFormsModel::get_form_meta_by_id( $post_id );
	$form = ampforwp_gravity_prepare_forms_for_export( $forms ) ;
	$form = $form[0]; 
	$form['confirmations'][0]['message'] = GFCommon::replace_variables( $form['confirmations'][0]['message'], $form, array(), false, true,true );
   
   //print_r($form);die;
   //$html = '';
	$html = '<input type="hidden" name="action" value="gravity_form_submission">';
	$sec_check = '';

	// set default value for dynamic populated.
	if(!empty($form['fields'])){
	foreach ($form['fields'] as $key => $field) {
		 if(isset($explode_value[$field['inputName']])){
		 	$form['fields'][$key]['defaultValue'] = $explode_value[$field['inputName']];
		 }
	}
}
			global $redux_builder_amp;
	        $sitetkey = $redux_builder_amp['amp-gf-Recaptcha-site'];
	// set default value for dynamic populated code end here
	if(isset($form['pagination']['pages']) && count($form['pagination']['pages'])>0){
		$currentPageNo = 1;
		$defaultValues['showpage'] = 1;

		$html .= '<div id="gform_page_'.$form['id'].'_'.$currentPageNo.'" class="gform_page show" :openbrack:class:closebrack:="ampforwp_gravity_data_'.$form['id'].'.showpage==1? \'gform_page show\': \'gform_page hide\'">';
	}else{
		$html .= '<div>';
	}
			
	$html .= '<ul id="gform_fields_'.$form['id'].'" class="gform_fields top_label form_sublabel_below description_below">';

	if ( rgar( $form, 'enableHoneypot' ) == 1 ) {	
				$form['fields'][] =  get_honeypot_ampforwp_gf_field( $form );
	}
  if (is_array($form['fields'])){
	foreach ($form['fields'] as $key => $fields) {

		$conditionClass = '';
		$defaultShowField = $defaultLiClass = 'gfield field_sublabel_below field_description_below gfield_visibility_visible ';

		if($fields['choices']!="" && count($fields['choices'])>0) {
			foreach($fields['choices'] as $choicekey => $choice) {
				if(!isset($defaultValues['fieldId_'.$fields['id']])){
					
					$defaultValues['fieldId_'.$fields['id']] = ($choice['isSelected']? $choice['value']:'');
				}
			}
        }		
		if(isset($fields['conditionalLogic']) && is_array($fields['conditionalLogic'])){

			if($fields['conditionalLogic']['actionType']=='show'){

				$conditionExpression = '';
				$conditions = '';
				$defaultShow =  array();
				foreach ($fields['conditionalLogic']['rules'] as $condition) {

					$condition['fieldId'] =  str_replace(".", "_", $condition['fieldId']);
					//Get the operator
					$operator = str_replace(array('isnot','is','contains'), array("!=",'==','indexOf'), $condition['operator']);					

					if($conditionExpression!=""){
						$conditionExpression .= ' && ';
					}
					$conditionExpression = 'ampforwp_gravity_data_'.$fields['formId'].'.fieldId_'.$condition['fieldId'].$operator.'\''.$condition['value'].'\'';

					if( isset($defaultValues['fieldId_'.$condition['fieldId']]) ){
						switch(trim($operator)){
							case '!=':
								if( $defaultValues['fieldId_'.$condition['fieldId']] != $condition['value'] ){
									$defaultShow[] = "show";
								}else{
									$defaultShow[] = "hide";
								}
							break;
							case '<':
								if( $defaultValues['fieldId_'.$condition['fieldId']] < $condition['value'] ){
									$defaultShow[] = "show";
								}else{
									$defaultShow[] = "hide";
								}
							break;
							case ">":
								if( $defaultValues['fieldId_'.$condition['fieldId']] > $condition['value'] ){
									$defaultShow[] = "show";
								}else{
									$defaultShow[] = "hide";
								}
							break;
							case 'indexOf':
								if( strpos($defaultValues['fieldId_'.$condition['fieldId']], $condition['value']) !== false ){
									$defaultShow[] = "show";
								}else{
									$defaultShow[] = "hide";
								}
							break;
							case '==':
							default: 									
								if( $defaultValues['fieldId_'.$condition['fieldId']] == $condition['value'] ){
									$defaultShow[] = "show";

								}else{
									$defaultShow[] = "hide";
								}
							
								
							break;
						}
					}
					if($conditions != ''){
						//$conditions .= ' && ';

					if($form['button']['conditionalLogic']['logicType'] == 'all'){
						$conditions .= '&&';
					}else{
							$conditions .= '||';
					  }
					}
					if($operator == 'indexOf'){
						$conditions .= 'ampforwp_gravity_data_'.$fields['formId'].'.fieldId_'.$condition['fieldId'].".".$operator.'(\''.$condition['value'].'\')!=-1';
					}else{
						$conditions .= 'ampforwp_gravity_data_'.$fields['formId'].'.fieldId_'.$condition['fieldId'].$operator.'\''.$condition['value'].'\'';
					}

				}
				$defaultShow = array_values(array_unique($defaultShow));
				if(count($defaultShow)==1 && $defaultShow[0]=='show'){
					$defaultShowField .= 'show';
				}else{
					$defaultShowField .= 'hide';
				}
				$conditionClass = ':openbrack:class:closebrack:="'.$conditions.' ? \''. $defaultLiClass .' show\': \''. $defaultLiClass .' hide\' " ';
			}
			elseif($fields['conditionalLogic']['actionType']=='hide'){
		
				$conditionExpression = '';
				$conditions = '';
				$defaultShow =  '';
				foreach ($fields['conditionalLogic']['rules'] as $condition) {

					$condition['fieldId'] =  str_replace(".", "_", $condition['fieldId']);
					//Get the operator
					$operator = str_replace(array('isnot','is','contains'), array("!=",'==','indexOf'), $condition['operator']);
					

					if($conditionExpression!=""){
						$conditionExpression .= ' && ';
					}
					$conditionExpression = 'ampforwp_gravity_data_'.$fields['formId'].'.fieldId_'.$condition['fieldId'].$operator.'\''.$condition['value'].'\'';
					

					if( isset($defaultValues['fieldId_'.$condition['fieldId']]) ){
						switch($operator){
							case '!=':
								if( $defaultValues['fieldId_'.$condition['fieldId']] != $condition['value'] ){
									$defaultShow[] = "hide";
								}else{
									$defaultShow[] = "show";
								}
							break;
							case ' <':
								if( $defaultValues['fieldId_'.$condition['fieldId']] < $condition['value'] ){
									$defaultShow[] = "hide";
								}else{
									$defaultShow[] = "show";
								}
							break;
							case " >":
								if( $defaultValues['fieldId_'.$condition['fieldId']] > $condition['value'] ){
									$defaultShow[] = "hide";
								}else{
									$defaultShow[] = "show";
								}
							break;
							case 'indexOf':
								if( strpos($defaultValues['fieldId_'.$condition['fieldId']], $condition['value']) !== false ){
									$defaultShow[] = "hide";
								}else{
									$defaultShow[] = "show";
								}
							break;
							case '==':
							default:
								if( $defaultValues['fieldId_'.$condition['fieldId']] == $condition['value'] ){
									$defaultShow[] = "hide";

								}else{
									$defaultShow[] = "show";
								}
							
								
							break;
						}
					}
					if($conditions != ''){
						//$conditions .= ' || ';
						if($form['button']['conditionalLogic']['logicType'] == 'all'){
						$conditions .= '&&';
					}else{
							$conditions .= '||';
					  }
					}
 
					if($operator == 'indexOf'){
						$conditions .= 'ampforwp_gravity_data_'.$fields['formId'].'.fieldId_'.$condition['fieldId'].".".$operator.'(\''.$condition['value'].'\')!=-1';
					}else{
						$conditions .= 'ampforwp_gravity_data_'.$fields['formId'].'.fieldId_'.$condition['fieldId'].$operator.'\''.$condition['value'].'\'';
					}
					//$conditions .= 'ampforwp_gravity_data_'.$fields['formId'].'.fieldId_'.$condition['fieldId'].$operator.'\''.$condition['value'].'\'';

				}
				$defaultShow = array_values(array_unique($defaultShow));
				if(count($defaultShow)==1 && $defaultShow[0]=='hide'){
					$defaultShowField .= 'hide';
				}else{
					$defaultShowField .= 'show';
				}

				$conditionClass = ':openbrack:class:closebrack:="'.$conditions.' ? \''. $defaultLiClass .' hide\': \''. $defaultLiClass .' show\' " ';
			}

		}
 		 
		if($fields['type']!='page'){
			 $hide_class = '';
			if( ( $fields['type'] == 'hidden' ) || ( $fields['visibility'] == 'hidden' ) || ($fields['type'] == 'honeypot' ) ) {			 
				$hide_class = 'hide';
			}
			if( $form['pagination'] == '' ) {
				if( $fields['type']=='section' && $sec_check == 'true' )
				{ 
					$html .= '</div></li>';
				}
			}
			$html .= '<li id="field_'.$key.'_'.$fields['id'].'" class="'.
			$defaultShowField.' '. $fields['cssClass'] .' '. $hide_class .'" '.($conditionClass!='' ? $conditionClass: '').'>' ;
		}
		 
		switch($fields['type']){
			case 'radio':
				$defaultValues['fieldId_'.$fields['id']] = $fields['defaultValue'];
				$html .= ampforwp_gv_label($fields['label'],$fields);
				$html .=  '<div><ul>';
					if(count($fields['choices'])>0 && $fields['choices']!=""){
						foreach ($fields['choices'] as $choicekey => $choice) {
							$html .= "<li>".ampforwp_gv_radio($choice, $fields, $choicekey)."</li>";
						}
					}
				$html .= '</ul></div>';
			break;
			case 'checkbox':
				$defaultValues['fieldId_'.$fields['id']] = $fields['defaultValue'];
				$html .= ampforwp_gv_label($fields['label'],$fields);
				$html .=  '<div><ul>';
					if(count($fields['choices'])>0 && $fields['choices']!=""){
						foreach ($fields['choices'] as $choicekey => $choice) {
							$html .= "<li>".ampforwp_gv_checkbox($choice, $fields, $choicekey)." ". ampforwp_gf_description($fields['description']) ." </li>";
						}
					}
				$html .= '</ul></div>';  
			break;
			case 'text':
			case 'number':
			case 'hidden':
			case 'honeypot':
			case 'website':
				$defaultValues['fieldId_'.$fields['id']] = $fields['defaultValue'];
				$html .= ampforwp_gv_input($fields,$key);
			break;
			case 'phone':
				$defaultValues['fieldId_'.$fields['id']] = $fields['defaultValue'];
				$html .= ampforwp_gv_tel($fields,$key);
			break;
			case 'html':
				$html .= $fields['content'];
			break;
			case 'multiselect':
				$html .= ampforwp_gv_multiselect($fields,$key);
			break;
			case 'select':
			if($fields['placeholder']  != '') {
				$defaultValues['fieldId_'.$fields['id']] = $fields['placeholder'];
			}
			elseif($fields['defaultValue'] != '') {
				$defaultValues['fieldId_'.$fields['id']] = $fields['defaultValue'];
			}
			else {
				$defaultValues['fieldId_'.$fields['id']] = $fields['choices'][0]['value'];
			}	 
				$html .= ampforwp_gv_select($fields,$key);
			break;
			case 'textarea':
				$defaultValues['fieldId_'.$fields['id']] = $fields['defaultValue'];
				$html .= ampforwp_gv_textarea($fields,$key);
			break;
			case 'page':
				$currentPageNo = $currentPageNo+1;
				$html .= ampforwp_gv_page_bottom($fields, $key, $form, $currentPageNo);
			break;

			case 'name':
				$html .= ampforwp_gv_label($fields['label'],$fields);
				//$html .= GFCommon::get_field_input( $fields, $fields['defaultValue'], 0, $fields['formId'], $form );
				$nonamp_ver = GFCommon::get_field_input( $fields, $fields['defaultValue'], 0, $fields['formId'], $form );
				  
				$ampforwp_advn_gf_fields = advanced_conditional_id($nonamp_ver, $fields, $defaultValues);

		 		$html .= $ampforwp_advn_gf_fields[0];
		 		$defaultValues = array_merge($defaultValues, $ampforwp_advn_gf_fields[1]);
		 		
				/*$fields = GF_Fields::get($fields['type']);	
				 $html .= $fields->get_field_input($fields); */	  
			break;
			case 'address':
				$html .= ampforwp_gv_label($fields['label'],$fields);
				$html .= GFCommon::get_field_input( $fields, $fields['defaultValue'], 0, $fields['formId'], $form ); 
			break;
			case 'quiz':
				$html .= ampforwp_gv_label($fields['label'],$fields);
				$html .= GFCommon::get_field_input( $fields, $fields['defaultValue'], 0, $fields['formId'], $form ); 
			break;
			case 'email':
				$html .= ampforwp_gv_label($fields['label'],$fields);
				$nonamp_ver = GFCommon::get_field_input( $fields, $fields['defaultValue'], 0, $fields['formId'], $form ); 

				$ampforwp_advn_gf_fields = advanced_conditional_id($nonamp_ver, $fields, $defaultValues);
				
				 
		 		$html .= $ampforwp_advn_gf_fields[0];
		 		$html .= ampforwp_gf_description($fields['description']);
		 		$defaultValues = array_merge($defaultValues, $ampforwp_advn_gf_fields[1]);


			break;

			case 'fileupload':
				$html .= ampforwp_gv_label($fields['label'],$fields);
				$nonamp_ver = GFCommon::get_field_input( $fields, $fields['defaultValue'], 0, $fields['formId'], $form ); 

				$ampforwp_advn_gf_fields = advanced_conditional_id($nonamp_ver, $fields, $defaultValues);
				 
		 		$html .= $ampforwp_advn_gf_fields[0];
		 		$defaultValues = array_merge($defaultValues, $ampforwp_advn_gf_fields[1]);
			break;


			case 'section':
				$html .= ampforwp_gv_label($fields['label'],$fields);
				$nonamp_ver = GFCommon::get_field_input( $fields, $fields['defaultValue'], 0, $fields['formId'], $form ); 

				$ampforwp_advn_gf_fields = advanced_conditional_id($nonamp_ver, $fields, $defaultValues);
				
				 
		 		$html .= $ampforwp_advn_gf_fields[0];
		 		$html .= ampforwp_gf_description($fields['description']);
		 		$defaultValues = array_merge($defaultValues, $ampforwp_advn_gf_fields[1]);
 
			break;

			case 'product':
				$html .= ampforwp_gv_label($fields['label'],$fields);
				$nonamp_ver = GFCommon::get_field_input( $fields, $fields['defaultValue'], 0, $fields['formId'], $form ); 

				$ampforwp_advn_gf_fields = advanced_conditional_id($nonamp_ver, $fields, $defaultValues);
				
				 
		 		$html .= $ampforwp_advn_gf_fields[0];
		 		$defaultValues = array_merge($defaultValues, $ampforwp_advn_gf_fields[1]);


			break;
			case 'date':
				$html .= ampforwp_gv_label($fields['label'],$fields);
				$ampforwp_gf_date_picker = GFCommon::get_field_input( $fields, $fields['defaultValue'], 0, $fields['formId'], $form ); 
				$ampforwp_date_gf_fields = ampforwp_gf_date_picker($ampforwp_gf_date_picker, $fields, $defaultValues);

				$html .= $ampforwp_date_gf_fields[0]; 
				$defaultValues = array_merge($defaultValues, $ampforwp_date_gf_fields[1]); 	 		 
			break;
			case 'section':
				$sec_check = 'true';
				$html .='<h2 class="gsection_title">'.$fields['label'].'</h2>'; 
				$html .=  ampforwp_gv_section_type($fields,$form );
			break;

/*			case 'captcha':
			$html .= '<amp-recaptcha-input layout="nodisplay" name="g-recaptcha-response" data-sitekey="'.$sitetkey.'" data-action="g_recaptcha_response">
            </amp-recaptcha-input>';
            break;*/
			default;
			break;

		 }
		
		if($fields['type']!='page'){
			$html .= '</li>';
		}

	}
	} 
	if( $sec_check == 'true' ) { 
		$html .= '</div>';
	}
	  if(isset($redux_builder_amp['amp-gf-Recaptcha']) && $redux_builder_amp['amp-gf-Recaptcha'] == 1){

	 	if( empty($redux_builder_amp['amp-gf-Recaptcha-site']) ||  empty($redux_builder_amp['amp-gf-Recaptcha-secerete']) ){

        $html .='<li>V3 ReCAPTCHA sitekey or secret key is not placed in amp gravity form panel.</li>';
	 	}
        $html .= '<li><amp-recaptcha-input layout="nodisplay" name="g-recaptcha-response" data-sitekey="'.$sitetkey.'" data-action="g_recaptcha_response">
          </amp-recaptcha-input></li>';
       add_filter('amp_post_template_data','amp_recaptcha_scripts', 10);
      }
	$html .= ' </ul></div>'; 

	$content = '

 		<div class="gform_wrapper" id="gform_wrapper_'.$post_id.'">
        '.ampforwp_gv_heading($form, $atts).'
    	'.ampforwp_gv_page($form,'heading').'
    	'.ampforwp_start_gvform($actionXhrUrl).'
	        <div class="gform_body">
	                '.$html.'
	            
	        </div>
        '.ampforwp_gv_submit($form).'
       '.amp_after_submit_notice($form).'

    </form>
</div>';  

	add_filter('amp_post_template_data','amp_add_required_scripts', 21);
	add_action('amp_post_template_css', 'amp_gravity_form_styling');

	add_action('amphtml_template_css', 'amp_gravity_form_styling'); // For WP AMP plugin

	add_action('amp_post_template_footer', function() use ($defaultValues,$form)  {
			global $ampforwp_AMPState_check;

			$ampforwp_amp_state_init = '<amp-state id="ampforwp_gravity_data_'.$form['id'].'"><script type="application/json">
 	
 		  					'.json_encode($defaultValues,JSON_PRETTY_PRINT).'

 		   				</script></amp-state>';

		   	if ( !isset($ampforwp_AMPState_check[$form['id']]) ){
		   		$ampforwp_AMPState_check[$form['id']] = '';
 		   	echo $ampforwp_amp_state_init;   }
 		   	else { return; }
	});

	// for WP AMP plugin
	add_filter( 'amphtml_template_footer_content', function() use ($defaultValues,$form)  {
			global $ampforwp_AMPState_check;

			$ampforwp_amp_state_init = '<amp-state id="ampforwp_gravity_data_'.$form['id'].'"><script type="application/json">
 	
 		  					'.json_encode($defaultValues,JSON_PRETTY_PRINT).'

 		   				</script></amp-state>';

		   	if ( !isset($ampforwp_AMPState_check[$form['id']]) ){
		   		$ampforwp_AMPState_check[$form['id']] = '';
 		   	echo $ampforwp_amp_state_init;   }
 		   	else { return; }
	});
	
	return $content; 
}

// for WP AMP plugin
add_action('amphtml_template_head','ampforwp_gf_with_wp_amp');
function ampforwp_gf_with_wp_amp() { 
	$post_id = get_the_ID();
	$ampforwp_gravity_form_checker = '';
	if ( is_singular() || is_home() || is_archive() || is_front_page() ) {
		$ampforwp_gravity_form_checker = get_post_meta($post_id, 'amp-gravity-form-checker', true); 
		if ( ! empty( $ampforwp_gravity_form_checker )) {  ?>
			<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-latest.js"></script>

			<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
		<?php } ?>
	<?php } 
}


function ampforwp_gf_date_picker($ampforwp_gf_date_picker, $fields, $defaultValues) {
		
		preg_match_all('/<input(.*?)type=\'text\'.*?class=\'datepicker.*?/si ', $ampforwp_gf_date_picker, $matches);
  				if(count($matches)>0){ 
					foreach($matches[0] as $typekey=>$input){  
							$ampforwp_gf_date_picker = str_replace( "type='text' value='' class='datepicker", "type=\"date\" value='' class='datepicker", $ampforwp_gf_date_picker );
					}}  
				
				return array($ampforwp_gf_date_picker, $defaultValues );
	}

function advanced_conditional_id($nonamp_ver, $fields, $defaultValues) {
	
preg_match_all('/<input(.*?)type=\'(.*?)\'[^*]name=\'(.*?)\'/si', $nonamp_ver, $matches);
	if(count($matches)>0 && count($matches[3])){
		foreach($matches[3] as $typekey=>$input){
			//if($matches[1][$typekey]=="text"){
				$inputName = str_replace(array(".","input_"), array("_",""), $input);
				 
				$con = 'on="change:AMP.setState({ampforwp_gravity_data_'.$fields['formId'].': {fieldId_'.$inputName.': event.value}})"';
				   
				//$nonamp_ver = str_replace('<input type=\'text\' name=\''.$input.'\'', " <input type='text' name='".$input."' ".$con , $nonamp_ver);
				$nonamp_ver = str_replace('name=\''.$input.'\'', "name='".$input."' ".$con , $nonamp_ver);


				$nonamp_ver = str_replace('name="'.$input.'"', "name=\"".$input."\" ".$con , $nonamp_ver);

				 $defaultValues['fieldId_'.$inputName] = '';

			//}
		}
	}
if(count($matches)>0 && count($matches[3])==0){
	preg_match_all('/<input(.*?)name=\'(.*?)\'(.*?)\/>/si', $nonamp_ver, $matches);
	if(count($matches)>0 && count($matches[2])){
		foreach($matches[2] as $typekey=>$input){
			//if($matches[1][$typekey]=="text"){
				$inputName = str_replace(array(".","input_"), array("_",""), $input);
				 
				$con = 'on="change:AMP.setState({ampforwp_gravity_data_'.$fields['formId'].': {fieldId_'.$inputName.': event.value}})"';
				   
				//$nonamp_ver = str_replace('<input type=\'text\' name=\''.$input.'\'', " <input type='text' name='".$input."' ".$con , $nonamp_ver);
				$nonamp_ver = str_replace('name=\''.$input.'\'', "name='".$input."' ".$con , $nonamp_ver);


				$nonamp_ver = str_replace('name="'.$input.'"', "name=\"".$input."\" ".$con , $nonamp_ver);

				 $defaultValues['fieldId_'.$inputName] = '';

			//}
		}
	}
}	
				
				return array($nonamp_ver, $defaultValues );
}

//add_action('ampforwp_body_beginning','add_gravity_json');

/*function add_gravity_json($formId){
	$form_ids = array(2);
	$forms = GFFormsModel::get_form_meta_by_id( $form_ids );
	$form = ampforwp_gravity_prepare_forms_for_export( $forms ) ;
	$gravityContent = '<amp-state id="gravityFormData"><script type="application/json">';
	$condional_form = array('condional_form'=>$form[0]);
	$gravityContent .= json_encode($condional_form);
	$gravityContent .="</script></amp-state>";

	//echo($gravityContent);die; 
	echo $gravityContent;
}*/

function amp_after_submit_notice($form){

	// {{^message}} ' . $form['confirmations'][0]['message'] . ' {{/message}}	
	$output = '';
	$output = '
		<div submit-success class="amp-form-status-success-new" >
			<template type="amp-mustache"  on="submit-success:gform_fields_{{form_id}}.hide;">
				<div class="ampforwp-form-status amp_gravity_success" >

				{{#message}} <div class="ampforwp-merge-tag">  {{{message}}}  </div> 

				{{/message}}

				{{^message}}
					' . $form['confirmations'][0]['message'] . '

				{{/message}}
		   
				</div>
				
			</template>
		</div>
		<div submit-error>
			<template type="amp-mustache">
				<div class="ampforwp-form-status amp_gravity_error">
				One or more fields have an error. Please enter required fields and try again.
				<div class="ampforwp-form-status amp_gravity_error">
				 {{#errors}} 
				 	<div>{{error_label}} : {{error_detail}}</div>
				 {{/errors}} 
				</div> 
				</div>
			</template>
		</div>';
	return $output;
}

function amp_recaptcha_scripts($data) { 		

     if( empty($data['amp_component_scripts']['amp-recaptcha-input'])) {
        $data['amp_component_scripts']['amp-recaptcha-input']='https://cdn.ampproject.org/v0/amp-recaptcha-input-0.1.js';
     } 
   return $data;      
}

function amp_add_required_scripts($data) { 		
 global $redux_builder_amp;
	if ( is_singular() || is_home() ) {
		// Adding Form Script
		if ( empty( $data['amp_component_scripts']['amp-form'] ) ) {
			$data['amp_component_scripts']['amp-form'] = 'https://cdn.ampproject.org/v0/amp-form-0.1.js';
		}
		// Adding bind Script
		if ( empty( $data['amp_component_scripts']['amp-bind'] ) ) {
			$data['amp_component_scripts']['amp-bind'] = 'https://cdn.ampproject.org/v0/amp-bind-0.1.js';
		}// Adding Mustache Script
		if ( empty( $data['amp_component_scripts']['amp-mustache'] ) ) {
			$data['amp_component_scripts']['amp-mustache'] = 'https://cdn.ampproject.org/v0/amp-mustache-latest.js';
		}

	}
	return $data;
}

function amp_gravity_form_styling(){ ?>
 .ampforwp-merge-tag i {
    display: inline-block;
    width: 100%;
    background-color: #EEE;
}
.gform_wrapper form{
	margin-bottom: 40px;
    font-family: sans-serif;
}
.gform_title {text-transform: uppercase;}
.gform_wrapper label{font-size:11px;color:#555;letter-spacing:.5px;text-transform:uppercase;line-height:1}
.gform_body ul{
	    list-style-type: none;
    padding: 0;
}
.gform_body ul li{
	        margin-bottom: 7px;
}
.ginput_complex span:last-child{
    margin-bottom: 0;
}
 .ginput_container input{
     padding: 10px 9px;
    border-radius: 2px;
    border: 1px solid #ccc;
    font-size: 14px;
 }
.ginput_container textarea{ padding: 10px 9px;}
 .ginput_complex{
    display: inline-block;
    border-top: 1px solid #ddd;
    padding-top: 10px;
    width: 100%;
 }
 .ginput_complex span{    margin-bottom: 10px; width: 100%;
    display: inline-block;}
 .ginput_complex label{
     display: block;
    position: relative;
    float: left;
    top: 12px;
    margin-right: 10px;
    width: 15%;
 }
.gform_wrapper button {
	background: #333;
    border: 0;
    font-size: 11px;
    padding: 15px 30px;
    text-transform: uppercase;
    margin-top: -5px;
    letter-spacing: 2px;
    font-weight: 700;
    color: #fff;
    box-shadow: 2px 3px 6px rgba(102,102,102,0.33);
}
.gform_footer {
    margin-top: 20px;
}
form.amp-form-submit-success .gform_body, form.amp-form-submit-success .gform_button {
  display: none
}
.gform_validation_container, .gform_wrapper .gform_validation_container, body .gform_wrapper .gform_body ul.gform_fields li.gfield.gform_validation_container, body .gform_wrapper li.gform_validation_container, body .gform_wrapper ul.gform_fields li.gfield.gform_validation_container {
    display: none ;
    position: absolute ;
    left: -9000px;
}
.gform_wrapper .gf_page_steps{
	width: 100%;
	margin: 0 0 8px;
	padding: 0 0 4px;
	border-bottom: 1px dotted #CCC;
}
.gform_wrapper .gf_step{
      width: auto;
    display: -moz-inline-stack;
    display: inline-block;
    margin: 16px 32px 16px 0;
    font-size: 14px;
    height: 14px;
    line-height: 1.25;
    filter: alpha(opacity=20);
    -moz-opacity: .2;
    -khtml-opacity: .2;
    opacity: .2;
    font-family: inherit;
    }
.gform_wrapper .gf_step.gf_step_active{
	    opacity: 1;
}
.gform_wrapper .gf_step_clear {
    display: block;
    clear: both;
    height: 1px;
    overflow: hidden;
}
.show {
	display: block;
}
.hide{
	display: none;
}
.ampforwp-merge-tag	{
    padding: 15px;
    margin-bottom: 31px;
    border: 1px solid transparent;
    border-radius: 4px;
    background-color: #fff;
  }
.amp_gravity_success {
    padding: 15px;
    margin-bottom: 31px;
    border: 1px solid transparent;
    border-radius: 4px;
 	background-color: #e7f4e7;
}
.gsection_title {
	border-bottom: 1px solid #CCC;
    padding: 0 16px 8px 0;
    margin: 28px 0;
    clear: both;
}
<?php
}

if ( ! function_exists( 'amp_get_array' ) ) {
	function amp_get_array( $array, $keys, $default = null ) {

		if ( empty( $keys ) && $keys !== 0 ) {
			return $array;
		}

		if ( ! is_array( $array ) ) {
			return $default;
		}

		if ( ! is_array( $keys ) && isset( $array[ $keys ] ) ) {
			return $array[ $keys ];
		}

		if ( is_array( $keys ) ) {
			$current = $array;
			foreach ( $keys as $key ) {
				if ( ! array_key_exists( $key, $current ) ) {
					return $default;
				}

				$current = $current[ $key ];
			}

			return $current;
		}

		return $default;
	}
}  
	/*add_action( 'amp_post_template_head', 'amp_add_post_template_scripts' );
	function amp_add_post_template_scripts( $amp_template ) {
		$scripts =  $amp_template->get( 'amp_component_template', array() );
		if ( $scripts) { 
			foreach ( $scripts as $template => $script ) : ?>
				<script custom-template="<?php echo esc_attr( $template ); ?>" src="<?php echo esc_url( $script ); ?>" async></script>
			<?php endforeach;
		}
	}*/


// Convert the <input type="button"> to <button> due to validation issues
//add_filter( 'gform_next_button', 'input_to_button', 10, 2 );
//add_filter( 'gform_previous_button', 'input_to_button', 10, 2 );
//add_filter( 'gform_submit_button', 'input_to_button', 10, 2 );
function input_to_button( $button, $form ) {
	if(function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint() ) {

		    $dom = new DOMDocument();
		    if(function_exists('mb_convert_encoding')){
		    	$dom->loadHTML(mb_convert_encoding($button, 'HTML-ENTITIES', 'UTF-8'));
		    }else{
		    	$dom->loadHTML( $button );
		    }
		    $input = $dom->getElementsByTagName( 'input' )->item(0);
		    $new_button = $dom->createElement( 'button' );
		    $button_span = $dom->createElement( 'span', $input->getAttribute( 'value' ) );
		    $new_button->appendChild( $button_span );
		    $input->removeAttribute( 'value' );
		    foreach( $input->attributes as $attribute ) {
		        $new_button->setAttribute( $attribute->name, $attribute->value );
		    }
		    $input->parentNode->replaceChild( $new_button, $input );
		    return $dom->saveHtml( $new_button );
		}
	else{
		return $button;
	}
}


function ampforwp_gravity_prepare_forms_for_export( $forms ){
	// clean up a bit before exporting
		foreach ( $forms as &$form ) {

			foreach ( $form['fields'] as &$field ) {
				$inputType = RGFormsModel::get_input_type( $field );

				if ( isset( $field->pageNumber ) ) {
					unset( $field->pageNumber );
				}

				if ( $inputType != 'address' ) {
					unset( $field->addressType );
				}

				if ( $inputType != 'date' ) {
					unset( $field->calendarIconType );
					unset( $field->dateType );
				}

				if ( $inputType != 'creditcard' ) {
					unset( $field->creditCards );
				}

				if ( $field->type == $field->inputType ) {
					unset( $field->inputType );
				}

				// convert associative array to indexed
				if ( isset( $form['confirmations'] ) ) {
					$form['confirmations'] = array_values( $form['confirmations'] );
				}

				if ( isset( $form['notifications'] ) ) {
					$form['notifications'] = array_values( $form['notifications'] );
				}
			}

			/**
			 * Allows you to filter and modify the Export Form
			 *
			 * @param array $form Assign which Gravity Form to change the export form for
			 */
			$form = gf_apply_filters( array( 'gform_export_form', $form['id'] ), $form );

		}

		$forms['version'] = GFForms::$version;
		return $forms;
}



/*


add_action("the_content",'ampforwp_amp_state',45);
function ampforwp_amp_state($content){
		global $completeAMPState;
		echo  $content."sdcdsc".implode("", $completeAMPState);
}
*/

ob_start('ampforwp_conditions_logic');
function ampforwp_conditions_logic($content){

if(strpos($content, ':openbrack:') !== -1){	 

 $contents = '';
 
	  $contents .= str_replace(array(':openbrack:',':closebrack:'), array('[',']'), $content);
	return $contents;	
	}
else {
	return $content;
	}
}
add_action('wp_ajax_gravity_form_submission','gravity_form_submission');
add_action('wp_ajax_nopriv_gravity_form_submission','gravity_form_submission');
if(isset($_GET['ampsubmit']) && $_GET['ampsubmit'] ==1){
	add_action("plugins_loaded",function(){
		remove_action( 'wp', array( 'RGForms', 'maybe_process_form' ), 9);
		remove_action( 'admin_init', array( 'RGForms', 'maybe_process_form' ), 9 );
		remove_action( 'wp', array( 'GFForms', 'maybe_process_form' ), 9 );
		remove_action( 'admin_init', array( 'GFForms', 'maybe_process_form' ), 9 );
	});
}
function gravity_form_submission (){
	remove_action( 'wp', array( 'GFForms', 'maybe_process_form' ), 10 );
	remove_action( 'admin_init', array( 'GFForms', 'maybe_process_form' ), 10 );
	require_once AMP_GRAVITY_PLUGIN_PATH .'/class-amp-gravity-forms.php';
} 


//Gravity Form Setting
add_filter("redux/options/redux_builder_amp/sections", 'ampforwp_gf_settings');
if( ! function_exists('ampforwp_gf_settings') ){
  function ampforwp_gf_settings($sections){
    $sections[] = array(
          'title'      	=> __('AMP Gravity Form','redux-framework-demo'),
          'icon' 		=> 'el el-envelope ',
      	  'id'  		=> 'ampforwp-gf-subsection',
          'desc' 		=> " ",
          'subsection' 	=> false,
          'fields'    => array(
                array(
                    'id'        => 'ampforwp-gf-pages',
                    'type'      => 'switch',
                    'title'     => 'Show Form On All Pages',
                    'default'   =>  0,
                    ),
                array(
                    'id'       => 'ampforwp-gf-shortcode',
                    'type'     => 'text',
                    'title'    => __('Form ID', 'accelerated-mobile-pages'),
                    'required'  => array(array('ampforwp-gf-pages','=','1')),
                    'default'  => '',
                    ),
                array(
                    'id'        => 'ampforwp-gf-content-top',
                    'type'      => 'switch',
                    'title'     => 'Above Content',
                    'default'   =>  0,
                    'required'  => array('ampforwp-gf-pages','=','1'),
                    ),
                array(
                    'id'        => 'ampforwp-gf-content-bottom',
                    'type'      => 'switch',
                    'title'     => 'Below Content',
                    'default'   =>  0,
                    'required'  => array('ampforwp-gf-pages','=','1'),
                    ),
                array(
                     'id'        => 'ampforwp-gf-above-footer',
                     'type'      => 'switch',
                     'title'     =>'Above Footer',
                     'default'   =>0,
                     'required'  => array('ampforwp-gf-pages','=','1'),
                  ),

                   array(
                       'id'       => 'amp-gf-Recaptcha',
                       'type'     => 'switch',
                       'title'    => __(' ReCAPTCHA v3 ', 'accelerated-mobile-pages'),
                       'desc'  => __('Enable or disable the Recaptcha v3 feature','accelerated-mobile-pages'),
                       'default'  => '0',
                   ),	
		            array(
                        'id'            =>'amp-gf-Recaptcha-site',
                        'type'          => 'text',
                       'title'         => esc_html__('Site Key','accelerated-mobile-pages'),
                       'tooltip-subtitle'  => esc_html__('You can get the site key from here ','accelerated-mobile-pages').'<a target="_blank" href="https://www.google.com/recaptcha/admin/create">'.esc_html__('form here','accelerated-mobile-pages').'</a>',
                        'default'       => '',
                        'required' => array(
                          array('amp-gf-Recaptcha', '=' , '1')),
                    ),  
		            array(
                        'id'            =>'amp-gf-Recaptcha-secerete',
                        'type'          => 'text',
                        'title'         => esc_html__('Secret Key','accelerated-mobile-pages'),
                        'tooltip-subtitle'  => esc_html__('You can get the Secret Key from here ','accelerated-mobile-pages').'<a target="_blank" href="https://www.google.com/recaptcha/admin/create">'.esc_html__('form here','accelerated-mobile-pages').'</a>',
                        'default'       => '',
                        'required' => array(
                          array('amp-gf-Recaptcha', '=' , '1')),
                    ),
        ),
        );

    return $sections;
  }
}

// Gravity Form for All Pages
add_action('pre_amp_render_post','ampforwp_gf_for_all_pages_enable');
if(!function_exists('ampforwp_gf_for_all_pages_enable')){
function ampforwp_gf_for_all_pages_enable(){
       global $post, $redux_builder_amp;
       $post_id = get_the_ID();
       add_filter('amp_post_template_data','amp_add_required_scripts', 21);
       if($redux_builder_amp['ampforwp-gf-pages'] == 1 ){       	
       add_action('amp_post_template_css', 'amp_gravity_form_styling');
       }

       update_post_meta($post_id,'amp-gravity-form-checker',1);

       if(isset($redux_builder_amp['ampforwp-gf-above-footer']) && true==$redux_builder_amp['ampforwp-gf-above-footer']){
         add_action('amp_post_template_above_footer', 'ampforwp_gf_for_all_pages');
       	 add_filter('amp_post_template_data','amp_recaptcha_scripts', 10);
       }
       if(isset($redux_builder_amp['ampforwp-gf-content-top']) && true==$redux_builder_amp['ampforwp-gf-content-top']){
         add_action('ampforwp_before_post_content','ampforwp_gf_for_all_pages');
         if(is_singular()){
       	 add_filter('amp_post_template_data','amp_recaptcha_scripts', 10);
       	}
       }

       if(isset($redux_builder_amp['ampforwp-gf-content-bottom']) && true==$redux_builder_amp['ampforwp-gf-content-bottom']){
         add_action('ampforwp_after_post_content','ampforwp_gf_for_all_pages');
         add_action('ampforwp_inside_post_content_before','ampforwp_gf_for_all_pages');
         if(is_singular()){
       	 add_filter('amp_post_template_data','amp_recaptcha_scripts', 10);
       	}
       	}
		
  }
}
/*Gravity From Widget Code*/
if (  class_exists( 'GFWidget' ) ) {
add_action('widgets_init', 'ampforwp_gf_widget_registration');	

function ampforwp_gf_widget_registration() {
	$url_path = trim(parse_url(add_query_arg(array()), PHP_URL_PATH),'/' );
  	$explode_path = explode('/', $url_path);
	if ( 'amp' === end( $explode_path) ){
	  		unregister_widget('GFWidget');
	  		register_widget('AMP_GFWidget');
  	}
}
Class AMP_GFWidget extends GFWidget {

	function widget( $args, $instance ) {

		extract( $args );
		echo $before_widget;
		/**
		 * Filters the widget title
		 * @param string $instance['title'] The title
		 */
		$title = apply_filters( 'widget_title', $instance['title'] );

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		$tabindex = is_numeric( $instance['tabindex'] ) ? $instance['tabindex'] : 1;

		// Creating form
		$form = RGFormsModel::get_form_meta( $instance['form_id'] );
		echo amp_generate_amp_gravity_form($form);
		echo $after_widget;
	}
  }
}
/*Gravity From Widget Code*/

  if(!function_exists('ampforwp_gf_for_all_pages')){
        function ampforwp_gf_for_all_pages(){
       global $redux_builder_amp;
    $gravity_form_shortcode = $redux_builder_amp['ampforwp-gf-shortcode'];
    $gravity_form_data = do_shortcode('<div class="gf_footer">[gravityform id="'.$gravity_form_shortcode.'" title="true" description="true"]</div>');
    $sanitized_form_data = new AMPFORWP_Content( $gravity_form_data,
                  apply_filters( 'amp_content_embed_handlers', array() ),
                  apply_filters( 'amp_content_sanitizers', array('AMP_Style_Sanitizer' => array() )  )
                  );
	$form_content = $sanitized_form_data->get_amp_content();
    $form_content = preg_replace('/(<[^>]+) onclick=".*?"/', '$1', $form_content);
    $form_content = preg_replace('/(<[^>]+) onkeypress=".*?"/', '$1', $form_content);
    
   
    echo $form_content;
}
}
// for honeypot option	
 function get_honeypot_ampforwp_gf_field( $form ) {
			 
		$gf_max_id     = get_max_ampforwp_gf_field_id( $form );
		$gf_labels     = get_honeypot_ampforwp_gf_labels();
			 
		$ampforwp_properties = array( 'type' => 'honeypot', 'label' => $gf_labels[ rand( 0, 3 ) ], 'id' => $gf_max_id + 1, 'cssClass' => 'gform_validation_container', 'description' => __( 'This field is for validation purposes and should be left unchanged, from ampforwp team.', 'gravityforms' ) );
		 
		$gf_field      = GF_Fields::create( $ampforwp_properties );
	 
		return $gf_field;
	}
  function get_max_ampforwp_gf_field_id( $form ) {
		$max = 0;
		  
		foreach ( $form['fields'] as $field ) {
			if ( intval( $field->id ) > $max ) {
				$max = intval( $field->id );
			}
		}

		return $max;
	}
 function get_honeypot_ampforwp_gf_labels() {
		$honeypot_ampforwp_gf_labels = array( 'Name', 'Email', 'Phone', 'Comments' );
 
		return apply_filters( 'gform_honeypot_labels_pre_render', $honeypot_ampforwp_gf_labels );
	}

add_filter('amp_sidebar_sanitizers','amp_remove_specific_sanitizer',100 );
function amp_remove_specific_sanitizer( $data ){ 
	unset($data['AMP_Tag_And_Attribute_Sanitizer']);	
	return $data;
}

add_action('ampforwp_modify_the_content','skip_invalid_content');
function skip_invalid_content($content){
	$content = str_replace('caption=""', '', $content);
	$content = preg_replace("/caption=\"(.*?)\"/", "", $content);
	return $content;
}

add_filter('amp_post_template_data','inc_amp_script');
function inc_amp_script( $data ){
	if ( empty( $data['amp_component_scripts']['amp-carousel'] ) ) {
				$data['amp_component_scripts']['amp-carousel'] =
				'https://cdn.ampproject.org/v0/amp-carousel-0.1.js';
			}
	return $data;
	}

/*
	Plugin Update Method
 */
require_once dirname( __FILE__ ) . '/updater/EDD_SL_Plugin_Updater.php';

// Check for updates
function amp_gravity_forms_plugin_updater() {

    // retrieve our license key from the DB
    //$license_key = trim( get_option( 'amp_ads_license_key' ) );
    $selectedOption = get_option('redux_builder_amp',true);
    $license_key = '';//trim( get_option( 'amp_ads_license_key' ) );
    $pluginItemName = '';
    $pluginItemStoreUrl = '';
    $pluginstatus = '';
    if( isset($selectedOption['amp-license']) && "" != $selectedOption['amp-license'] && isset($selectedOption['amp-license'][AMP_GF_ITEM_FOLDER_NAME])){

       $pluginsDetail = $selectedOption['amp-license'][AMP_GF_ITEM_FOLDER_NAME];
       $license_key = $pluginsDetail['license'];
      // $pluginItemName = $pluginsDetail['item_name'];
       $pluginItemStoreUrl = $pluginsDetail['store_url'];
       $pluginstatus = $pluginsDetail['status'];
    }
    
    // setup the updater
    $edd_updater = new AMP_GRAVITY_FORMS_EDD_SL_Plugin_Updater( AMP_GRAVITY_FORMS_STORE_URL, __FILE__, array(
            'version'   => AMP_GRAVITY_FORMS_VERSION,                // current version number
            'license'   => $license_key,                        // license key (used get_option above to retrieve from DB)
            'license_status'=>$pluginstatus,
            'item_name' => AMP_GRAVITY_FORMS_ITEM_NAME,          // name of this plugin
            'author'    => 'Mohammed Kaludi',                   // author of this plugin
            'beta'      => false,
        )
    );
}
add_action( 'admin_init', 'amp_gravity_forms_plugin_updater', 0 );

// Notice to enter license key once activate the plugin

$path = plugin_basename( __FILE__ );
    add_action("after_plugin_row_{$path}", function( $plugin_file, $plugin_data, $status ) {
        global $redux_builder_amp;
         if(! defined('AMP_GF_ITEM_FOLDER_NAME')){
        $folderName = basename(__DIR__);
            define( 'AMP_GF_ITEM_FOLDER_NAME', $folderName );
        }
        $pluginsDetail = isset($redux_builder_amp['amp-license'][AMP_GF_ITEM_FOLDER_NAME])?$redux_builder_amp['amp-license'][AMP_GF_ITEM_FOLDER_NAME]:' ';
        
        $pluginstatus= '';
        if(isset($redux_builder_amp['amp-license'][AMP_GF_ITEM_FOLDER_NAME])){
        $pluginsDetail = $redux_builder_amp['amp-license'][AMP_GF_ITEM_FOLDER_NAME];
        $pluginstatus = $pluginsDetail['status'];
      }

        if(empty($redux_builder_amp['amp-license'][AMP_GF_ITEM_FOLDER_NAME]['license'])){
            echo "<tr class='active'><td>&nbsp;</td><td colspan='2'><a href='".esc_url(  self_admin_url( 'admin.php?page=amp_options&tabid=opt-go-premium' )  )."'>Please enter the license key</a> to get the <strong>latest features</strong> and <strong>stable updates</strong></td></tr>";
                }elseif($pluginstatus=="valid"){
                	$update_cache = get_site_transient( 'update_plugins' );
            $update_cache = is_object( $update_cache ) ? $update_cache : new stdClass();
            if(isset($update_cache->response[ AMP_GF_ITEM_FOLDER_NAME ]) 
                && empty($update_cache->response[ AMP_GF_ITEM_FOLDER_NAME ]->download_link) 
              ){
               unset($update_cache->response[ AMP_GF_ITEM_FOLDER_NAME ]);
            }
            set_site_transient( 'update_plugins', $update_cache );
            
       }
    }, 10, 3 );