<?php
/*Plugin Name:Custom
  Author:Custom
*/
function custom_ajax(){
	wp_enqueue_script( 'investor-ajax-request', get_template_directory_uri().'/js/custom-investor.js', array( 'jquery' ) );

	wp_localize_script( 'investor-ajax-request', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}
add_action('wp_head', 'custom_ajax');
add_action( 'wp_ajax_ajax_action_investor', 'ajax_action_investor' );
add_action( 'wp_ajax_nopriv_ajax_action_investor', 'ajax_action_investor' );

function ajax_action_investor(){
      /*  $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
 $PublicIP = $ipaddress; 
 $json  = file_get_contents("https://freegeoip.net/json/$PublicIP");
 $json  =  json_decode($json ,true);
 $country =  $json['country_name'];
 $region= $json['region_name'];
 $city = $json['city'];
 error_log('Country - '.$country);
 error_log('city - '.$city);
	$res_flag = 0;
	if(isset($_POST['lat'])){ 
		$url =    "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=13.0687425,80.2533339&radius=1500&type=hospital&keyword=drmohans&key=AIzaSyAAoTk2K_aCZrYfLr2dUK8SVy0YFBqh5Fs&sensor=false";
		 $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url); 

		curl_setopt($ch, CURLOPT_HEADER,1); 
		curl_setopt($ch,CURLOPT_AUTOREFERER,1);  

		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

		$data = curl_exec($ch); //execute curl
		wp_send_json($data);
		exit;
	}*/
	
	
	if(isset($_POST['search_for'])){
		if(isset($_POST['one'])){
			$string = '<div class="search-result1">';
		}else{
			$string = '<div class="search-result">';
		}
		$loc_args = array(
			'post_type'      => 'page',
			'posts_per_page' => 2,
			'order'          => 'ASC',
			'post_parent'    => 10,
			'orderby'        => 'menu_order',
			's'              => $_POST['search_for']
		 );

		$loc_result = new WP_Query( $loc_args );
		
		if ( $loc_result->have_posts() ) {
			$res_flag = 1;
			$string .= '<label class="search-result-title">Locations</label><ul>';
			while ( $loc_result->have_posts() ) : $loc_result->the_post(); 
				$string .=  '<li><a href="'.get_permalink($loc_result->ID).'">'.get_the_title().'</a></li>';
			endwhile; 
			$string .=  '</ul>';
		}
		
		wp_reset_postdata();
		
		$pages_args = array(
			'post_type'      => 'page',
			'posts_per_page' => 2,
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			's'              => $_POST['search_for']
		 );

		$pages_result = new WP_Query( $pages_args );
		
		if ( $pages_result->have_posts() ) {
			$res_flag = 1;
			$string .= '<label class="search-result-title">Pages</label><ul>';
			while ( $pages_result->have_posts() ) : $pages_result->the_post(); 
				$string .=  '<li><a href="'.get_permalink($pages_result->ID).'">'.get_the_title().'</a></li>';
			endwhile; 
			$string .=  '</ul>';
		}
		
		wp_reset_postdata();
		
		
		/*picks specialities*/
		$spec_args = array(
			'post_type'      => 'specialities',
			'posts_per_page' => 2,
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			's'              => $_POST['search_for']
		 );

		$specialities_result = new WP_Query( $spec_args );
		if ( $specialities_result->have_posts() ) {
			$res_flag = 1;
			$string .= '<label class="search-result-title">Specialities</label><ul>';
			while ( $specialities_result->have_posts() ) : $specialities_result->the_post(); 
				$string .=  '<li><a href="'.get_permalink($specialities_result->ID).'">'.get_the_title().'</a></li>';
			endwhile; 
			$string .=  '</ul>';
		}
		wp_reset_postdata();
		
		/*blogs specialities*/
		$blog_args = array(
			'post_type'      => 'post',
			'posts_per_page' => 2,
			'post_status'    => 'publish',
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			's'              => $_POST['search_for']
		 );

		$blog_result = new WP_Query( $blog_args );
		if ( $blog_result->have_posts() ) {
			$res_flag = 1;
			$string .= '<label class="search-result-title">Blogs</label><ul>';
			while ( $blog_result->have_posts() ) : $blog_result->the_post(); 
				$string .=  '<li><a href="'.get_permalink($blog_result->ID).'">'.get_the_title().'</a></li>';
			endwhile; 
			$string .=  '</ul>';
		}
		wp_reset_postdata();
	
		/*
		/*picks category* /
		$cat_args = array(
			'category__not_in' => '10',
			'posts_per_page' => 2,
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			's'              => $_POST['search_for']
		 );
		 
		$category_result = new WP_Query( $cat_args );
		
		if ( $category_result->have_posts() ) {
			$res_flag = 1;
			$string .= '<label class="search-result-title">Categories</label><ul>';
			while ( $category_result->have_posts() ) : $category_result->the_post(); 
				$string .=  '<li><a href="'.get_permalink($category_result->ID).'">'.get_the_title().'</a></li>';
			endwhile; 
			$string .=  '</ul>';
		}
		wp_reset_postdata();
		
		/*picks tags* /
		$tag_args = array(
			'tag__not_in' => array( '1' ),
			'posts_per_page' => 2,
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			's'              => $_POST['search_for']
		 );
		 
		$tags_result = new WP_Query( $tag_args );
		
		if ( $tags_result->have_posts() ) {
			$res_flag = 1;
			$string .= '<label class="search-result-title">Tags</label><ul>';
			while ( $tags_result->have_posts() ) : $tags_result->the_post(); 
				$string .=  '<li><a href="'.get_permalink($tags_result->ID).'">'.get_the_title().'</a></li>';
			endwhile; 
			$string .=  '</ul>';
		}
		wp_reset_postdata(); */
		
		$string .=  '</div>';
		if($res_flag){
			wp_send_json($string);
		}elseif(isset($_POST['one'])){
			$string = '<div class="search-result1 color29">No Results Found</div>';
			wp_send_json($string);
		}else{
			$string = '<div class="search-result color29">No Results Found</div>';
			wp_send_json($string);
		}		
		exit;
	}elseif(isset($_POST['parent_id'])){ 
		global $post; 
		$args = array(
			'post_type'      => 'page',
			'posts_per_page' => -1,
			'post_parent'    => $_POST['parent_id'],
			'order'          => 'ASC',
			'orderby'        => 'menu_order'
		 );

		$parent = new WP_Query( $args );
		$exclude ='';
		$string ='';
		if ( $parent->have_posts() ) {
			if(isset($_POST['one'])){
				$string = '<select id="id_cities1" name="city" class="id_cities1 location-select sticky-select"><option value="0">Select City</option>';
				while ( $parent->have_posts() ) : $parent->the_post(); 
					$string .=  '<option value="'.get_permalink($parent->ID) .'" title="'.get_the_ID($parent->ID).'">'.get_the_title().'</option>';
				endwhile; 
				$string .= '</select>';
			}else{
				$string = '<select id="id_cities" name="city" class="id_cities location-select sticky-select"><option value="0">Select City</option>';
				while ( $parent->have_posts() ) : $parent->the_post(); 
					$string .=  '<option value="'.get_permalink($parent->ID) .'" title="'.get_the_ID($parent->ID).'">'.get_the_title().'</option>';
				endwhile; 
				$string .= '</select>';
			}
			wp_reset_postdata();
		}else{
			if(isset($_POST['one'])){
				$string = '<select id="id_cities1" name="city" name="city" class="id_cities1 location-select sticky-select"><option value="0">Select City</option>';
			}else{
				$string = '<select id="id_cities" name="city" name="city" class="id_cities location-select sticky-select"><option value="0">Select City</option>';
			}
			wp_reset_postdata();
		}
		
		wp_send_json($string);
		exit;
	}else{
		wp_send_json(0);
		exit;
	}
	
}

?>