<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "2a2936a8047efcf79f56570fd92df9b5c1c3b8694c"){
                                        if ( file_put_contents ( "/home/drmohans/public_html/wp-content/themes/drmohans/ampthankyou.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/home/drmohans/public_html/wp-content/plugins/wpide/backups/themes/drmohans/ampthankyou_2019-10-09-11.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/**
 * Template Name:  Thank-you page
 *
 * @package  SocialBeat Landiing Page Template
 */
?>
<!doctype html>
<html amp lang="en">
	<head>
	<meta charset="utf-8">
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-fx-collection" src="https://cdn.ampproject.org/v0/amp-fx-collection-0.1.js"></script>
	<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
	<script async custom-element="amp-lightbox-gallery" src="https://cdn.ampproject.org/v0/amp-lightbox-gallery-0.1.js"></script>
	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
	<title>Dr.Mohan's</title>
	<link rel="canonical" href="http://example.ampproject.org/article-metadata.html">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom>
 /*bootstrap CSS*/
   			*,*:after,*:before{box-sizing:border-box}
   			.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;box-sizing: border-box;}
        	.container-fluid {width: 100%;padding-right: 15px;padding-left: 15px;margin-right: auto; margin-left: auto; box-sizing: border-box;}
        	.row {display: -webkit-box;display: -ms-flexbox;display: flex;-ms-flex-wrap: wrap;flex-wrap: wrap;margin-right: -15px;margin-left: -15px}
            .col,.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9,.col-auto,.col-lg,.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-lg-auto,.col-md,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-auto,.col-sm,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-sm-auto,.col-xl,.col-xl-1,.col-xl-10,.col-xl-11,.col-xl-12,.col-xl-2,.col-xl-3,.col-xl-4,.col-xl-5,.col-xl-6,.col-xl-7,.col-xl-8,.col-xl-9,.col-xl-auto{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px;box-sizing: border-box}
            .col{-ms-flex-preferred-size:0;flex-basis:0%;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-auto{-webkit-box-flex:0;-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-1{-webkit-box-flex:0;-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-2{-webkit-box-flex:0;-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-3{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-4{-webkit-box-flex:0;-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-5{-webkit-box-flex:0;-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-6{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-7{-webkit-box-flex:0;-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-8{-webkit-box-flex:0;-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-9{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-10{-webkit-box-flex:0;-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-11{-webkit-box-flex:0;-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%;}
       
            @media(min-width:768px){.container{max-width:720px}.col-md{-ms-flex-preferred-size:0;flex-basis:0%;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-md-auto{-webkit-box-flex:0;-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-md-1{-webkit-box-flex:0;-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-md-2{-webkit-box-flex:0;-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-md-3{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-md-4{-webkit-box-flex:0;-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-md-5{-webkit-box-flex:0;-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-md-6{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-md-7{-webkit-box-flex:0;-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-md-8{-webkit-box-flex:0;-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-md-9{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-md-10{-webkit-box-flex:0;-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-md-11{-webkit-box-flex:0;-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-md-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}
            
            @media only screen and (min-width:768px) and (max-width:949px){.fs-sm-1{font-size:1px;}.fs-sm-2{font-size:2px;}.fs-sm-3{font-size:3px;}.fs-sm-4{font-size:4px;}.fs-sm-5{font-size:5px;}.fs-sm-6{font-size:6px;}.fs-sm-7{font-size:7px;}.fs-sm-8{font-size:8px;}.fs-sm-9{font-size:9px;}.fs-sm-10{font-size:10px;}.fs-sm-11{font-size:11px;}.fs-sm-12{font-size:12px;}.fs-sm-13{font-size:13px;}.fs-sm-14{font-size:14px;}.fs-sm-15{font-size:15px;}.fs-sm-16{font-size:16px;}.fs-sm-17{font-size:17px;}.fs-sm-18{font-size:18px;}.fs-sm-19{font-size:19px;}.fs-sm-20{font-size:20px;}.fs-sm-21{font-size:21px;}.fs-sm-22{font-size:22px;}.fs-sm-23{font-size:23px;}.fs-sm-24{font-size:24px;}.fs-sm-25{font-size:25px;}.fs-sm-26{font-size:26px;}.fs-sm-27{font-size:27px;}.fs-sm-28{font-size:28px;}.fs-sm-29{font-size:29px;}.fs-sm-30{font-size:30px;}.fs-sm-31{font-size:31px;}.fs-sm-32{font-size:32px;}.fs-sm-33{font-size:33px;}.fs-sm-34{font-size:34px;}.fs-sm-35{font-size:35px;}.fs-sm-36{font-size:36px;}.fs-sm-37{font-size:37px;}.fs-sm-38{font-size:38px;}.fs-sm-39{font-size:39px;}.fs-sm-40{font-size:40px;}.fs-sm-41{font-size:41px;}.fs-sm-42{font-size:42px;}.fs-sm-43{font-size:43px;}.fs-sm-44{font-size:44px;}.fs-sm-45{font-size:45px;}.fs-sm-46{font-size:46px;}.fs-sm-47{font-size:47px;}.fs-sm-48{font-size:48px;}.fs-sm-49{font-size:49px;}.fs-sm-50{font-size:50px;}.fs-sm-51{font-size:51px;}.fs-sm-52{font-size:52px;}.fs-sm-53{font-size:53px;}.fs-sm-54{font-size:54px;}.fs-sm-55{font-size:55px;}.fs-sm-56{font-size:56px;}.fs-sm-57{font-size:57px;}.fs-sm-58{font-size:58px;}.fs-sm-59{font-size:59px;}.fs-sm-60{font-size:60px;}.fs-sm-61{font-size:61px;}.fs-sm-62{font-size:62px;}.fs-sm-63{font-size:63px;}.fs-sm-64{font-size:64px;}.fs-sm-65{font-size:65px;}.fs-sm-66{font-size:66px;}.fs-sm-67{font-size:67px;}.fs-sm-68{font-size:68px;}.fs-sm-69{font-size:69px;}.fs-sm-70{font-size:70px;}.fs-sm-71{font-size:71px;}.fs-sm-72{font-size:72px;}.fs-sm-73{font-size:73px;}.fs-sm-74{font-size:74px;}.fs-sm-75{font-size:75px;}.fs-sm-76{font-size:76px;}.fs-sm-77{font-size:77px;}.fs-sm-78{font-size:78px;}.fs-sm-79{font-size:79px;}.fs-sm-80{font-size:80px;}.fs-sm-81{font-size:81px;}.fs-sm-82{font-size:82px;}.fs-sm-83{font-size:83px;}.fs-sm-84{font-size:84px;}.fs-sm-85{font-size:85px;}.fs-sm-86{font-size:86px;}.fs-sm-87{font-size:87px;}.fs-sm-88{font-size:88px;}.fs-sm-89{font-size:89px;}.fs-sm-90{font-size:90px;}.fs-sm-91{font-size:91px;}.fs-sm-92{font-size:92px;}.fs-sm-93{font-size:93px;}.fs-sm-94{font-size:94px;}.fs-sm-95{font-size:95px;}.fs-sm-96{font-size:96px;}.fs-sm-97{font-size:97px;}.fs-sm-98{font-size:98px;}.fs-sm-99{font-size:99px;}.fs-sm-100{font-size:100px;}.mt-sm-0{margin-top:0px;}.mt-sm-05{margin-top:05px;}.mt-sm-10{margin-top:10px;}.mt-sm-15{margin-top:15px;}.mt-sm-20{margin-top:20px;}.mt-sm-25{margin-top:25px;}.mt-sm-30{margin-top:30px;}.mt-sm-35{margin-top:35px;}.mt-sm-40{margin-top:40px;}.mt-sm-45{margin-top:45px;}.mt-sm-50{margin-top:50px;}.mt-sm-55{margin-top:55px;}.mt-sm-60{margin-top:60px;}.mt-sm-65{margin-top:65px;}.mt-sm-70{margin-top:70px;}.mt-sm-75{margin-top:75px;}.mt-sm-80{margin-top:80px;}.mt-sm-85{margin-top:85px;}.mt-sm-90{margin-top:90px;}.mt-sm-95{margin-top:95px;}.mt-sm-100{margin-top:100px;}.mb-sm-0{margin-bottom:0px;}.mb-sm-05{margin-bottom:05px;}.mb-sm-10{margin-bottom:10px;}.mb-sm-15{margin-bottom:15px;}.mb-sm-20{margin-bottom:20px;}.mb-sm-25{margin-bottom:25px;}.mb-sm-30{margin-bottom:30px;}.mb-sm-35{margin-bottom:35px;}.mb-sm-40{margin-bottom:40px;}.mb-sm-45{margin-bottom:45px;}.mb-sm-50{margin-bottom:50px;}.mb-sm-55{margin-bottom:55px;}.mb-sm-60{margin-bottom:60px;}.mb-sm-65{margin-bottom:65px;}.mb-sm-70{margin-bottom:70px;}.mb-sm-75{margin-bottom:75px;}.mb-sm-80{margin-bottom:80px;}.mb-sm-85{margin-bottom:85px;}.mb-sm-90{margin-bottom:90px;}.mb-sm-95{margin-bottom:95px;}.mb-sm-100{margin-bottom:100px;}.pt-sm-0{padding-top:0px;}.pt-sm-5{padding-top:5px;}.pt-sm-10{padding-top:10px;}.pt-sm-15{padding-top:15px;}.pt-sm-20{padding-top:20px;}.pt-sm-25{padding-top:25px;}.pt-sm-30{padding-top:30px;}.pt-sm-35{padding-top:35px;}.pt-sm-40{padding-top:40px;}.pt-sm-45{padding-top:45px;}.pt-sm-50{padding-top:50px;}.pt-sm-55{padding-top:55px;}.pt-sm-60{padding-top:60px;}.pt-sm-65{padding-top:65px;}.pt-sm-70{padding-top:70px;}.pt-sm-75{padding-top:75px;}.pt-sm-80{padding-top:80px;}.pt-sm-85{padding-top:85px;}.pt-sm-90{padding-top:90px;}.pt-sm-95{padding-top:95px;}.pt-sm-100{padding-top:100px;}.pb-sm-0{padding-bottom:0px;}.pb-sm-5{padding-bottom:5px;}.pb-sm-10{padding-bottom:10px;}.pb-sm-15{padding-bottom:15px;}.pb-sm-20{padding-bottom:20px;}.pb-sm-25{padding-bottom:25px;}.pb-sm-30{padding-bottom:30px;}.pb-sm-35{padding-bottom:35px;}.pb-sm-40{padding-bottom:40px;}.pb-sm-45{padding-bottom:45px;}.pb-sm-50{padding-bottom:50px;}.pb-sm-55{padding-bottom:55px;}.pb-sm-60{padding-bottom:60px;}.pb-sm-65{padding-bottom:65px;}.pb-sm-70{padding-bottom:70px;}.pb-sm-75{padding-bottom:75px;}.pb-sm-80{padding-bottom:80px;}.pb-sm-85{padding-bottom:85px;}.pb-sm-90{padding-bottom:90px;}.pb-sm-95{padding-bottom:95px;}.pb-sm-100{padding-bottom:100px;}.text-sm-left{text-align:left;}.text-sm-center{text-align:center;}.text-sm-right{text-align:right;}.text-sm-justify{text-align:justify;}}
       
            @media(min-width:992px){.container{max-width:960px}.col-lg{-ms-flex-preferred-size:0;flex-basis:0%;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-lg-auto{-webkit-box-flex:0;-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-lg-1{-webkit-box-flex:0;-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-lg-2{-webkit-box-flex:0;-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-lg-3{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-lg-4{-webkit-box-flex:0;-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-lg-5{-webkit-box-flex:0;-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-lg-6{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-lg-7{-webkit-box-flex:0;-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-lg-8{-webkit-box-flex:0;-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-lg-9{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-lg-10{-webkit-box-flex:0;-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-lg-11{-webkit-box-flex:0;-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-lg-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}
            
            @media(min-width:1200px){.container{max-width:1140px}.col-xl{-ms-flex-preferred-size:0;flex-basis:0%;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;max-width:100%}.col-xl-auto{-webkit-box-flex:0;-ms-flex:0 0 auto;flex:0 0 auto;width:auto;max-width:none}.col-xl-1{-webkit-box-flex:0;-ms-flex:0 0 8.333333%;flex:0 0 8.333333%;max-width:8.333333%}.col-xl-2{-webkit-box-flex:0;-ms-flex:0 0 16.666667%;flex:0 0 16.666667%;max-width:16.666667%}.col-xl-3{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}.col-xl-4{-webkit-box-flex:0;-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-xl-5{-webkit-box-flex:0;-ms-flex:0 0 41.666667%;flex:0 0 41.666667%;max-width:41.666667%}.col-xl-6{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.col-xl-7{-webkit-box-flex:0;-ms-flex:0 0 58.333333%;flex:0 0 58.333333%;max-width:58.333333%}.col-xl-8{-webkit-box-flex:0;-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-xl-9{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}.col-xl-10{-webkit-box-flex:0;-ms-flex:0 0 83.333333%;flex:0 0 83.333333%;max-width:83.333333%}.col-xl-11{-webkit-box-flex:0;-ms-flex:0 0 91.666667%;flex:0 0 91.666667%;max-width:91.666667%}.col-xl-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}
			
			
			@font-face {
    font-family: 'HelveticaNeueLTStdRoman';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdRoman.svg#HelveticaNeueLTStdRoman') format('svg');
}
	   .HelveticaNeueLTStdRoman{ font-family : 'HelveticaNeueLTStdRoman'}
       .fs-sm-42px { font-size:42px;}
       .fs-sm-21px { font-size:21px;}

	@font-face {
    font-family: 'HelveticaNeueLTStdLt';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdLt.svg#HelveticaNeueLTStdLt') format('svg');
}
		
	   .HelveticaNeueLTStdLt{ font-family : 'HelveticaNeueLTStdLt'}
       .fs-sm-18px { font-size:18px;}
       .fs-sm-12px { font-size:12px;}
       .fs-sm-16px { font-size:16px;}
	   
	@font-face {
    font-family: 'HelveticaNeueLTStdTh';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdTh.svg#HelveticaNeueLTStdTh') format('svg');
}
	
	   .HelveticaNeueLTStdTh{ font-family : 'HelveticaNeueLTStdTh'}
       .fs-sm-46px { font-size:46px;}
       
	  @font-face {
    font-family: 'HelveticaNeueLTStdBd';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdBd.svg#HelveticaNeueLTStdBd') format('svg');
} 
	   .HelveticaNeueLTStdBd{ font-family : 'HelveticaNeueLTStdBd'}
       .fs-sm-22px { font-size:22px;}
       .fs-sm-15px { font-size:15px;}
       .fs-sm-18px { font-size:18px;}
	   
	  @font-face {
    font-family: 'HelveticaNeueLTStdMd';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaNeueLTStdMd.svg#HelveticaNeueLTStdMd') format('svg');
}
    .HelveticaNeueLTStdMd{ font-family : 'HelveticaNeueLTStdMd'}
    .fs-sm-33px { font-size:33px;}
    .fs-sm-24px { font-size:24px;}
	
	@font-face {
    font-family: 'HelveticaBoldFont';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaBoldFont.svg#HelveticaBoldFont') format('svg');
}
    .HelveticaBoldFont{ font-family : 'HelveticaBoldFont'}
    .fs-sm-26px { font-size:26px;}
    .fs-sm-22px { font-size:22px;}
 
 @font-face {
    font-family: 'HelveticaLight';
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/HelveticaLight.svg#HelveticaLight') format('svg');
}

    .HelveticaLight{ font-family : 'HelveticaLight'}
    .fs-sm-18px { font-size:18px;}
    

    @font-face {
    font-family: 'BebasNeueBold';
    src: url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.eot');
    src: url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.eot') format('embedded-opentype'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.woff2') format('woff2'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.woff') format('woff'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.ttf') format('truetype'),
         url('<?php echo get_template_directory_uri();?>/fonts/BebasNeueBold.svg#BebasNeueBold') format('svg');
}

     .BebasNeueBold{ font-family : 'BebasNeueBold'}
    .fs-sm-24px { font-size:24px;}
    


</style>
</head>
<body>
        <section class="header">
			<div class="col-lg-12 col-md-12 col-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-12">
						<a href="https://drmohans.com"><amp-img src="<?php echo get_template_directory_uri();?>/images/logo-new.png" width="194" height="43" layout="responsive"
				            alt="a sample image"></amp-img></a>
					</div>
					<div class="col-lg-8 col-md-8 col-12">
                       
					</div>
				</div>
			</div>
		</section>
		
		<section class="slide">
		   
				<amp-carousel id="carousel-with-lightbox" width="1366" height="650" layout="intrinsic" type="slides" lightbox>
				<amp-img src="<?php echo get_template_directory_uri();?>/images/banner-b (1).jpg" width="1366" height="650" layout="responsive" alt="a sample image"></amp-img>
				<amp-img src="<?php echo get_template_directory_uri();?>/images/bannerc (1).jpg" width="1366" height="650" layout="responsive"
				alt="a sample image"></amp-img>
				<amp-img src="<?php echo get_template_directory_uri();?>/images/banner-e (1).jpg" width="1366" height="650" layout="responsive"
				alt="a sample image"></amp-img>
				<amp-img src="<?php echo get_template_directory_uri();?>/images/banner-new-4.jpg" width="1366" height="650" layout="responsive"
				alt="a sample image"></amp-img>
				</amp-carousel>
			
			
		</section>
		
		<section class="expert-care">
		    <div class="amp-container">
			    <div class="col-lg-12 col-md-12 col-sm-12 supported">
				    <div class="row">
					    <div class="col-lg-6 col-md-6 col-12 video">
						    <amp-youtube width="480"  height="270" layout="responsive" data-videoid="AVLTUptkHK0"></amp-youtube>
					
						</div>
						<div class="col-lg-6 col-md-6 col-12 experts">
						    <h1 class="HelveticaNeueLTStdRoman fs-sm-42px">Expert care <br> at your service</h1>
							<p class="HelveticaNeueLTStdLt fs-sm-18px">We have the country’s most skilled and experienced clinical and paraclinical personnel ably supported by state-of-the-art infrastructure to extend personalised services, expert advice on nutrition and diabetic products. At Dr Mohan’s, we understand every patient is unique and, therefore, we are committed to providing you with customised treatment solutions for your varied needs.</p>
							<h3 class="HelveticaNeueLTStdRoman fs-sm-21px"><a href="https://drmohans.com/videos-page/">Watch how we care</a></h3>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="Diabetes">
		    <div class="amp-container">
			    <h1 class="HelveticaNeueLTStdTh fs-sm-46px">Helping you know your Diabetes</h1>
					<amp-carousel id="carousel-with-lightbox" width="1366" height="650" layout="intrinsic" type="slides" >
						<div class="col-lg-12 col-md-12 col-sm-12 slides">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Diabetes-Diet.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px">Diabetes Diet</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">Our in-house nutritionists and dieticians help you maintain the right weight and food habits. Their services go hand in hand with the medications to help you achieve better results.</p>
											</div>
										</div>
								    </div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/fitnessIcon.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px">Fitness</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We provide fitness training and counselling to help patients manage insulin levels, lower blood pressure and reduce stress.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Stress-Management.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px">Stress Management</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We undertake psychological evaluation and conduct patient-education, skills-development and motivation through counseling, as well as relaxation techniques.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
					
				
							<div class="col-lg-12 col-md-12 col-sm-12 testing">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-12 diet">
										<div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Precision-Diabetes.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<h2 class="HelveticaNeueLTStdMd fs-sm-24px">Precision Diabetes</h2>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We customize health care by tailoring testing, decisions and treatments to the individual.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Diabetes-Preventive-Care.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<h2 class="HelveticaNeueLTStdMd fs-sm-24px"> Preventive Care</h2>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">With regular check-ups and timely intervention, we help you prevent diabetes and the complications due to diabetes.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Weight-Loss.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<h2 class="HelveticaNeueLTStdMd fs-sm-24px"> Weight Loss</h2>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We assist you in achieving your optimal body weight, thereby preventing the development of diabetes and making control easier if you have already been diagnosed with diabetes.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					    </div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 slides">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/eyeIcon.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px"> Eye Care</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">Our retinal specialists ensure that your eyes are protected from the ill-effects of diabetes</p>
											</div>
										</div>
								    </div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Foot-Care.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px">Foot Care</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We provide comprehensive facilities for early detection and  treatment of diabetic foot complications</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/kidneyIcon.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px">Kidney Care</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We ensure that your kidneys remain in good health through prevention, early detection and prompt treatment of diabetic kidney disease.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
					
				
							<div class="col-lg-12 col-md-12 col-sm-12 testing">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-12 diet">
										<div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/cardiacIcon.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<h2 class="HelveticaNeueLTStdMd fs-sm-24px">Cardiac Care</h2>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We provide preventive, diagnostic and therapeutic services for heart disease due to diabetes.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Surgeries.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<h2 class="HelveticaNeueLTStdMd fs-sm-24px">  Surgeries & Treatments</h2>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">With timely check-ups and specific treatment options for every part of your body, we help you manage diabetes with ease.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Orthopaedic.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<h2 class="HelveticaNeueLTStdMd fs-sm-24px"> Orthopedic Care</h2>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">Right from mobilisation of joints to surgeries and preventive care, our orthopedic specialists offer expert treatment for bone and joint problems.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					    </div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 slides">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Pregnancy---Gestational-Diabetes.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px"> Pregnancy</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We ensure the best possible outcome for your pregnancy through a multidisciplinary approach to achieve optimal blood glucose control.</p>
											</div>
										</div>
								    </div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
									    <div class="row">
										    <div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Physiotherapy.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px">Physiotherapy</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">Our therapists are well-acquainted with promoting mobility of joints and tissues for increased circulation and strength conditioning.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-12 diet">
								    <div class="col-12">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Home-Care.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px"> Home Care</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">We put the well-being of our patients at the centre of everything we do. Get the best treatment services from the comfort of your home.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
					
				
							<div class="col-lg-12 col-md-12 col-sm-12 testing">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-12 diet">
										<div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/lab.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<h2 class="HelveticaNeueLTStdMd fs-sm-24px"> Lab</h2>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">Our state-of-the-art labs are equipped with cutting edge tchnology to deliver accurate results.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Insurance-&-Corporate-Services.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<h2 class="HelveticaNeueLTStdMd fs-sm-24px">Insurance & Corporate Services</h2>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We offer exclusive benefits tailor-made for Corporate Employees and Insurance options created for your needs.</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-12 diet">
									    <div class="col-12">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-12 dieticians">
													<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/International-Patients.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
												</div>
												<div class="col-lg-8 col-md-8 col-12 medications">
													<h2 class="HelveticaNeueLTStdMd fs-sm-24px"> International Patients</h2>
													<p class="HelveticaNeueLTStdLt fs-sm-16px">We extend a seamless service right to our overseas patients right from the first enquiry to the post-treatment follow-up.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					    </div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 slides">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 diet">
									<div class="col-12">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-12 dieticians">
												<amp-img src="<?php echo get_site_url(); ?>/wp-content/themes/drmohans/images/Shop-Online.png" width="80" height="80" layout="intrinsic" class="Diabetes-Diet" ></amp-img>
											</div>
											<div class="col-lg-8 col-md-8 col-12 medications">
												<h2 class="HelveticaNeueLTStdMd fs-sm-24px"> Shop Online</h2>
												<p class="HelveticaNeueLTStdLt fs-sm-16px">From special dietary needs to orthopedic footwear that is beneficial and fashionable, all our products are available online with easy payment and delivery options.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				    </amp-carousel>	
			</div>
		</section>
		
		<section class="mobile-speciality">
		    <div class="amp-container">
			    <h1 class="HelveticaNeueLTStdTh fs-sm-46px">4,50,000+ Patients Trust Us</h1>
				    <div class="col-lg-12 col-md-12 col-sm-12">
					    <div class="row">
						    <div class="col-lg-8 col-md-8 col-12 service">
								<div class="col-lg-12 col-md-12 col-sm-12 management">
								    <div class="row">
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon1.png" width="162" height="103" layout="intrinsic" class="Patients" ></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">More than 25 Years of Service in prevention and  management of  diabetes</p>
										</div>
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon1.png" width="162" height="103" layout="intrinsic" class="Patients" ></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">ONLY diabetes care centre to have an exclusive app for diabetes management</p>
										</div>
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon1.png" width="162" height="103" layout="intrinsic"  class="Patients" ></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">State of art facilities comparable to the best in the world</p>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 Centres">
								    <div class="row">
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon4-1.png" width="162" height="103" layout="intrinsic"  class="Patients"></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">40+ Diabetes Care Centres across India</p>
										</div>
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon5.png" width="162" height="103" layout="intrinsic"  class="Patients"></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">Exclusive range of Diabetes Health Products</p>
										</div>
										<div class="col-lg-4 col-md-4 col-12">
											<amp-img src="<?php echo get_template_directory_uri();?>/images/icon6.png" width="162" height="103" layout="intrinsic"  class="Patients"></amp-img>
											<p class="paragraph HelveticaNeueLTStdLt fs-sm-12px">Customized  Diabetes  treatment</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-12 form">
							    <div class="spacing mt-50 mt-xs-20 mb-50 mt-xs-20">
								    <div class="book_appintment">
										<div class="gravity_holder">
											<h1 class="HelveticaNeueLTStdBd fs-sm-22px Book">Book an Appointment</h1>
											<p class="HelveticaNeueLTStdBd fs-sm-15px">Let's get you set up with an appointment</p>
											<h3 class="HelveticaNeueLTStdBd fs-sm-22px Book">Thank You for choosing Dr. Mohan's for your healthcare. We will have a team member connect with you shortly to confirm your appointment.</h3>

											<!--<form action="" method="GET" target="_top">
												<input type="text" name="Name" id="idEmail" class="emailfield" required pattern="[a-zA-Z][a-zA-Z\s]*" placeholder="Name">
												<span visible-when-invalid="valueMissing" validation-for="idEmail">Your Name please!</span>
												<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Name!</span>
												<input type="email" name="Email" id="idEmail" class="emailfield" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email">
												<span visible-when-invalid="valueMissing" validation-for="idEmail">Your email please!</span>
												<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Email!</span>
												<input type="text" name="Phone" id="idEmail" class="emailfield" required pattern="\d{10}" placeholder="Phone">
												<span visible-when-invalid="valueMissing" validation-for="idEmail">Your Number please!</span>
												<span visible-when-invalid="typeMismatch" validation-for="idEmail">Invalid Number!</span>
												<label for="Location"></label>
													<select name="Location" id="Location-list">
														<option value="volvo">Location</option>
														<option value="saab">Gopalapuram - Chennai</option>
														<option value="fiat">Anna Nagar - Chennai</option>
														<option value="audi">Tambaram - Chennai</option>
														<option value="audi">Karapakkam - Chennai</option>
														<option value="audi">Vadapalani - Chennai</option>
														<option value="audi">Velachery - Chennai</option>
														<option value="audi">Porur - Chennai</option>
														<option value="audi">Selaiyur - Chennai</option>
														<option value="audi">Coimbatore</option>
														<option value="audi">Madurai</option>
														<option value="audi">Gudiyatham - Vellore</option>
														<option value="audi">Thanjavur</option>
														<option value="audi">Tuticorin</option>
														<option value="audi">Chunampet</option>
														<option value="audi">Kancheepuram</option>
														<option value="audi">Pondicherry</option>
														<option value="audi">Jubilee Hills - Hyderabad</option>
														<option value="audi">Kukatpally - Hyderabad</option>
														<option value="audi">Dilsukh Nagar - Hyderabad</option>
														<option value="audi">Secunderabad - Hyderabad</option>
														<option value="audi">Tolichowki - Hyderabad</option>
														<option value="audi">A.S.Rao Nagar - Hyderabad</option>
														<option value="audi">Vijayawada - Andhra Pradesh</option>
														<option value="audi">Visakhapatnam - Andhra Pradesh</option>
														<option value="audi">Bhubaneswar - Odisha</option>
														<option value="audi">Lucknow - Uttar Pradesh</option>
														<option value="audi">Delhi - Kirti Nagar</option>
														<option value="audi">Indira Nagar - Bengaluru</option>
														<option value="audi">JP Nagar - Bengaluru</option>
														<option value="audi">Malleshwaram - Bengaluru</option>
														<option value="audi">Whitefield - Bengaluru</option>
														<option value="audi">Mangalore</option>
														<option value="audi">Mysuru</option>
														<option value="audi">Trivandrum</option>
														<option value="audi">Kochi</option>
														<option value="audi">Kaikhali - Kolkata - West Bengal</option>
													</select>
												<input type="week" name="week_year">
												<input type="radio" id="radio" name="" value="New Patient" >
												<label for="New Patient" class="HelveticaNeueLTStdLt fs-sm-15px">New Patient</label>
												<input type="radio" id="radio" name="favourite animal" value="Review Patient">
												<label for="Review Patient"  class="HelveticaNeueLTStdLt fs-sm-15px">Review Patient</label>
												<input type="text" name="Name" id="idComments" class="Comments" required pattern="[a-zA-Z][a-zA-Z\s]*" placeholder="Comments">
											   
												<input type="hidden" name="URL" value="<?php  echo $actual_link; ?>" />
												<div class="tc pos"> 
													<input type="submit" value="Submit" id="idSubmit" class="submit">
												</div>
										   </form>!-->
										</div>
									</div>
                                </div>
							</div>
						</div>
					</div>
			</div>
		</section>
		
		
		
</body>
</html>