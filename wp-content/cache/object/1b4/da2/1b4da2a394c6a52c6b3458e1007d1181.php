d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6075;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-07-04 16:16:37";s:13:"post_date_gmt";s:19:"2019-07-04 11:16:37";s:12:"post_content";s:6648:"<!-- wp:paragraph -->
<p>Diabetes is a Metabolic disorder of multiple etiology characterized by chronic hyperglycaemia with disturbances of carbohydrate, fat and protein metabolism resulting from defects in insulin secretion, insulin action or both leading to changes in both small blood vessels and large blood vessels. The high-risk problems in diabetes are retinopathy, nephropathy, neuropathy and cardiomyopathy. Patients with all form of diabetes are vulnerable to these complications </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Diabetic neuropathy</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diabetic Neuropathy is a microvascular complication that affects the nerves in the body causing a significant amount of morbidity. Diabetic neuropathy is a group of nerve disorders are the effects of diabetes on the body. The high-risk problems in diabetes increase with the duration of diabetes and glycaemic control. Diabetic neuropathy can cause a variety of clinical manifestations like pain, numbness and weakness in hands, arms, feet and legs (peripheral nerves ). It can affect almost every organ system in the body. Depending on the organ system involved it can cause symptoms like delayed gastric emptying (diabetic gastroparesis), bladder emptying abnormalities, cardiovascular problems, etc. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diabetic
Neuropathy can be classified in to two major types:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Autonomic Neuropathy </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Affects&nbsp; autonomic nerves of various organs such as
heart, genitor-urinary system, gastrointestinal system, heart and blood
vessels, sweat glands and eyes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Peripheral Neuropathy </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Affects nerves
of the hands and feet. It commonly manifests as distal sensory loss and pain,
however, there can be various other combinations. Although neuropathy is not
inevitable, the nerve damage can be reduced by keeping the blood sugar levels
under control. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Nephropathy (Kidney Disease)</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diabetic nephropathy is also called as diabetic kidney disease and it is a condition caused by damage to the filtering system of the kidneys with persistent proteinuria (protein loss in the urine) affecting patients with both Type 1 diabetes and Type 2 diabetes. it is a major microvascular complication and is associated with increased cardiovascular mortality. In severe cases, it can lead to kidney failure. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Risk factors</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Poor glycaemic
control</li><li>Hyperlipidaemia</li><li>Hypertension
</li><li>Genetic
predisposition</li><li>Family
history</li><li>Duration
and severity of diabetes</li><li>Smoking</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Control
of blood sugars and blood pressure is necessary for prevention of diabetic
nephropathy </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Diabetic
retinopathy</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diabetic Retinopathy is the most common diabetic eye disease and is a
general term for all disorders of the retina caused by diabetes. It is the
leading cause of blindness in diabetes having no initial symptoms. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The early stages of diabetic retinopathy have no symptoms.
The disease often progresses unnoticed until it affects retina to bleeds and
form ‘’ floating’’ spots. These spots sometimes clear on its own. But without
proper treatment, bleeding often recurs, increasing the risk of permanent
vision loss. &nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>There are two major
types of retinopathy: non-proliferative and proliferative</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Non-proliferative retinopathy</strong>
: in this type, the capillaries in the retina, balloon out and form pouches. It
has three stages-mild, moderate and severe</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Proliferative retinopathy :</strong>
In this type, the blood vessels are so damaged and they close off. In response
to this new blood vessels start growing in the retina. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Risk Factors </p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Long
duration of diabetes</li><li>Uncontrolled
blood sugars</li><li>Hypertension</li><li>Hyperlipidemia</li><li>Nephropathy</li><li>Pregnancy
</li><li>Tobacco
use</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Treatment </p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Early
/ prompt referral to the ophthalmologist</li><li>Control
of blood pressure</li><li>Control
of blood sugars</li><li>Control
lipids</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Macrovascular complications </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diabetes is an important risk factor for macrovascular diseases affecting large blood vessels. These complications can occur in blood vessels of any part of the body, eg.,-heart, brain, legs, etc. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Cardiovascular disease </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cardiovascular problems in diabetes are 2-4 times more likely to occur in people with diabetes compared to non- diabetics and it tends to occur at an earlier age. It is a leading cause of death among diabetics. Smoking increases the risk of diabetes patients. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Cerebrovascular disease </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diabetes patients are more likely
to have a stroke than non-diabetics. Stroke is caused due to formation of
plaque / blood clot in the blood vessles of the brain. The clogging of blood
vessels reduces blood supply to the brain and if this is not damaged early, it
ultimately causes a stroke </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Peripheral vascular disease </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Effects of diabetes on the body includes large vessels of their legs. This results in poor circulation impair healing and can develop into a serious infection. A serious foot infection, if ignored may lead to amputation. </p>
<!-- /wp:paragraph -->";s:10:"post_title";s:30:"High-risk problems in diabetes";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:35:"high-risk-complications-in-diabetes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:58:44";s:17:"post_modified_gmt";s:19:"2019-08-29 11:58:44";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=6075";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}