�b^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1300;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-25 16:42:18";s:13:"post_date_gmt";s:19:"2018-05-25 16:42:18";s:12:"post_content";s:5108:"Mr X was admitted to the hospital for recurrent hypoglycemia, the symptoms being palpitations, sweating and feeling faint. However, when he reported similar symptoms while in hospital, he was found to have normal/high sugars. Once the medical condition of hypoglycemia was ruled out with testing, it was diagnosed as a 'panic attack', or an acute form of anxiety disorder which the unsuspecting person is often unaware of. Panic attacks may resemble hypoglycemia and vice versa.
<h5>What is anxiety?</h5>
Anxiety is a feeling of nervousness, fear or worry. Some fears or worries are justified such as concern about one's family, one's health, or tension before a test or interview, etc. When the worry is out of proportion to the situation, then, it is called a disorder. It is usually accompanied by physical symptoms, related to the heart, lungs, nervous and gastro-intestinal symptoms, which includes diarrhoea, breathlessness, or even a feeling of having a heart attack. This response is usually an over -activity of the stress response system, leading to increased heart rate, stress hormone secretion, restlessness, vigilance and fear of a threatening situation.

External factors such as stress at work, school, personal relationships or stress from an emotional trauma, such as death of a loved one or physical abuse, or even side effects of certain drugs or serious medical illness, can cause anxiety. There are several types of anxiety disorders, such as Generalized Anxiety Disorder (GAD), social anxiety disorder, post traumatic stress disorder, phobia and panic attacks.

Experiencing high levels of stress across a variety of situations, is called GAD. Diagnosis of GAD is done, if three or more of the following symptoms are present for six months restlessness, fatigue, difficulty with concentration, irritability, muscle tension and sleep disturbance. Panic attack includes symptoms such as rapid heart beat, sweating, dizziness, fear of dying, chest pain, chills or hot flushes.
<h5>How does anxiety impact diabetes or vice versa?</h5>
Once a person is diagnosed with diabetes, fear of the illness and not being in control of the situation may lead to frustration. People diagnosed with diabetes are often worried about the long-term implications, such as micro and macro vascular complications, even before they occur. They may worry excessively about death, what would happen to them, and their family. Excessive worry about hypos may actually lead to 'social anxiety', wherein, they are afraid of leaving the house or place of comfort, being around people, who are strangers, and lead to avoidance of social interaction.
<h5>Anxiety disorder needs to be taken seriously</h5>
as it may have a direct or indirect effect on the course of the illness. On the one hand, fear of hypos, risk of complications and daily management can increase anxiety, while on the other hand, anxiety can disrupt management and metabolic control, and lead to complications, due to high sugars. The physiological effects of chronic anxiety leads to an elevation in the level of stress hormones, which can increase sugar levels. The 'fight or flight response' does not work very well, as the mind is the 'enemy' here. Excessive anxiety makes concentration difficult, disrupts eating habits and physical activity, or even daily tasks.
<h5>How does one counteract anxiety?</h5>
Firstly, management of stress may help in reducing symptoms. A number of relaxation techniques, like deep breathing, meditation, yoga, muscle relaxation may be effective in reducing anxiety, as our physical and mental health are continuously inter-twined. A combination of medication, which may be short-term or long-term, and psychotherapy are effective, too. Coping skills and time-management training also help in combating stress and anxiety.

It is important not to be hard on oneself, or over-demanding on oneself or others. Flexibility is the keyword here. It is also important for the family to offer emotional support and understanding. The individual needs to be reassured that an anxiety disorder is a medical condition, for which he or she is not to blame. Lastly, give them hope for recovery.
<h5>Letter of Appreciation</h5>
Dear Friends and Members of Dr. Mohan's Diabetes Centre,

We read with great interest your 'DIABETES MONITOR' issues with great interest and pass on the useful tips and advice to our family members and patients.

The nutritional snake and ladder developed by Dr. Saroja &amp; her team (2012 issue 3) is very interesting, informative and fun filled. It is not only a game but an eye opener to all parents and children especially the 'OBESITY EPIDEMIC' is grapping the globe. It is very significant to publish the novel game when 'Varkuda Ekadasi' is approaching. I feel all schools and parents should be able to get a colourful and big size chart model of the “Health Parampadam” and the cost earned should go to poor juvenile Diabetes Care.

Thank You,

Yours

Dr. (Mrs.) Nithya Srivatsan

Obsterician &amp; Gynaecologist

Lister Nursing Home

Chennai - 600 002.";s:10:"post_title";s:20:"Anxiety and Diabetes";s:12:"post_excerpt";s:51:"Anxiety is a feeling of nervousness, fear or worry.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:20:"anxiety-and-diabetes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-22 09:39:25";s:17:"post_modified_gmt";s:19:"2018-12-22 09:39:25";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=1300";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}