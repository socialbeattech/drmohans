g^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6079";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-07-04 16:21:59";s:13:"post_date_gmt";s:19:"2019-07-04 11:21:59";s:12:"post_content";s:4612:"<!-- wp:paragraph -->
<p> Prediabetes is defined when the blood sugar is elevated than the normal values but not yet high enough to be diagnosed as diabetes . It is the pre-diagnosis of diabetes. Without lifestyle changes, people with prediabetes are very likely to progress to developing type 2 diabetes. <br></p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>Diagnosis of categories of prediabetes</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Impaired glucose tolerance and Impaired fasting glucose are two entities in the causes of prediabetes </p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li> Impaired glucose tolerance (IGT) is diagnosed if 2 hour post glucose load plasma glucose is between 140-199 mg/dl with a fasting plasma glucose value in the non-diabetic range. </li><li>Impaired fasting glucose (IFG) is diagnosed if fasting plasma glucose is between100-125 mg/dl </li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Individuals in these two
categories have increased risk of progressing to diabetes and are also
considered to be at high risk of cardiovascular disease. </p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>Risk factors</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>The same factors that increase the risk of developing type 2 diabetes
increase the risk of developing prediabetes. These factors include:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li><strong>Being overweight or obese</strong>: The more fatty tissue that is present, the less sensitive to glucose the cells become. This is one of the major causes of prediabetes. </li><li><strong>Excess fat around the abdominal region</strong>: For women, a waist size over 35 inches is linked to a higher prevalence of prediabetes. For men, a waist size over 40 inches is considered a risk.</li><li><strong>Age</strong>: Prediabetes can develop in anyone of any age, but the risk of pre-diabetes is thought to rise after the age of 45 years. This may be due to inactivity, poor diet, and a loss of muscle mass, which typically declines with age.</li><li><strong>Diet</strong>: Excess&nbsp;<a href="http://www.medicalnewstoday.com/articles/161547.php">carbohydrate</a>, especially sweetened foods or beverages, can impair insulin sensitivity over time. Diets high in red or processed meats are also linked to the development of prediabetes.</li><li><strong>Sleep patterns</strong>: People with&nbsp;<a href="http://www.medicalnewstoday.com/articles/178633.php">obstructive sleep apnea</a>&nbsp;have an increased risk of developing prediabetes.</li><li><strong>Family history</strong>: Having an immediate relative with type 2 diabetes significantly increases the risk of developing prediabetes</li><li><strong>Stress</strong>: During periods of&nbsp;<a href="http://www.medicalnewstoday.com/articles/145855.php">stress</a>&nbsp;the body releases the hormone cortisol into the blood stream, raising blood glucose levels. </li><li><strong>Gestational diabetes</strong>: Women who develop gestational diabetes during pregnancy, and their children, are at a higher risk of developing the condition.</li><li><strong>Polycystic ovary syndrome (PCOS)</strong>: Women with PCOS are more susceptible to <a href="http://www.medicalnewstoday.com/articles/305567.php">insulin resistance</a>, which&nbsp;<a rel="noreferrer noopener" href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2683463/" target="_blank">can lead to</a>&nbsp;prediabetes, </li><li><strong>Ethnicity</strong>: The risk of developing pre-diabetes tends to be higher for African-Americans, Native Americans, Hispanics, Pacific Islanders, and Asian Americans. The reason remains unclear.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Causes of prediabetes </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In prediabetes &amp;
Type 2 diabetes, cells become resistant to the action of insulin, and pancreas
is unable to make enough insulin to overcome this resistance. Instead of moving
into the cells where it's needed for energy, sugar builds up in the bloodstream.
Exactly why this happens is uncertain, although it's believed that genetic and
environmental factors play a role in the development of prediabetes&amp;
diabetes . Being overweight is strongly linked to the development of type 2
diabetes, but not everyone with type 2 is overweight.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Eating healthy food,
losing weight and staying at a healthy weight, and being physically active can
help you bring your blood glucose level back into the normal range.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:31:"Risks and causes of prediabetes";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:31:"risks-and-causes-of-prediabetes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 17:19:56";s:17:"post_modified_gmt";s:19:"2019-08-29 12:19:56";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=6079";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}