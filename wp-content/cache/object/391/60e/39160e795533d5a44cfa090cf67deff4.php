ha^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6202;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-05 08:15:00";s:13:"post_date_gmt";s:19:"2019-08-05 03:15:00";s:12:"post_content";s:4906:"<!-- wp:paragraph -->
<p><strong>Dr. ASWIN MUKUNDAN, Consultant, Calicut DMDSC</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ambulatory Glucose Profile or AGP is one of the latest additions to the revolutionary changes happening in the field of diabetes and its treatment. This tool has incorporated the importance of glycaemic variability in preventing the complications of diabetes. In simpler terms, the variations in blood glucose in a day is found to have significant effects of development of complications in diabetes. <em>AGP blood test</em> can be used to detect it and sort it out by adjusting the treatment accordingly.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>What is Glycaemic Variability (GV) ?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Our blood sugars varies from hour to hour in a day. GV refers to the swings in blood sugars from minimum to maximum in a day. Most of the time we miss this difference while doing self monitoring or calculating the 3 months average as HbA1c. If we can reduce the Glycaemic Variability and smoothen the blood sugar values to keep it in a normal range throughout the day, studies have proved that we can reduce complications of diabetes significantly.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>What is Ambulatory Glucose Profile?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>AGP or Ambulatory Glucose Profile refers to a method of monitoring your blood sugar values for 14 days and almost 24 hours a day with 100 readings on average per day. All this without getting any pricks! AGP chip is a hardly a coin shaped patch which can be fixed in your arm and then you can lead your normal routine activities as before without taking any tension about it. 14 days later it gives around 1400 blood sugar values being plotted in a beautiful graph which can be read using a detector. You don't have to go back either , can be easily sent to the hospital through courier also. There are no restrictions for this even while travelling in flights.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center"} -->
<p style="text-align:center"><strong>Step 1 - Stick On</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center"} -->
<p style="text-align:center"><strong>Step 1 - Stick On</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"align":"center"} -->
<div class="wp-block-image"><figure class="aligncenter"><img src="https://lh6.googleusercontent.com/-zgXlMfurkemjWy5ExZa3wqem2I0CVnT-2nuQB5wcikOcXQ8x7vpV8_Qcspp247zXasR6hzxKH7-RkWiHPM5vBdVZIZoyc03Cq0y_2dqwKaKnxKbfVx935Obv_KuExVO8xB_mQI" alt=""/></figure></div>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"center"} -->
<p style="text-align:center"><strong>Step 2 - Scan It</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://lh4.googleusercontent.com/qofbOiXLcTWvmNYgITTDhfbUUY4kznqxHI_ILPYH51ZV_Qd3tIA4qgxUkZ5Ae8MdfY214nDGXEzhv1j_MzS23JuzuYVIDnUB0YiXoHQ_aLoGALCBvRWSLe1C6AHigBkagSHdoLI" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"center"} -->
<p style="text-align:center"><strong>Step 3 - Remove it</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://lh3.googleusercontent.com/ccb_XSifbhi_CBKG1eWXX_H9yVUWZgFM7U2372uC6zfr55kCF7GahaxzDYLqy0hiZtVGf4j7Jzzgs5Qi2n8LMLq_sIm_2wtC1t8LBrc9NSstZb06ca5BDpAmz-Slwg3Wap0Flko" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"center"} -->
<p style="text-align:center"><strong>Step 4 - Read It</strong></p>
<!-- /wp:paragraph -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://lh6.googleusercontent.com/V85dDSQ3SLQK2E8urEurfhBEYABIZrBYuA5t87K8HH8GK4AC4R3RcN2I65lZlHasOZ9Mc51x-hBwQZPqb1gBddN4Ntxif60NOxZ3ev2RG1lGyr3MysctEJzKMZkvLqaw9sdLqO4" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p><strong>How does it help?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ambulatory Glucose Profile test is really helpful in finding out the Glycaemic variability and fine tuning your treatment without getting low sugars or high sugars throughout the day. You can avoid getting multiple pricks a day and even this is really cost effective. It hardly costs around Rs. 3000/- for 1400 blood sugar values! Several studies have found that AGP is really a useful tool for the patients as well as the diabetologists in reducing blood sugars and choosing a better treatment plan which can cover the whole day.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>To know more about AGP and its usefulness, kindly ask your diabetologist about the details next time you visit and the doctor can guide you with necessary sample graphs too.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><br></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:46:"AGP : A Precious tool for people with diabetes";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:44:"agp-a-precious-tool-for-people-with-diabetes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-05 11:53:43";s:17:"post_modified_gmt";s:19:"2019-08-05 06:53:43";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=6202";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}