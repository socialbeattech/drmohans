�`^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1160;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-24 12:38:52";s:13:"post_date_gmt";s:19:"2018-05-24 12:38:52";s:12:"post_content";s:7532:"<h5>Physical activity and its benefits</h5>
The benefits of physical activity have been well established and regular moderate to vigorous-intensity physical activity can reduce the risk of cardiovascular diseases, diabetes, metabolic syndrome, colon and breast cancer and depression. In addition, physical activity is the key determinant of energy expenditure and is the basis to energy balance and weight control.

Doing any regular physical activity is better than doing nothing. One should look for different ways to add it in our daily lives and limit sedentary behaviour to set a good example for your family. Teaching and encouraging young children to be active will establish good habits which will be carried throughout their lives.

Humans are designed to move from the minute we are born. A practical and easiest way to be more physically active is to include a number of activities as a part of our family routine. We should keep in mind that the activities done are age and development appropriate.
<h5>PHYSICAL ACTIVITY GUIDELINES</h5>
a. Infants (Birth to 1 year)

Physical activity - particularly supervised floor-based play in safe environments- should be encouraged from birth.

b. Toddlers (1-3 years) and Pre-Schoolers (3-5 years)

Be physically active every day for at least three hours, spread throughout the day.

c. Children (5-12 years) and Young People (13-17 years)
Children and young people should accumulate at least 60 minutes of moderate to vigorous-intensity physical activity every day.

They should do a variety of aerobic activities including some vigorous-intensity physical activity every day. Engaging in more activity up to several hours per day for additional health benefits.

d. Adults (18-64 years)

Adults should accumulate 150 to 300 minutes (2.5 hours to 5 hours) of moderate intensity physical activity or 70 to 150 minutes (1.25 to 2.5 hours) of vigorous intensity physical activity, or an equivalent combination of both moderate and vigorous activities each week.

If currently not involved in any physical activity, then start doing by some and gradually build up to the recommended amount.
<h5>Reducing sedentary behaviour</h5>
Sedentary behaviour is the time you spend in sitting or lying down except when you are sleeping. One should limit sedentary behaviour time every day to reduce the risk of poorer health outcomes including type 2 diabetes. You should minimise sedentary behaviour at home, at work during travel and for leisure every day even if you are physically. Activities like reading, doing school work, working on a computer or travelling may need to be done while sitting but the key is to find a balance and look for opportunities to stand and move whenever you can.
<h5>SEDENTARY BEHAVIOUR GUIDELINES</h5>
a. Children (0-5 years) – Should not be sedentary, restrained or kept inactive for more than one hour at a time with the exception of sleeping.

<img class="aligncenter wp-image-1161 size-full" src="https://drmohans.com/wp-content/uploads/2018/05/Capture-5.png" alt="" width="557" height="249" />
<h5>What are moderate and vigorous-intensity activities?</h5>
a. Moderate-Intensity Activities: Activities which takes some effort, but you will be able to talk while doing them
Example of moderate-intensity activities:

<img class="aligncenter wp-image-1163 size-full" src="https://drmohans.com/wp-content/uploads/2018/05/Capture-6.png" alt="" width="557" height="124" />

b. Vigorous-Intensity Activities- It requires more effort and makes you breathe harder and faster. Example of vigorous intensity activities:

<img class="aligncenter wp-image-1164 size-full" src="https://drmohans.com/wp-content/uploads/2018/05/Capture-7.png" alt="" width="559" height="106" />

b. Children (5-12 years), Young people (13-17 years) and Adults (18-64 years)- Minimise the sedentary time spent every day. Break up long periods of sitting as often as possible.
<h5>REDUCING SCREEN TIME:</h5>
Screen time – The time spent using electronic media such as television, seated electronic games, portable electronic devices or computers for entertainment is called as screen time. They involve either sitting or lying down for longer time periods.

Children aged less than 2 years- It is recommended that they do not spend any time watching television or any other electronic media like DVDS, computer and other electronic games.

Children (2-5 years) – Sitting and watching television and use of electronic media like DVDs, computer and electronic media should be limited to less than one hour per day.

Children (5-12 years) and Young people (13-17 years) – Limit the use of electronic media for entertainment, like television, computer use, to not more than two hours a day.
<h5>Other ways to limit family screen time include:</h5>
<ul>
 	<li>having allotted time periods for electronic media and preferably not using them at daytime when one can be active outside.</li>
 	<li>Turning off the television during meal times.</li>
 	<li>Making bedrooms as television and computer free zones.</li>
</ul>
<h5>BARRIERS TO PHYSICAL ACTIVITY AND WAYS TO OVERCOME THEM</h5>
The common barriers to regular physical activity includes:
<ul>
 	<li>No time/too busy to exercise</li>
 	<li>It will not help me</li>
 	<li>Lack of confidence Embarrassed of appearance Too costly</li>
 	<li>Exercises not interesting / painful</li>
</ul>
Ways to overcome these barriers
<ul>
 	<li>Try to add physical activity to your daily routine. For example, walk or ride to work.</li>
 	<li>Exercise while you watch TV, park your vehicle faraway from your destination.</li>
 	<li>Activities which require minimal time can be included like walking, jogging or stair climbing.</li>
 	<li>Involve your friends and family members to exercise with you. Plan some social activities which include exercise.</li>
 	<li>Exercise with the kids – Go for a walk together, play tag or other running games, get an aerobic dance or exercise tape for kids and exercise together so you still get your exercise.</li>
</ul>
<h5>Diabetes Management</h5>
If retired, then use it as an opportunity to become more active instead of doing less. Spend more time in gardening, walking with the dog and playing with grandchildren. Children with short legs and grandparents with slower gaits are often great walking partners.
<h5>Use of Technology to monitor physical activity levels</h5>
Smart watches and Fitness trackers are rapidly gaining traction in India as they provide an easy health-tracking mechanism to the users.

These devices can be used as activity tracker, pedometer, calorie counter, setting goals and can be connected with the mobile phone applications.

Mobile applications- The advancements in mobile technology have made a lot easier to lose weight, burn calories and to stay fit than before. A number of popular applications are like My Fitness Pal, Fitbit, My Track can be used in our smart phones which can be easily downloaded and used.

In conclusion, physical inactivity levels are on a rise in India and alarmingly among children. The guidelines mentioned above should used as a guidance to increase the levels of physical activity among individuals and as well among family members. With the improvement in technology, the gadgets also aid in reducing the increased sedentary activity in India and motivate us to keep moving towards a better and healthy India.
<h5>Make it Easy, Make it Fun and Make it Daily!</h5>";s:10:"post_title";s:52:"Benefits of Physical Activity in Preventing Diabetes";s:12:"post_excerpt";s:65:"Doing any regular physical activity is better than doing nothing.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:52:"benefits-of-physical-activity-in-preventing-diabetes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-24 11:50:11";s:17:"post_modified_gmt";s:19:"2018-12-24 11:50:11";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=1160";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}