�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1134;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-24 10:10:50";s:13:"post_date_gmt";s:19:"2018-05-24 05:10:50";s:12:"post_content";s:7515:"<h5>The Importance of Exercise for Diabetes Patients</h5>
<h5>Key Points:</h5>
Exercise delays the onset of diabetes, helps in control of diabetes and prevents diabetic complications.
Exercise should include a warm up, a cool down and stretching of the muscles.
Blood sugar level should be tested before and after exercising.

Exercise forms one of the cornerstones of diabetes management. A regular program can delay the onset of diabetes, help in control of diabetes, prevent diabetic complications and slow the progression of complications. Unfortunately, many patients with diabetes find that they are unable to exercise, in spite of their best intentions. This might be due to lack of space and time or for want of adequate facilities. Most people also find it difficult to exercise without proper guidance. Many diabetes patients have other complications, which may prevent them from doing all the types of exercise that a non-diabetic individual can do. Never start a new exercise program without consulting the physician or fitness consultant.
<h5>Why exercise?</h5>
<ul>
 	<li>It lowers blood pressure.</li>
 	<li>It helps to correct abnormal blood lipids by raising the good (HDL) cholesterol level.</li>
 	<li>It strengthens the heart and circulatory system.</li>
 	<li>It can decrease body fat and increase muscle tone.</li>
 	<li>It relieves tension, stress and helps the patient to feel relaxed.</li>
 	<li>It increases basal metabolic rate and helps in weight loss.</li>
 	<li>It improves the general sense of well-being.</li>
 	<li>In some people, exercise combined with a meal plan can control type 2 diabetes without the need for medications.</li>
</ul>
<h5>How to exercise:</h5>
There are some basic principles that govern the world of exercise, and knowing them can help patients become more fit. These include:
<ul>
 	<li>Frequency - How often we exercise</li>
 	<li>Intensity - How hard we exercise</li>
 	<li>Time-How long we exercise</li>
</ul>
Type- The type of exercise we’re doing (e.g., running, walking, etc.)

When we work out at sufficient intensity, time and frequency, our body will improve (also called the Training Effect) and we’ll start seeing changes in our weight, body fat percentage, cardio endurance and strength. When our body adjusts to our current levels, it’s time to manipulate one or more of them. For example, if we’ve been walking 3 times a week for 20 minutes, we can change our program by implementing one or more of the following ideas:
<ul>
 	<li>Frequency - We can add one more day of walking.</li>
 	<li>Intensity - We can add short bursts of jogging or speed walking.</li>
 	<li>Time - We can add 10-15 minutes to our usual workout time</li>
</ul>
Type - We can do a different activity such as cycling, swimming or aerobics
Exercise should also include a warm up, a cool down and stretching of the muscles that we are going to use.
Furthermore, having some understanding about exercise will make it more enjoyable and help you stay motivated.
Warming up and cooling down: Depending on the exercise, warm up and cool down could be the same activity, but performed at a less intense level. For example, if we planned a walk, we have to walk at a slower pace for warm up and cool down.
Warming up increases the blood flow to the muscles and decreases the chance of injuries to the muscles or joints. Warm up should be done for 5-10 minutes at a very low intensity.
Cooling down prevents blood pooling in the extremities, e.g. legs. It should be performed for about 5 minutes, gradually reducing the intensity level.

Stretching: We have to stretch the muscles after warm up and cool down. Stretching is very important: it reduces risk of injury and stiffness, helps muscles perform better and improves flexibility. A common mistake is to stretch muscles before they are warmed up.

When to exercise: Early morning is probably the best time to exercise. Early morning exercise makes one fresh for the day’s work. But it doesn’t mean that we have to exercise only in the morning. It doesn’t matter when we do it as long we get an adequate amount of physical activity.

We all know the benefits of being active, but we always make excuses not to do it because we’re busy and don’t have the time. Being active can increase the number of calories you burn. There are many ways to be active in your everyday life.
<ul>
 	<li>Walk around while you talk on the phone.</li>
 	<li>Play with kids.</li>
 	<li>Take your dog for a walk.</li>
 	<li>Get up to change the TV channel instead of using the remote control.</li>
 	<li>Work in the garden.</li>
 	<li>Clean the house.</li>
</ul>
<h5>Diabetes Management</h5>
<ul>
 	<li>Wash the car.</li>
 	<li>Park at the far end of the shopping center lot and walk to the store.</li>
 	<li>At the grocery store, walk down every aisle.</li>
 	<li>At work, walk over to see a co-worker instead of calling or emailing.</li>
 	<li>Take the stairs instead of the elevator.</li>
 	<li>Stretch or walk around instead of taking a coffee break and eating.</li>
</ul>
<h5>Dos and Don’ts of Exercise:</h5>
<h5>Do’s:</h5>
<ul>
 	<li>Talk to your doctor about the right exercise to do.</li>
 	<li>Check blood sugar levels before and after exercising.</li>
 	<li>Check feet for blisters or sores before and after exercising.</li>
 	<li>Wear proper shoes and socks.</li>
 	<li>Drink plenty of fluid before, during and after exercising.</li>
 	<li>Warm up before exercising and cool down afterward.</li>
 	<li>Have a snack handy in case your blood sugar level drops too low.</li>
 	<li>Carryadiabetes ID card so that proper treatment can be given in case of an injury.</li>
</ul>
<h5>Don’ts:</h5>
<ul>
 	<li>Do not exercise if sick or experiencing pain due to exercise.</li>
 	<li>Do not exercise if the blood sugar is over 300mg/dl.</li>
</ul>
In conclusion, regular exercise, physical activity and fitness are essential for sound health and well-being of people both young and old. Spend less time in activities that use little energy like watching television and playing video games, and more time in physical activities.

Voluntary, non-sensory nervous system (i.e. the autonomic nervous system) affecting mostly the internal organs such as the bladder muscles, the cardiovascular system, the digestive tract, and the genital organs. These nerves are not under a person’s conscious control and function automatically. It can also cause hypoglycemia unawareness.
<ul>
 	<li>Digestive system</li>
 	<li>Urinary tract</li>
 	<li>Sexorgans</li>
 	<li>Sweat glands</li>
 	<li>Eyes</li>
 	<li>Lungs</li>
</ul>
<h5>Proximal neuropathy</h5>
Proximal neuropathy causes pain in the proximal regions like thighs, hips, buttocks and causes weakness in the legs.
<h5>Focal neuropathy</h5>
Focal neuropathy results in the sudden weakness of one nerve or a group of nerves, causing muscle weakness or pain. Any nerve in the body can be affected.
<h5>Diabetes Management</h5>
<ul>
 	<li>Eyes</li>
 	<li>Facialmuscles</li>
 	<li>Ears</li>
 	<li>Pelvisand lower back</li>
 	<li>Chest</li>
 	<li>Abdomen</li>
 	<li>Thighs</li>
 	<li>Legs</li>
 	<li>Feet</li>
</ul>
<h5>Investigations:</h5>
The tests that are available to check neuropathies are:
<ul>
 	<li>Diabetic peripheral neuropathy - Nerve</li>
</ul>
<h5>Conduction</h5>
<h5>Study</h5>
<ul>
 	<li>Diabetic autonomic neuropathy - Autonomic Neuropathy test</li>
</ul>";s:10:"post_title";s:35:"Importance of Exercise for Diabetes";s:12:"post_excerpt";s:70:"Exercise delays the onset of diabetes, helps in control of diabetes...";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:21:"exercise-for-diabetes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:43:45";s:17:"post_modified_gmt";s:19:"2019-08-29 11:43:45";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=1134";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}