�a^<?php exit; ?>a:1:{s:7:"content";a:72:{s:13:"meta-checkbox";a:1:{i:0;s:0:"";}s:10:"_edit_last";a:1:{i:0;s:1:"1";}s:10:"_edit_lock";a:1:{i:0;s:12:"1548683528:1";}s:14:"desktop_banner";a:1:{i:0;s:4:"1090";}s:15:"_desktop_banner";a:1:{i:0;s:19:"field_5adf0a2c17ade";}s:13:"mobile_banner";a:1:{i:0;s:4:"1741";}s:14:"_mobile_banner";a:1:{i:0;s:19:"field_5adf0ab117adf";}s:11:"banner_text";a:1:{i:0;s:190:"<h1 class="animated bounceInLeft text-left Helvetica_Roman fs-48">Diabetes Laboratory</h1>
<h3 class="animated bounceInLeft text-left fs-30">Dr. Mohan’s Diabetes
Specialities Centre</h3>";}s:12:"_banner_text";a:1:{i:0;s:19:"field_5adf0af817ae0";}s:25:"speciality_section1_title";a:1:{i:0;s:23:"Services at Dr. Mohan's";}s:26:"_speciality_section1_title";a:1:{i:0;s:19:"field_5adf0b4d17ae1";}s:22:"speciality_description";a:1:{i:0;s:295:"The Laboratory services at Dr. Mohan’s Diabetes Specialties Centre is unique in using advanced technology and having fully automated walk away systems in the department of Biochemistry, Hematology, Pathology, Microbiology and Serology which provide round the clock services, seven days a week.";}s:23:"_speciality_description";a:1:{i:0;s:19:"field_5adf0bde17ae2";}s:46:"specialities_buttons_0_description_button_text";a:1:{i:0;s:9:"Know More";}s:47:"_specialities_buttons_0_description_button_text";a:1:{i:0;s:19:"field_5adf0d7717ae4";}s:30:"specialities_buttons_0_btn_url";a:1:{i:0;s:20:"https://drmohans.com";}s:31:"_specialities_buttons_0_btn_url";a:1:{i:0;s:19:"field_5adf23d05b7df";}s:46:"specialities_buttons_1_description_button_text";a:1:{i:0;s:11:"Watch Video";}s:47:"_specialities_buttons_1_description_button_text";a:1:{i:0;s:19:"field_5adf0d7717ae4";}s:30:"specialities_buttons_1_btn_url";a:1:{i:0;s:20:"https://drmohans.com";}s:31:"_specialities_buttons_1_btn_url";a:1:{i:0;s:19:"field_5adf23d05b7df";}s:20:"specialities_buttons";a:1:{i:0;s:1:"2";}s:21:"_specialities_buttons";a:1:{i:0;s:19:"field_5adf0cf617ae3";}s:23:"specialising_tabs_title";a:1:{i:0;s:43:"Dr Mohan's  - Specializing in your Diabetes";}s:24:"_specialising_tabs_title";a:1:{i:0;s:19:"field_5adf0de44d86c";}s:34:"specialities_tab_title_0_tab_title";a:1:{i:0;s:19:"Diagnosis and Tests";}s:35:"_specialities_tab_title_0_tab_title";a:1:{i:0;s:19:"field_5adf0e814d86e";}s:36:"specialities_tab_title_0_tab_content";a:1:{i:0;s:13668:"<label class="title fs-24">Diagnosis and Tests </label>

The laboratory is equipped with state-of-the-art equipment and has a team of efficient, motivated, knowledgeable and qualified biochemists and technical staff who are constantly involved in producing accurate and reliable results and upholding quality assurance.
To avoid cross contamination and pre analytical error all the blood samples are labeled with bi-directional barcodes. Bi-directional barcoded samples are directly fed into the respective analyzer and results are automatically transmitted to Laboratory Information System (LIS) without manual entry to avoid analytical and post analytical error.
<p data-toggle="modal" data-target="#myModal"><span style="color: #ff0000; cursor: pointer;">List of Analytes Table</span></p>

<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">
<table style="background-color: white; text-align: center;">
<thead>
<tr>
<th style="text-align: center; border:1px solid;">S. No.</th>
<th style="text-align: center; border:1px solid;">ANALYTES</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align: center; border:1px solid;">1</td>
<td style="text-align: center; border:1px solid;">Glucose</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">2</td>
<td style="text-align: center; border:1px solid;">Urea</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">3</td>
<td style="text-align: center; border:1px solid;">Creatinine</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">4</td>
<td style="text-align: center; border:1px solid;">Cholesterol Total</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">5</td>
<td style="text-align: center; border:1px solid;">Triglycerides</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">6</td>
<td style="text-align: center; border:1px solid;">HDL Cholesterol</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">7</td>
<td style="text-align: center; border:1px solid;">AST</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">8</td>
<td style="text-align: center; border:1px solid;">ALT</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">9</td>
<td style="text-align: center; border:1px solid;">Alkaline Phosphatase</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">10</td>
<td style="text-align: center; border:1px solid;">Total Protein</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">11</td>
<td style="text-align: center; border:1px solid;">Albumin</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">12</td>
<td style="text-align: center; border:1px solid;">Bilirubin Total</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">13</td>
<td style="text-align: center; border:1px solid;">Bilirubin Direct</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">14</td>
<td style="text-align: center; border:1px solid;">Gamma GT</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">15</td>
<td style="text-align: center; border:1px solid;">Amylase</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">16</td>
<td style="text-align: center; border:1px solid;">Sodium</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">17</td>
<td style="text-align: center; border:1px solid;">Potassium</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">18</td>
<td style="text-align: center; border:1px solid;">Chloride</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">19</td>
<td style="text-align: center; border:1px solid;">Bicarbonate</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">20</td>
<td style="text-align: center; border:1px solid;">CPK</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">21</td>
<td style="text-align: center; border:1px solid;">Calcium</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">22</td>
<td style="text-align: center; border:1px solid;">Phosphorus</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">23</td>
<td style="text-align: center; border:1px solid;">Uric Acid</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">24</td>
<td style="text-align: center; border:1px solid;">Lactate</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">25</td>
<td style="text-align: center; border:1px solid;">C – Peptide</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">26</td>
<td style="text-align: center; border:1px solid;">Insulin</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">27</td>
<td style="text-align: center; border:1px solid;">Apo A</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">28</td>
<td style="text-align: center; border:1px solid;">Apo B</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">29</td>
<td style="text-align: center; border:1px solid;">Beta Hydroxybutrate</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">30</td>
<td style="text-align: center; border:1px solid;">Fructosamine</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">31</td>
<td style="text-align: center; border:1px solid;">HbA1c</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">32</td>
<td style="text-align: center; border:1px solid;">FT3</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">33</td>
<td style="text-align: center; border:1px solid;">FT4</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">34</td>
<td style="text-align: center; border:1px solid;">TSH</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">35</td>
<td style="text-align: center; border:1px solid;">Total PSA</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">36</td>
<td style="text-align: center; border:1px solid;">Free PSA</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">37</td>
<td style="text-align: center; border:1px solid;">Lipase</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">38</td>
<td style="text-align: center; border:1px solid;">GAD Antibody</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">39</td>
<td style="text-align: center; border:1px solid;">Osmolality (Serum &amp; Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">40</td>
<td style="text-align: center; border:1px solid;">Micro albumin (Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">40</td>
<td style="text-align: center; border:1px solid;">Micro albumin (Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">41</td>
<td style="text-align: center; border:1px solid;">Protein (Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">42</td>
<td style="text-align: center; border:1px solid;">Sodium (Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">43</td>
<td style="text-align: center; border:1px solid;">Potassium (Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">44</td>
<td style="text-align: center; border:1px solid;">Chloride (Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">45</td>
<td style="text-align: center; border:1px solid;">hs CRP</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">46</td>
<td style="text-align: center; border:1px solid;">Creatinine (Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">47</td>
<td style="text-align: center; border:1px solid;">Glucose (Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">48</td>
<td style="text-align: center; border:1px solid;">Urea (Urine)</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">49</td>
<td style="text-align: center; border:1px solid;">Folate</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">50</td>
<td style="text-align: center; border:1px solid;">Ferritin</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">51</td>
<td style="text-align: center; border:1px solid;">Vitamin B12</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">52</td>
<td style="text-align: center; border:1px solid;">Iron</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">53</td>
<td style="text-align: center; border:1px solid;">UIBC</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">54</td>
<td style="text-align: center; border:1px solid;">TIBC</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">55</td>
<td style="text-align: center; border:1px solid;">25 Hydroxyvitamin D</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">56</td>
<td style="text-align: center; border:1px solid;">Testosterone</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">57</td>
<td style="text-align: center; border:1px solid;">Cortisol</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">58</td>
<td style="text-align: center; border:1px solid;">Urine Sugar</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">59</td>
<td style="text-align: center; border:1px solid;">Urine Protein</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">60</td>
<td style="text-align: center; border:1px solid;">Urine Acetone</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">61</td>
<td style="text-align: center; border:1px solid;">Urine pH</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">62</td>
<td style="text-align: center; border:1px solid;">Urine Specific gravity</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">63</td>
<td style="text-align: center; border:1px solid;">Urine Colour</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">64</td>
<td style="text-align: center; border:1px solid;">Urine Appearance</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">65</td>
<td style="text-align: center; border:1px solid;">Urine Bile salts</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">66</td>
<td style="text-align: center; border:1px solid;">Urine Bile pigments</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">67</td>
<td style="text-align: center; border:1px solid;">Urine Urobilinogen</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">68</td>
<td style="text-align: center; border:1px solid;">Urine Deposits</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">69</td>
<td style="text-align: center; border:1px solid;">Urine Macroscopic and Microscopic Examination</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">70</td>
<td style="text-align: center; border:1px solid;">Motion Occult Blood</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">71</td>
<td style="text-align: center; border:1px solid;">Complete blood cell count</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">72</td>
<td style="text-align: center; border:1px solid;">Erythrocyte Sedimentation Rate</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">73</td>
<td style="text-align: center; border:1px solid;">Smear for MF</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">74</td>
<td style="text-align: center; border:1px solid;">Smear for MP</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">75</td>
<td style="text-align: center; border:1px solid;">Peripheral Smear Study</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">76</td>
<td style="text-align: center; border:1px solid;">PT</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">77</td>
<td style="text-align: center; border:1px solid;">aPTT</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">78</td>
<td style="text-align: center; border:1px solid;">Blood Grouping &amp; Rh Typing</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">79</td>
<td style="text-align: center; border:1px solid;">Optimal Malaria</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">80</td>
<td style="text-align: center; border:1px solid;">Bleeding Time</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">81</td>
<td style="text-align: center; border:1px solid;">Clotting Time</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">82</td>
<td style="text-align: center; border:1px solid;">Widal Test</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">83</td>
<td style="text-align: center; border:1px solid;">RA Factor</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">84</td>
<td style="text-align: center; border:1px solid;">HBsAg</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">85</td>
<td style="text-align: center; border:1px solid;">Anti-HCV</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">86</td>
<td style="text-align: center; border:1px solid;">RPR</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">87</td>
<td style="text-align: center; border:1px solid;">HIV</td>
</tr>
<tr>
<td style="text-align: center; border:1px solid;">88</td>
<td style="text-align: center; border:1px solid;">Gram Stain</td>
</tr>
</tbody>
</table>
</div>
</div>";}s:37:"_specialities_tab_title_0_tab_content";a:1:{i:0;s:19:"field_5adf0ec24d86f";}s:34:"specialities_tab_title_1_tab_title";a:1:{i:0;s:11:"Departments";}s:35:"_specialities_tab_title_1_tab_title";a:1:{i:0;s:19:"field_5adf0e814d86e";}s:36:"specialities_tab_title_1_tab_content";a:1:{i:0;s:4246:"The laboratory’s Quality Monitoring system helps to improve the accuracy and reliability of results in term of internal Quality Control (QC) as well as external proficiency testing materials. Internal QC material is analyzed at predetermined intervals along with patient samples to monitor systemic and random errors. These materials are traceable to international certified reference material so that accuracy of measurements is monitored periodically.

Our laboratory’s quality is assured by the International and National EXTERNAL QUALITY ASSURANCE (EQA) programme conducted by
<ul class="primary-ul" style="padding-left: 25px; font-size: 18px;">
 	<li>Bio-Rad Laboratories, California, USA</li>
 	<li>Christian Medical College (CMC), Vellore, India</li>
 	<li>All India Institute of Medical Sciences (AIIMS), New Delhi, India</li>
</ul>
<label class="title fs-24">Department of Clinical Biochemistry</label>

The initial step towards a cure is the diagnosis. The faster and more accurate the diagnosis, the easier and better the cure. The biochemistry department goes the extra mile by being committed to ensuring accuracy and precision in specimen handling, clinical testing and making available the latest investigation in the field, so as to facilitate faster diagnosis.
The Biochemistry Department performs more than 3000 tests per day pertaining to 48 parameters. All the biochemistry assays are performed in high throughput analyzers
<ul class="primary-ul" style="padding-left: 25px; font-size: 18px;">
 	<li>Beckman Coulter AU680 chemistry analyzer (throughput 1200 tests/hr)</li>
 	<li>Roche Cobas e601 Immunoassay module for selective sample processing of immunological parameters with the ECL technology. (throughput 170 tests/hr)</li>
 	<li>India’s first Bio-Rad Variant II turbo Glycated Hemoglobin (HbA1c) analyzer (throughput 40 test/hr), which has been certified by the National Glycohaemoglobin Standardization Program (NGSP), USA.</li>
 	<li>Siemens Advia Centaur XPT Immunoassay Analyzer (throughput 240 tests / hr)</li>
 	<li>SEBIA CAPILLARYS 2 Flex Piercing System, a fully automated solution for high quality HbA1c testing (throughput 36 test/hr)</li>
 	<li>EasyLyte Na/K &amp; Cl analyzers, a completely automated, microprocessor controlled electrolyte system which used to analyze sodium, potassium, and chloride in whole blood, serum, plasma, or urine.</li>
 	<li>Bio-Rad iMark™ microplate ELISA reader for the analysis of Anti-GAD, Zinc Transporter 8 antibody etc.</li>
</ul>
<label class="title fs-24">Department of Hematology</label>

The Hematology Department provides laboratory services for the diagnosis and treatment of all blood related disorders such as anemia, bleeding disorders and malignant conditions related to blood cells such as leukemia, lymphoma and myeloma. Complete Blood Counts (CBC) are analysed by Sysmex XN-1000 (100 samples/hour), XN-350(60 samples/hr) analyzer and Coagulation tests are performed in the automated Random Access Coagulation analyzer Sysmex CA 50.

<label class="title fs-24">Department of Clinical Pathology</label>

Urine analysis is used in detecting a variety of renal, pre-renal, urinary tract diseases. A semi-automated Cobas u 411 (600 test strips/hour) is used to rapidly detect PH, specific gravity, protein, glucose, bilirubin and other compounds in urine. The technician examines urine for microscopic elements such as red blood cells, pus cells, epithelial cells, casts and crystals.

<label class="title fs-24">Department of Microbiology and Serology</label>

The Microbiology and Serology Department has high-end facilities for processing and reporting patient’s specimens in the field of diagnostic bacteriology, virology and parasites, which may cause a range of diseases.
In addition to identification, the department is actively involved in infection control activities of the hospital.

<label class="title fs-24">60 Minutes Reports</label>

The samples are processed immediately; the reports are validated continuously and released. The turnaround time after postprandial sample reaches the lab, is on an average 60 minutes. The results are available within this period which reduces waiting time of patients.";}s:37:"_specialities_tab_title_1_tab_content";a:1:{i:0;s:19:"field_5adf0ec24d86f";}s:34:"specialities_tab_title_2_tab_title";a:1:{i:0;s:7:"FAQ’s";}s:35:"_specialities_tab_title_2_tab_title";a:1:{i:0;s:19:"field_5adf0e814d86e";}s:36:"specialities_tab_title_2_tab_content";a:1:{i:0;s:3072:"<label class="title fs-24">FAQ’s </label>
<ul class="primary-ul" style="padding-left: 25px; font-size: 18px;">
 	<li>I am type 2 diabetic patients. If I do C-Peptide testing once, should I repeat it? If so how often?
In Type 2 DM there is progressive loss of insulin secretion capacity so it is important to repeat C-peptide tests yearly once from diagnosis to decide changes in treatment requirements</li>
</ul>
<ul class="primary-ul" style="padding-left: 25px; font-size: 18px;">
 	<li>For Type 1 Diabetic patients. What is the duration of C-Peptide to be done? If I done it once should I repeat it every time?
C-peptide levels taken at the time of diagnosis on within first few years of diagnosis of DM is useful in confirming Type – 1 DM if results are low. However it is important to repeat C-peptide after few years of diagnosis and treatment with insulin, as higher results may reflect honeymoon period or may suggest MODY. If c-peptide is totally absent and GAD is also positive it may not be repeated every time</li>
</ul>
<ul class="primary-ul" style="padding-left: 25px; font-size: 18px;">
 	<li>What are the uses of AGP?
It helps to assess the glycemic variability throughout the day and helps to treat the patient very precisely. It is also very useful to detect early morning hypos especially in elderly.</li>
 	<li>HbA1c that does not match SMB</li>
 	<li>Non-compliant patients</li>
 	<li>Therapy adjustment</li>
 	<li>Insulin adjustment</li>
 	<li>High glucose variability</li>
 	<li>Hypoglycemia pattern</li>
 	<li>Hyperglycemia patterns</li>
 	<li>Baseline assessment</li>
</ul>
<ul class="primary-ul" style="padding-left: 25px; font-size: 18px;">
 	<li>What is the specialty of HbA1c testing?
HbA1c test measure your average blood glucose over previous 8-12 weeks
Gives an indication of your long time blood glucose control
It is used as a screening test to identify diabetes and regular monitoring tool if you have been diagnosis with DM to know how well your DM is controlled.</li>
</ul>
<ul class="primary-ul" style="padding-left: 25px; font-size: 18px;">
 	<li>Why should I do my electrolytes?
Electrolytes are minerals in your blood such as sodium and potassium that maintain the balance of fluids in your body.
High sugars upsets body’s electrolytes control system specially sodium and potassium Acid / base upset my result in a condition known as DICA which can be seen such as life threatening. So it’s important to check electrolytes.</li>
</ul>
<ul class="primary-ul" style="padding-left: 25px; font-size: 18px;">
 	<li>I did not do my Haemogram last time when I visited six months back. Will it affect my diagnosis?
Haemogram plays a major role in determining the health of a patient. Various blood related components and details are known by this particular test which plays a vital role in diagnosis. Hemogram is important to assess infection, anaemia and platelet abnormalities. Sometimes anaemia can interfere with HbA1c values.
Therefore Haemogram needs to be compulsorily checked.</li>
</ul>";}s:37:"_specialities_tab_title_2_tab_content";a:1:{i:0;s:19:"field_5adf0ec24d86f";}s:22:"specialities_tab_title";a:1:{i:0;s:1:"3";}s:23:"_specialities_tab_title";a:1:{i:0;s:19:"field_5adf0e4c4d86d";}s:6:"slider";a:1:{i:0;s:48:"a:3:{i:0;s:3:"942";i:1;s:3:"943";i:2;s:3:"944";}";}s:7:"_slider";a:1:{i:0;s:19:"field_5adf0ee04d870";}s:30:"book_appointment_section_title";a:1:{i:0;s:24:"Understand your Diabetes";}s:31:"_book_appointment_section_title";a:1:{i:0;s:19:"field_5adf0f9863828";}s:15:"know_more_video";a:1:{i:0;s:11:"AVLTUptkHK0";}s:16:"_know_more_video";a:1:{i:0;s:19:"field_5adf0fd863829";}s:4:"form";a:1:{i:0;s:0:"";}s:5:"_form";a:1:{i:0;s:19:"field_5adf2b1eca389";}s:13:"related_blogs";a:1:{i:0;s:1:"3";}s:14:"_related_blogs";a:1:{i:0;s:19:"field_5adf18c35a8d8";}s:25:"related_blogs_0_blog_name";a:1:{i:0;s:17:"MANAGING DIABETES";}s:26:"_related_blogs_0_blog_name";a:1:{i:0;s:19:"field_5adf1bd25a8d9";}s:32:"related_blogs_0_blog_description";a:1:{i:0;s:48:"Blood Sugar Monitoring –When to check and why?";}s:33:"_related_blogs_0_blog_description";a:1:{i:0;s:19:"field_5adf1c515a8da";}s:30:"related_blogs_0_read_more_text";a:1:{i:0;s:9:"Read More";}s:31:"_related_blogs_0_read_more_text";a:1:{i:0;s:19:"field_5adf1c765a8dc";}s:25:"related_blogs_1_blog_name";a:1:{i:0;s:11:"FOOD & DIET";}s:26:"_related_blogs_1_blog_name";a:1:{i:0;s:19:"field_5adf1bd25a8d9";}s:32:"related_blogs_1_blog_description";a:1:{i:0;s:43:"Fruits that patients with Diabetics can eat";}s:33:"_related_blogs_1_blog_description";a:1:{i:0;s:19:"field_5adf1c515a8da";}s:30:"related_blogs_1_read_more_text";a:1:{i:0;s:9:"Read More";}s:31:"_related_blogs_1_read_more_text";a:1:{i:0;s:19:"field_5adf1c765a8dc";}s:25:"related_blogs_2_blog_name";a:1:{i:0;s:14:"STAYING ACTIVE";}s:26:"_related_blogs_2_blog_name";a:1:{i:0;s:19:"field_5adf1bd25a8d9";}s:32:"related_blogs_2_blog_description";a:1:{i:0;s:45:"Exercise tips for people with type-2 diabetes";}s:33:"_related_blogs_2_blog_description";a:1:{i:0;s:19:"field_5adf1c515a8da";}s:30:"related_blogs_2_read_more_text";a:1:{i:0;s:9:"Read More";}s:31:"_related_blogs_2_read_more_text";a:1:{i:0;s:19:"field_5adf1c765a8dc";}s:10:"sub_videos";a:1:{i:0;s:1:"0";}s:11:"_sub_videos";a:1:{i:0;s:19:"field_5b02b58e23a09";}s:26:"_yoast_wpseo_content_score";a:1:{i:0;s:2:"30";}s:18:"_yoast_wpseo_title";a:1:{i:0;s:65:"Diabetic Laboratory test | Diabetic Care | Diagnosis | Dr Mohan's";}s:21:"_yoast_wpseo_metadesc";a:1:{i:0;s:280:"Are you looking for the best clinics for a diabetes laboratory test? Contact Dr. Mohan's healthcare centres for world-class diabetes testing laboratories across India. We have over 37 centers spread across the nation with branches in Chennai, Bangalore, Hyderabad and Bhubaneswar.";}}}