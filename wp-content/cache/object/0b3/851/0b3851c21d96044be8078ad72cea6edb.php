�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6120";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-07-12 11:48:44";s:13:"post_date_gmt";s:19:"2019-07-12 06:48:44";s:12:"post_content";s:10090:"<!-- wp:paragraph -->
<p>The three words, “You have
diabetes”, usually have a profound effect on the individual, young or old,
especially the former. People tend to go through myriad emotions, ranging from
guilt, confusion, fear, anger, self –pity and confusion, to even, relief,
ultimately. Some people tend to blame themselves, for their poor food habits or
erratic lifestyle, and stress. Some people even feel that its life’s way of
punishing them for some wrong –doing. Of course, one has the right to be on an
emotional ‘see-saw’, with the onset of diabetes, since it is a major life
change like any other.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;Onset of diabetes
has many implications due to its chronic and indefinite nature. Although
diabetes is a clinical disorder, it has a psychological impact on the
individual due to nature of the illness, rigorous dietary restrictions and
complex treatment regimen. Diagnosis of diabetes maybe viewed as a three stage
process:</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li>Prior
health status - good, generally not on much medication.</li></ol>
<!-- /wp:list -->

<!-- wp:list -->
<ul><li>Insidious onset of diabetes and one’s reactions to it have been likened to the four stages of grief, viz.,</li><li> &nbsp;<strong>Denial</strong>: It is a way of coping with something when a person is not ready to face it. It is the opposite of acceptance. Hence, one tries to underplay it – “I’m on the borderline”, “Diabetes is not a big deal’, “The tests are wrong”, and so on. </li><li> <strong>Anger</strong>: “Why me” comes to the fore here, along with, “I do not deserve this”, “I have not hurt anybody”, “Why bother, I’m going to get complications anyway”. There is a feeling of injustice, and of life being unfair. </li><li> <strong>Depression</strong>: &nbsp;The individual feels overwhelmed by the many changes they have to make to their lifestyle. They feel that the joy has gone out of their lives. Fear of taking insulin, even if it is only at sometime in the future, sets in. Depression is usually characterized by insomnia, loss of appetite, mood instability, fatigue, and loss of interest in usual activities. If symptoms persist, social support and therapy may be necessary. </li><li> <strong>Acceptance</strong>: The final stage is acceptance, when the individual comes to terms with reality, accepts he has diabetes and learns to manage it. It is the opposite of denial, in other words. </li><li> Learning self management and coping skills. </li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Initially, it often takes
time to come to terms with diabetes, from several months to even one or two
years, occasionally. It is easier to be aware of one’s physical and emotional
limits, and be realistic about one’s inner feelings about diabetes. Take things
slowly and do not push yourself or let others push you. When one is in denial,
or simply not wanting to acknowledge diabetes, sugars can shoot up due to non
–compliance with diet, exercise and medication.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Those who have not been
seriously ‘sick’ before, experience a sense of loss of previous good health.
There is a sense of permanence and powerlessness. Compounded to that, anxiety
of family members, and well meaning friends and relatives, who are willing to
give a lot of free advice (Take bitter gourd juice and fenugreek seeds first
thing in the morning, Do not take medicines as it will damage the kidney, Try
acupuncture, If you start insulin, it’s for a life time, so avoid it….),
confuses and stresses the individual even more. Knowledge and understanding
about diabetes helps in coping with the daily challenges of managing diabetes
and a sense of being in ‘control’.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>However, despite ones best
efforts, the fight with diabetes can often appear like a losing battle,
sometimes, both to the individual and the medical team. Some patients are hard on
themselves, which can be self-defeating and frustrating. This is because they
are trying everything to keep their sugars under control, but with age,
duration, severity, and a host of other clinical factors beyond their control,
it becomes a huge challenge.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Stress and non-compliance
are the two main culprits that act as barriers to effective treatment of
diabetes. While stress is beyond one’s control (sometimes), compliance, to a
certain extent, is not. Rigidity of habits and unhealthy lifestyle can be a
huge deterrent, too. We often find patients unwilling to change, say, their
diet pattern, timing or lifestyle, simply because they do not want to let go,
or for some strange reason, feel they are being disloyal to their ingrained
values, customs and traditions. E g. fasting.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Many a time, stress is
self-imposed, usually due to impractical thinking, high expectations and
wanting to be in control. More flexibility in thinking, and adaptation to
situations, as and when they come, can help reduce one’s stress. However,
sometimes, stress can be genuine and beyond one’s control, as in having
financial problems, relationship issues, job pressures, ill health, going
through an emotional crisis, and so on. At such times, one uses coping
strategies, but unfortunately, the sugars predictably shoot up. Professional
help for those who are overwhelmed and unable to cope would be a good idea.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Apart from the pressures of
dealing with situational stress of this kind, those with diabetes also have to
manage their sugars – by eating on time, the right foods, taking their
medication, and doing regular physical activity. Any deviations from the
regimen could result in hyperglycemia or even hypos. This results in a rush to
the doctor/ lab, to check if everything is in order. Patients run through a gamut
of emotions at the hospital, ranging from fear, guilt, sadness, hopelessness.
There is a lot of anguish for putting their families through financial
constraints and emotional distress when they fall ill, or have to be dependent on
others. At such times, social and emotional support can help bolster one’s
flagging morale.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Acceptance of diabetes is
the first step to controlling it. It is more helpful to accept the things that
one cannot change than trying to resist it, or keep complaining to aggravate the
problem. Listed below are some coping strategies to help the newly diagnosed to
manage diabetes effectively:</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li> <strong>Commitment</strong>:      Make a commitmentto manage      your diabetes. Learning more about diabetes can give one a feeling of      empowerment. Learn the basics of diabetes, and follow the rules of healthy      eating and physical activity, apart from prescribed medication. </li><li><strong>Stress Management</strong>: Stress may lead to      lack of compliance and self care, paving the way for high sugars.      Simultaneously, stress hormones (in response to prolonged stress), oppose      the action of insulin from being released, thereby aggravating the      problem. Prioritizing one’s work and schedule, time management, and de-      stressing through yoga, some sport or hobby helps in compartmentalizing      one’s life.</li><li><strong>Lifestyle changes: </strong>Make necessary changes      to one’s lifestyle to achieve good results, as far as diabetes is      concerned. It may be a gradual process, but pays off in the long run.      Adaptation is the key word here.</li><li><strong>Positive acceptance: </strong>Try and replace      negative, self-defeating thoughts with more positive, proactive ones.      Staying positive helps in avoiding resentment and depression, and      maintaining good health and mental well-being. Acceptance does not mean      ‘surrendering’ to diabetes or feeling resigned. It’s a matter of choice,      which will help you take care of it.</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>At the end of it all, one
wonders whether there is some specific formula to manage one’s diabetes
perfectly, but on the brighter side, readiness to change, self-motivation, a
sense of personal choice, and use of problem solving techniques can make one
feel more in control of one’s health, and improve one’s quality of life, in the
long run. Presently, there are many mobile applications and technological
devices that help in diabetes management.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Peter Asperella, in his
fictional work, “Good Like This”, has neatly summed up the protagonist’s
feelings about diabetes thus, “It’s not a horrible disease, it’s not something
I have to conquer or destroy, it’s not an obstacle that I have to surpass.
Maybe it’s just something I learn to live with happily, like a friend”. </p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":6121,"align":"center"} -->
<div class="wp-block-image"><figure class="aligncenter"><img src="https://drmohans.com/wp-content/uploads/2019/07/tree.jpg" alt="" class="wp-image-6121"/></figure></div>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Individuals with a
healthy lifestyle and good compliance remain ‘rooted’, like the green tree.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Individuals with an
unhealthy lifestyle and lack of self care could end up weak and fragile, like the
brown tree.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Stay healthy and
‘green’.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Vidyulatha
Ashok, Psychologist</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Dr Mohans
Diabetes Specialities Center&nbsp; </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Gopalpuram</strong></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:22:"Acceptance of diabetes";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:24:"acceptance-of-diabetes-2";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:58:39";s:17:"post_modified_gmt";s:19:"2019-08-29 11:58:39";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=6120";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}