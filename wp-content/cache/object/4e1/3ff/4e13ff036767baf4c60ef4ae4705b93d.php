�b^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:5966;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-06-24 11:22:19";s:13:"post_date_gmt";s:19:"2019-06-24 06:22:19";s:12:"post_content";s:9040:"<!-- wp:paragraph -->
<p><strong>Dr. ASWIN MUKUNDAN, MD</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="mailto:draswin@drmohans.com"><strong>draswin@drmohans.com</strong></a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diabetes and kidney diseases have age old association as more than 50% among the diabetic patients are prone to devolop kidney disease over time. It is important to know how to prevent it from occuring, how to halt the progression and how to detect early. In modern medicine, it is really surprising to see that we can prevent complications from occuring rather than treating the complications after it has happened. The money spent for treating the complications can be saved by doing simple tests and routinely visiting your doctor. A penny spent today for keeping your blood sugar values normal, is lakhs saved in the future. So in other words this is the best saving option for you, if you are working day in and day out trying to earn more.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Who all are at increased risk?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diabetes and kidney disease can literally affect any diabetic patient. But some are at more risk than others.</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<ol style="padding: 0 0 0 7%;"><li>Pateints with uncontrolled diabetes.</li><li>Patients with  history of diabetes and diabetic kidney disease in family.</li><li>Patients with other associated conditions such as hypertension        along with diabetes.</li><li>Patients taking pain killers such as  NSAIDs for longer periods.</li></ol>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Creatinine an accurate marker for kidney diseases?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We all might have heard about a blood test called serum creatinine measurement for kidney function and we have got it checked also at one point or the other, if not for us, for our relatives or friends. Is it really that important to measure creatinine for looking at the kidney function? Is it accurate?  Is there any alternative better methods? If yes then why doing creatinine to know the renal function? We will look into it one by one.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>What is creatinine?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Creatinine is a waste product in your body majorly produced from the muscles. It is normally filtered by your kidneys to excrete through urine after filtration. The usual normal value of serum creatinine is 0.8-1.2 mg/dl, though the reference values can vary slightly from lab to lab. It can be falsely low in frail elderly and people with lean body mass, even with a deranged kidney function. So there are fallacies in measuring serum creatinine.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Is it important to measure serum creatinine?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Yes, it is important. It gives us a gross idea of your kidney function and if raised gives us an alarm to consult a nephrologist. It also gives us a chance to measure your eGFR (estimated Glomerular Filtration Rate) indirectly, which in turn can help the doctor in determining the drug doses to be adjusted and avoid certain medication if the eGFR is reduced to a certain extent.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>When to measure?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The serum creatinine should be measured for all diabetic patients , especially type 2 diabetes, visiting the doctor for the first time or during diagnosis. If normal, it should be repeated at regular intervals, ideally every 3-6 months or more frequently during every visit if found abnormal. It should be measured during any acute conditions such as infections or heart conditions or anything requiring a hospital admission. It should also be measured before any radiological studies are done with contrast being injected. The symptoms you might experience are decreased urine output, swelling of lower limbs, nausea, vomiting, lethargy etc. if the creatinine is abnormally high. Watch out for these symptoms if you have long standing diabetes and your sugars are not under control.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Preparations before the test:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Usually there is no specific preparation for the test. Your blood sample is drawn from your vein irrespective of the meals consumed and is processed. Results can be published in no time with good accuracy , thanks to the most modern machines. Try to alert the doctor if previous abnormal values were detected as it can be used for comparison and see the trend if its increasing or decreasing.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Problems concerning the test:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The serum creatinine can vary between individuals depending on their race, age, sex or build. Healthy men are found to have a higher value than age matched women and younger individuals with more muscle mass are found to have a higher creatinine than an old patient with low BMI (Body Mass Index). One more very important thing concerning the test is that the serum creatinine is usually a late marker to rise in chronic kidney disease. It can be normal even when a major part of the kidney is damaged or non functioning. For example you can have a normal serum creatinine levels even after donating your one kidney. The remaining part of the kidney can over work and keep the serum creatinine on check.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Is it still advisable to measure serum creatinine. If so, why?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As told earlier, it is a cheaper screening test for renal functions. If raised we can intervene with the help of a nephrologist and get it back to normal, if its an acute kidney injury. We can not give certain diabetic medications if creatinine is derranged and we have to reduce the dose of insulin if patient is already on insulin. Usually we switch over to ultra short acting insulin and avoid longer acting ones in diabetic patients with raised creatinine as the half life of insulin increases exponentially with raised creatinine values. It is an easier and cheaper alternative for regular follow up in pateints with altered renal functions, especially for the people on renal replacement therapy such as hemodialysis etc. You have to avoid certain medications if you have raised creatinine levels as many drugs are excreted via kidney and metabolised in the kidney. Your doctor will know about it and he will adjust the dose of medications when required or completely avoid certain medications which can further damage the kidneys.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>What are the better alternatives?</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>There are lot better alternatives for measuring the renal function which includes measuring Cystatin C, Direct measurement of GFR, Urine ACRor microalbuminuria etc. Few of them are costly but gives more accurate measurement of kidney function. Among this, the test for microalbuminuria is worth mentioning as it can detect micro gram amounts of albumin getting leaked in urine. In simple terms our kidneys are like filters which filters out the waste products through urine. So if the whole filter is damaged, we have to replace the filter externally by dialysis or renal transplantation preferrably a dual kidney-pancreas transplantation for diabetic patients. But this new test helps us detect the renal damage in a very early stage. The surprising element is, we can still reverse the albumin leak by treating with specific measures and tablets and keep it completely normal. This can help us protect your kidneys from going to irreversible damage. Important measures are to keep your blood sugars under strict control and your blood pressure normal and also with tablets of the class ACE Inhibitors or ARB’s. Be on regular follow up with your doctor and we can almost reverse the minor damages or insults your kidney has faced.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>TAKE HOME MESSAGE:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Serum creatinine measurement is still an important test for diabetologists which has to be repeated in regular intervals as adviced by your doctor. It helps in chosing the mode of treatment and dose of medications. Better alternatives are available for early detection and prevention to protect your kidneys. So always remember, prevention is the best treatment for diabetes and its complications!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Dr. ASWIN MUKUNDAN, MD</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Consultant</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Calicut , DMDSC</strong></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:38:"Busting the Diabetes and Kidneys Myths";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:17:"busting-the-myths";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:59:20";s:17:"post_modified_gmt";s:19:"2019-08-29 11:59:20";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=5966";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}