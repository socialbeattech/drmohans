�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"1042";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-22 12:29:57";s:13:"post_date_gmt";s:19:"2018-05-22 12:29:57";s:12:"post_content";s:5831:"“I have no appetite in the morning!”, “I want to reduce my weight…so I am skipping breakfast”, “I’m too busy!” – We see people coming up with reasons like this and rushing off to deal with the world on an empty stomach. We hear all of these excuses a million times. Read on and find out the why, when, and what, on the importance of this delicious and nutritious daily ritual.

As we all know, breakfast is a meal preceding lunch or dinner and usually eaten in the morning. Today, however, hampered by busy morning schedules, some people delay their breakfast or skip it entirely. Eating breakfast might have beneficial effects on appetite, insulin resistance and energy metabolism.

Skipping breakfast doesn’t make anyone thinner. There is no evidence that skipping meals will help in losing weight. In fact, most people who skip breakfast tend to load up on more calories later in the day, which is rather unhealthy. Even while following a diet, remember to eat your breakfast. Researchers say that eating a good breakfast can protect against weight gain, diabetes, and heart diseases. Studies have shown that those who eat breakfast regularly are less likely to be overweight than those who regularly skipped it. They also indicate a direct link between breakfast consumption and weight management. Those who successfully lost excess body weight and then maintained a healthy weight regularly ate breakfast. As breakfast-skippers are more likely to have insulin resistance, it illustrates that skipping breakfast could be a risk factor for diabetes and cardiovascular diseases.

Breakfast makes people perform better during the rest of the day. Having a healthy breakfast makes children more alert in their studies and enables them to perform other activities well. According to the American Dietetic Association, children who eat breakfast are more likely to have better concentration, problem solving skills and hand-eye coordination. They may also be more alert, creative and less likely to miss days of school. Children who eat breakfast were found to be more productive during morning and even had faster reaction times. Hence, parents should remember to give their children a healthy breakfast in the morning before sending them to school.
<h5>Some ways to have a healthy breakfast:</h5>
<ul>
 	<li>Cereals and fruits are ideal for breakfast and for overall health.</li>
 	<li>Build a breakfast around foods that take little preparation time.</li>
 	<li>Make a variety of dishes with idli or dosa batter if feeling bored with the same recipe.</li>
 	<li>Try to choose foods from at least two or more food groups. Protein foods take longer to digest and will provide sustained energy and keep one feeling full longer.</li>
 	<li>Enrich your breakfast by incorporating colourful vegetables.</li>
</ul>
“No time” is no excuse; Time is a luxury for most of us, however, it pays to make time for what may be the most important meal of the day.
<h5>Diabetes Management...✍</h5>
Explore the nutrients
<h5>MAGNESIUM RICH FOODS</h5>
<ul>
 	<li>Cereals : Bajra, jowar, whole wheat, buckwheat, wheat germ, wheat grain bread.</li>
 	<li>Pulses : Bengal gram whole, black gram whole, cow pea, white soya bean</li>
 	<li>Leafy vegetables: Paruppu keerai, spinach, amaranth</li>
 	<li>Other Vegetables: Onion stalks, plantain flower, cluster beans, bitter gourd, ladies finger, ridge gourd</li>
 	<li>Nuts &amp; oil seeds: Almond, walnut, peanut</li>
 	<li>Condiments &amp; spices : Green chillies, cumin seeds, ginger, poppy seeds, turmeric</li>
 	<li>Fruits: Cape gooseberry, jambu fruit, muskmelon, avocado</li>
</ul>
<h5>CALCIUM RICH FOODS</h5>
<ul>
 	<li>Cereals: Ragi, whole wheat flour</li>
 	<li>Pulses: Bengal gram whole, black gram, horse gram, rajma, soya bean</li>
 	<li>Leafy vegetables: Amaranth, curry leaves, drumstick leaves</li>
 	<li>Other Vegetables: Cluster beans, chowchow, beans, kovai, sundakai</li>
 	<li>Condiments &amp; Spices: Asafoetida, dry cloves, cumin seeds, pepper, gingelly seeds, mustard seeds</li>
 	<li>Fruits: Amla, guava, lemon, sweet lime, orange, tomato</li>
 	<li>Milk products: Skimmed milk, cheese, skimmed curd.</li>
</ul>
<h5>IRON RICH FOODS</h5>
<ul>
 	<li style="list-style-type: none">
<ul>
 	<li>Cereals: Whole wheat, rice flakes</li>
 	<li>Pulses: Bengal gram roasted, soya bean, dry peas, green gram whole, horse gram</li>
 	<li>Leafy vegetables: Amaranth, mint leaves, carrot leaves, celery, manathakali leaves, turnip, greens, radish leaves</li>
 	<li>Other Vegetables: Onion stalks, cow pea pods, dry sundakai, double beans, bitter gourd, green plantain, tomato, green, plantain flower, snake gourd
<ul>
 	<li>Condiments &amp; spices: Asafoetida, cumin seeds, black pepper, fenugreek seeds, turmeric</li>
 	<li>Fruits: Peaches, gooseberry, guava, watermelon</li>
 	<li>Nuts &amp; Oil seeds: Almond, gingelly seeds, mustard seeds.</li>
 	<li>Milk products: Skimmed milk powder</li>
</ul>
</li>
</ul>
</li>
</ul>
&nbsp;
<h5>ANTIOXIDANT RICH FOODS</h5>
<ul>
 	<li style="list-style-type: none;">
<ul>
 	<li>Vitamin C: Amla, cape gooseberry, lemon, sweet lime, straw berries, green pepper, broccoli, agathi, amaranth (red and green), drumstick leaves, coriander leaves, turnip greens, raw cabbage (dark green cabbage).</li>
 	<li>Vitamin E: Wheat germ, almond, peanut butter, whole grains (wheat), sunflower oil and olive oil</li>
 	<li>CAROTENE: Agathi, amaranthus, coriander leaves, curry leaves, drumstick leaves, fenugreek leaves, gogu, lettuce, mint, spinach, carrot, broccoli, giant chillies, pumpkin, red cherries, guava, jambu fruit, orange, papaya, tomato, watermelon</li>
 	<li>Selenium: Wholegrain, Garlic.</li>
 	<li>Cryptoxanthins : Red capsicum, Pumpkins</li>
</ul>
</li>
</ul>";s:10:"post_title";s:28:"Basic Truths About Breakfast";s:12:"post_excerpt";s:107:"Researchers say that eating a good breakfast can protect against weight gain, diabetes, and heart diseases.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:28:"basic-truths-about-breakfast";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-22 09:46:38";s:17:"post_modified_gmt";s:19:"2018-12-22 09:46:38";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=1042";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}