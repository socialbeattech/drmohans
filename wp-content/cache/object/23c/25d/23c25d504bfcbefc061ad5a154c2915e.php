f^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6056;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-07-04 14:56:49";s:13:"post_date_gmt";s:19:"2019-07-04 09:56:49";s:12:"post_content";s:5027:"<!-- wp:paragraph -->
<p>Blood pressure is determined both by the amount of
blood your heart pumps and the amount of resistance to blood flow in your
arteries. The more blood your heart pumps and the narrower your arteries, the
higher your blood pressure.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hypertension can be asymptomatic. Even without symptoms, damage to blood vessels and heart continues and can be detected.  The risk of hypertension in diabetes is the most common co-morbid condition present. It is a major risk factor for both the microvascular as well as macrovascular complications of diabetes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Relationship between Hypertension and Diabetes</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hypertension is an important correlate of metabolic syndrome. Hypertension in diabetes increases the risk of developing high blood pressure and other cardiovascular problems, because diabetes adversely affects the arteries, predisposing them to atherosclerosis (narrowing of the arteries). Concomitant hypertension increases the risk of left ventricular hypertrophy, cardiac failure, stroke, peripheral vascular disease, renal dysfunction and retinopathy. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Types of
Hypertension</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Essential hypertension.</strong> This is a most common type of hypertension with no symptoms, some experience of frequent headaches, tiredness, dizziness, or nose bleeds. Although the cause is unknown, the research has shown that obesity, smoking, alcohol, high-calorie diet, and heredity plays an important role in essential hypertension.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Secondary hypertension.</strong> This type of high blood pressure, called secondary hypertension, tends to appear suddenly and cause higher blood pressure than does primary hypertension. Various conditions and medications can lead to secondary hypertension, the risks of hypertension include:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Obstructive sleep apnea</li><li>Kidney problems</li><li>Adrenal gland tumors</li><li>Thyroid problems</li><li>Certain defects in blood vessels you're born with
(congenital)</li><li>Certain medications, such as birth control pills,
cold remedies, decongestants, over-the-counter pain relievers and some
prescription drugs</li><li>Illegal drugs, such as cocaine and amphetamines</li><li>Alcohol abuse or chronic alcohol use</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>&nbsp;<strong>Staging
of hypertension </strong></p>
<!-- /wp:paragraph -->

<!-- wp:table -->
<table class="wp-block-table"><tbody><tr><td>
  &nbsp;
  </td><td>
  <strong>SYSTOLIC
  BP(mmHg)</strong>
  </td><td>
  <strong>DIASTOLIC
  BP(mmHg)</strong>
  </td></tr><tr><td>
  Normal
  </td><td>
  &lt;120
  </td><td>
  &lt;80
  </td></tr><tr><td>
  Prehypertension
  </td><td>
  120-139
  </td><td>
  80-89
  </td></tr><tr><td>
  Stage 1 hypertension
  </td><td>
  140-159
  </td><td>
  90-99
  </td></tr><tr><td>
  Stage 2 hypertension
  </td><td>
  &gt;160
  </td><td>
  &gt;100
  </td></tr></tbody></table>
<!-- /wp:table -->

<!-- wp:paragraph -->
<p>Systolic BP &gt;140 mmHg is a
more important risk factor for cardio vascular disease (CVD) than diastolic BP.
The risk of CVD doubles with increment of 20/10 mmHg beginning at 115/75mmHg. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Risk factors for hypertension </strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Age</li><li>Race</li><li>Family history</li><li>Being overweight or obese</li><li>Not being physically
active</li><li>Using of tobacco</li><li>Too much salt (sodium) in
the diet</li><li>Stress</li><li>Drinking too much alcohol</li><li>Certain chronic
conditions such as diabetes, kidney disease, sleep apnea</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Prevention </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lifestyle changes can help you control and prevent high blood pressure,
even if you're taking blood pressure medication. Here's what you can do:</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Eat
healthy foods</li><li>Decrease
the salt in your diet</li><li>Maintain
a healthy weight</li><li>Increase
physical activity&nbsp; </li><li>Limit
alcohol&nbsp;</li><li><strong>&nbsp;</strong>Don't
smoke&nbsp;</li><li>Manage
</li><li>Monitor
your blood pressure at home&nbsp;</li><li>Practice
relaxation or slow, deep breathing&nbsp;</li><li><strong>&nbsp;</strong>Control
blood pressure during pregnancy&nbsp;</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>Sticking to lifestyle changes can be difficult, especially when there are
no symptoms of high blood pressure. Motivate yourself, by remembering the risks
associated with uncontrolled high blood pressure. It would help to enlist the
support of family and friends as well.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:42:"RISK OF HYPERTENSION FOR DIABETIC PATIENTS";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:42:"risk-of-hypertension-for-diabetic-patients";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:53:08";s:17:"post_modified_gmt";s:19:"2019-08-29 11:53:08";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=6056";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}