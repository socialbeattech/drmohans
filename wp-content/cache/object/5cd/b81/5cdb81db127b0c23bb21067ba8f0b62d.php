�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1136;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-24 10:33:17";s:13:"post_date_gmt";s:19:"2018-05-24 10:33:17";s:12:"post_content";s:3791:"<h5>Key Points:</h5>
HbA1c test is a simple and effective tool in diabetes monitoring.
Higher the HbA1c value is, poorer is the diabetes control and also greater is the risk of heart, kidney, nerve and eye damage.
One can lead a longer and a happier life if tested for HbA1c regularly

With India experiencing a diabetes epidemic of sorts, the risk of diabetes-related complications can be prevented with good management of blood glucose levels, blood pressure and blood lipids. HbA1c test is a simple and effective tool in diabetes monitoring. When a part of the blood glucose combines with hemoglobin, it forms a complex called glycosylated hemoglobin, also called as HbA1c or A1c value which is usually expressed as a percentage. The amount of HbA1c is directly related to the average concentration of glucose in the blood. The HbA1c value is fairly stable for a period of two to three months and therefore is an indication of the long-term diabetic control over the past two to three months.
<h5>IMPORTANCE OF HbA1c:</h5>
A recent study revealed that many patients do not know the results of their HbA1c test. This is unfortunate because knowing the value of HbA1c is just one more way that patients can participate in their care and make sure that the sugar levels are under good control. People with diabetes who have a test result that is greater than 8% need to work with their healthcare provider to change their treatment plan. When people with diabetes have a test result that is less than 7%, their treatment plan is probably working and it is likely that their blood sugar is under good control.

A normal person usually has an HbA1c value between 4-6%. However, this value is expected to be higher than 6% for a diabetes patient. Higher the HbA1c value, poorer is the diabetic control and also greater is the risk of heart, kidney, nerve and eye damage. Therefore, while any decrease in HbA1c level is good for your long-term health, you should aim for a value below 7% for a longer and happier life.

There are many benefits of monitoring diabetes based on the HbA1c value:

Long-term picture: HbA1c value indicates the diabetes control status over the last 60-90 days, as against just a few hours like in the case of a glucose test. Moreover, the HbA1c value does not fluctuate. Therefore, doctors find it easier to determine the long-term effects of therapy based on your HbA1c test report.

Fasting not required: The HbA1c test can be done anytime. No fasting is required before you give your blood samples.

Not affected by just administered drugs: Unlike a glucose result, the HbA1c result is not affected by medication taken before the test. So there is no need to disturb the daily schedule of medicines.
<h5>To sum up, the HbA1c value is not affected by:</h5>
<ul>
 	<li>Time of the day at which blood sample is given</li>
 	<li>Meal intake at the time the sample is given</li>
 	<li>Exercise</li>
 	<li>Recently administered diabetes drugs</li>
 	<li>Emotional stress</li>
</ul>
But it doesn’t mean that the blood glucose tests are not important. HbA1c test gives a picture of the efficacy of long-term diabetes control. The blood glucose test indicates the current glucose level in the blood so as to help maintain normal blood glucose levels throughout the day. This helps in preventing hypoglycemia (lower level of glucose in blood than normal) and also to see how food, exercise and medicine are affecting blood glucose on a day-to-day basis. Keeping a track of HbA1c value at least 4 times a year, as recommended by American Diabetic Association (ADA), ensures that one takes the right medicines to control diabetes and avoid complications.

<strong>ONE CAN LEAD A LONGER AND A HEALTHIER LIFE IF TESTED FOR HBA1C REGULARLY!</strong>";s:10:"post_title";s:45:"Control Diabetes - Easy through HbA1c testing";s:12:"post_excerpt";s:65:"HbA1c test is a simple and effective tool in diabetes monitoring.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:49:"control-diabetes-it-is-easy-through-hba1c-testing";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-22 09:54:03";s:17:"post_modified_gmt";s:19:"2018-12-22 09:54:03";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=1136";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}