�b^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6221;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-09 22:15:12";s:13:"post_date_gmt";s:19:"2019-08-09 17:15:12";s:12:"post_content";s:5173:"<!-- wp:heading -->
<h2>As World Diabetes Day comes up, here’s how India can protect its people in the time of growing non-communicable diseases</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Till the 1990s, the majority of deaths in India were due to communicable diseases. Only Kerala and Goa showed a different trend, with more than 50 per cent of the deaths resulting from non-communicable diseases (NCDs) like diabetes, hypertension, coronary artery disease or cancer.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>But by 2016, all the States of India showed NCDs contributing to over 50 per cent and, in some States, up to 75-80 per cent of all deaths. This dramatic change in disease patterns results from a combination of globalisation, urbanisation, and an improved socio-economic status in our country. Diabetes Mellitus alone contributes to a good measure of all NCDS, as it is among the easiest to monitor.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The epidemiology of diabetes has undergone three important changes in India, over the last 2-3 decades. Earlier, diabetes was considered a disease of the rich. Today, it affects the middle class and the poorest of the poor. This has serious implications because it is estimated that 25-30 per cent of a poor person’s income can go towards the cost of treating diabetes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Besides, diabetes, a disease earlier associated with older people, now affects the youth and even adolescents and children. This is worrisome, because people run the risk of developing chronic complications of diabetes, like blindness, renal failure, amputation, heart attack and stroke by the age of 40 to 45 years, i.e, at the prime of life. Also, the epidemic of diabetes has now moved to the rural areas, due to improvement in socio-economic status, unhealthy diet and decreased physical activity. In some rural areas of India, even basic healthcare may be difficult to find.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>All these point to an urgent need for a diabetes prevention programme. The Government has a National Programme for Prevention and Control of Cancer, Diabetes, Cardiovascular Diseases and Stroke (NPCDCS). Also, Ayushman Bharat proposes to upgrade 150 thousand health and wellness centres to include diagnosis and treatment of NCDs like diabetes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We should ensure that people with diabetes have proper medical insurance. Today, most policies exclude people with pre-existing illnesses like diabetes. Incentivising people with diabetes to control it is one way to encourage them. For example, if the glycated haemoglobin (HbA1c) is brought down to normal, the insurance premium can be lowered, as the risk of complications comes down.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Today, we have a plethora of medicines in the market, both oral drugs (tablets) as well as different forms of insulin. Many of the newer ones undoubtedly have advantages. However, they come with a hefty price tag.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The good news is that even with the older drugs like the inexpensive metformin and sulhonylurea agents (especially the short acting ones), good control of diabetes can be maintained in the majority of patients.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We have shown that patients can live for 50 or more years with type 2 diabetes, without any complications. We also recently showed that people with diabetes live beyond 90 years of age. It would be tempting to suggest that, very soon, people with diabetes may actually outlive those without, because they look after their health better, with regular check-ups, better control of sugar, blood pressure and lipids, increased physical activity and healthy diet.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>But to achieve this, we all have to work together: medical professionals, the patient and family, society and the government and non-governmental organisations.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Prevention, first</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Our first goal should be to try to prevent diabetes. In those with pre-diabetes (the stage before one gets overt diabetes), we have shown that by diet and exercise, over 30 per cent can be prevented from getting diabetes. In those who already have diabetes, one should ensure that good control of diabetes is practised, right from the beginning, so that they do not develop its dreaded complications.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Finally, if people do develop complications, we should have enough specialised centres, which offer the best treatment. On the occasion of World Diabetes Day, I dream of a day when diabetes will no longer be considered a ‘silent killer’ and people can live a full and healthy life despite the disorder.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The writer is a leading Diabetologist and Founder of Dr Mohan’s Diabetes Specialities Centre. Views expressed are personal</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:55:"Diabetes control in line with changing disease patterns";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:55:"diabetes-control-in-line-with-changing-disease-patterns";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-09 22:28:33";s:17:"post_modified_gmt";s:19:"2019-08-09 17:28:33";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=6221";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}