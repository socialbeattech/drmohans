g^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6084";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-07-04 16:25:50";s:13:"post_date_gmt";s:19:"2019-07-04 11:25:50";s:12:"post_content";s:4561:"<!-- wp:paragraph -->
<p>Diabetes
is a metabolic cum vascular syndrome of multiple etiology characterized by
chronic hyperglycaemia with disturbances of carbohydrate, fat and protein
metabolism resulting from defects in insulin secretion, insulin action or both
leading to changes in both small blood vessels and large blood vessels </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Smoking &amp;
Diabetes</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cigarette smoking and exposure to tobacco smoke are
associated with premature death from chronic diseases, economic losses to
society, and a substantial burden on the health-care system.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;In fact,
smokers are 30–40% more likely to develop type 2 diabetes than nonsmokers.
Smoking makes the body more resistant to insulin, which can lead to higher
blood sugar levels.&nbsp; &nbsp;Irrespective of the
type of diabetes, smoking makes diabetes harder to control. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Risks of
smoking </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The dangers of smoking include the risk of potentially fatal diseases, such as <a href="http://www.healthline.com/health/heart-disease">heart disease</a>, <a href="http://www.healthline.com/health/copd">chronic obstructive pulmonary disease</a> (COPD), and many types of cancer.  Smoking and diabetes increases the risk of health complications even more.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Smoking
causes heart and blood vessel damage </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Smoking and diabetes
both increase the risk of heart disease in very similar ways, and so when
combined, they greatly exacerbate the chances of suffering a heart related
condition such as a heart attack or stroke. Both high levels of&nbsp;<a href="http://www.diabetes.co.uk/diabetes_care/Diabetes_and_blood_glucose.html">glucose in the blood</a>&nbsp;and smoking
damage the walls of the arteries in such a way that fatty deposits can build up
much easier. As this occurs, the blood vessels narrow and make circulating
blood much harder. When this happens to the coronary arteries (the arteries
that supply the heart muscle with blood and therefore oxygen) a heart attack
can occur. Similarly, a stroke is when not enough blood can get to the brain,
and so anything that may limit blood flow increases the risks of a stroke.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>High blood glucose levels also have an effect on the blood vessels and blood flow, so smoking increases the greater risk of suffering a heart attack or stroke.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Smoking
causes respiratory diseases </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Smoking directly affects the lungs and can lead to <a href="http://www.healthline.com/health/copd/understanding-chronic-bronchitis">chronic bronchitis</a>, <a href="http://www.healthline.com/health/emphysema">emphysema</a>, and other respiratory diseases. People with these diseases are at higher risk of developing lung infections, such as pneumonia. These infections can be especially dangerous for people with diabetes. Being sick also raises blood sugar levels. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Smoking
damages eyes</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>People with diabetes
also have a higher risk of several eye diseases, including&nbsp;<a href="http://www.healthline.com/health/cataract">cataracts</a>&nbsp;and&nbsp;<a href="http://www.healthline.com/health/glaucoma">glaucoma</a>. Poorly controlled diabetes can also lead to an eye condition
called&nbsp;<a href="http://www.healthline.com/health/type-2-diabetes/retinopathy">diabetic retinopathy</a>. Smoking can
accelerate the development of diabetic retinopathy and make it worse. This can
eventually lead to blindness.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Conclusion </strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Recognizing the dangers of smoking and the benefits of quitting is an important first step. Make an appointment with your doctor to learn about the treatment and support options that can help to quit for good. Avoiding tobacco products lowers the risk of complications from diabetes. It can help to limit the damage to organs, blood vessels, and nerves which helps to live a longer and healthier life.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:49:"Risk and dangers of smoking for diabetic patients";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:49:"risk-and-dangers-of-smoking-for-diabetic-patients";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:53:44";s:17:"post_modified_gmt";s:19:"2019-08-29 11:53:44";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=6084";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}