�e^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6321";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-09-25 12:06:14";s:13:"post_date_gmt";s:19:"2019-09-25 07:06:14";s:12:"post_content";s:12195:"<!-- wp:paragraph -->
<p><strong>Challenges of keeping up with type 1 diabetes in the family</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Type 1 diabetes diagnosis can be an earth shattering event for the entire family due to its sudden onset and continuous life changes. Apart from the child, the family members go through many ups and downs trying to cope with fluctuating sugars, adjustment of insulin doses, concomitant illnesses, leave of absence from school/college/work, and so on. Parents also need a support system to deal with the daily hassles of living with diabetes, coping strategies, handling volatile emotions, and stress that children go through due to juvenile diabetes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dhillipa, who has finished her engineering and is doing freelancing civil work now, articulately states, “I have been diagnosed with type 1 diabetes since I was 12. From day one, I was pretty adamant about type 1 diabetes treatments and decided to take care of it myself without relying on other people. I’ve got great family and great friends that want to help me and make my life easier; a lot of growth in saying ‘thats okay’.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The thing that drives me in a lot of ways the real life challenges of this disorder and ways to approach it to make it easier for me. I didnt know anything about diabetes before I was diagnosed. Its all about educating when you can and helping the overall cause. I think my diabetes has definitely made me stronger. Its a big contributor to my character as a person.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>[To kids diagnosed with type 1] <strong>Stay strong and know that its all going to be okay. There’s some comfort there knowing you’re not alone”.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Type 1 diabetes cannot be viewed from the physiological perspective alone, as there can be many psychosocial aspects too. Family outings and functions are severely compromised as the rich food inherent in Indian diets during such times may lead to high sugars. Constant vigilance during these occassions may not always be possibe, due to which&nbsp; there may be hospitalization, financial strain and the inevitable guilt the parent faces. This may lead to avoidance behaviour due to fear of recurrence and the agony the child may have to go as a result of ‘negligence’. Children feel shortchanged not because they fell ill but because they have been deprived of another outing.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Another working professional has this to say regarding type 1 diabetes, “Initially I was not able to accept the type 1 diabetes diagnosis and the fact that I had to undergo needle pricks 2-3 times a day. But I started getting used to it considering my high sugar levels. I feel our mental health needs to be healthy as well if we want to fight diabetes. I always keep myself occupied at work and stay active. My profession as a corporate trainer has helped me to fight negative thoughts and stay with positive vibes. I always had this in my mind- diabetes is not a disease, its a disorder<strong>. But I advise all type 1 diabetics to go for a regular diabetic check up once in every 3 months.”</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Caregiver burden of those who are taking care of individuals with chronic or life threatening illnesses is getting wide spread attention. Taking care of children from childhood to adolescence to adulthood can be a major source of stress for the parents. Children need constant monitoring till they are autonomous, while adolescents resent any kind of monitoring.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;Adolescence comes with its major share of mood swings due to hormonal changes, quest for independence, identity crisis, peer pressures, even without diabetes. Juvenile diabetes makes it all the more difficult to cope. Rebellious behaviour may lead to poor glycaemic control due to lack of dietary adherence, physical activity, and skipping of insulin doses. Eating disorders are common among girls who resist taking insulin for fear of weight gain. Parents should watch out for changes in dietary pattern, obsession with body image, etc and address the issue.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Vritti, who has finished her architecture and charting out her career now, has shared her experiences from childhood, “Diabetes has been my acquaintance for the last 15 years. Through years of highs &amp; lows, it has been a roller coaster ride. On a family trip to Sikkim, I could barely walk, when I was diagnosed with type 1 diabetes at the age of 9. My relationship with diabetes has been a rocky road but as I have grown older(maybe wiser) with the years, <strong>I have learned to make it my friend.</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>At 9 years of age, every moment like any other clueless child, I did not understand why I had all these big ‘No’s (No biscuits, cakes, ice cream etc) in my life,nor did I understand why I was relentlessly asked by my parents to exercise every day. But I learnt to live by these rules. My parents who were naturally upset by my condition would do everything for me, pushing me to exercise, have the right diet, etc.. I just thought that it doesnt matter if I sneaked in a few chocolates or biscuits or ice cream once in a while. Nobody would know of my secret. My sugar levels would be over the roof...and I’d act all surprised on my high sugar levels. This didnt mean being ‘bad’, this was just a way of coping with my emotions, which I felt were out of control.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As I entered college, in my twenties, I began to accept my condition as my parents could no longer hold the reigns to my journey, due to erratic hours, umpteen submissions, deadlines, and so on. Type 1 diabetes treatment was challenging. It was not easy as I was in denial mode all these years. My acceptance started with realisation and fair warning from my doctor. I had to take responsibility for my health and make diabetes my friend. I went for several trips within India and abroad due to my parents trust in me.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I still have my rainy days &amp; many days which are far from perfect, because there is no holiday from it. Its frustrating and pointless to go through so much difficulty. But the wisdom that comes from diabetes is profound. It teaches you a better sense of self control, disciplined lifestyle, good eating habits and overall good health. My deepest gratitude to Dr. Mohan &amp; the centre which taught me to be a fighter and be a survivor of diabetes, and for making me feel its a disorder, not a disease.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Despite my far from perfect sugar levels, Dr. Mohan has helped me remain optimistic. His faith in me gives me the motivation to fight. They say, a good doctor’s reassuring words are more powerful than any medicine; this is true in my case. Tough times dont last but tough people do. Diabetes is a challenge that has to be risen to every day. I remain thankful for my struggle because I would not have stumbled upon my strength<strong>. Life is where you look &amp; I think with acceptance, patience, positive attitude, journey of diabetes management just becomes easier.”</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Adulthood comes with a sense of maturity and a relative ease in managing one’s sugars compared to before, as we can see from Vritti’s words. For those who are thinking of getting married,&nbsp; pre-marital counselling  with the partner may be helpful in understanding the realities and challenges of a person living  with the disorder from a young age. Type 1 individuals, like Deepak, go on to get married after discussing thier health status with their partners, and are able to balance work and family life successfully.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Deepak, who is a regular at our centre and will do anything to lend a helping hand to others in a similar position, has this to say, “I’m glad to say I’m not a patient<strong>, I am a survivor</strong>, since diabetes really gives me lots of self confidence &amp; self-motivation. I had got type 1 diabetes diagnosis at the age of 13, when awareness was really poor. A neighbour told us about Dr. Mohan sir, who guided my parents, but parents are always parents; they cannot control their emotions when they see their children suffer. They think about the future. My parents are my backbones. My mother used to say these beautiful lines,</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>“If you can’t fly,then run,</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>If you can’t run, then walk,</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>If you can’t walk, then crawl,</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>But whatever you do,</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>You have to keep moving forward.”&nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>I follow this till now in my life. My inner talent has been established in Dr.Mohan’s hospital only. Every year World Diabetes day arrives, I give my dance performance. Later on, I slowly improve my dance moves &amp; slowly enter into big screen as a backstage dancer, now, I can proudly say that “I am a CHOREOGRAPHER”. Dr.Mohan’s is my mother home.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Now I am 33 years old, married, have 2 children, aged 7 years and 2.5 years. Now, I am working with private IT sector as manager and continue my passion(dance) too. I want to thank Mallika Mam, Poongothai Mam, Thangamani Mam, Jaishree Mam, Vidyulatha Mam. Special thanks to Dr.Mohan sir, Dr. Rema Mohan Mam, Dr.Unni sir, Dr. Anjana Mam.”</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Another professional, Harish Kumar, says, “I have been having diabetes for the past 10 years now. I work for an IT company. I depend on the external pancreas(automatic insulin pump). Major challenges I had so far were educating people about the difference between type 1 and type 2 and the common myth that they had.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Apart from that, I wasnt much aware of type 1 diabetes treatments and less educated. Self study about it helped me a lot in managing it now. <strong>Understanding ones body metabolism and being ones own doctor helped me achieve good glycaemic control.”</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As one can see, there is a good system of networking and support for type 1 diabetes children and parents. Friends Forever has been functioning as a support group for type 1 children since 2002 to avoid feelings of isolation, stepping up group activities and outings, to enhance a sense of camerederie amongst the children. Here is a personal tribute to all the children who face the challenges of diabetes daily:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No matter how high or low,</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Their sugars go,</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>They are far from slow,&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>And that we know.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Never far behind,</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In body or mind,</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>They are one of a kind.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>From the word “Go” ,</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>&nbsp;They go with the flow,&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>To&nbsp; become a&nbsp; ‘Super Hero”.&nbsp;<br></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>VIDYULATHA ASHOK.<br></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:59:"Challenges of keeping up with type 1 diabetes in the family";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:59:"challenges-of-keeping-up-with-type-1-diabetes-in-the-family";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-10-14 14:55:54";s:17:"post_modified_gmt";s:19:"2019-10-14 09:55:54";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=6321";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}