�c^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1262;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-25 10:14:34";s:13:"post_date_gmt";s:19:"2018-05-25 05:14:34";s:12:"post_content";s:4650:"Diabetic nephropathy (diabetic kidney disease) is one of the chronic vascular complications of diabetes, which tends to develop after several years of diabetes and results in progressive loss of kidney function. The overall risk of developing diabetic nephropathy varies between about 10% of Type 2 (non insulin dependent) diabetic individuals to about 30% of Type 1 (insulin dependent) diabetic individuals. The final stage of nephropathy is called end-stage renal disease, or ESRD. Diabetes is the most common cause of ESRD, accounting for 30-40% of all cases of ESRD.

Stages of Diabetic Nephropathy

Diabetic nephropathy usually progresses slowly through several stages. In Stage 1, there is elevation in the glomerular filtration rate (GFR) or hyper filtration of urine but no leakage of albumin or protein. In Stage 2, leakage of tiny amounts of protein or albumin is present, this stage is called stage of Microalbuminuria. In Stage 3 or Macro proteinuria stage, increasing proteinuria leading to loss of large amounts of protein, causing 'nephrotic syndrome' a condition defined by fluid retention and swelling due to low amounts of protein in the blood is observed. In Stage 4, the kidneys become less able to remove 'poisons' from the blood resulting in a build up in the levels of various chemicals such as urea and creatinine. This is known as 'Chronic Renal Failure'. Stage 5 is known as 'End Stage Renal Disease' (ESRD) and in this stage, the urine output decreases, serum creatinine levels go very high and there is an impending need for renal replacement therapy (RRT). Microalbuminuria in the range of 30299 mg/24 hour has been shown to be the earliest detectable and treatable stage of diabetic nephropathy and is also a significant marker for cardiovascular diseases in both diabetic and non-diabetic individuals.

Risk factors for diabetic nephropathy

Not everyone with long duration of diabetes gets nephropathy. In fact, over 50% of diabetic patients will never develop kidney disease even if their blood sugars are poorly controlled. There are many factors [modifiable and non modifiable] that affect the individual risk of developing diabetic nephropathy.

RISK FACTORS FOR DIABETIC NEPHROPATHY
<table>
<thead style="text-align: center;">
<tr>
<td style="border: 1px solid;"><strong>NON-MODIFIABLE</strong></td>
<td style="border: 1px solid;"><strong>MODIFIABLE</strong></td>
</tr>
</thead>
<tbody style="text-align: center;">
<tr>
<td style="border: 1px solid;">Genetic predisposition</td>
<td style="border: 1px solid;">High blood sugar level</td>
</tr>
<tr>
<td style="border: 1px solid;">Male gender</td>
<td style="border: 1px solid;">Elevated blood pressure</td>
</tr>
<tr>
<td style="border: 1px solid;">High dietary protein intake</td>
<td style="border: 1px solid;">High cholesterol</td>
</tr>
<tr>
<td style="border: 1px solid;">Long duration of diabetes</td>
<td style="border: 1px solid;">Smoking</td>
</tr>
</tbody>
</table>
<h5>Symptoms</h5>
There are no symptoms in the early stages of diabetic nephropathy. However, a vast array of signs and symptoms may manifest when kidney disease has progressed, which include:
<ul>
 	<li>Swelling (edema) of the feet and legs and later throughout the body</li>
 	<li>Weight gain due to fluid accumulation</li>
 	<li>Tiredness</li>
 	<li>Poor appetite</li>
 	<li>Nausea or vomiting</li>
 	<li>Itching</li>
 	<li>Increase in blood pressure</li>
 	<li>Troubled sleep and fatigue</li>
 	<li>Screening for diabetic nephropathy</li>
</ul>
Screening for nephropathy at its earlier stage of microalbuminuria, is important because it is reversible at this stage.
<h5>Screening schedule for diabetic nephropathy</h5>
<table>
<thead style="text-align: center;">
<tr>
<td style="border: 1px solid;"><strong>
Microalbuminuria spot
urine
</strong></td>
<td style="border: 1px solid;"><strong>Type 2 diabetes - at diagnosis</strong></td>
<td style="border: 1px solid;"><strong>Annually</strong></td>
</tr>
</thead>
<tbody style="text-align: center;">
<tr>
<td style="border: 1px solid;"></td>
<td style="border: 1px solid;">Type 1 diabetes - 3-5 years after diagnosis</td>
<td style="border: 1px solid;"></td>
</tr>
</tbody>
</table>
<h5>Steps to reduce risk and/or slow the progression of Nephropathy</h5>
<ul>
 	<li>Monitoring/screening for microalbuminuria</li>
 	<li>Optimize glucose control</li>
 	<li>Optimize blood pressure control</li>
 	<li>Angiotension-converting enzyme therapy</li>
 	<li>Controlling blood lipids and cholesterol</li>
 	<li>Modify diet</li>
 	<li>Cessation of smoking</li>
</ul>";s:10:"post_title";s:20:"Diabetic Nephropathy";s:12:"post_excerpt";s:70:"Diabetic nephropathy usually progresses slowly through several stages.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:20:"diabetic-nephropathy";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:52:06";s:17:"post_modified_gmt";s:19:"2019-08-29 11:52:06";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=1262";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}