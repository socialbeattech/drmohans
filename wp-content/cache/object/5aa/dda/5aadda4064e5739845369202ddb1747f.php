�e^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6366";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-10-11 11:33:00";s:13:"post_date_gmt";s:19:"2019-10-11 06:33:00";s:12:"post_content";s:4948:"<!-- wp:paragraph -->
<p><strong>Dr. V Mohan</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Chairman, Dr. Mohan’s Diabetes Specialities Centre</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Of the various complications of diabetes, visual
impairment is perhaps the most feared by people with diabetes. Diabetic
individuals are much more prone to visual disability than non-diabetic
individuals. The impact of visual impairment among diabetic individuals has
profound implications for the person affected and society as a whole. Visual
disability significantly affects the quality of life and increases the risk of
injury. The most distressing effects of diabetes in the eye with regard to
visual impairment are in the retina (inner layer of the eye) called as diabetic
retinopathy. Preventive approaches to diabetic retinopathy can be categorized
into three levels, the first is <strong>Primary
Prevention </strong>of retinopathy. This is an early approach where early detection
and appropriate treatment are the cornerstones for delaying the onset of the
diabetic retinopathy symptoms by good control of blood sugar, blood pressure
and lipids. It is mandatory that every type 2 diabetic individuals should have
a retinal examination at the time of diagnosis of diabetes and for type 1
diabetic subjects retinal examination should be done 5 years after diagnosis
and every year thereafter.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diabetic retinopathy classification can be
basically divided into two types – non-proliferative diabetic retinopathy and
proliferative diabetic retinopathy. Non-proliferative diabetic retinopathy is
the early stage of the disease and at this stage there are small balloon like
sacs (out pouching of capillaries) called as micro aneurysms. In the next stage,
these sacs leak fluid and blood into the retina. During this stage, no sight
loss is present. However, once diabetic retinopathy progresses, prevention of
the same would form the second approach or <strong>Secondary
Prevention</strong>. As part of secondary prevention one can prevent the progression
of this stage by advocating good control of diabetes in addition to documenting
the retinal changes by colour retinal photography. This is made possible by the
advent of the digital system of colour photography, which makes it easier for
the eye specialist to explain to the subjects about the retinal condition. As a
follow up, dilated retinal examination should be done more frequently at this
stage of the disease. As individuals may not have any diabetic retinopathy
symptoms, life-long evaluation for retinopathy is a valuable and necessary
strategy. It has been estimated that in whom eye examination had not previously
been performed, 50% subsequently had visual disability due to diabetic
retinopathy.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The two sight threatening forms of diabetic
retinopathy classification are diabetic macular edema (DME) and Proliferative
diabetic retinopathy. In DME there is collection of fluid in the critical
central region of the retina, called as the “macula” (the “seeing” portion of
the eye). In the second condition – Proliferative DR there are “New Vessels” on
the retina. These new vessels are outgrowths from the normal vessels and appear
in the retina due to lack of oxygen called as hypoxia. The problem with the new
vessels is that their walls are very friable and they have a tendency to bleed.
If this stage is not treated it may lead to gross visual disability.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Tertiary Prevention</strong> would be the late approach where complications
have reached a very critical stage and interventional procedures like laser
therapy and or surgical procedures are required to prevent the progression of
disease and stabilize vision. Laser therapy is a simple out-patient painless
procedure. If done in the proper way, the diabetic changes in the retina can be
completely reversed. After the procedure, regular monitoring is necessary as
diabetic retinopathy symptoms may recur being a systemic disease. Timely
treatment can prevent up to 90% of vision loss from diabetic retinopathy.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Other diabetes related eye problems are glaucoma
(damage to optic nerve fibres), senile cataract due to ageing (clouding of the
lens) and nerve palsies (affection of extra ocular muscles). People with
diabetes are twice likely to suffer from glaucoma than people without diabetes
and risk increases with age and duration of diabetes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Thus, people with diabetes can prevent/reduce
the occurrence of diabetic eye complications. Improved awareness, education,
screening and treatment, will go far toward preventing/reducing blindness due
to diabetic eye disease.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:20:"Diabetic Retinopathy";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:20:"diabetic-retinopathy";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-10-11 11:34:51";s:17:"post_modified_gmt";s:19:"2019-10-11 06:34:51";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=6366";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}