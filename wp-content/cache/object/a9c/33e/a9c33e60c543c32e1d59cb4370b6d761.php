�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"1019";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-22 12:09:32";s:13:"post_date_gmt";s:19:"2018-05-22 12:09:32";s:12:"post_content";s:6125:"<h5>DIABETES AND DEPRESSION - A CRY FOR HELP</h5>
“The mind is its own place, and in itself, can make heaven of hell, and a hell of heaven”. John Milton.
“Let’s talk”, launched on April 7th, 2017, with a spotlight on depression, seems to be a fitting theme on World Health Day. According to WHO statistics, nearly 350 million people suffer from depression worldwide and it is also the leading cause of disability. Those suffering from any chronic illnesses such as kidney and heart disease, lupus, HIV/AIDS, and, of course, diabetes, are more prone to depression.
Depression is twice as common in people with diabetes. The problem is bifacial; those with diabetes are at increased risk of developing depression, due to its chronic nature and subsequent complications, while those who are depressed are at risk of getting diabetes. The predominance of poor lifestyle decisions, unhealthy food habits, smoking and alcohol, less physical activity, and weight gain when one is depressed, are all risk factors for diabetes.
The rise in incidences of depression could be due to the pressures of modern living, materialism, a competitive environment, occupational and family demands. Physical and mental health are closely interlinked which is why diabetes and depression can be a double whammy. Depression can be attributed to many factors, such as genetic, brain biochemistry, stressful life events, trauma, and strained interpersonal relationships, apart from chronic stress.
Diabetes and depression can be likened to two sides of a coin, wherein there is a biological and behavioural link. On the one hand, the over-activation of stress hormones, such as cortisol and ACTH can aggravate sugar levels. On the other side of the coin, lack of self-care, which is invariable when one is depressed, can lead to poor health outcomes. Some individuals may get overwhelmed with the challenges of managing diabetes on a daily basis, which can lead to depression.
<h5>What is depression and what are the risk factors?</h5>
It is normal to feel grief at the loss of a loved one, or show emotional reactivity to some distressing situation, but when it takes longer than usual to return to normal, look out for warning signs. If it has been at least 2 months since a major life event has occurred, or if there has not been any such major life event but one is experiencing several of these symptoms, it may be DEPRESSION:-
<ul>
 	<li style="list-style-type: none;">
<ul>
 	<li>Feeling sad or empty most of the time for at least two weeks.</li>
 	<li>Diminished interest or pleasure in the usual activities.</li>
 	<li>Crying spells without reason.</li>
 	<li>Low self-esteem or feelings of guilt.</li>
 	<li>Difficulty in sleeping or excessive sleepiness throughout the day.</li>
 	<li>Poor appetite or eating excessively.</li>
 	<li>Unusual fatigue and loss of energy.</li>
 	<li>Difficulty concentrating on normal activities.</li>
 	<li>Feeling agitated, lethargic or slow.</li>
 	<li>Weight gain or weight loss, without any effort.</li>
 	<li>Recurrent thoughts of death or suicidal ideas.</li>
</ul>
</li>
</ul>
If these symptoms are generally making one feel dysfunctional by coming in the way of social and personal relationships and hindering one’s responsibilities at work, one could be depressed.
<h5>Risk factors for depression include:</h5>
<ul>
 	<li style="list-style-type: none;">
<ul>
 	<li style="list-style-type: none;">
<ul>
 	<li>Family history of depression</li>
 	<li>Abuse, either physical, sexual or emotional</li>
 	<li>Death or loss of a loved one</li>
 	<li>Conflict due to interpersonal relationships, outside or within the family</li>
 	<li>Major life events such as marriage, losing one’s job, divorce, relocating, etc.</li>
 	<li>Certain medications taken for other conditions may trigger depression.</li>
</ul>
</li>
</ul>
</li>
</ul>
Apart from these, studies have shown that women and older people are more vulnerable and likely to get depressed.
Those with diabetes should be screened for depression regularly, as it can largely go undetected. Hence, appropriate detection and early intervention will help resolve complex health problems. In the larger picture, self-management and good control of diabetes could decrease the risk of depressive symptoms and complications.
Despite so much progress and awareness regarding treatment of depression, prognosis continues to be poor, perhaps due to the following reasons:
<ul>
 	<li style="list-style-type: none;">
<ul>
 	<li style="list-style-type: none;">
<ul>
 	<li>Stigma regarding ‘mental illnesses’</li>
 	<li>Feelings of worthlessness and failure that prevents one from acknowledging that one is depressed.</li>
 	<li>Financial constraints that act as a barrier to effective treatment.</li>
 	<li>Negative perceptions about side effects of anti-depressants.</li>
</ul>
</li>
</ul>
</li>
</ul>
<h5>Management of depression:</h5>
Management of depression and diabetes should be a collaborative effort which involves the following aspects:
Professional help.
A combination of cognitive behavior therapy and medication has been found to be effective in combating depression.
Social support from family, friends and support groups help in lessening feelings of isolation.
Proper adherence to the diabetes regimen in the form of healthy diet, regular medication and physical exercise.
Regular assessments by the concerned physician as well as mental health professionals have the twin benefits of alleviating feelings of depression as well as controlling sugars.

Apart from all these, involving oneself in pleasurable activities and following a structured lifestyle will be greatly beneficial for those who are going through ‘low’ phases. However, despite one’s best efforts, sometimes it’s easy to be weighed down by lethargy and low energy levels due to diabetes and depression. So do set realistic goals for yourself, take small steps, stay motivated, and do not give up! As Margaret Thatcher put it, “You may have to fight a battle more than once to win it.”";s:10:"post_title";s:23:"Diabetes and Depression";s:12:"post_excerpt";s:95:"Diabetes and depression can be likened to two sides of a coin, wherein there is a biological...";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:23:"diabetes-and-depression";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-24 11:38:40";s:17:"post_modified_gmt";s:19:"2018-12-24 11:38:40";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=1019";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}