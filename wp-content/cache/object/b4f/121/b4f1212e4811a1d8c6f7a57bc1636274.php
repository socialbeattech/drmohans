�e^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6398";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-10-25 11:18:19";s:13:"post_date_gmt";s:19:"2019-10-25 06:18:19";s:12:"post_content";s:4297:"<!-- wp:paragraph -->
<p><strong>Dr.
Anjana receives&nbsp;the Sakunthala Amir Chand Prize for Young Scientist Under
40</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong><em>Dr. Harsh Vardhan,&nbsp;</em>Minister of Health and
Family Welfare<em>&nbsp;and&nbsp;Dr. Balram Bhargava,&nbsp;Director General of
Indian Council of Medical Research (ICMR) present the award to Dr. Anjana in
the Under 40 category&nbsp;</em></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Chennai, October 17<sup>th</sup>,2019</strong>: <strong>Dr. R M Anjana</strong>, Managing Director of Dr. Mohan’s Diabetes
Specialities Centre and Consultant Diabetologist &amp; Vice-President of Madras
Diabetes Research Foundation today received the&nbsp;<strong>Sakunthala Amir Chand
Prize for Young Scientist </strong>by the Indian Council of Medical Research (ICMR).
This is the highest award by ICMR to young scientists under the age of 40. She
was conferred the Award by <strong>Dr. Harsh Vardhan,&nbsp;</strong> Minister of Health
and Family Welfare, Government of India and <strong>Dr. Balram Bhargava,</strong>
Director General (DG), Indian Council of Medical Research (ICMR).&nbsp;The
Shakuntala Amir Chand Prize recognizing significant contributions to Biomedical
Sciences by young scientists was instituted by (late) Major General Amir Chand
in 1953. This award is presented for the best published research work in the
field of Biomedical Science including clinical research. Dr. Anjana was awarded
for her research work in ‘ <strong><em>Epidemiology of Diabetes and its risk factors
in India</em></strong>’.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Commenting on the occasion Dr. R M Anjana
said,“&nbsp;&nbsp;&nbsp;&nbsp;I am very humbled to receive this prestigious
award from ICMR and I thank my whole team from MDRF and DMDSC for all their
support. “&nbsp; Dr. Anjana has been a great
champion and a pioneer in the subject of Lifestyle Diabetes along with having
achieved laurels for her path breaking research in the field of Diabetology
right from her college days. She pioneered a concept of a high intensity
exercise suitable for adolescents in India using Bollywood dance called&nbsp; ‘THANDAV’
(Taking HIIT And Dance to Adolescents for Victory) which has won worldwide
appreciation . She has also contributed to several studies on Gestational
Diabetes Mellitus. Her most important contribution has been overseeing the
largest epidemiological study on Diabetes called as the ‘ICMR-India Diabetes
INDIAB Study’ which had produced authentic data on the prevalence of Diabetes,
Hypertension, Dyslipidemia and Obesity in the whole country.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>With over 210 research
papers being published having attracted over 23,000 citations with a H-index of 46, Dr. Anjana’s work has
been recognized&nbsp;by several national and international bodies.She has also
been conferred Fellowships by all the three Royal Colleges of UK namely FRCP
London, Glasgow and Edinburgh and also the Fellowship of American College of
Physicians.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>About Dr.
Mohan’s Diabetes Specialties Centre:</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dr. Mohan’s Diabetes Specialties Centre is a
diabetes speciality chain founded in the year 1991 headquartered in Chennai,
Tamil Nadu. It is India’s leading diabetes care provider offering comprehensive
services for diabetes patients. Dr. Mohan’s Diabetes Specialties Centres now
has 50 Diabetes centres and clinics in India and over 4.5 lakh diabetic
patients have been registered at these centres. The centre specializes in Total
Diabetes Care, Diabetes Eye Care, Diabetes Foot Care services, Diabetes Cardiac
Care, Diabetes Dental Care, Preventive Care, Diet counselling and an advanced
laboratory. Appointments can be booked by visiting <a href="www.drmohans.com">www.drmohans.com</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>For further details contact:
S.Siddharth Samson | brand-comm </strong><a href="mailto:|siddharths@brand-comm.com"><strong>|siddharths@brand-comm.com</strong></a><strong> |Mob no : 9600127829 </strong><strong></strong></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:51:"Dr. Anjana receives the Sakunthala Amir Chand Prize";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:50:"dr-anjana-receives-the-sakunthala-amir-chand-prize";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-10-25 11:18:51";s:17:"post_modified_gmt";s:19:"2019-10-25 06:18:51";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=6398";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}