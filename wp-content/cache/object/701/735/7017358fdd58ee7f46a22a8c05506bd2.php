�a^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1252;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-25 09:11:55";s:13:"post_date_gmt";s:19:"2018-05-25 09:11:55";s:12:"post_content";s:1157:"<h5>Some tips for foot care in diabetes</h5>
<ul>
 	<li>Keep blood glucose under control</li>
 	<li>Wash feet everyday and make sure that you dry them thoroughly. Inspect between toes.</li>
 	<li>Always cut nails straight across and then smooth the edges.</li>
 	<li>Check toes for any fungal infection</li>
 	<li>Consult a podiatrist for corns, calluses or ingrown toenails. Do not attempt to self-treat these conditions.</li>
 	<li>Nerve damage can be unpredictable. Consult your doctor if you feel any change in sensation (pain, tingling, burning, numbness, etc) in toes, feet, or legs.</li>
 	<li>If skin is dry, apply cream or petroleum jelly to feet and heels, but avoid the area between your toes</li>
 	<li>Protect feet from extremes in temperature.</li>
 	<li>Regular exercise improves circulation to all extremities. Try non-impact exercises on feet like swimming, cycling and yoga. Don’t exercise when there are open sores on the feet.</li>
 	<li>Avoid walking barefoot, always wear shoes or slippers. Wear socks with your shoes, since leather, plastics, and manmade shoe materials can irritate skin and cause infections.</li>
</ul>";s:10:"post_title";s:35:"Some Tips for Foot Care in Diabetes";s:12:"post_excerpt";s:84:"Wash feet everyday and make sure that you dry them thoroughly. Inspect between toes.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:35:"some-tips-for-foot-care-in-diabetes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-22 11:39:59";s:17:"post_modified_gmt";s:19:"2018-12-22 11:39:59";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=1252";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}