f^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:1:"1";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-03-26 07:12:47";s:13:"post_date_gmt";s:19:"2018-03-26 07:12:47";s:12:"post_content";s:4859:"The first line of management of all patients with diabetes is healthy eating and regular physical activity. Medications are second line treatment measures, if diet and exercise fail to achieve adequate control. Medication in diabetes can either be oral tablets (oral hypoglycemic agents or OHAs) or insulin injections.

A diabetes person’s diet need not be a complete deviation from the normal diet. The nutritional requirements of a person with diabetes are the same as in the person without diabetes. However, the nutrient intake has to be tailor made based on the age, sex, weight, height, physical activity, physiological needs and current dietary history and routine of the patient.
Heathy eating is the corner stone of therapy for diabetes mellitus. An individual with diabetes can often manage his condition by diet and exercise alone. When a patient with diabetes neglects this important aspect of diabetes management, then his/her blood sugars become uncontrolled.
<h5>Do's</h5>
<img class="size-medium wp-image-769 alignleft" src="https://drmohans.com/wp-content/uploads/2018/03/dos-blog-300x173.jpg" alt="" width="300" height="173" />
<ul>
 	<li>Diet plan should be balanced and individualized.</li>
 	<li>The calculated caloric requirement should allow the patient to lose or gain weight as required and maintain body weight close to the ideal/ desirable body weight.</li>
 	<li>Restrict refined and starchy food items such as maida, rava, white bread, potatoes, other tubers, processed foods and meats.</li>
</ul>
<ul>
 	<li>Include loads of vegetables and 1 or 2 helpings of fruits such as oranges,papaya,mosambi,</li>
 	<li>guava or watermelon. Sweet tasting fruits need to be restricted;for ex.Sitaphal (custard apple)</li>
 	<li>chickoo,sweet bananas,grapes,mangoes etc..</li>
 	<li>Include mainly high fibre foods (whole grains, pulses, and all green vegetables).</li>
 	<li>Daily fibre intake should be at least 20-35 g. Fibre helps to reduce postprandial blood</li>
 	<li>glucose levels and blood cholesterol.</li>
 	<li>Make greens and vegetables part of your daily diet.</li>
 	<li>Eat diet low in glycemic index which helps keep the blood sugars in normal range.</li>
 	<li>Butter, ghee, coconut oil and palm oil are rich in saturated fats and should be restricted.</li>
 	<li>Mustard oil, corn oil, sunflower oil, groundnut oil, ricebran oil and gingelly oil can be</li>
 	<li>preferred i.e. saturated fats less than 10 percent , monounsaturated fats and</li>
 	<li>polyunsaturated fats in the ratio 1:1 of the remaining fat %.Olive oil best used for salads</li>
 	<li>in Indian cooking. Avoid trans fats such as margarine and dalda/vanaspathi</li>
 	<li>Maintain a small food diary. Keep track of all the food you eat in a day. You will be amazed</li>
 	<li>at the amount and type of food you eat.</li>
 	<li>An obese patient should restrict calories through reduction in the intake of carbohydrate</li>
 	<li>and fat. Ensure that food is eaten not only at the appropriate time but also in appropriate</li>
 	<li>amounts.</li>
 	<li>Include 4-6 small frequent meals rather than 3- big meals a day.</li>
 	<li>When you shop, cook or eat out, make healthy choices.</li>
 	<li>Always read labels and select foods that are low in fat, salt, sugars.</li>
 	<li>Chew your food well and drink sufficient amount of water.</li>
 	<li>Try and Include salads in lunch and dinner.</li>
 	<li>Avoid Table salt and restrict use of processed foods.</li>
</ul>
<h5>Don't</h5>
<img class="size-medium wp-image-771 alignleft" src="https://drmohans.com/wp-content/uploads/2018/03/donts-blog-300x173.jpg" alt="" width="300" height="173" />
<ul>
 	<li>Patients who are on insulin or oral hypoglycemic
agents should not fast, because it may result in
hypoglycemia (low blood sugar levels).</li>
 	<li>They should not skip a meal assuming that it can
be made up by consuming extra food at the next
meal. This may result in low blood sugar and also
blood glucose fluctuations which leads to
microvascular complications.</li>
 	<li>Do not eat white bread, chips, and pastries, which quickly increase blood sugar.</li>
 	<li>Avoid processed foods and meats as they will be rich in salt and oil.</li>
 	<li>Restrict fried and fatty foods.</li>
 	<li>Do not take full fat dairy products.</li>
 	<li>Alcohol increases blood pressure and triglycerides and heavy drinking weakens the heart</li>
 	<li>muscle (cardiomyopathy). In excess, it affects the liver and peripheral nerves.</li>
 	<li>Do not use artificial sweeteners beyond the recommended quantity. If possible get used to</li>
 	<li>tea/coffee without sugar gradually.</li>
 	<li>Do not exercise on empty or full stomach.</li>
 	<li>Do not watch TV while eating food.</li>
 	<li>Do not smoke.</li>
 	<li>Do not miss your medication.</li>
</ul>";s:10:"post_title";s:28:"Dos and Don’ts in Diabetes";s:12:"post_excerpt";s:81:"A diabetes person’s diet need not be a complete deviation from the normal diet.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:25:"dos-and-donts-in-diabetes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-22 10:05:20";s:17:"post_modified_gmt";s:19:"2018-12-22 10:05:20";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:25:"https://drmohans.com/?p=1";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"1";}}