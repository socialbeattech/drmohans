�c^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1269;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-25 10:37:08";s:13:"post_date_gmt";s:19:"2018-05-25 10:37:08";s:12:"post_content";s:3297:"<h5>Key points:</h5>
<ul>
 	<li>Patients with diabetes acquire urinary tract infections (UTIs) very often.</li>
 	<li>Cure for UTI in diabetic patients happens over a longer period.</li>
 	<li>Keeping blood sugar level within normal range can prevent UTI.</li>
</ul>
<h5>Introduction</h5>
Individuals with diabetes are at higher risk for urinary tract infection (UTIs). Increased susceptibility in patients with diabetes is positively associated with increased duration and severity of diabetes. Patients with diabetes acquire UTIs very often because excess glucose is filtered in the kidneys and results in significantly higher urine glucose concentrations when compared to the urine of non-diabetic individuals. High glucose concentrations in the urine provide an abundant source of nutrients for bacteria, which can proliferate and cause an infection. Besides increased urine glucose, diabetes may increase the risk of UTIs through additional mechanisms, including impaired immune cell delivery, inefficient white blood cells and inhibition of bladder contractions that allow urine to remain stagnant in the bladder.
<h5>Classification</h5>
UTIs are of two types. They are:
<ul>
 	<li>Lower urinary tract infection (affects the urethra and bladder)</li>
 	<li>Upper urinary tract infection (affects the ureters and kidneys)</li>
</ul>
<h5>Symptoms of UTI</h5>
Symptoms of urinary tract infections often include, pain while voiding, blood in the urine, and increased urgency and frequency of urination. Other symptoms may include nausea, vomiting, and pain in the back and rib region.
<h5>Risk factors of UTI:</h5>
The risk of UTI increases with any negative change in the immune system of the body. Diabetes, like many other disorders, affects the immune system, increasing the risk of a urinary tract infection.The other risk factors include:
<ul>
 	<li>Urinary tract anatomical defects</li>
 	<li>Kidney stones – It obstructs the flow of urine and sets the stage for an infection.</li>
 	<li>An enlarged prostate gland also can slow the flow of urine, thus raising the risk of infection.</li>
 	<li>Menopause</li>
 	<li>Nervous system disorders that make it difficult to empty the bladder</li>
 	<li>Pregnancy</li>
 	<li>Females using a diaphragm for birth control</li>
 	<li>Use of urinary catheter for long time can infect the urinary tract</li>
 	<li>Having surgery on the urinary tract system</li>
</ul>
<h5>Diagnosis</h5>
<ul>
 	<li>Urine analysis</li>
 	<li>Urine culture</li>
 	<li>Abdomen ultrasound</li>
 	<li>X-rays and</li>
 	<li>Evaluation of bladder function</li>
</ul>
<h5>Treatment of UTI</h5>
The cure for diabetic patients requires a longer period, lasting from seven to fourteen days, of oral antibiotic treatment but some severe kidney infections may require hospital care, including a course of intravenous antibiotics.
<h5>Prevention and Control</h5>
<ul>
 	<li>Keep blood sugar level within normal range and blood pressure under control are essential to prevent the UTI.</li>
 	<li>Drink plenty of water everyday.</li>
 	<li>Do not drink fluids that irritate the bladder, such as alcohol.</li>
 	<li>Genital area must be kept clean.</li>
 	<li>Urinate when you feel the need; don’t resist the urge to urinate.</li>
</ul>";s:10:"post_title";s:36:"Urinary tract infection and Diabetes";s:12:"post_excerpt";s:74:"Patients with diabetes acquire urinary tract infections (UTIs) very often.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:36:"urinary-tract-infection-and-diabetes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-22 11:41:10";s:17:"post_modified_gmt";s:19:"2018-12-22 11:41:10";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=1269";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}