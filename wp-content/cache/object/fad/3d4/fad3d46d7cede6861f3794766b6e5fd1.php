�b^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:5856;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-06-14 17:07:34";s:13:"post_date_gmt";s:19:"2019-06-14 12:07:34";s:12:"post_content";s:17331:"<!-- wp:paragraph -->
<p><strong>Dr. ASWIN MUKUNDAN, MD</strong><strong><br><br></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Pregnancy and blood sugar</strong> related problems are a big issue in primary health care in developing countries like India. Gestational Diabetes Mellitus (GDM) is found to have a prevalence of around 10% in India which has caused a huge problem for Obstetricians as they need to collaborate with a diabetologist and a special team to tackle this menace. Moreover, many women Type 1 and <a href="https://drmohans.com/type-2-diabetes-mellitus-going-through-pregnancy/">Type 2 diabetes</a> are conceiving without a pre-planned pregnancy&nbsp; which could affect the health of the baby and the mother. Extensive studies and research has been conducted in this particular area as the health of mothers and babies is given a lot of importance in deciding a country's developmental parameters. In this background we need to know why it needs special attention and what are the normal physiological changes during pregnancy.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Normal pregnancy is a period of physiological changes in the body which are often reflected in the blood tests as well. The passage that connects the mother to the baby is called the placenta which carries all the essential nutrients for the baby. The placenta also secretes various hormones and chemicals which can alter the blood glucose levels. Early pregnancy or the first few weeks of gestation is usually a period of increased insulin sensitivity and increased insulin secretion. As it reaches the late first trimester the insulin resistance increases exponentially as a mechanism to deliver adequate nutrition to the growing baby inside the mother. Thus the diabetes of pregnancy AKA Gestational Diabetes Mellitus is seen as a period of increased insulin resistance. This usually occurs during the second trimester.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Increased blood sugars during pregnancy</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The blood sugar levels of a patient who is pregnant can increase for the following reasons:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<ol style="padding-left:4%;"><li>Known type 1 or type 2 diabetes patient getting pregnant</li><li><a href="https://drmohans.com/gestational-diabetes-amongst-pregnant-mothers/">Gestational diabetes</a> mellitus</li><li>Impaired glucose tolerance</li></ol>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>It is very important to educate and inform all the adolescent girls regarding the effects of high <a href="https://drmohans.com/risks-and-dangers-of-sugar-for-diabetic-patients/">blood sugar during pregnancy</a>. Each pregnancy needs to be planned properly with the sugar levels in control before conceiving as increased blood sugars during conception or undetected early pregnancy can cause fetal malformations or death. So a pre-planned pregnancy starts with appropriate contraception and&nbsp; planning at a preconception level for all girls with diabetes just to make sure the baby and the mother stay healthy even post delivery.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Symptoms to watch out for</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Early detection is the key in diabetes management to avoid complications. In a normal healthy individual it's easy to identify diabetes symptoms . You need to watch out for increased urination, increased thirst, increased hunger, tiredness, delayed wound healing and sexual disturbances. In some cases it maybe totally asymptomatic and can be detected during a routine check up. But things are difficult in <a href="https://drmohans.com/blood-sugars-during-pregnancy/"><strong>pregnancy and blood sugar</strong></a><strong> </strong>as all these symptoms can occur in normal pregnancy too. It would be optimal to get your blood sugar checked even before you plan pregnancy and during each trimester as the tests can differ. Cut off values are also different before and after conception.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>What to do if blood sugars are high</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>If your <strong>blood sugar levels during pregnancy </strong>are high, go to a specialist and take help. There are only a few treatment options available in diabetes during pregnancy and certain tablets cannot be prescribed. It is also found that proper <a href="https://drmohans.com/exercise-diet/">nutrition and lifestyle</a> modifications can help women achieve normoglycemia during pregnancy. A recent study suggested that 70-85% of women diagnosed with GDM can control it with just a few lifestyle modifications. So it is essential that pregnant women with deranged blood glucose values be treated by a team of diabetologist, dietician and diabetes educator when available. The earlier we intervene, the better the outcome.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Steps to be taken before starting medications</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Once high blood sugars are detected, treatment starts with medical nutrition therapy, physical activity and weight management. Medical nutrition therapy is decided individually by a trained dietician as proper nutrition is also essential for the growth of the baby. It is very important that a pregnant woman with diabetes eats a consistent amount of carbohydrates to match with the treatment given and avoid low sugars as well as high sugars. The Dietary Reference Intake for all pregnant women is a minimum of 175g of carbohydrates, a minimum of 71g of protein, and a 28g of fiber. Other than the physical and dietary aspects, mental support from the family members is an important factor for the adequate management.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>What all can be given</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Insulin is the appropriate treatment when you have <strong>pregnancy and blood sugar</strong>. The second line tablets, which can be given are metformin and glyburide , but both of these can cross the placenta to reach the baby's body. The fear of taking insulin by the patient should be taken care by the health care providers to make them understand and give necessary counseling regarding the same.&nbsp; Taking multiple injections of  insulin can be stressful for the patient, so they should be taught the importance of adherence to treatment. Any tablets you were taking before pregnancy for chronic diseases or any other illness should be reviewed as many commonly used tablets are not given during pregnancy. Common tablets such a cholesterol reducing tablets and commonly used antihypertensives are also included. So be wary of it and inform your doctor regarding all the treatments you are taking.<br></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Difficulties faced when taking treatment</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A lot of patients are scared of getting vaccinated when pregnant, but it should be emphasised that it is beneficial for their health and the baby's health. In my experience once a women understands this, she will be ready to face anything and everything. Blood sugar variations are a part and parcel of treatment during pregnancy. You may often get slightly lower or higher blood sugars than expected or sometimes higher but don’t panic . Your doctor will help you out as every month the insulin sensitivity and metabolism changes in your body during pregnancy. So regular visits to a diabetes care team can prevent you from any untoward reactions.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Keep a glucometer at home and check your blood sugars multiple times as advised by the doctor and when it is required. The symptoms of low sugars are increased sweating, anxiety, tremors, palpitation, headache etc. Check your sugars immediately and take simple sugars immediately if low (i.e. less than 70). Have simple carbohydrates such as glucose powder , around 25 g mixed in water which are easily available in the market and consult your doctor immediately if persistent.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Keep a diary and note down your<strong> blood sugars during pregnancy</strong> as and when you check it in relation to the timing of your meals. It will also be easy for the diabetologist to keep a check on your medication dosages with the data you provide.&nbsp;<br></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Altered <strong>blood sugar levels during pregnancy </strong>are easily managed when found out early and given the best treatment possible with regular visits to the diabetology care team with the active participation of a patient, diabetologist, dietician and a fitness educator. Your pregnancy and your baby's health is of utmost importance as it will decide his/her future. So leave your worries to the doctor and enjoy the sweetness of your pregnancy!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Dr. ASWIN MUKUNDAN, MD</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Consultant</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>DMDSC, Calicut</strong><br></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>ological changes during pregnancy.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Normal pregnancy is a period of
physiological changes in body which will reflect in lot of blood tests too. It
is divided into three trimesters where changes in each trimester is critical
for a person undergoing this change to understand. The mother to baby passage
called the placenta is the passage channel for all nutrients essential for the
baby. The placenta secretes various hormones and chemicals too which can alter
the blood glucose levels. Early pregnancy or the first few weeks of gestation
is usually a period of increased insulin sensitivity and increased insulin
secretion. As it reaches the late first trimester the insulin resistance
increases exponentially as a mechanism to deliver adequate nutrition to the
growing baby inside the mother. Thus the diabetes of pregnancy AKA Gestational
Diabetes Mellitus is seen during this period of increased insulin resistance,
the second trimester.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Increased blood sugars during pregnancy:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>This can happen due to any of the following possibilities   </p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul style="padding-left:4%;"><li> Known type 1 or type 2 diabetes patient getting pregnant  </li><li> Gestational diabetes mellitus</li><li> Impaired glucose tolerance  </li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>It is very important to educate and
inform all the adolscent girls regarding the effects of high blood sugar during
pregnancy. Because each pregnancy has to be planned properly with the sugars reaching
adequate control even before conceiving as increased blood sugars during
conception or undetected early pregnancy can cause even fetal malformations or
death. So it starts with appropriate contraception and planning pregnancy at a
pre conception level for all girls with diabetes just to make sure the baby and
the mother stay healthy even post delivery.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Symptoms to watch out for:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Early detection is the key in diabetes
management to avoid complications. In a normal healthy individual its easy to
identify diabetes symptoms . You need to watch out for increased urination,
increased thirst, increased hunger, tiredness, delayed wound healing, sexual
disturbances or even it maybe totally asymptomatic, detected during a routine
check up. But things are difficult in pregnancy as all these symptoms can occur
in normal pregnancy too. The best thing to confirm is to get your blood sugar
checked , maybe even before , when you plan pregnancy and during each
trimesters as the tests are different and cut off values are also different
before and after conception.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>What to do if blood sugars are high:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>If your blood sugars are high go to a
specialist and take help. There are only very few treatment options in diabetes
during pregnancy and certain tablets cannot be given. It is also found that
proper nutrition and life style modifications can help women to achieve
normoglycemia during pregnancy. A recent study suggested that 70-85% of women
diagnosed with GDM can control GDM with lifestyle modification alone. So its
essential that a pregnant women with derranged blood glucose values be treated
by a team of diabetologist, dietician and diabetes educator when available. The
earlier we intervene, the better the outcome.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Steps to be taken before starting
medications:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Once high blood sugars are detected,
treament starts with medical nutrition therapy, physical activity and weight
management. Medical nutrition therapy is decided individually by the trained
dietician as proper nutrition is also essential for the growth of the baby. It
is very important that a pregnant women with diabetes eat consistent amount of
carbohydrates to match with the treatment given and avoid low sugars as well as
high sugars. The Dietary Reference Intake for all pregnant women recommends a
minimum of 175g of carbohydrate, a minimum of 71g of protein, and a 28g of
fiber. Other than the physical and diet aspects mental support from the family
members is an important factor for the adequate management. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>What all can be given:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Insulin is the treament of choice for sugar
control in pregnancy. The second line tablets, which can be given are metformin
and glyburide , but both of these can cross palcenta to reach the babys body.
The fear of taking insulin by the patient should be taken care by the health
care providers to make them understand and give necessary counseling regarding
the same.&nbsp; Taking multiple injections
of&nbsp; insulin can be a stress for the
patient and thus she should be taught the importance of adherence to treatment.
Any tablets you were taking before pregnancy for chronic diseases or any other
illness should be reviewed as many commonly used tablets are not given during
pregnancy. Common tablets such a cholesterol reducing tablets and commonly used
anti hypertensives are also included. So be wary of it and inform your doctor
regarding all the treatments you are taking.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Difficulties faced when taking
treatment:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A lot of patients are scared of taking
injections when pregnant, but it should be emphasised that it is for their good
health and the babys health. In my experience once a women understands this ,
they are ready to face anything and everything. Blood sugar variations are a
part and parcel of treatment during pregnancy. You might get slightly lower
blood sugars than expected or sometimes higher. Dont panic . Your doctor will
help you out as every month the insulin sensitivity and metabolism changes in
your body during pregnancy. So regular visit to a diabetes care team can
prevent you from any untoward reactions. Keep a glucometer at home and check
your blood sugars multiple times as doctors advice and as and when required.
The symptoms of low sugars are increased sweating, anxiety, tremors,
palpitation, headache etc. Check your sugars immediately and take simple sugars
immediately if low (i.e. less than 70). Have simple carbohydrates such as
glucose powder , around 25 g mixed in water which are available easily in
market and consult your doctor immediately if persistent. Keep a diary and note
down your blood sugars as and when you are checking with time and relationship
with your meals. It will be easy for the diabetologist also to keep a check on
your medication dosages with this data you are providing.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>TAKE HOME MESSAGE:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Altered blood sugar values are easily
managed during pregnancy when found out early and given the best treatment
possible with regular visits to the diabetology care team with the active
participation of a patient, diabetologist, dietician and a fitness educator.
Your pregnancy and your babys health is of utmost importance as it will decide
his/her future. So leave your worries to the doctor and enjoy the sweetness of
your pregnancy!</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Dr. ASWIN MUKUNDAN, MD</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Consultant</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>DMDSC, Calicut</strong></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:38:"Pregnancy and Blood Sugar: A Conundrum";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:37:"pregnancy-and-blood-sugar-a-conundrum";s:7:"to_ping";s:0:"";s:6:"pinged";s:297:"https://drmohans.com/type-2-diabetes-mellitus-going-through-pregnancy/
https://drmohans.com/blood-sugars-during-pregnancy/
https://drmohans.com/gestational-diabetes-amongst-pregnant-mothers/
https://drmohans.com/exercise-diet/
https://drmohans.com/risks-and-dangers-of-sugar-for-diabetic-patients/";s:13:"post_modified";s:19:"2019-08-29 16:58:42";s:17:"post_modified_gmt";s:19:"2019-08-29 11:58:42";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=5856";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}