�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6208";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-09 21:13:52";s:13:"post_date_gmt";s:19:"2019-08-09 16:13:52";s:12:"post_content";s:2159:"<!-- wp:paragraph -->
<p><strong>Two of my grandparents had diabetes, though neither of my parents have it. Does it skip a generation and come to the next?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As a rule, diabetes does not skip generations. However, if the parents have been careful, maintained ideal body weight, exercised regularly and have been eating healthy, they can avoid diabetes. On the other hand, if their children have been consuming a lot of junk food, not exercising and have become obese, it could lead to diabetes in them. In such cases, diabetes can pass on to the third generation after skipping one generation.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>I started exercising when I was diagnosed with diabetes. Now, I would like to take it up a notch, and start training for an endurance sport. Is this advisable, and what should I keep in mind before I begin the training?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Yes, people with diabetes can take part in endurance sport. There are Olympic gold medallists and test cricketers with type 1 diabetes, who have achieved the peaks in their career after getting diabetes. The precautions that you have to keep in mind are that you should always carry glucose, or carbohydrate in any other form with you. You must also assess your heart condition and ensure that you are fit to take up the endurance sport. Please consult your doctor before you take up very strenuous exercise.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Dr V Mohan is a diabetologist based in Chennai, who heads a WHO Collaborating Centre for Prevention and Control of Non-Communicable Diseases and an International Diabetes Federation Centre of Education</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><em>Nothing in this column is intended to be, and is not, a substitute for professional medical advice, diagnosis or treatment. Please seek independent advice from a licensed practitioner if you have any questions regarding a medical condition. Email us your questions at mp_health@thehindu.co.in</em></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:39:"Can diabetics train for endurance sport";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:39:"can-diabetics-train-for-endurance-sport";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:47:03";s:17:"post_modified_gmt";s:19:"2019-08-29 11:47:03";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=6208";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}