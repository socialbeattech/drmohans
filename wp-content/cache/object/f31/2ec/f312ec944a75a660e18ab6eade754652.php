x`^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"1653";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-06-05 11:42:44";s:13:"post_date_gmt";s:19:"2018-06-05 11:42:44";s:12:"post_content";s:4703:"<h5>CHANGE LIFESTYLE - REDUCE WEIGHT</h5>
<img class="size-medium wp-image-1654 aligncenter" src="https://drmohans.com/wp-content/uploads/2018/06/image2-300x225.jpg" alt="" width="300" height="225" />

In today’s fast-paced world with the increased usage of convenience foods, fast foods and decreased physical activity, weight management has a very important place. Weight-gain occurs, when the daily intake of calories is greater than what is required and when a person does not exercise enough to burn the calories consumed in food. In most developed and developing countries, 50 percent of the population suffers from malnutrition. Malnutrition includes not only under-nutrition but also over-nutrition. Persons who are plump and stout seem healthy but they are actually not. Due to abundant availability of food and increased economic status, parents buy their children whatever they ask to eat. Children are generally influenced by the colourful ads perpetuated by media. Daily intake of unbalanced diet, indulging in sedentary activities like video games and TV and long hours of tuitions become a contributing factor for developing obesity and other non communicable diseases in the future.

Obesity refers to the amount of fat that gets deposited over the body parts particularly around the waist and hip region. Because of this, people weigh more than they should.
<h5>How can we know whether a person is obese?</h5>
<img class="size-full wp-image-1655 aligncenter" src="https://drmohans.com/wp-content/uploads/2018/06/image1.jpg" alt="" width="200" height="184" />

There is a simple way. By subtracting 100 from your height (cm), you can obtain your ideal body weight. There is one more way which requires a little mathematical calculation. That is your BMI {Body Mass Index = Weight in kg/ height * height (m2)}.  In order to check these values, consult your nutritionist for guidance.
<h5>What are the reasons for obesity?</h5>
Family history, dietary errors, sedentary lifestyle, hormonal imbalance and mental disturbances are the main contributing factors. When the parents or grandparents are obese, children acquire their obesity gene and become obese.

<img class="size-medium wp-image-1656 aligncenter" src="https://drmohans.com/wp-content/uploads/2018/06/image3-300x214.jpg" alt="" width="300" height="214" />

Some people continue to eat even after satiety has set in. Just for relishing the taste, they ignore the signal sent by the brain to stop eating. Some nibble their snacks once in half an hour and move towards food when they are depressed. Because of these, they eat more than they require leading to weight gain.

Sedentary lifestyle is the second important factor contributing to weight gain. Nowadays, children no longer walk to school. Mechanization has made us lazy. Our physical activities are replaced by machines. To burn the calories, you need to do some form of exercise.
<h5>What are the health effects of obesity?</h5>
Obese persons are prone to non-communicable diseases like diabetes, heart diseases, cancer, osteoporosis, stress, obstructive sleep apnoea, etc.

<img class="size-medium wp-image-1657 aligncenter" src="https://drmohans.com/wp-content/uploads/2018/06/image4-300x214.jpg" alt="" width="300" height="214" />

&nbsp;
<h5>How to control or prevent obesity?</h5>
We need to rely on 4 pillars
<ol>
 	<li>Diet</li>
 	<li>Exercise</li>
 	<li>Lifestyle modification</li>
 	<li>Medication</li>
</ol>
<h5>Diet:</h5>
Choose low calorie foods and fibre rich foods. Most of the low calorie foods are rich in fibre.
<ol>
 	<li>Instead of having plain idly/ dosa, make vegetable idly, vegetable dosa or oothappam.</li>
 	<li>Use whole wheat bread instead of white bread</li>
 	<li>Make chapathi out of soya, oats, bajra and ragi.</li>
 	<li>Tofu can be used in place of paneer</li>
 	<li>Prefer buttermilk and lemon juice over high calorie beverages or cool drinks</li>
 	<li>Replace snacks with salads</li>
 	<li>Have small frequent meals.</li>
 	<li>Eat in a calm environment</li>
</ol>
<h5>Physical activity:</h5>
<img class="size-medium wp-image-1658 aligncenter" src="https://drmohans.com/wp-content/uploads/2018/06/image5-300x202.jpg" alt="" width="300" height="202" />

Regular walking is recommended. Inculcate a hobby that involves more physical activity. Instead of watching a cricket match on TV, go and play outside yourself. It relaxes your mind too. Excuses are not acceptable!

To follow these, you need to get motivated to change your lifestyle. You must have control over yourself. Making a lifestyle change will give long term benefits not only to you, but also to the future generations.";s:10:"post_title";s:32:"Change Lifestyle and Lose Weight";s:12:"post_excerpt";s:90:"Just for relishing the taste, people ignore the signal sent by the brain to stop eating...";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:32:"change-lifestyle-and-lose-weight";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-22 09:51:24";s:17:"post_modified_gmt";s:19:"2018-12-22 09:51:24";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=1653";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}