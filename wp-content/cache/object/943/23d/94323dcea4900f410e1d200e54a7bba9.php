f^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"1310";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-05-25 17:01:31";s:13:"post_date_gmt";s:19:"2018-05-25 17:01:31";s:12:"post_content";s:4146:"Smoking affects all people who smoke. However, for people with another illness, such as diabetes, smoking worsens symptoms and makes the illness harder to manage. India has the second largest population (1210 million) and number of people with diabetes (69 million) and tobacco users (275 million) after China. Both diabetes prevalence and tobacco use are increasing rapidly in India. Smoking is strongly linked to the risk of diabetes, morbidity as well as mortality. The International Diabetes Federation (IDF) in 2003 and the American Diabetes Association (ADA) in 2004 have both strongly recommended that people with diabetes not smoke because of increased risk of diabetes complications.
<h5>What do cigarettes contain?</h5>
The main ingredient in cigarettes is tobacco. Tobacco is a green, leafy plant that is grown in warm climates. Farmers use many chemicals to grow tobacco. They use fertilizers to make the soil rich and insecticides to kill the insects that eat the tobacco plant. After the tobacco plants are picked, they are dried, and machines break up the leaves into small pieces.

Artificial flavours and other chemicals are added. Some chemicals are put in cigarettes to keep them burning, otherwise, they would go out. There are over 4,000 chemicals in cigarettes. 51 of them are known to cause cancer.
<h5>Does tobacco increase the risk of diabetes?</h5>
Yes. Tobacco can increase blood sugar levels and leads to resistance of insulin in the body. Studies have shown an increase in the occurrence of diabetes among smokers i.e. smokers are three times at more risk of developing diabetes than non-smokers.
<h5>Why is smoking particularly bad for people with diabetes?</h5>
Diabetes is a chronic disease and if not managed properly can lead to long-term complications such as heart disease, kidney disease (nephropathy), nerve disease (neuropathy), eye disease (retinopathy) and foot disease. Smoking combined with diabetes increases the risk and severity of diabetes complications.

Smoking can in fact accelerate the onset of diabetes related complications due to its (damage to the inner lining of blood vessels).
<h5>What are the harmful effects of smoking with diabetes?</h5>
As smoking increases blood sugar levels, maintaining good blood sugar count can be difficult for smokers. It is clearly established that smoking is a fairly large and independent risk factor for heart disease, stroke and peripheral vascular disease in people with diabetes. Smokers are also more at risk for nerve and kidney damage.
<h5>How does smoking increase heart disease risk in people with diabetes?</h5>
About 20% of deaths from heart diseases are attributed to smoking. Smoking can damage the lining of the blood vessels and cause the build-up of fatty deposits in the arteries (atherosclerosis). It raises the heart rate and blood pressure by causing narrowing of the blood vessels (vasoconstriction). It increases the likelihood of forming blood clots in the arteries leading to heart attacks. It reduces the flow of oxygen to the heart and damages the heart muscles.
<h5>Tips for quitting smoking</h5>
Quitting smoking can be difficult, but it is possible. Ex-smokers enjoy a better quality of life, have fewer diseases and an increased lifespan compared to those who continue to smoke.
<h5>Here are five simple steps to get started:</h5>
<ul>
 	<li>Talk to doctor and create a quit plan. Ask about nicotine replacement
therapy (NRT).</li>
 	<li>Talk to friends and family about quitting. Ask them for support.</li>
 	<li>Never stay bored. Get busy. Try to get out of your home and go for a walk or exercise if you feel alone and bored.</li>
 	<li>Avoid people, places, materials and situations that can trigger your urge to smoke.</li>
 	<li>Stay positive. Whenever you feel like having your next puff, think about why you quit in the first place. It can be hard but the reward is more than worth it. Doctors are the best people to address your concerns and make further recommendations.</li>
</ul>
There is no evidence that e-cigarettes are a healthier alternative to smoking. Cessation is the best advice.";s:10:"post_title";s:46:"Smoking and Diabetes: The Double Health Hazard";s:12:"post_excerpt";s:69:"Smoking can accelerate the onset of diabetes related complications...";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:45:"smoking-and-diabetes-the-double-health-hazard";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-12-22 11:37:40";s:17:"post_modified_gmt";s:19:"2018-12-22 11:37:40";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:28:"https://drmohans.com/?p=1310";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}