�b^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6229;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-09 22:23:59";s:13:"post_date_gmt";s:19:"2019-08-09 17:23:59";s:12:"post_content";s:5747:"<!-- wp:paragraph -->
<p>There are around 72 million people with diabetes in India and these numbers are expected to increase to 134 million in the next 10-15 years. Uncontrolled diabetes can affect many organs and the kidney is one of the most serious of these complications. On&nbsp;<a href="https://www.hindustantimes.com/lucknow/world-kidney-day-pgi-to-raise-awareness-on-kidney-diseases-among-women/story-j0AGYwV5Xc1QZoPpUOGYNI.html">World Kidney Day</a>, its time we start paying attention to this issue.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>After 15 to 20 years of having diabetes, nearly a third of people with the condition&nbsp;<a href="https://www.hindustantimes.com/health/world-kidney-day-women-are-happier-to-donate-kidney-to-partner-than-men/story-cz1gTq9R88mPgSHbhgbJPM.html">develop kidney complications</a>. Diabetes mellitus is the most common cause of chronic kidney disease and kidney failure in the world. Therefore, it is essential that all clinicians understand how to appropriately screen patients for diabetic kidney disease with a focus on prevention of kidney diseases, and if is already present, to try to slow down its progression.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Stages of diabetic kidney disease</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>This first stage of diabetic kidney disease (DKD) is called microalbuminuria, and at this stage small amounts of albumin begin to leak into the urine. As the disease progresses larger quantities of albumin leak into the urine. This stage is called macroalbuminuria or proteinuria. Slowly the kidneys’ filtering capacity begins to drop and the body begins to retain various waste products as filtration falls.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As kidney damage develops further, the blood pressure rises and this further damages the kidney. After months or years, the DKD may progress to end stage renal disease (ESRD), which requires either a kidney transplant or regular dialysis.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>What should one do to prevent diabetic kidney disease (DKD)?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Blood glucose control</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>If the diabetes is detected early and treated aggressively, it is possible to prevent diabetic kidney disease in most patients. One must aim to keep the glycated haemoglobin (HbA1c) below 7% at all times.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Blood pressure control</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>It is ideal to keep the BP less than 130/80 mmHg or even at 120/70 mmHg, if there is already some evidence of kidney involvement.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Avoid nephrotoxic agents</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>It is important to avoid medicines that can affect the kidney eg., pain killers like NSAID drugs and certain antibiotics. Contrast dyes used for doing CT Scans can also affect the kidney.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>One should also treat any urinary or bladder or kidney infections aggressively. Finally, one should drink enough water and keep oneself well hydrated.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Dietary factors in the prevention and control of diabetic kidney disease:</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>1.One may prevent or delay health problems from chronic kidney disease (CKD), by eating the right foods and avoiding foods high in sodium, potassium, and phosphorus.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>2.Protein foods such as meat and dairy products break down into waste products that healthy kidneys remove from the blood. When one takes too much protein, ie., far more than the body needs, one may put an extra burden on the kidneys and cause kidney function to decline faster.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>3.Those with high blood pressure should limit their daily sodium intake to no more than 1,500 mg. Choosing sodium-free or low-sodium food products will help. Alternative seasonings such as lemon juice, salt-free seasoning mixes and hot pepper sauce can help people reduce their salt intake.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>4.When kidneys are damaged, potassium builds up in the blood and may cause serious heart problems. Potassium is found in many fruits and vegetables and hence if instructed by the dietician these may have to be curtailed.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>5.People with advanced chronic kidney disease may need to limit their fluid (water) intake because damaged kidneys can’t remove the extra fluid.</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<figure><iframe src="https://tpc.googlesyndication.com/safeframe/1-0-35/html/container.html" width="728" height="90"></iframe></figure>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>In addition to diet, other lifestyle modifications should be initiated to control the progression of kidney disease.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Physical activity helps to use the insulin in our body. Maintaining an ideal body weight helps to maintain blood glucose levels, and losing weight to achieve ideal body weight benefits to lower blood sugar levels.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Quitting smoking and using tobacco in any form is essential to prevent all complications of diabetes. Smoking increases the risk for many associated illnesses, and can also increase the risk for kidney failure.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:38:"How to prevent diabetic kidney disease";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:38:"how-to-prevent-diabetic-kidney-disease";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-09 22:33:12";s:17:"post_modified_gmt";s:19:"2019-08-09 17:33:12";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=6229";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}