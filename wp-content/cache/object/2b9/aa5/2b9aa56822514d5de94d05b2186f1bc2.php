f^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6062;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-07-04 16:06:03";s:13:"post_date_gmt";s:19:"2019-07-04 11:06:03";s:12:"post_content";s:12984:"<!-- wp:paragraph -->
<p><strong>Introduction</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Gestational diabetes mellitus (GDM) is a condition of high blood glucose levels are detected for the first trimester during pregnancy.  GDM is very common and some studies have reported that 10-20% of pregnant women in India have GDM. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Who are at risk of GDM?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Older
women</li><li>Overweight
or obese.</li><li>Family
history of diabetes</li><li>If
there was a previous history of Gestational Diabetes.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Screening and diagnosis of hyperglycemia in
pregnancy</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p> &nbsp;All pregnant women should be screened at the first visit to antenatal clinic. Early in pregnancy screening is done usually at the booking visit to see if the woman already has diabetes complicating pregnancy which she did not know about. Screening can be done using either fasting plasma (FPG), glycosylated hemoglobin (A1C),or random plasma glucose(RPG). If a woman has high blood sugars early in pregnancy (FPG&gt;7mmol/l (126 mg/dl), A1c &gt;6.5% or RPG &gt;11.1 mmol/l(200mg/dl) then she should be treated as having pre-existing diabetes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p> All women who have normal blood glucose levels of sugar early in pregnancy should be screened again with an oral glucose tolerance test (OGTT) between 24-28 weeks of the pregnancy to rule out GDM. They should come to the centre in fasting state at least 8 hrs-10 hrs and should not consume anything except water before the test. Fasting blood and urine are collected, 75 gms of oral glucose is given to drink in 300 ml of water. Blood is then drawn at one and two hrs of the glucose load. If fasting plasma glucose is between 92-125mg/dl or 1 hr of glucose load >180mg/dl or 2 hrs level is >153mg/dl (International Association of Diabetes and Pregnancy Study Group, IADPSG criteria) the woman is said to have Gestational Diabetes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Weight gain during pregnancy</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>  All pregnant women should gain some weight during pregnancy and the recommended weight gain is decided by the Pre-pregnancy body mass index (BMI) </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BMI = Weight (kg)/ Height in m<sup>2</sup> </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Changes of body weight based on BMI</p>
<!-- /wp:paragraph -->

<!-- wp:table -->
<table class="wp-block-table"><tbody><tr><td>
  &lt; 18.5
  </td><td>
  Underweight
  </td></tr><tr><td>
  18.5-22.9
  </td><td>
  Normal
  </td></tr><tr><td>
  23-24.9
  </td><td>
  Overweight
  </td></tr><tr><td>
  &gt;25
  </td><td>
  Obese
  </td></tr></tbody></table>
<!-- /wp:table -->

<!-- wp:paragraph -->
<p>(According
to Asian Indian guidelines<sup>1</sup>)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Recommended
weight gain according to pre pregnancy BMI</p>
<!-- /wp:paragraph -->

<!-- wp:table -->
<table class="wp-block-table"><tbody><tr><td>
  BMI
  </td><td>
  Weight gain
  </td></tr><tr><td>
  &lt;18.5
  </td><td>
  12.5-18 Kg
  </td></tr><tr><td>
  18.5-24.9
  </td><td>
  11.5-16 KG
  </td></tr><tr><td>
  25-29.9
  </td><td>
  7-11.5 KG
  </td></tr><tr><td>
  &gt;30.0
  </td><td>
  5-7 KG
  </td></tr></tbody></table>
<!-- /wp:table -->

<!-- wp:paragraph -->
<p><strong>Managing Gestational Diabetes</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p> Gestational Diabetes levels can be managed through balanced and healthy diet, medication and exercise. Medication is usually indicated if blood sugars do not come to normal with diet and exercise. Recommended levels of glucose are fasting&lt;90mg/dl(5.0 mmol/l),1 hr&lt;140mg/dl(&lt;7.8 mmol/l),2 hrs &lt;120mg/dl(&lt;6.7 mmol/l)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>General guidelines for healthy eating</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p> Generally good nutrition is important during pregnancy and en effective way to manage gestational diabetes levels. A dietitian can guide to take portion size, right amount of protein, fat and carbohydrate.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Here
are some general tips:-</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Eat
variety of foods distributing calories, carbohydrate and taking enough protein</li><li>4-5
serving of vegetables is recommended</li><li>Adequate
fluids 2-3 liters should be taken but however water has to be restricted if
there is &nbsp;pedal edema.</li><li>Iron
rich foods should be included. Eg: - Greens</li><li>Consume
650 ml of milk/ day to meet calcium requirements.</li><li>Avoid
direct sugars, artificial sweeteners.</li><li>Avoid
Alcohol and tobacco.</li><li>Use
a combination of oils Eg: - Mono unsaturated fats and Poly unsaturated fats</li><li>Include
plant protein like pulses, dhal etc.</li><li>Restrict
animal protein like red meat.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Healthy plate</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>One
way to decide how much to eat to divide the plate is given below. Up to half plate
should contain vegetables and fruits, about one quarter of plate should be
starch or grains and the reminder protein and calcium rich foods.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":6063,"align":"center"} -->
<div class="wp-block-image"><figure class="aligncenter"><img src="https://drmohans.com/wp-content/uploads/2019/07/health-food-plate.png" alt="" class="wp-image-6063"/><figcaption> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </figcaption></figure></div>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"center"} -->
<p style="text-align:center"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <br>Healthy food plate </p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":6064,"align":"center"} -->
<div class="wp-block-image"><figure class="aligncenter"><img src="https://drmohans.com/wp-content/uploads/2019/07/south-asian-foods.png" alt="" class="wp-image-6064"/><figcaption><br><br></figcaption></figure></div>
<!-- /wp:image -->

<!-- wp:paragraph {"align":"center"} -->
<p style="text-align:center"> Example of Healthy food plate with South-Asian foods </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Physical Activity</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Aerobic activity of moderate intensity for 30mins/day on most days of the week has shown benefits in metabolic control. Start with light to moderate exercise. Any simple physical activity is better than no activity.<sup>2, 3</sup></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Caution</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Don’t exercise in flat position.</li><li>Stop if your heart rate is &gt;140/min.</li><li>Stop if you feel contraction.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Insulin</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong> </strong> Insulin is indicated when target blood glucose levels are not attained with diet and physical activity. Different types of insulin is prescribed by the Diabetologist.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Materials
required for insulin injection</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Insulin </li><li>Syringe or pen type needle.</li><li>Denatured spirit</li><li>Cotton.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>General
guidelines for patient on bottle type insulin </strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Insulin must be refrigerated, not frozen</li><li>Store away from heat</li><li>It must be rolled between two hands before using.</li><li>Common site for taking Insulin are upper part of thigh. You may not want
to use abdomen during pregnancy, hence arms and legs are preferred.</li><li>If refrigerator not available (ex. in villages) Insulin can be stored in
clay pot.</li><li>Each bottle type contains 400 units</li><li>If using 100 u insulin then use a 100u&nbsp;
syringe</li><li>If using 40u&nbsp; insulin then use a
40u&nbsp; syringe.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>General
guidelines for patients on pen type insulin</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>The pens can be stored at room temperature while in use.</li><li>Should not get exposed to direct sunlight</li><li>Pen needle can be used 4-5 times without sterilization.</li><li>Each pen contains 300 units of insulin.</li><li>Insulin pens are available in two basic types, disposable and reusable.</li><li>&nbsp;Insulin pens are portable,discreet
and convient to carry,</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Hypoglycemia
during pregnancy</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Only those on insulin during pregnancy are at risk
of getting hypoglycemia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Blood sugar drops when</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>When skipping or delaying meals</li><li>One does lot of physical activity but has not eaten enough.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Symptoms
of Hypoglycemia</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Excessive hunger</li><li>Sweating</li><li>Shivering</li><li>Palpitation</li><li>Weakness</li><li>Blurred vision</li><li>Giddiness</li><li>Tremors</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Treatment
of Hypoglycemia</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>If possible blood sugar should be checked.</li><li>15 grams of sugar should be given i.e. ½ cup fruit juice, 3 tsp of sugar.</li><li>Wait for 15 minutes. If no improvement again 15 grams of sugar must be
given.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>&nbsp;Prevention of Hypoglycemia</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Follow 4 meal pattern</li><li>Adhere to timely meals</li><li>Include low calorie foods in between major meals.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Monitoring
blood sugar</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Blood sugar will need to be checked several times during pregnancy.</li><li>It can be done using Glucometer or in nearby clinics.</li><li>Monitoring helps to improve blood sugar and achieve better glycosylated
hemoglobin (HbA1C) level.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Post
delivery screening</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>After the delivery, blood sugar to be checked. If
the blood sugars return to normal, women will no longer require insulin. For
some women blood sugar will not come under normal range level after the baby is
born this mean the diabetes is continuing after delivery also. They should be
encouraged healthy eating and to follow proper medication if needed. All women
need to check an oral glucose tolerance test between 6-12 weeks after the delivery
in the fasting state to see whether the diabetes has gone into remission or
not.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong><em>Breast feeding should be encouraged. Breast feeding
improves blood sugar and has been shown to reduce the chances of developing diabetes
in future.<sup>4</sup></em></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong><em>Breast feeding offers some protection against next
pregnancy but it is not a guaranteed birth control method. </em></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Reference</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li>Misra A,Chowbey, Makkar PM,Vikram NK,Wasir JS,Chanha D,et al.Consensus
statement for diagnosis of Obesity, Abdominal obesity and the Metabolic
Syndrome for Asian Indians and Recommendations for Physical Activity, Medical
and surgical management.JAPI2009;57</li><li>Metzger BE,BuchananTA,Coustan DR,De LeivaA,Hadden DR,Hod M.Summary and
recommendations of the fifth international workshop-conference on gestational
diabetes mellitus,Diabetes care 2007;30(Suppl 2 ):S251-260</li><li>Harris ,GD,White,RD.Diabetes management and exercise in pregnant patients
with diabetes. Clinical Diabetes 2005;23(4):165-168</li><li>Stuebe AM,
Rich-Edwards JW0, Willett WC, Manson JE, Michels KB: Duration of lactation and
incidence of type 2 diabetes.&nbsp;<em>JAMA</em>&nbsp;294:2601–2610,&nbsp;2005</li></ol>
<!-- /wp:list -->";s:10:"post_title";s:50:"GESTATIONAL DIABETES LEVELS AMONG PREGNANT MOTHERS";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:45:"gestational-diabetes-amongst-pregnant-mothers";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:47:46";s:17:"post_modified_gmt";s:19:"2019-08-29 11:47:46";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=6062";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}