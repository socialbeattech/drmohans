�b^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6225;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-08-09 22:17:48";s:13:"post_date_gmt";s:19:"2019-08-09 17:17:48";s:12:"post_content";s:4330:"<!-- wp:paragraph -->
<p>

In the bygone days, doctors were given a status in society which was next only to God. Those were days when we had a ‘family doctor’ was not just a&nbsp;<a href="https://health.economictimes.indiatimes.com/tag/physician">physician</a>&nbsp;but a friend, philosopher and guide. All that changed with increasing specialization and super-specialization in medicine. This has led to distancing the patient from the doctor. More recently we have had some deplorable and sad instances of doctors being beaten up by patients or their relatives.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>What went wrong?&nbsp;</strong><br>In one word, it is lack of trust. This is due to many factors but one of the most important, is poor communication. Things don’t always go the way we plan, as all patients may not respond to a treatment, in the same way.Thus, while some patients show remarkable improvement, there could be others who simply fail to respond or even worse, deteriorate or develop side effects after a particular treatment is given.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Did this not happen in the past?</strong><br>Of course, it did. However, the compassionate doctor was always at the side of the patient explaining everything to them. Thus, the patients and relatives had complete trust in the doctor. Today, the way doctors are trained, they are taught the ‘science of medicine’ but not the ‘art of medicine’. Hence, unfortunately, the empathy is often missing. I would rate this as one of the important reasons for the serious undermining of the faith that patients have in doctors. Most doctors are sincere and caring in nature. Admittedly,there are some ‘black sheep’ in the medical profession as in all professions. It is these few bad ones who bring disrepute to the profession as a whole.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>What can the medical profession do to regain its past glory?</strong><br></p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li>In these days of ‘google’ doctors, internet lies and fake news on Whatsapp, doctors must constantly update their knowledge.</li><li>One cannot be expected to know everything about everything. One should have the humility to say that one does not know but will get back to the patient after doing adequate research on the subject.</li><li>It would always be wise to take a second opinion, especially about conditions about which we do not have sufficient knowledge. This will help to gain the patient’s trust.</li><li>Always be honest and transparent in all dealings with the patient and relatives.</li><li>If we have made a mistake (after all, it is human to err), one should take the patient or relatives into confidence, accept that we have made a mistake and try to make up for it in whatever way we can.</li><li>Research is evolving today at a fast pace and hence every doctor should try hard to keep himself / herself updated by attending continuing medical education programmes and conferences.</li><li>As far as possible, try to follow national or international guidelines that are laid down for the treatment of a condition. Then, in the unforeseen event of a medico-legal situation arising, one would still be protected.</li><li>Cost considerations are often the major cause for dissatisfaction among patients. There are always less expensive alternatives which can be offered to the patients, in case they are unable to afford a particular line of treatment.</li><li>If the prognosis is poor, it is better to tell the patient and /or the relatives upfront so that they are prepared even for the worst eventuality.</li><li>Today in the era of the electronic medical records, it is extremely important to carefully document whatever we have done, in our case notes.</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>If these simple precautions are followed by all doctors, the pristine glory of the medical profession will soon be restored. Above all, we should follow the first principle of&nbsp;<a href="https://health.economictimes.indiatimes.com/tag/hippocratic+oath">Hippocratic Oath</a>&nbsp;which says ‘<em>Primum non nocere</em>’ which means ‘first, do no harm’.&nbsp;

</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:50:"The 10 Commandments of Doctor-Patient Relationship";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:50:"the-10-commandments-of-doctor-patient-relationship";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-09 22:17:49";s:17:"post_modified_gmt";s:19:"2019-08-09 17:17:49";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=6225";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}