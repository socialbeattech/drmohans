f^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6071;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-07-04 16:11:28";s:13:"post_date_gmt";s:19:"2019-07-04 11:11:28";s:12:"post_content";s:4788:"<!-- wp:paragraph -->
<p><strong>How
to reduce blood sugars during pregnancy?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Maintaining tight blood glucose levels is the most important aspect during pregnancy and it helps to ensure the best chance of a successful pregnancy.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Control of diabetes levels is important for people who have diabetes going into their pregnancy as well as people who develop diabetes during their pregnancy (gestational diabetes</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Managing Pregnancy Diabetes</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Women with pregnancy diabetes require more frequent antenatal check ups. Sonologic studies should be carried out at specified intervals to confirm fetal well-being and to rule out malformations. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>You can have maintained control of diabetes with a healthy diet, medication and exercise. Medication is usually indicated if blood sugars do not come to normal with diet and exercise. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>General guidelines for healthy eating</strong> </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In order to get all the nutrition, vitamins and minerals for the baby and mother, you should eat food from different sources every day. Diet should aim to provide sufficient calories proportion to body weight. 40-50 % of the total calories should be derived from carbohydrates. 20-25% of calories from protein and not more than 25% calories should come from fat. You can allow a “healthy plate “, the concept for each meal. For example, each meal should be 25 % protein, 25% starch, and 50 % non-starchy foods, such as vegetables or salad. </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Calcium and folate should be supplemented in the diet. A dietitian can guide you in taking the right portion sizes and the right amount of protein, fat and carbohydrate.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Here
are some general tips:-</p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Eat a variety of foods
distributing calories and carbohydrate and taking enough protein</li><li>Eat 3 meals and 3 snacks a
day. Spreading the food out over the day will help to keep blood sugar in the
target range and will reduce hunger</li><li>Include 4—5 servings of
greens, yellow and green vegetables in daily diet</li><li>Adequate fluids (2-3 liters
/day) should be taken .</li><li>Iron rich foods should be
included. Eg: - Greens</li><li>Consume 650 ml of milk/ day
to meet calcium requirements.</li><li>Avoid direct sugars,
artificial sweeteners.</li><li>Avoid alcohol and tobacco.</li><li>Use a combination of oils
Eg: - Groudnut oil, Gingelly oil, Rice bran oil, Canola oil</li><li>Include plant protein like
pulses, dhal etc.</li><li>Restrict animal protein like
red meat.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Physical Activity</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Aerobic activity of moderate intensity for 30mins/day on most days of the week has shown benefits in metabolic control. Start with light to moderate exercise. Any simple physical activity is better than no activity.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Caution</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Don’t
exercise in flat position.</li><li>Stop
if your heart rate is &gt;140/min.</li><li>Stop
if you feel uterine contraction</li><li>If
your gynecologist has advised against exercising, please follow her advice</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><strong>Insulin</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong> </strong>Insulin is indicated when target blood glucose levels are not attained with diet and physical activity. Different types of insulin are prescribed by the Diabetologist.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Monitoring
blood sugar</strong></p>
<!-- /wp:paragraph -->

<!-- wp:list -->
<ul><li>Blood
sugars need to be checked several times during pregnancy.</li><li>It
can be done using glucometer or with other continuous monitoring devices or in nearby
clinics.</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>All women with pregnancy diabetes should be encouraged to monitor their blood sugars on a daily basis. In addition to fasting and postprandial sugars, the measurement of Serum Fructosamine (3 weeks blood glucose average) is a useful method of assessing long –term glycemic control. This is a marker of glycosylation of serum proteins, which gives an idea of the glycemic control over the preceding 2-3 weeks. </p>
<!-- /wp:paragraph -->";s:10:"post_title";s:29:"Blood sugars during pregnancy";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:29:"blood-sugars-during-pregnancy";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-08-29 16:41:15";s:17:"post_modified_gmt";s:19:"2019-08-29 11:41:15";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:28:"https://drmohans.com/?p=6071";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}