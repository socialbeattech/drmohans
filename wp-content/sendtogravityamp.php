<?php
require('wp-config.php');

//API for gravity form
$api_key = '7267462139';
$private_key = '073fa89748ba99d';
 
//set route
$route = 'forms/10/entries';

function calculate_signature( $string, $private_key ) {
    $hash = hash_hmac( 'sha1', $string, $private_key, true );
    $sig = rawurlencode( base64_encode( $hash ) );
    return $sig;
}

//creating request URL
$expires = strtotime( '+60 mins' );
$string_to_sign = sprintf( '%s:%s:%s:%s', $api_key, 'POST', $route, $expires );
$sig = calculate_signature( $string_to_sign, $private_key );
$url = get_site_url() . '/gravityformsapi/' . $route . '?api_key=' . $api_key . '&signature=' . $sig . '&expires=' . $expires;

$entries = array(
    array(
        'date_created' => date('Y-m-d H:i:s'),
        'is_starred'   => 0,
        'is_read'      => 0,
        'ip'           => '::1',
        'currency'     => 'USD',
        'created_by'   => 1,
        'user_agent'   => 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
        'status'       => 'active',
        '1'            => $_GET['Name'],
        '2'            => $_GET['Email'],
        '3'            => $_GET['Phone'],
        '4'            => $_GET['Location'],
        '9'            => $_GET['Preferred Date'],
        '6'            => $_GET['Patient Type'],
        '7'            => $_GET['Patient No'],
        '10'            => $_GET['Comments'],
        '11'            => $_GET['URL'],
    )
);

//json encode array
$entry_json = json_encode( $entries );

//retrieve data
$response = wp_remote_request( $url , array( 'method' => 'POST', 'body' => $entry_json, 'timeout' => 25 ) );
/*if ( wp_remote_retrieve_response_code( $response ) != 200 || ( empty( wp_remote_retrieve_body( $response ) ) ) ){
    //http request failed
    die( 'There was an error attempting to access the API.' );
}*/
if ( (wp_remote_retrieve_response_code( $response ) != 200) || (  wp_remote_retrieve_body( $response ) =="" ) ){
    //http request failed
    /*echo "<br>wp_remote_retrieve_response_code :".wp_remote_retrieve_response_code( $response );
	echo "<br>wp_remote_retrieve_body :";
	print_r(wp_remote_retrieve_body( $response )); */
	die( 'There was an error attempting to access the API.' );
	
}

//result is in the response "body" and is json encoded.
$body = json_decode( wp_remote_retrieve_body( $response ), true );


  
 $Name = $_GET['Name'];
 $Email = $_GET['Email'];
 $Phone = $_GET['Phone'];
 $Comments = $_GET['Comments'];
 $Location = $_GET['Location'];
 $Preferred Date = $_GET['Preferred Date'];
 $Patient Type = $_GET['Patient Type'];
 $Patient No = $_GET['Patient No'];
 $URL = $_GET['URL'];
/* exit();
die(); */
//Gravity form mail Notifications

// Multiple recipients
$to = 'velmurugan@socialbeat.in';

// Subject
$subject = 'Socialbeat AMP Desktop Enquiry';


// Message
$message = '<html><head><title>Socialbeat AMP Desktop Enquiry</title></head><body>';
$message.= '<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">';
$message.= '<tr><td bgcolor="#EAF2FA"><strong>Name:</strong></td></tr>';
$message.= '<tr bgcolor="#FFFFFF">'.$Name.'</td></tr>';
$message.= '<tr bgcolor="#EAF2FA"><td><strong>Email :</strong></td></tr>';
$message.= '<tr bgcolor="#FFFFFF">'.$Email.'</td></tr>';
$message.= '<tr bgcolor="#EAF2FA"><td><strong>phone:</strong></td></tr>';
$message.= '<tr bgcolor="#FFFFFF"><td>'.$phone.'</td></tr>';
$message.= '<tr bgcolor="#EAF2FA"><td><strong>Comments:</strong></td></tr>';
$message.= '<tr bgcolor="#FFFFFF"><td>'.$Comments.'</td></tr>';
$message.= '<tr bgcolor="#EAF2FA"><td><strong>Preferred Date:</strong></td></tr>';
$message.= '<tr bgcolor="#FFFFFF"><td>'.$Preferred Date.'</td></tr>';
$message.= '<tr bgcolor="#EAF2FA"><td><strong>Patient Type:</strong></td></tr>';
$message.= '<tr bgcolor="#FFFFFF"><td>'.$Patient Type.'</td></tr>';
$message.= '<tr bgcolor="#EAF2FA"><td><strong>Patient No:</strong></td></tr>';
$message.= '<tr bgcolor="#FFFFFF"><td>'.$Patient No.'</td></tr>';
$message.= '<tr bgcolor="#EAF2FA"><td><strong>URL:</strong></td></tr>';
$message.= '<tr bgcolor="#FFFFFF"><td>'.$URL.'</td></tr>';

// To send HTML mail, the Content-type header must be set
$headers = "From: KLAY Schools in India <outreach@klayschools.com>\r\n";
$headers .= "Reply-To: enquiry@klayschools.com\r\n";
$headers .= 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "X-Mailer: PHP/".phpversion();

// Mail it
mail($to, $subject, $message, $headers);

//print_r($body); exit;
if( $body['status'] > 202 ){
    $error = $body['response'];
 
        //entry update failed, get error information, error could just be a string
    if ( is_array( $error )){
        $error_code     = $error['code'];
        $error_message  = $error['message'];
        $error_data     = isset( $error['data'] ) ? $error['data'] : '';
        $status     = "Code: {$error_code}. Message: {$error_message}. Data: {$error_data}.";
    }
    else{
        $status = $error;
    }
    die( "Could not post entries. {$status}" );
}
else
{
	header('Location:https://drmohans.com/amp-thankyou/');
}


?>