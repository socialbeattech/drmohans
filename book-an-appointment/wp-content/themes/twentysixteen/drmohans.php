<!doctype html>

<html amp lang="en">
<?php /* Template Name: AMP Dr Mohan's New template */ ?>
  <head>
    <meta charset="utf-8">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<script async custom-element="amp-video" src="https://cdn.ampproject.org/v0/amp-video-0.1.js"></script>
	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    <script custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js" async></script>
    <script custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Cabin|Libre+Franklin" rel="stylesheet"> 
    <title>Dr.Mohans - Book your appointment</title>
    <link rel="canonical" href="https://drmohans.com/book-an-appointment-chennai/new/">
	<link rel="shortcut icon" href="https://drmohans.com/wp-content/uploads/cropped-m-32x32.png">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom>
.about{
padding-left:25px;
padding-right:25px;
}
.number{
font-weight: 700;
font-family: 'Open Sans', sans-serif;
}
.number1{
font-family: 'Open Sans', sans-serif;
}
.joint{
padding-left:25px;
padding-right:25px;
}
.button{
color:white;
background-color:#428bca;
 font-family: 'Open Sans', sans-serif;
 font-size:18px;
}
.icon{
color:#337ab7;
font-size:14px;
text-align:center;
}
.asta{
padding-left:25px;
padding-right:25px;
}
.whitc{
color:white;
}
.loca{
text-align:center;
padding-left:25px;
padding-right:25px;
}
.head{
text-align:center;
font-size:34px;
color:#1c70b7;
 font-family: 'HelveticaNeueBlack';
}
.colorr{
color:#1c70b7;
}
.headl{
text-align:center;
font-size:30px;
color:#615f5f;
 font-family: 'Open Sans', sans-serif;
}
.backmain{
background-image:url('https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Header-bg.png');
}
.backmain1{
background-image:url('https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Branches-bg.png')
}
.headl1{
text-align:center;
font-size:30px;
color:#615f5f;
 font-family: 'Open Sans', sans-serif;
 background-color:#eeeded;
 padding-top:20px;
}
.padtop{
padding-top:10px;
}
.padbot{
padding-bottom:10px;
}
.text{
font-size:18px;
color:#4e5860;
font-family: 'Open Sans', sans-serif;
}
.text4{
font-size:18px;
color:white;
background-color:#1c70b7;
text-align:center;
font-family: 'Open Sans', sans-serif;
}
.text1{
font-size:14px;
color:#4e5860;
}
.copy{
padding-left:40px;
padding-right:40px;
font-weight: 700;
font-family: 'Open Sans', sans-serif;
}
.imgp{
padding-left:12px;
padding-right:12px;
padding-bottom:20px;
}
.imgasta{
padding-left:35px;
padding-right:35px;
}
/*@font-face {
    font-family: 'HelveticaNeueBlack';
    src: url('fonts/HelveticaNeueBlack.eot');
    src: url('fonts/HelveticaNeueBlack.eot') format('embedded-opentype'),
         url('fonts/HelveticaNeueBlack.woff2') format('woff2'),
         url('fonts/HelveticaNeueBlack.woff') format('woff'),
         url('fonts/HelveticaNeueBlack.ttf') format('truetype'),
         url('fonts/HelveticaNeueBlack.svg#HelveticaNeueBlack') format('svg');
}
@font-face {
    font-family: 'OpenSansRegular';
    src: url('fonts/OpenSansRegular.eot');
    src: url('fonts/OpenSansRegular.eot') format('embedded-opentype'),
         url('fonts/OpenSansRegular.woff2') format('woff2'),
         url('fonts/OpenSansRegular.woff') format('woff'),
         url('fonts/OpenSansRegular.ttf') format('truetype'),
         url('fonts/OpenSansRegular.svg#OpenSansRegular') format('svg');
}
@font-face {
    font-family: 'OpenSansSemibold';
    src: url('fonts/OpenSansSemibold.eot');
    src: url('fonts/OpenSansSemibold.eot') format('embedded-opentype'),
         url('fonts/OpenSansSemibold.woff2') format('woff2'),
         url('fonts/OpenSansSemibold.woff') format('woff'),
         url('fonts/OpenSansSemibold.ttf') format('truetype'),
         url('fonts/OpenSansSemibold.svg#OpenSansSemibold') format('svg');
}
@font-face {
    font-family: 'MerriweatherBoldIt';
    src: url('fonts/MerriweatherBoldIt.eot');
    src: url('fonts/MerriweatherBoldIt.eot') format('embedded-opentype'),
         url('fonts/MerriweatherBoldIt.woff2') format('woff2'),
         url('fonts/MerriweatherBoldIt.woff') format('woff'),
         url('fonts/MerriweatherBoldIt.ttf') format('truetype'),
         url('fonts/MerriweatherBoldIt.svg#MerriweatherBoldIt') format('svg');
}
@font-face {
    font-family: 'MerriweatherBold';
    src: url('fonts/MerriweatherBold.eot');
    src: url('fonts/MerriweatherBold.eot') format('embedded-opentype'),
         url('fonts/MerriweatherBold.woff2') format('woff2'),
         url('fonts/MerriweatherBold.woff') format('woff'),
         url('fonts/MerriweatherBold.ttf') format('truetype'),
         url('fonts/MerriweatherBold.svg#MerriweatherBold') format('svg');
}
@font-face {
    font-family: 'MerriweatherRegular';
    src: url('fonts/MerriweatherRegular.eot');
    src: url('fonts/MerriweatherRegular.eot') format('embedded-opentype'),
         url('fonts/MerriweatherRegular.woff2') format('woff2'),
         url('fonts/MerriweatherRegular.woff') format('woff'),
         url('fonts/MerriweatherRegular.ttf') format('truetype'),
         url('fonts/MerriweatherRegular.svg#MerriweatherRegular') format('svg');
}
@font-face {
    font-family: 'HELR65W';
    src: url('fonts/HELR65W.eot');
    src: url('fonts/HELR65W.eot') format('embedded-opentype'),
         url('fonts/HELR65W.woff2') format('woff2'),
         url('fonts/HELR65W.woff') format('woff'),
         url('fonts/HELR65W.ttf') format('truetype'),
         url('fonts/HELR65W.svg#HELR65W') format('svg');
}
*/

.cabin{
font-family: 'Cabin', sans-serif;
font-weight: 700;
}
.cabin1{
font-family: 'Cabin', sans-serif;
}
.libre{
font-family: 'Libre Franklin', sans-serif;
}
.libre1{
font-family: 'Libre Franklin', sans-serif;
font-weight: 700;
}
.size{
text-align:center;
font-size:21px;
color:#2e2e2e;
font-weight: 700;
font-family: 'Open Sans', sans-serif;
}
.f14{
font-size:14px;
text-align:center;
color:#615f5f;

font-family: 'Open Sans', sans-serif;
}
.f24{
font-size:27px;
text-align:center;
color:#0e7d44;
padding-top:5px;
padding-bottom:5px;
font-family: 'HelveticaNeueBlack';
}
.tac{
text-align:center;
}
.bt1{
font-size:18px;
background-color:#428bca;
color:white;
padding-top:5px;
padding-bottom:5px;
padding-left:10px;
padding-right:10px;
text-align:center;

font-family: 'Open Sans', sans-serif;
}
.butgre{
background-color:#0e7d44;
color:white;
}
.opensr{
font-family: 'Open Sans', sans-serif;
}
.hori {
  border: 0;
  border-top: 1px solid #615f5f;
  border-bottom: 1px solid #615f5f;
  margin-left:25px;
  margin-right:25px;
  }
 .solid {
  border-style: solid;
  margin-left:20px;
  margin-right:20px;
  padding-bottom:30px;
  }
  .solidb
  {
  border-color:white;
  }
  .padrl{
  padding-left:7px;
  padding-right:7px;
  color:#797979;
  }
  .padleft{
  padding-left:125px;
  }
  .ftc{
  color:#797979;
  }
  .bro{
  font-size:18px;
font-weight: 700;
font-family: 'Open Sans', sans-serif;
  padding-bottom:20px;
  color:#1c70b7;
  }
  .navb{
  font-family: 'HelveticaNeueBlack';
  font-size:15px;
  color:#1c70b7;
  }
  .hellb{
  font-family: 'HELR65W';
  }
  .back{
  background-color:#eeeded;
  }
  .pdb5{
  padding-bottom:5px;
  }
    .pdb2{
  padding-bottom:2px;
  }
  .fsi18{
  font-size:18px;
  }
  .pdalll{
  padding-bottom:10px;
  padding-top:10px;
  font-size:18px;

font-family: 'Open Sans', sans-serif;
  color:#615f5f;
  }
    .pddalll{
  padding-bottom:4px;
  padding-top:4px;
  font-size:14px;
font-family: 'Open Sans', sans-serif;
  color:#615f5f;
  }
  .pdb20{
  padding-bottom:20px;
  }
  .merri{
  font-family: 'Merriweather', serif;
  font-weight: 700;
  }
  .merril{
  font-family:'Merriweather', serif;
  }
  .greeen{
  background-color:#19a94c;
  }
  .grenc{
  color:#19a94c;
  }
  .ggreyc{
  color:#999999;
  }
  .rredc{
  color:#d12727;
  }
  .padritolebt{
  padding-top:5px;
  padding-bottom:5px;
  padding-left:0px;
  padding-right:36px;
  margin-right:1px;
  color:#797979;
  }
    .padritolebt1{
  padding-top:5px;
  padding-bottom:5px;
  padding-left:0px;
  padding-right:7px;
  color:#797979;
  }
  .blackc{
  color:black;
  }
  .mar{
  margin:0;
  }
  .greybaa{
  background-color:#37404a;
  color:white;
  }
  .margrile{
  margin-left:20px;
  margin-right:20px;
  }
  .padtop2{
  padding-top:30px;
  }
  .bortoboc{
  border-top: double #999999;
  border-bottom: double #999999;
  }
    .padtop222{
  padding-top:50px;
  }
  .fsi28{
  font-size:28px;
  }
  .fsi13{
  font-size:13px;
  text-align: justify;
  }
  .hamburger {
  padding-left: 10px;
} 
.fsi14{
font-size:14px;
text-align:justify;
}
.butdec{
padding-top:10px;
padding-bottom:10px;
padding-left:61px;
padding-right:61px;
border-radius:5px;
}
.padhlefri{
padding-left:15px;
padding-right:12px;
}
.padalll{
padding-top:5px;
padding-bottom:5px;
padding-left:2px;
padding-right:2px;
}
.padalll1{
padding-top:5px;
padding-bottom:20px;
padding-left:2px;
padding-right:2px;
}
.talju{
text-align:justify;
}
.tal{
text-align:left;
}
.fsi16{
font-size:16px;
}
.pdallbu{
padding-top:10px;
padding-bottom:10px;
padding-left:15px;
padding-right:15px;
}
.blaa{
color:black;
}
.borbotf{
border:2px solid #bfbfbf;
}
.tal11{
text-align:left;
}
.bordgrey{
border:6px solid grey; 
}
.fsi21{
font-size:21px;
}
.tcnone{
text-decoration:none;
}
.ligrey{
background-color:#e8e8ea;
}
.backwhi{
background-color:white;
}
.fsi38{
font-size:38px;
}
.weightbold{
font-weight:bold;
}
.gree{
color:#0e7d44;
}
.fsi21{
font-size:20px;
}
.colwhi{
color:white;
}
.backwhite{
background-color:white;
}
.topp{
  margin:0;
  }
  .padlefor{
  padding-left:80px;
  }
  .padlefor1{
  padding-left:1px;
  padding-right:30px;
  }
  .padrigor{
  padding-right:100px;
  }
.sidebar {
  padding: 10px;
  margin: 0;
}
.sidebar > li {
  list-style: none;
  margin-bottom:10px;
}
.backblue{
background-color:#0087cb;
}
.fsi24{
font-size:22px;
color:#025fbc;
}
.font8{
font-size:13px;
}
.tdn{
text-decoration:none;
color:black;
cursor:pointer;
}
 .myList{ 
 list-style-type: none;
 } 
.sidebar a {
  text-decoration: none;
}
.close-sidebar {
  font-size: 1.5em;
  padding-left: 5px;
}
amp-accordion section[expanded] > h4.show-more {
  display: none;
}
amp-accordion section:not([expanded]) .show-less {
  display: none;
}
  .sticky{
  background-color:#f2622e;
  text-align:center;
  cursor:pointer;
  color:white;
  font-size:14px;
  padding-top:10px;
  padding-bottom:10px;
  padding-left:19px;
  padding-right:19px;
  }
  .dummy-div{
    height:50px;
  }
  .sticky-bar{

    position: fixed;
    bottom: 0;

  }

  input{
    width: 80%;
  }

  select{
    width: 80%;
  }
  .fixed-footerleft {
    font-size: 16px;
    background: #f2622e;
    color: #fff;
    line-height: 45px;
    position: fixed;
    text-align: center;
    width: 50%;
    z-index: 99999;
    right: 0;
    bottom: 0;
    border: none;
    text-decoration: none;
    pointer: cursor;
    border-left: 1px solid #fff;
  }
    .fixed-footerright {
    font-size: 16px;
    background: #1a7cea;
    color: #fff;
    line-height: 45px;
    position: fixed;
    text-align: center;
    width: 100%;
    z-index: 99999;
    left: 0;
    bottom: 0;
    border: none;
    text-decoration: none;
    pointer: cursor;
    border-right: 1px solid ;
  }
  .bluecol{
  color:#1a7cea;
  }
  .greyfor{
  color:#727272;
  }
  .pdhedbot{
  padding-bottom:20px;
  }
  .bgsub{
  background: #1a7cea;
  }
</style>
</head>
<body>
<header class="ampstart-headerbar fixed flex justify-start items-center top-0 left-0 right-0 pl2 pr4">
  <nav class="ampstart-headerbar-nav ampstart-nav xs-hide sm-hide">
    <div class="list-reset center m0 p0 flex justify-center nowrap">
      <div class="ampstart-nav-item ampstart-nav-dropdown relative">
        <amp-accordion layout="container" disable-session-states="" class="ampstart-dropdown absolute top-0 left-0 right-0 bottom-0">
        <section>
          <header class="pdhedbot backwhite tac"><br><b>☰</b></header>
          <ul class="ampstart-dropdown-items list-reset m0 p0">
              <li class="ampstart-dropdown-item myList">
                <a href="#" class="text-decoration-none tdn number1 tac"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Express-Checku.png" alt="Welcome" height="40px" width="40px"></amp-img><br>120 Minutes Express Checkup</a>
              </li><br>
              <li class="ampstart-dropdown-item myList">
                <a href="#" class="text-decoration-none tdn number1 tac"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Brown-Rice.png" alt="Welcome" height="32px" width="9px"></amp-img> <br>High Fibre Rice</a>
              </li><br>
			  <li class="ampstart-dropdown-item myList">
                <a href="#" class="text-decoration-none tdn number1 tac"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Event-Clinic.png" alt="Welcome" height="25px" width="25px"></amp-img> <br>Evening Clinic</a>
              </li><br>
			  <li class="ampstart-dropdown-item myList">
                <a href="#" class="text-decoration-none tdn number1 tac"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Home-Services.png" alt="Welcome" height="25px" width="31px"></amp-img> <br>Home Services</a>
              </li><br>
			  <li class="ampstart-dropdown-item myList">
                <a href="#" class="text-decoration-none tdn number1 tac"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Diabetic-shop.png" alt="Welcome" height="31px" width="28px"></amp-img> <br>Diabetic Shop</a>
              </li>
          </ul>
        </section>
        </amp-accordion>
<section class="backmain">
<div class="container-full">
<div class="row">
<div class="col-sm-12 tac">
<amp-img src="https://drmohans.com/book-an-appointment/wp-content/uploads/2016/05/Drmohans-diabetes-lp.png" alt="Welcome" height="70px" width="216px"></amp-img>
<amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Diabates-Care.png" alt="Welcome" height="83px" width="100px"></amp-img>
<p class="number fsi21 padhlefri tac">CONSULT THE BEST DIABETES CARE PROFESSIONALS ONLY AT DR MOHANS'S</p>
<p class="number1 fsi14 padhlefri">With 25 years in the field of diabetes care and over 3,60,000 satisfied patients, we have transformed into an international centre of excellence in diabetes. DMDSC stands committed to provide world class health care facilities for diabetes and all its complications.</p>
</div></p>
</div>
</div>
  <form id="form" method="GET"
  class="tac solid solidb backwhi borbotf"
  action="//www.indiahomehealthcare.com/lp/sendtogravitycarehome.php"
  target="_top">
  <p class="fsi24 number"><b>Book an Appointment</b></p>
  <div  class="ampstart-input inline-block relative">
    <input type="text" class="block ftc padalll weightbold" name="name" placeholder="Name" required pattern="[a-zA-Z\s]+" ><br><br>
	<input type="email" class="block ftc padalll weightbold" name="email"  placeholder="Email"  required><br><br>
    <span visible-when-invalid="valueMissing" validation-for="show-all-on-submit-email"></span>
    <span visible-when-invalid="typeMismatch" validation-for="show-all-on-submit-email"></span>
	  <input type="text" class="block ftc padalll weightbold" name="mobile" placeholder="Phone" required pattern="\d{10}"><br>
	  <p class="greyfor number font8">Patient type *</p>
	  <div class="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3">
  <input type="radio" value="1" name="rb" id="rb1" class="relative" checked>
  <label for="rb1" class="tar greyfor number font8" aria-hidden="true"><br>New Patient</label>
</div>
<div class="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3">
  <input type="radio" value="2" name="rb" id="rb2" class="relative">
  <label for="rb2" class="tar greyfor number font8" aria-hidden="true"><br>Review Patient</label>
</div><br>
<select name="location" required="required" data-error="Please select your Project" id="show-all-on-submit-location" class="padritolebt weightbold">
    <option selected disabled hidden>Select Location</option>
    <option class="blackc" value="1">Anna Nagar</option>
    <option class="blackc" value="2">Avadi</option>
    <option class="blackc" value="3">Gopalapuram</option>
    <option class="blackc" value="4">Karapakkam</option>
    <option class="blackc" value="5">Porur</option>
    <option class="blackc" value="6">Selaiyur</option>
    <option class="blackc" value="7">Tambaram</option>
    <option class="blackc" value="8">Vadapalani</option>
    <option class="blackc" value="9">Velachery</option>
</select>
 <span visible-when-invalid="valueMissing" validation-for="show-all-on-submit-location"></span>
   <span visible-when-invalid="typeMismatch" validation-for="show-all-on-submit-location"></span>
<br><br>	 
<select required="required" name="services" data-error="Please select your Service" id="show-all-on-submit-services" class="padritolebt1 weightbold">
    <option selected disabled hidden>Preferred Service</option>
    <option class="blackc" value="1">Regular Appointment</option>
    <option class="blackc" value="2">Express 120 minutes Appointment</option>
    <option class="blackc" value="3">Evening clinic Appointment</option>
    <option class="blackc" value="4">Home Care</option>
    <option class="blackc" value="5">Master Health Checkup</option>
    <option class="blackc" value="6">Woman's Day Package</option>
</select>
 <span visible-when-invalid="valueMissing" validation-for="show-all-on-submit-services"></span>
   <span visible-when-invalid="typeMismatch" validation-for="show-all-on-submit-services"></span><br>
  <input type="hidden" name="leadsource" value="Landing Page" />
     <input type="hidden" name="leadsourcemode" value="Get In Touch" />
    <input type="hidden" name="url" value="<?php the_permalink(); ?>" /> 
  </div><br>
  <input type="text" class="block ftc padalll1 weightbold" name="Comments" placeholder="Comments" required pattern="[a-zA-Z\s]+" ><br><br>
  <input type="submit"
    value="SUBMIT"
    class="ampstart-btn caps bgsub merril butdec whitc"><br><br><br>
</form><br>
</section>
<section class="backblue">
<div class="container-full about">
<div class="row"><br>
<div class="col-sm-12 tac number whitc fsi28">Our Branches</div><br>
</div>
</div>
</section>
<section class="backmain1">
<div class="container-full about">
<div class="row">
<div class="col-sm-12 tal"><p class="tal mar number"><br><a class="tdn" href="https://drmohans.com/diabetes-centre-locations/chennai/anna-nagar/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/locate.png"  alt="Welcome" height="13px" width="14px"></amp-img>Anna Nagar</a></p>
<p class="tal number"><a class="tdn" href="https://drmohans.com/diabetes-centre-locations/chennai/avadi/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/locate.png"  alt="Welcome" height="13px" width="14px"></amp-img>Avadi</a></p>
<p class="tal number"><a class="tdn" href="https://drmohans.com/diabetes-centre-locations/chennai/gopalapuram-main-centre/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/locate.png"  alt="Welcome" height="13px" width="14px"></amp-img>Gopalapuram</a></p>
<p class="tal number"><a class="tdn" href="https://drmohans.com/diabetes-centre-locations/chennai/omr-karapakkam/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/locate.png"  alt="Welcome" height="13px" width="14px"></amp-img>OMR-Karapakkam</a></p>
<p class="tal number"><a class="tdn" href="https://drmohans.com/diabetes-centre-locations/chennai/porur/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/locate.png"  alt="Welcome" height="13px" width="14px"></amp-img>Porur</a></p>
<p class="tal number"><a class="tdn" href="https://drmohans.com/diabetes-centre-locations/chennai/selaiyur/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/locate.png"  alt="Welcome" height="13px" width="14px"></amp-img>Selaiyur</a></p>
<p class="tal number"><a class="tdn" href="https://drmohans.com/diabetes-centre-locations/chennai/tambaram/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/locate.png"  alt="Welcome" height="13px" width="14px"></amp-img>Tambaram</a></p>
<p class="tal number"><a class="tdn" href="https://drmohans.com/diabetes-centre-locations/chennai/vadapalani-clinic/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/locate.png"  alt="Welcome" height="13px" width="14px"></amp-img>Vadapalani</a></p>
<p class="tal number"><a class="tdn" href="https://drmohans.com/diabetes-centre-locations/chennai/velachery/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/locate.png"  alt="Welcome" height="13px" width="14px"></amp-img>Velachery</a></p><br>
<p class="bordgrey"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Dr-mohans-branch-head.png" alt="Welcome" layout="responsive" height="323px" width="554px"></amp-img></p>
</div><br>
</div>
</div>
</section>
<section class="greybaa">
<div class="container-full about">
<div class="row">
<div class="col-sm-12"><br><p class="libre fsi13 tac number1">&copy; Copyright 2018 Dr. Mohan's Diabetes Specialities Centre</p>
<p class="tac"><a href="https://www.facebook.com/drmohansdiabetesinstitutions" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Fb.png"  alt="Welcome" height="30px" width="25px"></amp-img></a>
<a href="https://twitter.com/dmdsc" target="_blank"><amp-img src="https://drmohans.com/book-an-appointment-chennai/wp-content/themes/twentysixteen/images/Twitter.png"  alt="Welcome" height="30px" width="40px"></amp-img></a>
</p>
<p class="tac mar"><a class="colwhi tcnone number1" href="https://www.socialbeat.in/" target="_blank">Designed & Developed by Social Beat</a></p>
</div>
</div>
</div><br>
</section>
<section>
<div class="dummy-div">
</div>
<a on="tap:form.scrollTo(duration=200)" href="#form" class="fixed-footerright din-bold text-center">ENQUIRE NOW</a>
</section>
</body>
</html>