<?php
/**
 * Template Name: Thankyou
 *
 * 
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


<!-- Start of Branches -->
<section class="our_branch_block clearfix">
<div class="branch_title">
<div class="branch_top_icon"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Branches_icons.png" alt="Branch" class="img-responsive" width="83" height="53" /></div>
<h1>Our Branches</h1>
</div>
	<div class="container">
		<div class="branch_services">
			<div class="col-xs-12 col-sm-5 col-md-5 branch_left">	
				<div class="panel-group clearfix" id="accordion">
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#chennai">Chennai</a>
					</h4>
					</div>
					<div id="chennai" class="panel-collapse collapse in">
					<div class="panel-body">
						<ul class="subr_branch">
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/adyar">Adyar</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/anna-nagar">Anna Nagar</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/avadi">Avadi</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/broadway">Broadway</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/gopalapuram-main-centre">Gopalapuram</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/omr-karapakkam">OMR- Karapakkam</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/porur">Porur</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/tambaram">Tambaram</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/vadapalani-clinic">Vadapalani</a></li>
<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/velachery">Velachery</a></li>
						</ul>
					</div>
					</div>
				</div>
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#hyderabad">Hyderabad</a>
					</h4>
					</div>
					<div id="hyderabad" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
							<li><a href="http://drmohans.com/diabetes-centre-locations/hyderabad/indira-park-road-hyderabad">Indira Park Road</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/hyderabad/jubilee-hills-hyderabad">Jubilee Hills</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/hyderabad/kukatpally">Kukatpally</a></li>
						</ul>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#karnataka">Karnataka</a>
					</h4>
					</div>
					<div id="karnataka" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
							<li><a href="http://drmohans.com/diabetes-centre-locations/indira-nagar">Indira Nagar</a></li>	
							<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/jp-nagar">JP Nagar</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/malleswaram">Malleswaram</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/mangalore">Mangalore</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/mysuru">Mysuru</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/whitefield">Whitefield</a></li>						
						</ul>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#delhi">Delhi</a>
					</h4>
					</div>
					<div id="delhi" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
						<li><a href="http://drmohans.com/diabetes-centre-locations/delhi/kirti-nagar">Kirti Nagar</a></li>
						</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#up">Uttar Pradesh</a>
					</h4>
					</div>
					<div id="up" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
						<li><a href="http://drmohans.com/diabetes-centre-locations/uttar-pradesh/lucknow">Lucknow</a></li>
						</ul>
						</div>
					</div>
				</div>
				
				</div> 				
				<ul class="main_branch">
					<li><a href="http://drmohans.com/diabetes-centre-locations/chunampet">Chunampet</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/coimbatore">Coimbatore</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/gudiyatham">Gudiyatham</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/madurai">Madurai</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/muscat-oman">Muscat</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/pondicherry">Pondicherry</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/vellore">Vellore</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/bhubaneswar">Bhubaneswar</a></li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-7 branch_right">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Dr-mohans-branch-head.png" class="img-responsive" alt="Branch" width="554" height="323" />				
			</div>
		</div>
	</div>
</section>

<!-- End of Branches -->

<!-- Start of services -->

<section class="app_services_block clearfix">
	<div class="container">
		<div class="app_services">
			<div class="col-xs-12 col-sm-6 col-md-6 apps_left">	
				<div class="apps_symbol"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Hand-in-app.png" class="img-responsive" alt="Apps" width="127" height="127" /></div>
				<div class="apps_captions"><p>Dr Mohan's App</p><span class="border_bootm"></span></div>				
				<div class="apps_desc"><p class="text-center">We understand that living with diabetes may be challenging, but you can now simplify your diabetes management by downloading the Dr. Mohan's Diabetes </br>Management App</p></div>		
				<div class="apps_more"><p class="text-center"><a href="https://play.google.com/store/apps/details?id=com.janacare.habits.drmohan&hl=en">DOWNLOAD NOW</a></p></div>				
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 apps_right">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Dromohans_apps.png" class="img-responsive" alt="Apps" width="460" height="485" />				
			</div>
		</div>
	</div>
</section>
<!-- End of services -->

<?php get_footer(); ?>
