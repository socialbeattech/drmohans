<?php
/**
 * Template Name: Book Appointment
 *
 * 
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NGXBRHZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!-- Start of Branches -->
<section class="our_branch_block clearfix">
<div class="branch_title">
<div class="branch_top_icon"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Branches_icons.png" alt="Branch" class="img-responsive" width="83" height="53" /></div>
<h1>Our Branches</h1>
</div>
	<div class="container">
		<div class="branch_services">
			<div class="col-xs-12 col-sm-5 col-md-5 branch_left">	
				<div class="panel-group clearfix" id="accordion">
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#chennai">Chennai</a>
					</h4>
					</div>
					<div id="chennai" class="panel-collapse collapse in">
					<div class="panel-body">
						<ul class="subr_branch">
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/anna-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Anna Nagar</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/avadi?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Avadi</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/gopalapuram-main-centre?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Gopalapuram</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/omr-karapakkam?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">OMR- Karapakkam</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/porur?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Porur</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/selaiyur?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Selaiyur</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/tambaram?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Tambaram</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/vadapalani-clinic?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Vadapalani</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/chennai/velachery?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Velachery</a></li>
						</ul>
					</div>
					</div>
				</div>
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#hyderabad">Hyderabad</a>
					</h4>
					</div>
					<div id="hyderabad" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
                            <li><a href="http://drmohans.com/diabetes-centre-locations/hyderabad/indira-park-road-hyderabad?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Indira Park Road</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/hyderabad/jubilee-hills-hyderabad?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Jubilee Hills</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/hyderabad/kukatpally?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Kukatpally</a></li>
                            <li><a href="http://drmohans.com/diabetes-centre-locations/telangana/dilsukh-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Dilsukh Nagar</a></li>
							<li><a href="http://drmohans.com/diabetes-centre-locations/telangana/secunderabad?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Secunderabad</a></li>
							<li><a href="https://drmohans.com/diabetes-centre-locations/telangana/tolichowki?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Tolichowki</a></li>
							<li><a href="https://drmohans.com/diabetes-centre-locations/telangana/a-s-rao-nagar/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">A.S.Rao Nagar</a></li>
						</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#ap">Andhra Pradesh</a>
					</h4>
					</div>
					<div id="ap" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
							<li><a href="http://drmohans.com/diabetes-centre-locations/ap/vijayawada?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Vijayawada</a></li>							
							<li><a href="http://drmohans.com/diabetes-centre-locations/ap/visakhapatnam?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Visakhapatnam</a></li>
							<li><a href="https://drmohans.com/diabetes-centre-locations/ap/rajamundry?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Rajahmundry</a></li>
						</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#karnataka">Karnataka</a>
					</h4>
					</div>
					<div id="karnataka" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
						<li><a href="http://drmohans.com/diabetes-centre-locations/indira-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Indira Nagar</a></li>	
						<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/jp-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">JP Nagar</a></li>
						<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/malleswaram?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Malleswaram</a></li>
						
						<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/mysuru?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Mysuru</a></li>
						<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/whitefield?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Whitefield</a></li>
						
						</ul>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#delhi">Delhi</a>
					</h4>
					</div>
					<div id="delhi" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
						<li><a href="http://drmohans.com/diabetes-centre-locations/delhi/kirti-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Kirti Nagar</a></li>
						</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#up">Uttar Pradesh</a>
					</h4>
					</div>
					<div id="up" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
						
						</ul>
						</div>
					</div>
				</div>				
				<div class="panel panel-default clearfix">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#kerala">Kerala</a>
					</h4>
					</div>
					<div id="kerala" class="panel-collapse collapse">
						<div class="panel-body">
						<ul class="subr_branch">
						<li><a href="http://drmohans.com/diabetes-centre-locations/kerala/trivandrum?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Trivandrum</a></li>
						<li><a href="http://drmohans.com/diabetes-centre-locations/kerala/kochi?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Kochi</a></li>
						</ul>
						</div>
					</div>
				</div>
				
				</div> 				
				<ul class="main_branch">
					<li><a href="http://drmohans.com/diabetes-centre-locations/bhubaneswar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Bhubaneswar</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/chunampet?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Chunampet</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/coimbatore?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Coimbatore</a></li>
					<li><a href="https://drmohans.com/diabetes-centre-locations/chennai/erode?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Erode</a></li>
					<li><a href="https://drmohans.com/diabetes-centre-locations/tamilnadu/kancheepuram/?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Kancheepuram</a></li>				
					<li><a href="http://drmohans.com/diabetes-centre-locations/gudiyatham?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Gudiyatham</a></li>				
					<li><a href="http://drmohans.com/diabetes-centre-locations/madurai?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Madurai</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/pondicherry?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Pondicherry</a></li>
					<li><a href="https://drmohans.com/diabetes-centre-locations/tamilnadu/salem?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Selam</a></li>	
					<li><a href="http://drmohans.com/diabetes-centre-locations/vellore?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Vellore</a></li>
					<li><a href="https://drmohans.com/diabetes-centre-locations/tiruchirappalli?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Tiruchirappalli</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/thanjavur?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Thanjavur</a></li>									
					<li><a href="http://drmohans.com/diabetes-centre-locations/tuticorin?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Tuticorin</a></li>					
				</ul>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-7 branch_right">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Dr-mohans-branch-head.png" class="img-responsive" alt="Branch" width="554" height="323" />				
			</div>
		</div>
	</div>
</section>

<!-- End of Branches -->
<?php get_footer(); ?>