<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
</div><!-- .site-content -->

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="footer_rights clearfix">
	<div class="container">
		<div class="col-xs-12 col-sm-5 col-md-5 text-left foo_left">
			<p>&copy; Copyright <?php echo date("Y"); ?> Dr. Mohan's Diabetes Specialities Centre</p>
		</div>
		<div class="col-xs-12 col-sm-2 col-md-2 text-center foo_center">
			<ul>
				<li><a href="http://www.facebook.com/drmohansdiabetesinstitutions" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Fb.png" class="img-responsive" alt="facebook" width="25" height="30" /></a></li>
				<li><a href="http://twitter.com/dmdsc" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Twitter.png" class="img-responsive" alt="Twitter" width="40" height="30" /></a></li>
			</ul>
		</div>
		<div class="col-xs-12 col-sm-5 col-md-5 text-right foo_right">
			<p><a href="http://www.socialbeat.in/" target="_blank" rel="nofollow ">Designed & Developed by  Social Beat</a></p>			
		</div>
	</div>
	</div>
</footer>
<div class="col-xs-12 col-sm-12 col-md-12 chennai ">
	<p style="display:none">Best Diabetes Centre in Chennai  </p>
</div>

<?php if( is_page( 2 ) ) :?>

<!-- Google Code for Remarketing Tag for Home page : 2  -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 967380966;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/967380966/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<?php endif; ?>

<?php if( is_page( 7 ) ) :?>


<!-- Google Code for Thank You Conversion Page for thankyou page : 7 -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 967380966;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "HHBVCIrL1AkQ5p-kzQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/967380966/?label=HHBVCIrL1AkQ5p-kzQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '783042241791501');
fbq('track', "PageView");
fbq('track', 'ViewContent');</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=783042241791501&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<?php endif; ?>

<!-- Start : Google Analytics code -->
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-46433745-1', 'auto');
 ga('send', 'pageview');

</script>
<!-- End : Google Analytics code -->

<?php wp_footer(); ?>
	</div><!-- .site-inner -->
</div><!-- .site -->

</body>
</html>