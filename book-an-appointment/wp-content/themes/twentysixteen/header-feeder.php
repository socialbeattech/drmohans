<?php
	/**
	 * The template for displaying the header
	 *
	 * Displays all of the head element and everything up until the "site-content" div.
	 *
	 * @package WordPress
	 * @subpackage Twenty_Sixteen
	 * @since Twenty Sixteen 1.0
	 */

?><!DOCTYPE html>
	<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		
	<link rel="icon" href="http://drmohans.com/wp-content/uploads/cropped-m-32x32.png" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<?php wp_head(); ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NGXBRHZ');</script>
<!-- End Google Tag Manager -->

	</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<div class="site-inner">	
		
		<header id="masthead" class="site-header" role="banner">		
			<div class="container">
			
<!-- Start of Navigation . work for mobile alone, not for desktop and tablet -->
	 
	
<!-- End of Navigation . work for mobile alone, not for desktop and tablet -->
	
<!-- Start of logo and header -->
				<div class="logo_contact">
					<div class="col-xs-12 col-sm-5 col-md-5">
						<img src="http://drmohans.com/book-an-appointment/wp-content/uploads/2016/05/Drmohans-diabetes-lp.png" alt="Drmohans" width="216" height="70" />
					</div>
					<div class="col-xs-12 col-sm-7 col-md-7">	
					
						<div class="pull-right  text-right quality">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Diabates-Care.png" alt="Drmohans" width="100" height="83" />
						</div>
					</div>
				</div>
<!-- End of logo and header -->				
		
				
	<div class="our_services_block clearfix">
	<div class="container">
	<div class="our_services">
	<div class="col-xs-12 col-sm-7 col-md-9 service_left">
			<div class="service_paras">
				<div class="headings text-center">CONSULT THE BEST DIABETES DOCTORS ONLY AT DR MOHAN'S, CHENNAI</div>
				<div class="captions text-center">
				<p>With over 25 years in the field of diabetes care and over 4,50,000 satisfied patients, we have transformed into an international centre of excellence in diabetes. DMDSC stands committed to providing world-class healthcare facilities for diabetes and all its complications at its Headquarters in Gopalapuram, Chennai.</p></br></br>
				<p><strong>WHY YOU SHOULD VISIT DR MOHAN'S IN CHENNAI</strong>
				<ul>
				<li>India's No. 1 diabetes centre as awarded and ranked by Times of India and The Week.</li>
				<li>Founded by Padma Shri awardee Dr. V. Mohan for his  by the Govt. of India for his accomplishments in the field of Diabetology.</li>
				<li>More than 25 Years of Service in prevention and management of diabetes.</li>
				<li>State of art facilities comparable to the best in the world</li> 
				<li>40+ Diabetes Care Centres across India</li>
				<li>Customized Diabetes treatment</li>
				<li>First NABL Accredited Diabetes Biochemistry Lab in India.</li>
				<li>Recognized by the World Health Organization (WHO), American Diabetes Association (ADA) as well as by the International Diabetes Federation (IDF)</li> 
				</ul>
				</div>
			</div>
<!-- Thankyou page content -->
			<div class="service_paras thankyou">
				<div class="captions text-center">Thanks for contacting us! We will get in touch with you shortly.</div>
			</div>
<!-- End : Thankyou page content hide for home page-->
	</div>
	<div class="col-xs-12 col-sm-5 col-md-3 service_right">
		<h3>Book an Appointment</h3>
	<?php gravity_form( 2, $display_title = true, $ajax = true ); ?>
	</div>
	</div> <!-- our_services -->
	</div> <!-- container -->
	</div><!-- our_services_block -->
			
</div>	<!-- header container -->
</header><!-- header -->
			
	
			<div id="content" class="site-content">