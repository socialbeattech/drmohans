<?php
	require('wp-config.php');
	//API for gravity form
	$api_key = '7267462139';
	$private_key = '073fa89748ba99d';
	 
	//set route
	$route = 'forms/12/entries';

	function calculate_signature( $string, $private_key ) {
		$hash = hash_hmac( 'sha1', $string, $private_key, true );
		$sig = rawurlencode( base64_encode( $hash ) );
		return $sig;
	}


	//creating request URL
	$expires = strtotime( '+60 mins' );
	$string_to_sign = sprintf( '%s:%s:%s:%s', $api_key, 'POST', $route, $expires );
	$sig = calculate_signature( $string_to_sign, $private_key );
	$url = get_site_url() . '/gravityformsapi/' . $route . '?api_key=' . $api_key . '&signature=' . $sig . '&expires=' . $expires;

	$entries = array(
		array(
			'date_created' => date('Y-m-d H:i:s'),
			'is_starred'   => 0,
			'is_read'      => 0,
			'ip'           => '::1',
			'source_url'   => $_POST['url'],
			'currency'     => 'USD',
			'created_by'   => 1,
			'user_agent'   => 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0',
			'status'       => 'active',
			'1'            => $_POST['attender_name'],
			'2'            => $_POST['mnno'],
			'3'            => $_POST['feedback_branches'],
			'4'            => $_POST['visitdate'],
			'48'            => $_POST['email'],
			'5'            => $_POST['phone'],
			'6'            => $_POST['admission_formalities'],
			'7'            => $_POST['phone_handling'],
			'8'            => $_POST['laboratory'],
			'44'            => $_POST['billing'],
			'9'            => $_POST['investigatons'],
			'10'            => $_POST['diet_advice'],
			'11'            => $_POST['diagnosis_treatment'],
			'12'            => $_POST['nursing_care'],
			'13'            => $_POST['eyechecks'],
			'14'            => $_POST['fitness_phy'],
			'15'            => $_POST['guidance_care'],
			'16'            => $_POST['clinical_secs'],
			'17'            => $_POST['caterings'],
			'18'            => $_POST['pharmacys'],
			'18'            => $_POST['pharmacys'],
			'19'            => $_POST['footwears'],
			'20'            => $_POST['cleanhos'],
			'21'            => $_POST['insurances'],
			'22'            => $_POST['securitys'],
			'26'            => $_POST['admission_formalities_remarks_detailed'],
			'27'            => $_POST['phone_handling_remarks_detailed'],
			'28'            => $_POST['laboratory_remarks_detailed'],
			'45'            => $_POST['billing_remarks_detailed'],
			'29'            => $_POST['investigatons_remarks_detailed'],
			'30'            => $_POST['diet_advice_remarks_detailed'],
			'31'            => $_POST['diagnosis_treatment_remarks_detailed'],
			'32'            => $_POST['nursing_care_remarks_detailed'],
			'33'            => $_POST['eyechecks_remarks_detailed'],
			'34'            => $_POST['fitness_phy_remarks_detailed'],
			'35'            => $_POST['guidance_care_remarks_detailed'],
			'36'            => $_POST['clinical_secs_remarks_detailed'],
			'37'            => $_POST['caterings_remarks_detailed'],
			'38'            => $_POST['pharmacys_remarks_detailed'],
			'39'            => $_POST['footwears_remarks_detailed'],
			'40'            => $_POST['cleanhos_remarks_detailed'],
			'41'            => $_POST['insurances_remarks_detailed'],
			'42'            => $_POST['securitys_remarks_detailed'],
			'43'            => $_POST['suggestions'],
			'46'            => $_POST['sourceurl'],
			'47'            => $_POST['remoteip'],
		)
	);
	 
	//json encode array
	$entry_json = json_encode( $entries );

	//retrieve data
	$response = wp_remote_request( $url , array( 'method' => 'POST', 'body' => $entry_json, 'timeout' => 25 ) );
	/*if ( wp_remote_retrieve_response_code( $response ) != 200 || ( empty( wp_remote_retrieve_body( $response ) ) ) ){
		//http request failed
		die( 'There was an error attempting to access the API.' );
	}*/
	if ( (wp_remote_retrieve_response_code( $response ) != 200) || (  wp_remote_retrieve_body( $response ) =="" ) ){
		//http request failed
		/*echo "<br>wp_remote_retrieve_response_code :".wp_remote_retrieve_response_code( $response );
		echo "<br>wp_remote_retrieve_body :";
		print_r(wp_remote_retrieve_body( $response )); */
		die( 'There was an error attempting to access the API.' );
		
	}

	//result is in the response "body" and is json encoded.
	$body = json_decode( wp_remote_retrieve_body( $response ), true );

	$attender_name = $_POST['attender_name'];
	$mnno = $_POST['mnno'];
	$branch = $_POST['feedback_branches'];
	$visitdate = $_POST['visitdate'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	$admission_formalities = $_POST['admission_formalities'];
	$phone_handling = $_POST['phone_handling'];
	$laboratory = $_POST['laboratory'];
	$billing = $_POST['billing'];
	$investigatons = $_POST['investigatons'];
	$diet_advice = $_POST['diet_advice'];
	$diagnosis_treatment = $_POST['diagnosis_treatment'];
	$nursing_care = $_POST['nursing_care'];
	$eyechecks = $_POST['eyechecks'];
	$fitness = $_POST['fitness_phy'];
	$guidance_care = $_POST['guidance_care'];
	$clinical_secs = $_POST['clinical_secs'];
	$caterings = $_POST['caterings'];
	$pharmacys = $_POST['pharmacys'];
	$footwears = $_POST['footwears'];
	$cleanhos = $_POST['cleanhos'];
	$insurances = $_POST['insurances'];
	$securitys = $_POST['securitys'];
	$suggestions = $_POST['suggestions'];
	$afd = $_POST['admission_formalities_remarks_detailed'];
	$phrd = $_POST['phone_handling_remarks_detailed'];
	$lrd = $_POST['laboratory_remarks_detailed'];
	$brd = $_POST['billing_remarks_detailed'];
	$ird = $_POST['investigatons_remarks_detailed'];
	$dard = $_POST['diet_advice_remarks_detailed'];
	$dtrd = $_POST['diagnosis_treatment_remarks_detailed'];
	$ncrd = $_POST['nursing_care_remarks_detailed'];
	$erd = $_POST['eyechecks_remarks_detailed'];
	$fprd = $_POST['fitness_phy_remarks_detailed'];
	$gcrd = $_POST['guidance_care_remarks_detailed'];
	$csrd = $_POST['clinical_secs_remarks_detailed'];
	$crd = $_POST['caterings_remarks_detailed'];
	$prd = $_POST['pharmacys_remarks_detailed'];
	$frd = $_POST['footwears_remarks_detailed'];
	$clrd = $_POST['cleanhos_remarks_detailed'];
	$insrd = $_POST['insurances_remarks_detailed'];
	$srd = $_POST['securitys_remarks_detailed'];
	$source_url = $_POST['sourceurl'];
	$remoteip = $_POST['remoteip'];


		
		GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
	 
		$request = new WP_Http();
		$response = $request->post( $post_url, array( 'body' => $body ) );
		GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );



	//Gravity form mail Notifications

	// Multiple recipients
	$to = 'velmurugan@socialbeat.in, feedback@drmohans.com';

	// Subject
	$subject = 'Drmohans Feedback Enquiry';


	// Message
	$message = '<html>
	<head>
	  <title>Drmohans Feedback Enquiry</title>
	</head>
	<body>
	  <table width="100%" border="1" cellpadding="2" cellspacing="2" >
		<tr border="1" bgcolor="#EAF2FA" cellpadding="5" cellspacing="2">
			<td><strong>Patient / Attender Name:</strong></td>
			<td>'.$attender_name.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>M. No :</strong></td>
			<td>'.$mnno.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>Branch:</strong></td>
			<td>'.$branch.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>Visit date:</strong></td>
			<td>'.$visitdate.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>Email:</strong></td>
			<td>'.$email.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>Phone Number:</strong></td>
			<td>'.$phone.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="4" cellspacing="4" align="center">
			<td colspan="2"><strong>Kindly rate our services for the below mentioned attributes:</strong></td>		
		</tr> 
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>1. Registration / Admission formalities:</strong></td>
			<td>'.$admission_formalities.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$afd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>2. Phone call handling (Call Centre / Appointment):</strong></td>
			<td>'.$phone_handling.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$phrd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>3. Billing:</strong></td>
			<td>'.$billing.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$brd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>4. Laboratory & Blood collection:</strong></td>
			<td>'.$laboratory.'</td>
		</tr> 
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$lrd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>5. Investigations (ECG/ Doppler / Biothesiometry / Foot examination/ X-ray):</strong></td>
			<td>'.$investigatons.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$ird.'</td>
		</tr>	
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>6. Diabetes class & Diet advice:</strong></td>
			<td>'.$diet_advice.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$dard.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>7. Diagnosis & Treatment:</strong></td>
			<td>'.$diagnosis_treatment.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$dtrd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>8. Nursing Care (IP/OT/Surgical Dressing):</strong></td>	
			<td>'.$nursing_care.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$ncrd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="4" cellspacing="4" align="center">
			<td colspan="2"><strong>Other Specialities:<strong></td>	
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>1. Eye:</strong></td>
			<td>'.$eyechecks.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$erd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>2. Fitness/Physiotherapy:</strong></td>
			<td>'.$fitness.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$fprd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>Your ratings on:</strong></td>
			<td></td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>1. Adherence to appointment time:</strong></td>
			<td>'.$guidance_care.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$gcrd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>2. Homecare:</strong></td>
			<td>'.$clinical_secs.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$csrd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>3. Catering services:</strong></td>
			<td>'.$caterings.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$crd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>4. Pharmacy service:</strong></td>
			<td>'.$pharmacys.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$prd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>5. Footwear:</strong></td>
			<td>'.$footwears.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$frd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>6. Cleanliness:</strong></td>
			<td>'.$cleanhos.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$clrd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>7. Insurance:</strong></td>
			<td>'.$insurances.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$insrd.'</td>
		</tr>
	   <tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>8. Security:</strong></td>
			<td>'.$securitys.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td colspan="2">Remarks: '.$srd.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>Your suggestions/appreciations please</strong></td>
			<td>'.$suggestions.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>Source Url:</strong></td>
			<td colspan="2">'.$source_url.'</td>
		</tr>
		<tr bgcolor="#EAF2FA" cellpadding="2" cellspacing="2">
			<td><strong>Remote IP:</strong></td>
			<td colspan="2">'.$remoteip.'</td>
		</tr>
	  </table>
	</body>
	</html>';


	// To send HTML mail, the Content-type header must be set
	$headers = "From: Drmohans <recheck@drmohans.com>\r\n";
	$headers .= "Reply-To: recheck@drmohans.com\r\n";
	$headers .= 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "X-Mailer: PHP/".phpversion();

	// Mail it
	mail($to, $subject, $message, $headers);

	//End of gravity mail notifications



	 
	//print_r($body); exit;
	if( $body['status'] > 202 ){
		$error = $body['response'];
	 
			//entry update failed, get error information, error could just be a string
		if ( is_array( $error )){
			$error_code     = $error['code'];
			$error_message  = $error['message'];
			$error_data     = isset( $error['data'] ) ? $error['data'] : '';
			$status     = "Code: {$error_code}. Message: {$error_message}. Data: {$error_data}.";
		}
		else{
			$status = $error;
		}
		die( "Could not post entries. {$status}" );
	}
	else
	{
		header('Location:https://drmohans.com/feedback-thankyou/');
	}