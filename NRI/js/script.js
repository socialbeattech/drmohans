$(document).ready(function () {
    $('[data-toggle="slide-collapse"]').on('click', function () {
        $navMenuCont = $($(this).data('target'));
        $navMenuCont.animate({
            'width': 'toggle'
        }, 350);
        //        $(".menu-overlay").fadeToggle(500);
        $(".menu-overlay").fadeIn(500);

    });
    $(".menu-overlay").click(function (event) {
        $(".navbar-toggle").trigger("click");
        $(".menu-overlay").fadeOut(500);
    });
    $('li.dropdown').on('click', function () {
        var $el = $(this);
        if ($el.hasClass('open')) {
            var $a = $el.children('a.dropdown-toggle');
            if ($a.length && $a.attr('href')) {
                location.href = $a.attr('href');
            }
        }
    });

    // $('.testimonial .carousel').carousel({
    //     interval: false
    // })
    $(".form-inline .input-group.spouse").hide();
    $('input.couple').on('click', function () {  
        if($('input.couple1').is(":checked"))   
            $(".form-inline .input-group.spouse").show();
        else
            $(".form-inline .input-group.spouse").hide();
  
    });
    // $('#fade-quote-carousel').carousel({
    //     interval: 1000,
    //     cycle: true,
    //     autoplay: true
    //   }); 

});

