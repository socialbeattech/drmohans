-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2019 at 06:25 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drmohannri_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `nriclinet`
--

CREATE TABLE `nriclinet` (
  `appid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `submit_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nriclinet`
--

INSERT INTO `nriclinet` (`appid`, `name`, `details`, `submit_at`) VALUES
(1, 'Maxar', 'test@mail.com 9876543210', '2019-11-26 12:05:20'),
(2, 'test2', 'mail@test.com 1234567890', '2019-11-26 12:06:12'),
(4, 'Mathiews', 'please let us know your convenient time and method of getting back(mobile or email)  ', '2019-11-27 13:53:36'),
(5, 'Dominic', 'wwerert\r\n12345\r\nsdefggh', '2019-11-27 13:55:08'),
(6, 'Max', 'max@maxiz.com', '2019-11-28 14:17:43'),
(7, 'rafi', 'ok I will call you', '2019-12-17 10:41:25'),
(8, '', '', '2019-12-17 10:47:49'),
(9, 'test10', 'test10', '2019-12-17 10:52:36'),
(10, 'test10', 'test10', '2019-12-17 10:53:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nriclinet`
--
ALTER TABLE `nriclinet`
  ADD PRIMARY KEY (`appid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nriclinet`
--
ALTER TABLE `nriclinet`
  MODIFY `appid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
