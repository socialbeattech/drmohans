<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Dr.Mohan's &mdash;90 Day Fitness Challenge</title>
        <link rel="icon" href="images/fav.png">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  <!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header container">
        <h4 class="modal-title">Why the 90 day challenge can be perfect way to get back to being fit?</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p>
Today, diabetes is becoming increasingly common in the population. Every second case of diabetes is left undiagnosed. Diabetes when left unmanaged leads to various complications of the heart, kidneys, liver, eyes, foot, neuro amongst others. Early detection of the condition will help manage diabetes very efficiently and enable the person to lead a normal life.</p>
<p>
  The most common risk factor for Type 2 diabetes is obesity.
</p>
<p>
  Effectively educating the patient abut obesity, its complications, association with diabetes and other conditions that can deter the health of a person along with proper counseling by educators from the diabetes health management team can go a long way in benefiting the patient. A support care system is all that is required to motivate and guide the patient to handle obesity issues. And if you are looking for one, choose <strong><b class="bg-primary text-white"> Dr Mohan's 90 day Back to Fit challenge.</b></strong>

</p>
<p>
  <u class="bg-primary text-white">The 90 day back to fitness challenge</u> is designed specially anybody struggling with obesity problems. All you need is a willingness to get fit and healthy and we shall kick start it for you. Our team will collaborate with the individual and develop strategies that would enable sustained weight loss in the long term, of course with some life style changes.

</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
    <!--Video teaser start-->

<div id="light">
  <a class="boxclose" id="boxclose" onclick="lightbox_close();"></a>
  <video id="VisaChipCardVideo" width="600" controls>
      <source src="images/90d.mp4" type="video/mp4">
      <!--Browser does not support <video> tag -->
    </video>
</div>

<div id="fade" onClick="lightbox_close();"></div>

<div>
    <!-- video teaser end -->
    <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">
      
      <div class="container-fluid">
        <div class="d-flex align-items-center">
          <div class="site-logo logobg"><a href="index.html"><img src="images/90day.png" alt="" width="250" height="80" ></a></div> 
          <div>
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li><a href="#home-section" class="nav-link">Home</a></li>
                <!-- <li><a href="#work-section" class="nav-link">Work</a></li> -->
                <!-- <li><a href="#process-section" class="nav-link">Process</a></li> -->
                <li><a href="#services-section" class="nav-link">Know More</a></li>
                <li class="cta banim"><a href="#contact-section" class="nav-link"><span class="rounded border border-primary">Join Now</span></a></li>
              </ul>
            </nav>
        </div>
          <div class="ml-auto">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu site-menu-dark js-clone-nav mr-auto d-none d-lg-block">
                <!-- <li class="cta banim"><a href="#contact-section" class="nav-link"><span class="rounded border border-primary">Join Now</span></a></li> -->
                <li class=""><a href="#contact"><img src="images/m.png" alt="" width="260" height="80" ></a></li>
              </ul>
            </nav>
            <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black float-right"><span class="icon-menu h3"></span></a>
          </div>
        </div>
      </div>
      
    </header>

    <div class="intro-section" id="home-section">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-4 mr-auto" data-aos="fade-up">
            <h3>Are you ready <br>for the 90 day</h3>
            <h1>Back to Fitness<br>Challenge?</h1>
            <p class="mb-5">Dr.Mohan's<br>Gopalapuram Center</strong>.</p>
            <p><a href="#contact-section" class="btn btn-outline-light py-3 px-5 banim" data-toggle="modal" data-target="#myModal">Why 90 day challenge is perfect <br> for Everyone? </a></p>
            <p> <a href="#contact-section" class="btn btn-outline-light py-3 px-5 banim"><strong>Enroll Today!</strong> Rs 5000. for 3 Month</a></p>
              <!-- <p class="mb-5">*Dance Fitness: Monday,Wednesday & Friday - 5pm - 6pm <br>*Strength Training: - 5pm - 6pm<br>*Dance Fitness: Tuesday, Thursday, Saturday - 4pm - 8pm<br>*Yoga class:Monday,Wednesday & Friday - 8am - 9am<br>*Blood test: Fasting, HbA1c, Lipids, Thyroid function test, ECG, Hemogram, Creatinine. </p> -->
           <!--    <p class="mb-5"></p>
              <p class="mb-5"></p>
              <p class="mb-5"></p>
              <p class="mb-5"></p> -->
          </div>
          <div class="col-lg-2 ml-auto"  data-aos="fade-up" data-aos-delay="100">
            <figure class="img-absolute">
              <a href="#" onclick="lightbox_open();"><img src="images/665x.png" alt="Image" class="img-fluid"></a>
            </figure>
          </div>
        </div>
      </div>
    </div>

<!-- Call to Action section or join now -->
<div class="site-section" id="contact-section">
      <div class="container">
        <!-- <div class="row"><div class="col-md-6">
          <h4 class="text-center text-success" style="color:#fdc900;padding-bottom: 20px;" ><?=$result;?></h4>
        </div></div> -->
 <div class="row">
          <div class="col-md-6">
               <h2 class="section-title mb-3 banim">Take a Look</h2>
               <video width="450" controls autoplay poster="images/90d.png">
      <source src="images/90d.mp4" type="video/mp4">
      <!--Browser does not support <video> tag -->
    </video>
                
          </div>
      
        <!-- form  -->
        
          <div class="col-md-6">
           
            <h2 class="section-title mb-3 banim">Join Us</h2>
                      
            <form action="enrollform.php" method="post" id="form-group" data-aos="fade" >
              <div class="form-group row">
                <div class="col-md-12">
                  <input name="name" type="text" class="form-control" placeholder="First name" required>
                </div>
                
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <input name="phone" type="tel" class="form-control" placeholder="Phone Number" required>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <input name="email" type="email" class="form-control" placeholder="Email" required>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <textarea name="msg" class="form-control" id="" cols="30" rows="10" placeholder="Write your message here."></textarea>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-6 banim">
                  <!-- onclick="window.open('http://localhost/90days/enrollform.php','_blank')" -->
                  <input name="submit" type="submit"  class="btn btn-primary py-3 px-5 btn-block" value="Enroll Now">
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

<!-- service  -->

    <div class="site-section bg-light" id="services-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5">
            <span class="section-sub-title d-block">Services</span>
            <h2 class="section-title">Avail Our Services</h2>
            <p>We'll guide you through your fitness journey.</p>
            <p>Brace yourself for a <strong>"disciplined 90 days Back to Fit"</strong></p>
              
          </div>
        </div>

      </div>
        <div class="container">
        
        <div class="row">
          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="">
            <div class="process p-3">
              <span class="number">01</span>
              <div>
                <span class="display-4 text-primary mb-4 d-inline-block"><br></span>
                <h3><img src="images/blood-test.png">Blood Test</h3>
                <p>Fasting Blood Sugar, HbA1c, Lipids, Thyroid function test, ECG, Hemogram, Creatinine.</p>
              </div>

            </div>
          </div>
          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="process p-3">
              <span class="number">02</span>
              <div>
                <span class="display-4 text-primary mb-4 d-inline-block"><br></span>
                <h3><img src="images/apple.png">Personalised Nutrition Counseling</h3>
                
              </div>

            </div>
          </div>
          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="200">
            <div class="process p-3">
              <span class="number">03</span>
              <div>
                <span class="display-4 text-primary mb-4 d-inline-block"><br></span>
                <h3><img src="images/doc.png">Doctor's Counselling</h3>
                
              </div>

            </div>
          </div>
        </div>
        <h2 class="section-title">For Three Months</h2>
      </div>

      <div class="owl-carousel nonloop-block-14 border">
        <div class="service  border-danger">
          <div>
            <span class="display-4 text-primary mb-4 d-inline-block"><img src="images/dance.png"></span>
            <h3>Dance Fitness</h3>
            <p>Monday, Wednesday & Friday</p>
            <p>5pm - 6pm</p>
          </div>
        </div>
          
        <div class="service  border-danger">
          <div>
            <span class="display-4 text-primary mb-4 d-inline-block"><img src="images/stren.png"></span>
            <h3>Fitness Training</h3>
            <p>Tuesday, Thursday & Saturday</p>
            <p>4pm - 8pm</p>
          </div>
        </div>

        

        <div class="service  border-danger">
          <div>
            <span class="display-4 text-primary mb-4 d-inline-block"><img src="images/yoga.png"></span>
            <h3>Yoga Class</h3>
            <p>Monday, Wednesday & Friday</p>
            <p>8am - 9am</p>
          </div>
         </div>
    </div>
 <div class="container">
  <div class="row">
          <div class="col-lg-12 mb-6">
              <p>We'll guide you through your fitness journey with weekly Diet and Doctor review</p>
               
          </div></div>
      </div>
    </div>



    <!-- steps and fitnesstip session -->
<div class="site-section section-2" id="work-section">
      
<div class="">
  <div class="container">
    <h2 class="pb-3 pt-2 border-bottom mb-5 section-title">The Step To Follow for 90 Days Challenge</h2>
    <!--first section-->
    <div class="row align-items-center how-it-works d-flex">
      <div class="col-2 text-center bottom d-inline-flex justify-content-center align-items-center">
        <div class="circle font-weight-bold">1</div>
      </div>
      <div class="col-6">
        <h5>Step 1</h5>
        <p>Fill in your health details</p>
      </div>
    </div>
    <!--path between 1-2-->
    <div class="row timeline">
      <div class="col-2">
        <div class="corner top-right"></div>
      </div>
      <div class="col-8">
        <hr/>
      </div>
      <div class="col-2">
        <div class="corner left-bottom"></div>
      </div>
    </div>
    <!--second section-->
    <div class="row align-items-center justify-content-end how-it-works d-flex">
      <div class="col-6 text-right">
        <h5>Step 2</h5>
        <p>Enroll for the fitness challenge by paying Rs 5,000</p>
      </div>
      <div class="col-2 text-center full d-inline-flex justify-content-center align-items-center">
        <div class="circle font-weight-bold">2</div>
      </div>
    </div>
    <!--path between 2-3-->
    <div class="row timeline">
      <div class="col-2">
        <div class="corner right-bottom"></div>
      </div>
      <div class="col-8">
        <hr/>
      </div>
      <div class="col-2">
        <div class="corner top-left"></div>
      </div>
    </div>
    <!--third section-->
    <div class="row align-items-center how-it-works d-flex">
      <div class="col-2 text-center top d-inline-flex justify-content-center align-items-center">
        <div class="circle font-weight-bold">3</div>
      </div>
      <div class="col-6">
        <h5>Step 3</h5>
        <p>Walk in for the blood test</p>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="container">
      <h2 class="section-title">Fitness & Diet Regiment for Challenge</h2>

      <h4>Fitness & Diet</h4>
      <ul class="timeline">
        <li>
          <a target="_blank" href="">Tip1</a>
          <a href="" class="float-right">for diet</a>
          <p>Do not do crash dieting or starvation dieting , It can cause Gallbladder Stones.</p>
        </li>
        <li>
          <a href="">Tip2</a>
          <a href="" class="float-right">for fitness</a>
          <p>Do Not engage in strenuous activity  without consulting the physician.</p>
        </li>
        <li>
          <a href="">Tip3</a>
          <a href="" class="float-right">for diet</a>
          <p>Check your Blood Parameters before adapting to any diet.</p>
        </li>
      </ul>
    </div>
  </div>



  <!-- Team of experts -->

    <div class="site-section section-2" id="process-section" >
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5">
            <span class="section-sub-title d-block">Team Of</span>
            <h2 class="section-title">Experts</h2>
          
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="">
            <div class="process p-3">
           <div class="card logobg" style="width:300px">
    <img class="card-img-top" src="images/e4.jpg" alt="Card image" style="width:100%">
    <div class="card-body">
      <h4 class="card-title">Dr. Lovleena</h4>
      <p class="card-text">Consultant Diabetologist</p>
      <a href="" class="btn btn-primary">See Profile</a>
    </div>
  </div>
            </div>
          </div>
          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="process p-3">
             <div class="card logobg" style="width:300px">
    <img class="card-img-top" src="images/e5.jpg" alt="Card image" style="width:100%">
    <div class="card-body">
      <h4 class="card-title">Mr. Santhosh</h4>
      <p class="card-text">Dance Fitness Expert</p>
      <a href="" class="btn btn-primary">See Profile</a>
    </div>
  </div>

            </div>
          </div>
          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="200">
            <div class="process p-3">
              <div class="card logobg" style="width:300px">
    <img class="card-img-top" src="images/e1.jpg" alt="Card image" style="width:100%">
    <div class="card-body">
      <h4 class="card-title">Ms. Thangamani</h4>
      <p class="card-text">Expert Dietitian</p>
      <a href="" class="btn btn-primary">See Profile</a>
    </div>
  </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="">
            <div class="process p-3">
           <div class="card logobg" style="width:300px">
    <img class="card-img-top" src="images/e2.jpg" alt="Card image" style="width:100%">
    <div class="card-body">
      <h4 class="card-title">Mr. Murugan</h4>
      <p class="card-text">Fitness Expert</p>
      <a href="" class="btn btn-primary">See Profile</a>
    </div>
  </div>
            </div>
          </div>
          <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="process p-3">
             <div class="card logobg" style="width:300px">
    <img class="card-img-top" src="images/e3.jpg" alt="Card image" style="width:100%">
    <div class="card-body">
      <h4 class="card-title">Dr. Vinoth</h4>
      <p class="card-text">Yoga Expert</p>
      <a href="" class="btn btn-primary">See Profile</a>
    </div>
  </div>

            </div>
          </div>
          
        </div>
      </div>


    </div>
   


    <!--Contact page -->
 <div class="site-section section-2" id="contact" >
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5">
            <span class="section-sub-title d-block">Contact</span>
            <h2 class="section-title">Reception For More Detail</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6" data-aos="fade-up" data-aos-delay="">
            <div class="process p-3">
             
              <div>
               <h3>Phone</h3>
             
              <p><h2 class="section-title"><a href="tel:9962 42 8888"><img src="images/info.png"></a><a href="tel:9962 42 8888">9962 42 8888</h2></a></p>
              <p><h2 class="section-title"><a href="tel:9962 42 8888"><img src="images/info.png"></a><a href="tel:8939 11 0000">8939 11 0000</h2></a></p>
              
         
              </div>

            </div>
          </div>
          <div class="col-lg-6" data-aos="fade-up" data-aos-delay="100">
            <div class="process p-3">
            
              <div class="col-lg-3">
               
                <h3>Address</h3>
                
            <p class=""><a href="index.html"><img src="images/mlogo.png" alt="" width="250" height="70"></a></p>
                                  
        
              </div>
<p class=""><h2 class="section-title">No.6 Conran Smith Road, Gopallapuram</h2></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  


  
    
  </div> <!-- .site-wrap -->

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.fancybox.min.js"></script>
  <script src="js/jquery.sticky.js"></script>

  
  <script src="js/main.js"></script>
    <!-- Start of ADTARBO JS -->
<script type="text/javascript">
(function(d, s, id){
var js, ajs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) {return;}
js = d.createElement(s); js.id = id; js.aun_id = "8wV8EoN6SWg22P8";
js.src = "//adtarbo.eywamedia.com/scripts/adtarbo.min.js"; js.async=true;
ajs.parentNode.insertBefore(js, ajs);
}(document, 'script', 'adtarbo-js'));
</script>
<!-- End of ADTARBO JS -->
  </body>
</html>