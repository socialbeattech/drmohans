<?php
/**
 * Template Name: Thankyou
 *
 * 
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


<!-- Start of Branches -->
<section class="our_branch_block clearfix">
<div class="branch_title">
<div class="branch_top_icon"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Branches_icons.png" alt="Branch" class="img-responsive" width="83" height="53" /></div>
<h1>Our Branches</h1>
</div>
	<div class="container">
		<div class="branch_services">
			<div class="col-xs-12 col-sm-5 col-md-5 branch_left">			
				<ul class="main_branch">
					<li>Karnataka</li>
				</ul>				
				<ul class="subr_branch">
					<li><a href="http://drmohans.com/diabetes-centre-locations/indira-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Indira Nagar</a></li>	
					<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/jp-nagar?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">JP Nagar</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/malleswaram?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Malleswaram</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/mangalore?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Mangalore</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/mysuru?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Mysuru</a></li>
					<li><a href="http://drmohans.com/diabetes-centre-locations/karnataka/whitefield?utm_source=Landing%20Page&utm_term=DrMohans&utm_content=LandingPage">Whitefield</a></li>
				</ul>				
		  </div>
			
			<div class="col-xs-12 col-sm-7 col-md-7 branch_right">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Dr-mohans-branch-head.png" class="img-responsive" alt="Branch" width="554" height="323" />				
			</div>
		</div>
	</div>
</section>

<!-- End of Branches -->


<?php get_footer(); ?>